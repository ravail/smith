<?php

use Illuminate\Http\Request;
use App\User;
use App\MajorGroup;
use App\SubMajorGroups;
use App\BillTransferRequest;
use App\Tax;
use App\GLAccount;
use App\Offer;
use App\FamilyGroup;
use App\GeneralItems;
use App\MenuItemsGroup;
use App\AlcoholicFamilyGroup;
use App\AlcoholicSubFamilyGroup;
use App\DeliverySubMajorGroup;
use App\SystemSetup\PrintClasses;
use App\DeliveryFamilyGroup;
use App\DeliverySubFamilyGroup;
use App\PrepaidOrders;
use App\PostPaidOrders;
use App\OrderItems;
use App\PrePaidItems;
use App\ManageOrders\BillMaster;
use App\SystemSetup\Tables;
use App\VoidItems;
use App\Ingredients;
use App\Recipe;
use App\stock;
use App\AppVersion;
use App\RawIngredient;
use App\SalesIngredient;
use App\edit_ingredients_print_class;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

Route::middleware('auth:api')->get('/user', function (Request $request) {
    //return $request;
    return $request->user();
});

Route::post('login', function (Request $request) {
    //return $request;
     $version = AppVersion::where('id',1)->first();
        $version =$version->version;
        
        if($request->input('version') == $version){
            
        
        if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password'),'user_type'=>'waiter','is_active'=>'active'])) {
            // Authentication passed...
            $user = auth()->user();
            $user->api_token = str_random(60);
            $user->fcm_id = $request->fcm_id;
            $user->save();
            $version = AppVersion::where('id',1)->first();
            $version =$version->version;
            $array=array();
            $array['user']=$user;
            $array['version']=$version;
            
            return $array;
        } elseif (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password'),'user_type'=>'manager','is_active'=>'active'])) {
            // Authentication passed...
            $user = auth()->user();
            $user->api_token = str_random(60);
            $user->fcm_id = $request->fcm_id;
            $user->save();
            
            $version = AppVersion::where('id',1)->first();
            $version =$version->version;
            $array=array();
            $array['user']=$user;
            $array['version']=$version;
            
            
            return $array;
        }
        else{
            return response()->json([
        'error' => 'Unauthenticated user',
        'code' => 401,
    ], 401);
        }
    }else{
         return response()->json([
        'message' => 'please Use latest Apk ',
        'code' => 401,
    ], 401);
    }
    
   
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


    Route::post('void_items1', function (Request $request) {
      $data = $request->all();
    $order_id =$data['order_id'];
    $ordrssss=PostPaidOrders::where('id', $order_id)->first();
    $bill= BillMaster::where('bill_id',$ordrssss->bill_id)->first();
    if($bill->status !='Paid'){
        $addBill = VoidItems::create($data);
    
        $updateItem = OrderItems::where('id', $request->item_id)->first();
        
        $updateItem->void_request = "yes";
        $updateItem->save();
        
        $user = User::where('void_item_accept','on')->get();
        foreach($user as $key=>$value){
             $Text = "Alert\n Waiter Requested for Void Item  \norder_id : ".$request->order_id  ."\nitem_id :". $request->item_id."\nitem_name :".$request->item_name."\nwaiter_name :".$request->waiter_name."\nreason :".$request->comments."\namount :".$request->amount."\ndate :".$addBill->created_at;
                   $email=$value->email;
                 Mail::raw($Text, function ($message) use ($email) {
                  $message->from('info@thesmitherp.com', 'Smith Hotels');
          
                  $message->to($email);
                  $message->subject('Void Alert');
             });
        }
         return $addBill;
    }  
    });

Route::post('void_items', function (Request $request) {
    $data = $request->all();
    $print_class_id = GeneralItems::where('id',$request->general_item_id)->value('print_class_id');
    $data['print_class_id']=$print_class_id;
    $addBill=VoidItems::create($data);
    
    $updateItem = OrderItems::where('id', $request->item_id)->first();
    
    $updateItem->void_request = "yes";
    $updateItem->save();
    
    $user = User::where('void_item_accept','on')->get();
    foreach($user as $key=>$value){
         $Text = "Alert\n Waiter Requested for Void Item  \norder_id : ".$request->order_id  ."\nitem_id :". $request->item_id."\nitem_name :".$request->item_name."\nwaiter_name :".$request->waiter_name."\nreason :".$request->comments."\namount :".$request->amount."\nquantity :".$request->quantity."\ndate :".$addBill->created_at;
               $email=$value->email;
             Mail::raw($Text, function ($message) use ($email) {
              $message->from('info@thesmitherp.com', 'Smith Hotels');
      
              $message->to($email);
              $message->subject('Void Alert');
         });
    }
    
    

   
    // define( 'API_ACCESS_KEY', 'AAAA8S-zF1M:APA91bGVmmEyXUbN6vbEeJ15XK34k2g-uDq9ASXB3bidsAPo8qi1SbxWx4HVNar23Vcz95O02vJM2_GiLi6qNVqcCPuF84lVkSEJYNhGmUb0Tw56sjQ1EF9Y2RT3ktHLIuFAT-XwDJgC' );
    //                 // $registrationIds = ($_REQUEST['fcm_id']);

                 
                 
    //                 //echo $registrationIds;exit;
                
    //                 #prep the bundle
    //                  $msg = array
    //                       (
    //                 	   'body' 	=> 'Body  Of Notification',
    //                                 'code' => '0',
    //                                 'order_id' => $request->order_id,
    //                                 'item_id' => $request->item_id,
    //                                 'item_name' => $request->item_name,
    //                                 'waiter_id' => $request->waiter_id,
    //                                 'waiter_name' => $request->waiter_name,
    //                             // 'MerchantRequestID' => $MerchantRequestID,
    //             		            'title'	=> 'Title Of Notification',
    //                          	'icon'	=> 'myicon',/*Default Icon*/
    //                           	'sound' => 'mySound'/*Default sound*/
                                  
    //                       );
          
       
      
    //         	    $fields = array
    //             		(
    //             		    'to' => $user->fcm_id,
    //         				'notification'	=> $msg,
    //                         'data' => array('body' => array($msg),'sound' => 'default')
    //         			);
            
    
    //     	        $headers = array
    //             		(
    //         				'Authorization: key=' . API_ACCESS_KEY,
    //         				'Content-Type: application/json'
    //         			);
                        
    //                 #Send Reponse To FireBase Server
                     
    //         		$ch = curl_init();
    //         		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    //         		curl_setopt( $ch,CURLOPT_POST, true );
    //         		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    //         		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    //         		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    
                    
    //         		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    //                 $result = curl_exec($ch );
    //         		curl_close( $ch );
    //                 #Echo Result Of FireBase Server
    //                 // exit;
    //                 echo $result;
        
    return $addBill;
});

Route::post('change_password', function (Request $request) {
    $data = $request->all();

    $user=User::where('email', $data['email'])->first();
    if ($user) {
        $user->password=bcrypt($data['password']);
        $user->save();
        
        return response()->json([
            'success' => 'Password Changed',
            'code' => 200,
        ], 200);
    } else {
        return response()->json([
            'success' => 'User Does not exist',
            'code' => 401,
        ], 401);
    }
});

Route::get('major_groups', function () {
    $major_group=MajorGroup::all();
  
    $array['major_group']=$major_group;
        
    return $array;
});

/////////////////// all  groups ///////////////////

Route::get('allGroups/{waiter_id}', function ($waiter_id) {
    $major_group=MajorGroup::all();
    $subGroups = SubMajorGroups::with('menuItems')->get();
    $familyGroups = FamilyGroup::all();
    $alcoholGroups = AlcoholicFamilyGroup::all();
    $subalcoholGroups = AlcoholicSubFamilyGroup::all();
    $items = GeneralItems::all();
    $tables = Tables::all();
    $bills = BillMaster::where('waiter_id', $waiter_id)->where('status', 'Un-paid')->get();
    $paidOrders=PostPaidOrders::where('status', "NEW ORDERS")->where('waiter_id', $waiter_id)->with('orderItems')->get();
    
    $array['major_group'] = $major_group;
    $array['subGroups'] = $subGroups;
    $array['family_groups'] = $familyGroups;
    $array['alcoholGroups'] = $alcoholGroups;
    $array['subAlcoholicGroups'] = $subalcoholGroups;
    $array['items'] = $items;
    $array['tables'] = $tables;
    $array['bills'] = $bills;
    $array['paidOrders']=$paidOrders;
     
     
     
    return $array;
});

Route::get('waiters', function (Request $request) {
    $user = User::where('user_type', 'waiter')->get();
    $array['waiters']=$user;
    return $array;
});

Route::post('billTransfer', function (Request $request) {
    $user = User::where('id', $request->waiter)->first();
    $bill = BillMaster::where('id', $request->bill_id)->first();
    $data = array(
    'bill_master_id' => $request->bill_id,
   'from_waiter_id' => $bill->waiter_id,
   'from_waiter_name' => $bill->waiter_name,
   'to_waiter_id' => $user->id ,
   'to_waiter_name' => $user->name,
   'status' => 'Pending',  );
    $find = BillTransferRequest::where('bill_master_id', $request->bill_id)->Orwhere('from_waiter_id', $bill->waiter_id)->first();
    if (empty($find)) {
        $add1=BillTransferRequest::create($data);
        return response()->json([
                'success' => 'Bill Transfered request Goes To Admin Successfully!',
                'code' => 200,
            ], 200);
    } else {
        return response()->json([
            'error' => 'Bill Transfered request Already exist!',
            'code' => 404,
        ], 404);
    }
});

////////////////////////////////////////////////////////

Route::post('sub_major_groups', function (Request $request) {
    $subGroups = SubMajorGroups::with('menuItems')->where('major_group_name', $request->major_group_name)->get();
    $array['subGroups']=$subGroups;
    return $array;
});

Route::post('family_groups', function (Request $request) {
    $subGroups = FamilyGroup::where('menu_item_groups_id', $request->menu_items_group_id)->get();
    
    $array['family_groups']=$subGroups;
        
    return $array;
});

Route::post('alcoholic_groups', function (Request $request) {

    //return $request->sub_major_groups_id;
    $subGroups = AlcoholicFamilyGroup::where('menu_items_group_id', $request->menu_items_group_id)->get();
    
    $array['alcoholGroups']=$subGroups;
        
    return $array;
});

Route::post('sub_alcoholic_groups', function (Request $request) {

    //return $request->sub_major_groups_id;
   
    //return $id;
    $subGroups = AlcoholicSubFamilyGroup::where('family_groups_id', $request->family_groups_id)->get();
    $array['subAlcoholicGroups']=$subGroups;
        
    return $array;
});

Route::post('general_items', function (Request $request) {

//echo $request->is_alchoholic;
    $id=null;
    if ($request->is_alchoholic == "true") {
        $id = $request->family_groups_id."_"."alcohol_family" ;
    } else {
        $id = $request->family_groups_id."_"."family" ;
    }
    //return $id;
    //return $request->sub_major_groups_id;
    $subGroups = GeneralItems::where('family_groups_id', $id)->get();
    $array['items']=$subGroups;
        
    return $array;
});

Route::get('newOrders/{waiter_id}', function ($waiter_id) {
    $paidOrders=PostPaidOrders::where('waiter_id', $waiter_id)->where('status', "NEW ORDERS")->with('orderItems')->get();
    
    $array['paidOrders']=$paidOrders;
    
    return $array;
});

Route::post('prepaidOrders', function (Request $request) {
    $data=$request->all();
    $items = json_decode($data['items']);
    $arr=array();
    $date=date('Y-m-d');

    //return $date;
    
    $final_date=$date ." ".$data['date_time'];
    $data['date_time'] = $final_date;
    //return $data['date_time'];
    //print_r($data);exit;
    $add=PrepaidOrders::create($data)->id;
    
    foreach ($items as $item) {
        //echo $item->item_name;
        $arr['prepaid_orders_id'] = $add;
        //echo $add;
        $arr['name'] =$item->item_name;
        $arr['quantity'] = $item->item_quantity;
        $arr['price'] = $item->price;
        $arr['image'] = $item->image;
        $arr['print_class_id'] = $item->print_class_id ;
        $arr['general_item_id'] = $item->general_item_id;
        //print_r($arr);exit;
        $add1=PrePaidItems::create($arr);
    }
    //return $data['date_time'];
    //return $request->sub_major_groups_id;
    if ($add) {
        return response()->json([
            'success' => 'Order has been made Successfully!',
            'order_id' => $add,
            'code' => 200,
        ], 200);
    }
    return response()->json([
        'success' => 'Masachusa!',
        'code' => 401,
    ], 401);
});

// Route::post('offlinePostpaidOrders', function (Request $request) {
    
//     $data=$request->all();
//     $orders = json_decode($data['orders']);
//     return $orders;exit;
//     // $items = null;
    
//     // foreach($orders as $order){
        
//     //     $items = json_decode($order['items']);
        
//     // }
    
// });

///////////////////////////// Offline //////////////////////////////

Route::post('offlinePostpaidOrders', function (Request $request) {
    
    $dataa=App::call('App\Http\Controllers\HomeController@xeroInstance');
    $data=$request->all();
    $final_order_ids=array();
    $orders = json_decode($data['orders']);
    try{
    foreach ($orders as $order) {
        $arr_invoices=[];

        $invoice = new \XeroAPI\XeroPHP\Models\Accounting\Invoice;
        $contact = new \XeroAPI\XeroPHP\Models\Accounting\Contact;
        $add = 0;
        $arr=array();
     
       $items = json_decode($order->items);
     
        $grandtotal = 0;
        foreach ($items as $item) {
            $grandtotal += $item->item_quantity * $item->price;
        }

        $date=date('Y-m-d');
        $final_date=$date ." ".$order->date_time;
       
        $amount = 0 ;
        ///////////////// Bill Generation /////////////////////////
        
        
        $alreadyExist = PostPaidOrders::where('date_time', $final_date)->where('table_no', $order->table_no)->where('waiter_id', $order->waiter_id)->first();
        if (!empty($alreadyExist)) {
            $add = $alreadyExist->id;
            array_push($final_order_ids, $add);
        } else {
            //$date1 = explode(" ", $order->date_time);
            $data1['date'] = $date ." ".$order->date_time;
            $data1['waiter_id'] = $order->waiter_id;
            $data1['waiter_name'] = $order->waiter;
           
            $amount = $grandtotal;
            $data1['total_amount'] = $amount;
            $data1['no_orders'] = 1;
            $data1['orders'] = 0;
            $user =User::where('id',$data1['waiter_id'])->first();
            $addBill=BillMaster::create($data1)->id;
            //return $addBill;
            //////////////////// Order Generation///////////////////////

            $orderData['date_time'] = $final_date;
            $orderData['table_no'] = $order->table_no;
            $orderData['no_of_guests'] = $order->no_of_guests;
            $orderData['item_description'] = $order->item_description;
            $orderData['waiter'] = $order->waiter;
            $orderData['restaurant'] = $order->restaurant;
            $orderData['total_amount'] = $grandtotal;
            //   $orderData['print_class_id'] = $order->print_class_id;
            $orderData['waiter_id'] = $order->waiter_id;
            $orderData['section'] = $order->section;

            $orderData['bill_id'] = $addBill;
            //return $addBill;
            $add=PostPaidOrders::create($orderData)->id;
       
            $bill1=BillMaster::where('id', $addBill)->first();
             //set  contact for invoices
             $contact->setContactStatus(\XeroAPI\XeroPHP\Models\Accounting\Contact::CONTACT_STATUS_ACTIVE);
            
            $contact->setName($data1['waiter_name']);
            $contact->setEmailAddress($user->email);
            $invoice->setContact($contact);
            $invoice->setType(\XeroAPI\XeroPHP\Models\Accounting\Invoice::TYPE_ACCREC);
            $dateInstance = new DateTime();
            $dateInstance3 = new DateTime();
            $invoice->setDate($dateInstance);
            $dateInstance1 = $dateInstance3;
            $invoice->setDueDate($dateInstance1);
            $invoice->setCurrencyCode(\XeroAPI\XeroPHP\Models\Accounting\CurrencyCode::KES);
            $invoice->setStatus(\XeroAPI\XeroPHP\Models\Accounting\Invoice::STATUS_SUBMITTED);

            $bill1->orders = $add;
            $bill1->save();
        
            ///////////////////////////////////////////////////////////
            $items = json_decode($order->items);
            //return $items;exit;
            $s = sizeof($items);
            $count = 0;

            foreach ($items as $item) {
                $line= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
                $a = (array)$item;
                //print_r($a['item_name']);
                $count++;
                if ($count == $s) {
                    $arr['do_print'] = "yes";
                }
                $arr['post_paid_orders_id'] = $add;
                //echo $add;
                $arr['name'] =$a['item_name'];
                $arr['quantity'] = $a['item_quantity'];
                $arr['price'] = $a['price'];
                $arr['image'] = $a['image'];
                $arr['print_class_id'] = $a['print_class_id'];
                $arr['general_item_id'] = $a['general_item_id'];
                $arr['comments'] = $item->comments;
                
                //stock management
                $recipe = Recipe::where('general_item_id',$a['general_item_id'])->first();
                $print_class = PrintClasses::where('id',$a['print_class_id'])->first();
              
                if(sizeof($recipe) > 0){
                    $ingredients = Ingredients::where('recipe_id',$recipe->recipe_id)->get();
                }else{
                    $ingredients = null;
                }
                if(sizeof($ingredients) > 0){
       
       
                    foreach($ingredients as $key =>$value1){
                         
                     
                        $raw =stock::where('ingredient_id',$value1->ingredient_id)->where('print_class',$print_class->name)->first();
                        if(sizeof($raw) >0){
                            $quantity =$value1->quantity *  $a['item_quantity'];
                            $z=$raw->quantity_added;
                            $raw->quantity_added = $raw->quantity_added - $quantity ;
                            $raw->save();
                            
                            
                            $ingredients_sale = array(
                            'ingredient_id' => $value1->ingredient_id,
                            'price' => $value1->price,
                            'quantity' => $quantity,
                            'print_class_name' => $print_class->name,
                            'void'=>0
                        );
                        SalesIngredient::create($ingredients_sale);
                        
                        
                        $ingredient1 = array(
                        
                        'print_class_name' => $print_class->name,
                        'current_quantity' => $z,
                        'adjusted_quantity' =>(-1*$quantity) ,
                        'ingredient_id'=>$value1->ingredient_id,
                        'transferred_from'=>'app'
                        
                        );
                        edit_ingredients_print_class::create($ingredient1);
                            
                        }
                        
                        
                        
                    }

                }
          
                $add1=OrderItems::create($arr);
                $line->setDescription($a['item_name']);
                $item_id = 'item-'.$a['general_item_id'];
                $line->setQuantity($a['item_quantity']);
                $line->setItemCode($item_id);
                // $line->setAccountCode('001');
                $line->setUnitAmount($a['price']);
                $line->setTaxType('NONE');
                $line->setTaxAmount(0);
                $amount= $a['item_quantity'] * $a['price'];
                $line->setLineAmount($amount);
                $line->setDiscountRate(0); // Percentage
                // // Add the line to the order
                $lineArray=[];

                array_push($lineArray, $line);

                 $invoice->setLineItems($lineArray);
                 array_push($arr_invoices, $invoice);
                 $invoices = new \XeroAPI\XeroPHP\Models\Accounting\Invoices;
                $invoices->setInvoices($arr_invoices);

            }
              

            try{
                
                $new = $dataa['api_instance']->createInvoices($dataa['xero_tenant_id'],$invoices);
            } catch (\XeroAPI\XeroPHP\ApiException $exception) {
                print_r($exception);
                exit();
            } 
   
                
            // $sxml = simplexml_load_string($posted_payment->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
            
            BillMaster::where('id', $addBill)->update(['xero_invoice_id'=>$new->getInvoices()[0]->getInvoiceId()]);
            if ($add) {
                array_push($final_order_ids, $add);
            }
        }
    }
    return response()->json([
        'success' => 'Orders has been made Successfully!',
        'order_id' => $final_order_ids,
        'code' => 200,
    ], 200);
}catch(Exception $e){
        return response()->json([
        'success' => 'fail',
        'code' => 400,
    ], 400);
    }
});

////////////////////////////////////////////////////////////////////


Route::post('postpaidOrders', function (Request $request) {
    
    $dataa=App::call('App\Http\Controllers\HomeController@xeroInstance');
        $arr_invoices=[];

        $invoice = new \XeroAPI\XeroPHP\Models\Accounting\Invoice;
        $contact = new \XeroAPI\XeroPHP\Models\Accounting\Contact;
        $contact1 = new \XeroAPI\XeroPHP\Models\Accounting\Contact;

    $data=$request->all();
    $items = json_decode($data['items']);
    $s = sizeof($items);
    $grandtotal = 0;
   
    foreach ($items as $item) {
       
        // $arr['quantity'] = $item->item_quantity;
        // $arr['price'] = $item->price;
        
        $grandtotal += $item->item_quantity * $item->price;
    }
    //return $items;exit;
    $arr=array();
    $amount = 0;

    //print_r($data);exit;
    $date=date('Y-m-d');
       
    ////////////// BILL ///////////////
    $data['total_amount'] = $grandtotal;
    $date1 = explode(" ", $data['date_time']);
    $data1['date'] = $date ." ".$data['date_time'];
    $data1['waiter_id'] = $data['waiter_id'];
    $data1['waiter_name'] = $data['waiter'];
    $user=User::where('id',$data1['waiter_id'])->first();
           
      
    $data1['total_amount'] = $grandtotal;
    $data1['no_orders'] = 1;
    $data1['orders'] = 0;
    $addBill=BillMaster::create($data1)->id;

    //set  contact for invoices
    $contact->setContactStatus(\XeroAPI\XeroPHP\Models\Accounting\Contact::CONTACT_STATUS_ACTIVE);

    $contact->setName($data1['waiter_name']);
    $contact->setEmailAddress($user->email);
    $invoice->setContact($contact);
    $invoice->setType(\XeroAPI\XeroPHP\Models\Accounting\Invoice::TYPE_ACCREC);
    $dateInstance = new DateTime();
    $dateInstance3 = new DateTime();
    $invoice->setDate($dateInstance);
    $dateInstance1 = $dateInstance3;
    $invoice->setDueDate($dateInstance1);
    $invoice->setCurrencyCode(\XeroAPI\XeroPHP\Models\Accounting\CurrencyCode::KES);
            $invoice->setStatus(\XeroAPI\XeroPHP\Models\Accounting\Invoice::STATUS_SUBMITTED);
    //////////////////////////////////

    //return $date;
   
    $final_date=$date ." ".$data['date_time'];
    $data['date_time'] = $final_date;
    $data['bill_id'] = $addBill;
    $add=PostPaidOrders::create($data)->id;
   
    
    //$ch = PostPaidOrders::where('id',$add)->first();

    $bill1=BillMaster::where('id', $addBill)->first();

    $bill1->orders = $add;
    $bill1->save();

    $s = sizeof($items);
    $count = 0;
    
    foreach ($items as $item) {
        $line= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
        //echo $item->item_name;
        $count++;
        if ($count == $s) {
            $arr['do_print'] = "yes";
        }
        $arr['post_paid_orders_id'] = $add;
        //echo $add;
        $arr['name'] =$item->item_name;
        $arr['quantity'] = $item->item_quantity;
        $arr['price'] = $item->price;
        $arr['image'] = $item->image;
        $arr['print_class_id'] = $item->print_class_id;
        $arr['general_item_id'] = $item->general_item_id;
         
        $recipe = Recipe::where('general_item_id',$item->general_item_id)->first();
        $print_class = PrintClasses::where('id',$item->print_class_id)->first();
        
        if(sizeof($recipe) > 0){
             $ingredients = Ingredients::where('recipe_id',$recipe->recipe_id)->get();
        }else{
            $ingredients = null;
        }
       
       
       
         
          if(sizeof($ingredients) > 0){
       
       
             foreach($ingredients as $key =>$value1){
                 
                 
                $raw =stock::where('ingredient_id',$value1->ingredient_id)->where('print_class',$print_class->name)->first();
                if(sizeof($raw) >0){
                    $quantity =$value1->quantity * $item->item_quantity;
                    $z=$raw->quantity_added;
                    $raw->quantity_added = $raw->quantity_added - $quantity ;
                    $raw->save();
                    
                    
                    $ingredients_sale = array(
                        'ingredient_id' => $value1->ingredient_id,
                        'price' => $value1->price,
                        'quantity' => $quantity,
                        'print_class_name' => $print_class->name
                );
                    SalesIngredient::create($ingredients_sale);
                    
                     $ingredient1 = array(
                        'void'=>0,
                        'print_class_name' => $print_class->name,
                        'current_quantity' => $z,
                        'adjusted_quantity' =>(-1*$quantity) ,
                        'ingredient_id'=>$value1->ingredient_id,
                        'transferred_from'=>'app'
                        
                        );
                        edit_ingredients_print_class::create($ingredient1);
                    
                }
                 
            }
          }
        
        
        $arr['comments'] = $item->comments;
        //
        $line->setDescription($item->item_name);
        $item_id = 'item-'.$item->general_item_id;
        $line->setQuantity($item->item_quantity);
        $line->setItemCode($item_id);
        // $line->setAccountCode('001');
        $line->setUnitAmount($item->price);
        $line->setTaxType('NONE');
        $line->setTaxAmount(0);
        $amount= $item->item_quantity * $item->price;
        $line->setLineAmount($amount);
        $line->setDiscountRate(0); // Percentage
        // // Add the line to the order
        $lineArray=[];

        array_push($lineArray, $line);

         $invoice->setLineItems($lineArray);
         array_push($arr_invoices, $invoice);

         $invoices = new \XeroAPI\XeroPHP\Models\Accounting\Invoices;
$invoices->setInvoices($arr_invoices);
        //print_r($arr);exit;
        $add1=OrderItems::create($arr);
       
    }
    try{
                
        $new = $dataa['api_instance']->createInvoices($dataa['xero_tenant_id'],$invoices);
    } catch (\XeroAPI\XeroPHP\ApiException $exception) {
        print_r($exception);
        exit();
    } 
   
        
    // $sxml = simplexml_load_string($posted_payment->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
    
    BillMaster::where('id', $addBill)->update(['xero_invoice_id'=>$new->getInvoices()[0]->getInvoiceId()]);
   
        
    // $sxml = simplexml_load_string($posted_payment->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
    
    // BillMaster::where('id', $addBill)->update(['xero_invoice_id'=>$sxml->Invoices->Invoice->InvoiceID]);
    
    //return $request->sub_major_groups_id;
    
    if ($add) {
        return response()->json([
            'success' => 'Order has been made Successfully!',
            'order_id' => $add,
           
            'code' => 200,
        ], 200);
    } else {
        return response()->json([
            'failure' => 'Masachusa!',
            'code' => 401,
        ], 401);
    }
});

Route::post('generate_bill', function (Request $request) {
    $order_id = $request->order_id;
    $order = PostPaidOrders::where('id', $order_id)->first();
    $amount = 0;
    //return $order['date_time'];
    $data1['date'] = $order['date_time'];
    $data1['waiter_id'] = $order['waiter_id'];
    $data1['waiter_name'] = $order['waiter'];
           
    $amount = $order['total_amount'];
    $data1['total_amount'] = $amount;
    $data1['no_orders'] = 1;
    $data1['orders'] = $order['id'];
    $addBill=BillMaster::create($data1)->id;
    
    $order->bill_id = $addBill;
    $order->save();
    
    if ($addBill) {
        return response()->json([
            'success' => 'Order has been made Successfully!',
            'bill_id' => $addBill,
           
            'code' => 200,
        ], 200);
    } else {
        return response()->json([
            'failure' => 'Masachusa!',
            'code' => 401,
        ], 401);
    }
});


 Route::post(
     'updateStatus',
     function (Request $request) {
         $amount = 0;
         $order = PostPaidOrders::where('id', $request->order_id)->first();
         $order->status = "Completed";
         $order->save();
        
         $date = explode(" ", $order->date_time);
         $data['date'] = $date[0];
         $data['waiter_id'] = $order->waiter_id;
         $data['waiter_name'] = $order->waiter;
           
         $amount = $amount + $order->total_amount;
         $data['total_amount'] = $amount;
         $data['no_orders'] = 1;
         $data['orders'] = $order->id;
         //$add=BillMaster::create($data);
        
         if ($order->status == "Completed") {
             return response()->json([
                'success' => 'Order Status Updated Successfully!',
                
                'code' => 200,
            ], 200);
         }
     }
);

Route::post(
    'search',
    function (Request $request) {
        $items = GeneralItems::where('name', 'like', '%' . $request->name . '%')->get();
        
        $array['items']=$items;
        return $array;
    }
);

Route::get('InProgressOrders/{waiter_id}', function ($waiter_id) {
    $inProgress=PostPaidOrders::where('waiter_id', $waiter_id)->where('status', '!=', 'Completed')->with('orderItems')->orderBy('id', 'DESC')->get();
    
    $array['inProgress']=$inProgress;
    
    return $array;
});

Route::get('paidOrders', function () {
    $paidOrders=PrepaidOrders::with('Items')->get();
    
    $array['paidOrders']=$paidOrders;
    
    return $array;
});

Route::get('postpaidfinal', function () {
    $paidOrders=PostPaidOrders::where('status', "Closed Order Payment")->with('orderItems')->get();
    
    $array['paidOrders']=$paidOrders;
    
    return $array;
});

Route::get('ClosedPayments', function () {
    $inProgress=PostPaidOrders::with('orderItems')->where('status', "Closed Payment")->get();
    
    $array['closed_payments']=$inProgress;
    
        
    return $array;
});

Route::post('bills', function (Request $request) {
    $inProgress=BillMaster::where('waiter_id', $request->waiter_id)->where('status', 'Un-paid')->get();
    
    foreach ($inProgress as $progress) {
        $inOrders=PostPaidOrders::where('bill_id', $progress->id)->first();
        $progress->comments = $inOrders->item_description;
        $progress->table_no = $inOrders->table_no;
    }
    
    $array['bills']=$inProgress;
    
        
    return $array;
});

Route::post('change_order_status', function (Request $request) {
    $bill=BillMaster::where('id', $request->bill_id)->first();
    $bill->status = "Paid";
    $bill->save();
    
    return response()->json([
        'success' => 'Bill Status Updated Successfully!',
        'code' => 200,
    ], 200);
});

Route::post('bill_status', function (Request $request) {
    $bill=BillMaster::where('id', $request->bill_id)->first();
    $bill->status = "Paid";
    $bill->save();
    
    $orders = $bill->orders;
    $str=explode(',', $orders);
    //return $str;
    // $amount = 0;
        
    // $order = array();
        
    for ($i=0;$i<sizeof($str);$i++) {
        $j=0;
        $orders = PostPaidOrders::where('id', $str[$i])->first();
        //return $orders;
        //print_r($orders);
        $orders->status = "Closed Order Payment";
        $orders->save();
        //print_r($orders);
    }
    
    return response()->json([
        'success' => 'Bill Status Updated Successfully!',
        'code' => 200,
    ], 200);
});



/////////////////////////////////////////////////////////////////
Route::post('offlineAddToBill', function (Request $request) {
    $dataa=App::call('App\Http\Controllers\HomeController@xeroInstance');
    $data=$request->all();
    $orders = json_decode($data['orders']);
    $final_order_ids=array();
    $bills_ids = array();
    
    foreach ($orders as $order) {
        $invoice = new \XeroAPI\XeroPHP\Models\Accounting\Invoice;
    }
       
       try{  
        $items = json_decode($order->items);
        $grandtotal = 0;
        $existBill = BillMaster::where('id', $order->bill_id)->first();
        
        if ($existBill->status =='Paid') {
            array_push($bills_ids, $existBill->id);
        } else {
            $items3 = DB:: select("SELECT order_items.general_item_id,order_items.name,sum(order_items.quantity) as quantity  ,order_items.price FROM `order_items` JOIN post_paid_orders ON order_items.post_paid_orders_id = post_paid_orders.id JOIN bill_masters ON post_paid_orders.bill_id= bill_masters.id WHERE bill_masters.id ='". $order->bill_id."' group by  order_items.general_item_id ");
        foreach($items3 as  $key => $value ){
            $lineArray=[];
             $line= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
            $line->setQuantity($value->quantity);
            $line->setDescription($value->name);
            $item_id = 'item-'.$value->general_item_id;
            $line->setItemCode($item_id);
            // $line->setAccountCode('001');
            $line->setUnitAmount($value->price);
            $line->setTaxType('NONE');
            $line->setTaxAmount(0);
            $amount= $value->quantity * $value->price;
            $line->setLineAmount($amount);
            $line->setDiscountRate(0); // Percentage
            // // Add the line to the order
            array_push($lineArray, $line);
            foreach ($items as $item) {
       
                // $arr['quantity'] = $item->item_quantity;
                // $arr['price'] = $item->price;
                
                $grandtotal += $item->item_quantity * $item->price;
            }
            $arr=array();
            
            //print_r($data);exit;
            $date=date('Y-m-d');
                
            //return $date;
                
            $final_date=$date ." ".$order->date_time;
            $order->date_time = $final_date;
            $orderData['date_time'] = $final_date;
            $orderData['table_no'] = $order->table_no;
            $orderData['cd'] = $order->no_of_guests;
            $orderData['item_description'] = $order->item_description;
            $orderData['waiter'] = $order->waiter;
            $orderData['restaurant'] = $order->restaurant;
            $orderData['total_amount'] = $grandtotal;
            //   $orderData['print_class_id'] = $order->print_class_id;
            $orderData['waiter_id'] = $order->waiter_id;
            $orderData['section'] = $order->section;
            

            $add = PostPaidOrders::create($orderData)->id;
            $existBill->total_amount =$existBill->total_amount + $grandtotal;
            $existBill->no_orders = $existBill->no_orders+1;
            $existBill->orders = $existBill->orders.",".$add;
            
                
            $check_it = PostPaidOrders::where('id', $add)->first();
            $check_it->bill_id = $existBill->id;
            $check_it->save();
                
            $s = sizeof($items);
            $count = 0;
            foreach ($items as $item) {
                $count++;
                if ($count == $s) {
                    $arr['do_print'] = "yes";
                }
                //echo $item->item_name;
                $arr['post_paid_orders_id'] = $add;
                //echo $add;
                $arr['name'] =$item->item_name;
                $arr['quantity'] = $item->item_quantity;
                $arr['price'] = $item->price;
                $arr['image'] = $item->image;
                $arr['print_class_id'] = $item->print_class_id;
                $arr['general_item_id'] = $item->general_item_id;
                $arr['comments'] = $item->comments;

 //stock management
         $recipe = Recipe::where('general_item_id',$item->general_item_id)->first();
         $print_class = PrintClasses::where('id',$item->print_class_id)->first();
                if(sizeof($recipe) > 0){
                    $ingredients = Ingredients::where('recipe_id',$recipe->recipe_id)->get();
                }else{
                    $ingredients = null;
                }
                if(sizeof($ingredients) > 0){
       
       
                    foreach($ingredients as $key =>$value1){
                     
                        $raw =stock::where('ingredient_id',$value1->ingredient_id)->where('print_class',$print_class->name)->first();
                        if(sizeof($raw) >0){
                            $quantity =$value1->quantity * $item->item_quantity;
                            $raw->quantity_added = $raw->quantity_added - $quantity ;
                            $raw->save();
                        }
                      
                    }

                }

                //print_r($arr);exit;
                $add1=OrderItems::create($arr);
                
                
              
                $linee= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
                
                //xero items
                $linee->setDescription($item->item_name);
                $item_id = 'item-'.$item->general_item_id;
                $linee->setQuantity($item->item_quantity);
                $linee->setItemCode($item_id);
                // $line->setAccountCode('001');
                $linee->setUnitAmount($item->price);
                $linee->setTaxType('NONE');
                $linee->setTaxAmount(0);
                $amount= $item->item_quantity * $item->price;
                $linee->setLineAmount($amount);
                $linee->setDiscountRate(0); // Percentage
                // // Add the line to the order
               

                array_push($lineArray, $linee);

                 $invoice->setLineItems($lineArray);
                

            }
            if ($add) {
                array_push($final_order_ids, $add);
            }
        }
        
        
         $s= $existBill->orders;
 
     $items4 = DB:: select("SELECT * FROM `order_items` where post_paid_orders_id IN (".$s.")");

     $total=0;
     foreach($items4 as  $key => $value){
       $total=$total+($value->quantity*$value->price);
       
   }
   $total=$total-$existBill->discounted_amount;

   $existBill->total_amount=$total;
    $existBill->save();
        
        //return $request->sub_major_groups_id;
       
        try{
           
            $new = $dataa['api_instance']->updateInvoice($dataa['xero_tenant_id'],$existBill->xero_invoice_id,$invoice);
        } catch (\XeroAPI\XeroPHP\ApiException $exception) {
            print_r($exception);
           
        } 
       
            
        // $sxml = simplexml_load_string($posted_payment->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
        
        // BillMaster::where('id', $addBill)->update(['xero_invoice_id'=>$new->getInvoices()[0]->getInvoiceId()]);
       
   
        
         
    }
    
    $bills_ids= array_unique($bills_ids);
    return response()->json([
        'success' => 'Orders has been made Successfully!',
        'order_id' => $final_order_ids,
        'bill_id' => $bills_ids,
        'code' => 200,
    ], 200);
           
    }catch(Exception $e){
        return response()->json([
        'success' => 'fail',
        'code' => 400,
    ], 400);
    }
});
///////////////////////////////////////////////////////////////////
Route::post('add_to_bill', function (Request $request) {
    $dataa=App::call('App\Http\Controllers\HomeController@xeroInstance');
    $data=$request->all();
    $items = json_decode($data['items']);
    $arr=array();
    $existBill = BillMaster::where('id', $data['bill_id'])->first();
    if($existBill->status =='Un-paid'){
        //print_r($data);exit;
    $date=date('Y-m-d');

    //return $date;
    
    $final_date=$date ." ".$data['date_time'];
    $data['date_time'] = $final_date;
    $data['item_description'] = $data['item_description'];
   
    $invoice = new \XeroAPI\XeroPHP\Models\Accounting\Invoice;
    $items3 = DB:: select("SELECT order_items.general_item_id,order_items.name,sum(order_items.quantity) as quantity  ,order_items.price FROM `order_items` JOIN post_paid_orders ON order_items.post_paid_orders_id = post_paid_orders.id JOIN bill_masters ON post_paid_orders.bill_id= bill_masters.id WHERE bill_masters.id ='". $data['bill_id']."' group by  order_items.general_item_id ");
    
    foreach($items3 as  $key => $value ){
        $lineArray=[];
        $line= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
         $item_id = 'item-'.$value->general_item_id;
        $invoices2 = $dataa['api_instance']->getItems($dataa['xero_tenant_id'], null, null, null);
       
        foreach($invoices2 as $invoi){
         if( $item_id ==$invoi->getCode()){
             $code= $invoi->getSalesDetails()->getAccountCode();
            
            break;
         }  
        }
        
        // $Item= new \XeroPHP\Models\Accounting\Item();
        $line->setQuantity($value->quantity);
        $line->setDescription($value->name);
       
        $line->setItemCode($item_id);
        $line->setAccountCode($code);
        $line->setUnitAmount($value->price);
        $line->setTaxType('NONE');
        $line->setTaxAmount(0);
        $amount= $value->quantity * $value->price;
        $line->setLineAmount($amount);
        $line->setDiscountRate(0); // Percentage
        // // Add the line to the order
        array_push($lineArray, $line);
        
    }
    $add=PostPaidOrders::create($data)->id;
   
    $existBill->total_amount =$existBill->total_amount + $data['total_amount'];
    $existBill->no_orders = $existBill->no_orders+1;
    $existBill->orders = $existBill->orders.",".$add;
   
    
    // $check_it = PostPaidOrders::where('id', $add)->first();
    // $check_it->bill_id = $existBill->id;
    // $check_it->save = $data['bill_id'];
    
    // $check_it->save();
    
    $check_it = PostPaidOrders::where('id', $add)->first();
    $check_it->bill_id = $existBill->id;
    $check_it->save();
    
    $s = sizeof($items);
    $count = 0;
    
    $error=0;
    foreach ($items as $item) {
        
       
        $count++;
        if ($count == $s) {
            $arr['do_print'] = "yes";
        }
        //echo $item->item_name;
        $arr['post_paid_orders_id'] = $add;
        //echo $add;
        $arr['name'] =$item->item_name;
        $arr['quantity'] = $item->item_quantity;
        $arr['price'] = $item->price;
        $arr['image'] = $item->image;
        $arr['print_class_id'] = $item->print_class_id;
        $arr['general_item_id'] = $item->general_item_id;
        $arr['comments'] = $item->comments;
        
        //stock management
         $recipe = Recipe::where('general_item_id',$item->general_item_id)->first();
         $print_class = PrintClasses::where('id',$item->print_class_id)->first();
                if(sizeof($recipe) > 0){
                    $ingredients = Ingredients::where('recipe_id',$recipe->recipe_id)->get();
                }else{
                    $ingredients = null;
                }
                if(sizeof($ingredients) > 0){
       
       
                    foreach($ingredients as $key =>$value1){
                     
                        $raw =stock::where('ingredient_id',$value1->ingredient_id)->where('print_class',$print_class->name)->first();
                        if(sizeof($raw) >0){
                            $quantity =$value1->quantity * $item->item_quantity;
                            $raw->quantity_added = $raw->quantity_added - $quantity ;
                            $raw->save();
                        }
                      
                    }

                }
        
        //print_r($arr);exit;
         $item_id = 'item-'.$item->general_item_id;
         $invoices2 = XeroPrivate::load('Accounting\\Item')->execute();
       
        foreach($invoices2 as $invoi){
         if( $item_id ==$invoi->Code){
             $code= $invoi->SalesDetails->AccountCode;
            
            break;
         }  
        }
        $add1=OrderItems::create($arr);
        
        
        

        $linee= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
        
        //setting items for bill
        $linee->setDescription($item->item_name);
       
        $linee->setQuantity($item->item_quantity);
        $linee->setItemCode($item_id);
        $linee->setAccountCode($code);
        $linee->setUnitAmount($item->price);
        $linee->setTaxType('NONE');
        $linee->setTaxAmount(0);
        $amount= $item->item_quantity * $item->price;
         // Percentage
        // // Add the line to the order
        $linee->setLineAmount($amount);
        $linee->setDiscountRate(0);

        array_push($lineArray, $linee);
  $invoice->setLineItems($lineArray);
        

        
    }
    
  
   $s= $existBill->orders;
 
     $items4 = DB:: select("SELECT * FROM `order_items` where post_paid_orders_id IN (".$s.")");

     $total=0;
     foreach($items4 as  $key => $value){
       $total=$total+($value->quantity*$value->price);
       
   }
   $total=$total-$existBill->discounted_amount;

   $existBill->total_amount=$total;
    $existBill->save();
    


  $invoice->setInvoiceID($existBill->xero_invoice_id);
  
  try{
           
    $new = $dataa['api_instance']->updateInvoice($dataa['xero_tenant_id'],$existBill->xero_invoice_id,$invoice);
} catch (\XeroAPI\XeroPHP\ApiException $exception) {
    print_r($exception);
   
} 
    

    
    
    
    //return $request->sub_major_groups_id;
   
     
    

    
    if ($add) {
        return response()->json([
            'success' => 'Order has been made Successfully!And Added To Bill.',
            'order_id' => $add,
           
            'code' => 200,
        ], 200);
    } else {
        return response()->json([
            'failure' => 'failed!',
            'code' => 401,
        ], 401);
    }
    }
    else{
         return response()->json([
            'failure' => 'failed!',
            'code' => 401,
        ], 401);
    }
    
});


Route::post(
    'pay_bill',
    function (Request $request) {
        $api_publishable_key = '2fnCzJgFghRC6UWktQdJGKea1lRGrl9G';
        $api_secret_key = 'swYMKjZs4IN5jtlk';

        $url = "https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode($api_publishable_key.":".$api_secret_key);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);

        $res = json_decode($curl_response);
        //print_r($res->access_token);
        // print_r($res['access_token']."<br>");
        //$token=$res['access_token'];
        //echo $token."<br>";
        $timestamp =  date("Ymdhis");
        $signature = '370bf700aebc4bc746b63b31be6fb0b83e99d837426b0035e114da67e60994ad';

        $username = "928180";

        $password = base64_encode($username.$signature.$timestamp);

        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',"Authorization:Bearer $res->access_token")); //setting custom header


        $curl_post_data = array(
  //Fill in the request parameters with valid values
  'BusinessShortCode' => '928180',
  'Password' => "$password",
  'Timestamp' => "$timestamp",
  'TransactionType' => 'CustomerBuyGoodsOnline',
  'Amount' => $request->amount,
  'PartyA' => $request->phone,
  'PartyB' => '928181',
  'PhoneNumber' => $request->phone,
  'CallBackURL' => 'http://thesmitherp.digitalsystemsafrica.com/callback.php',
  'AccountReference' => 'The Smith Hotels',
  'TransactionDesc' => 'checking'
);

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);
        echo $curl_response;

        $res = json_decode($curl_response);

        //print_r($res);
        //print_r($res->CheckoutRequestID);

        $user=User::where('id', $request->waiter_id)->first();
        //return $user;
        $user->fcm_id = $request->fcm_id;
        $user->chequeRequest_id = $res->CheckoutRequestID;
        $user->save();
    }
);

Route::post(
    'prepaid_billing',
    function (Request $request) {
     
//return $request->amount;
     
        $api_publishable_key = '2fnCzJgFghRC6UWktQdJGKea1lRGrl9G';
        $api_secret_key = 'swYMKjZs4IN5jtlk';

        $url = "https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode($api_publishable_key.":".$api_secret_key);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        curl_close($curl);

        $res = json_decode($curl_response);
        //print_r($res->access_token);
        // print_r($res['access_token']."<br>");
        //$token=$res['access_token'];
        //echo $token."<br>";
        $timestamp =  date("Ymdhis");
        $signature = '370bf700aebc4bc746b63b31be6fb0b83e99d837426b0035e114da67e60994ad';

        $username = "928180";

        $password = base64_encode($username.$signature.$timestamp);

        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',"Authorization:Bearer $res->access_token")); //setting custom header


        $curl_post_data = array(
  //Fill in the request parameters with valid values
  'BusinessShortCode' => '928180',
  'Password' => "$password",
  'Timestamp' => "$timestamp",
  'TransactionType' => 'CustomerBuyGoodsOnline',
  'Amount' => $request->amount,
  'PartyA' => $request->phone,
  'PartyB' => '928181',
  'PhoneNumber' => $request->phone,
  'CallBackURL' => 'http://thesmitherp.digitalsystemsafrica.com/callback.php',
  'AccountReference' => 'The Smith Hotels',
  'TransactionDesc' => 'checking'
);

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);
        echo $curl_response;

        $res = json_decode($curl_response);

        //print_r($res);
        //print_r($res->CheckoutRequestID);

        $user=User::where('id', $request->waiter_id)->first();
        //return $user;
        $user->fcm_id = $request->fcm_id;
        $user->chequeRequest_id = $res->CheckoutRequestID;
        $user->save();
    }
);
Route::post('waiterTables', function (Request $request) {
    $table = Tables::where('waiter_id', $request->waiter_id)->get();

    $array['tables'] = $table;
    return $array;
});
