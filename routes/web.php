<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::post('admin/closed-orders-filter',"Admin\ManageOrdersController@closedOrdersFilter");
Route::post('admin/quantity-sales-filter',"Admin\AdminReportsController@quantitySalesFilter");


Route::get("xero","XeroController@index");
Route::get("callback","XeroController@callback");
Route::get("authorisedResource","XeroController@authorisedResource");
Route::get("admin/orders/postpaid/transfer-bill/{waiter_id}/{waiter_name}/{id}","Admin\ManageOrdersController@transferBill");
   
Route::post("admin/orders/postpaid/transfer-bill",'Admin\ManageOrdersController@billTransfer');
Route::get("/test1",'Admin\ManageOrdersController@test1');

    Route::get("admin/orders/void-items","Admin\ManageOrdersController@voidItems");
    
    Route::post("admin/orders/voidItems/{status}","Admin\ManageOrdersController@voidStatus");
    
    Route::get("/admin/orders/reject-voidItems/{status}/{order_id}/{item_id}/{item_name}","Admin\ManageOrdersController@rejectVoidStatus");
    
    Route::get("admin/orders/add-void-items/{order_id}/{item_id}","Admin\ManageOrdersController@addVoidItems");

    Route::post("admin/orders/print-master-bills-etr","Admin\ManageOrdersController@printEtr");    


Auth::routes();
Route::get('/', function () {
    // Update the user's profile...

     if (Auth::user()) {
        return redirect('/admin/dashboard');
    } else {
        return redirect('/login');
    }
});

Route::get('/public/print-class', function () {
    // Update the user's profile...

    return redirect('/public/print-class/login');
});
date_default_timezone_set('Africa/Nairobi');
Route::get('/home', 'HomeController@index')->name('home');

Route::get("/login","HomeController@userLogin")->name('login');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('/logoutprintClassuser', 'Auth\LoginController@logoutPrintClassUser');

Route::get("/public/print-class/login","HomeController@printClassLogin");
Route::post("/public/print-class/login","HomeController@printClassUserLogin");
Route::get("/public/print-class-orders","HomeController@orders");
Route::get("/public/print-class-orders1/{id}","HomeController@orders1");
Route::get("/public/print-class-orders-update","HomeController@ordersUpdate");
Route::get("/public/history/{id}","HomeController@orderHistory");
Route::post("/public/historyfilter","HomeController@orderHistoryFilter");

//Route::get('/check', 'HomeController@check');

Route::get("/admin/reports/void_reports","Admin\AdminReportsController@voidReport");
Route::get("/admin/reports/locationwise_reports","Admin\AdminReportsController@locationwiseReport");

Route::post("admin/reports/voidReportFilter","Admin\AdminReportsController@voidReportFilter");




Route::post('admin/reports/filter_sales_products',"Admin\AdminReportsController@filterReports");

    Route::get("admin/dashboard","Admin\AdminDashboardController@index");
    Route::get("admin/orders/bill_receipt/{bill_id}","Admin\ManageOrdersController@reciept");

    Route::post("admin/orders/print-master-bills","Admin\ManageOrdersController@print");
    Route::post("admin/orders/print-docket","Admin\ManageOrdersController@printDocket");
     Route::get("admin/orders/status/{status}/{id}/{order_id}","Admin\ManageOrdersController@updateOrderStatus");
    Route::post("admin/orders/updatePrint","Admin\ManageOrdersController@updatePrint");
    
    Route::post("admin/employees-table-assignment-with-user-section","Admin\ManageOrdersController@employeeTable");
    Route::post("admin/assign-or-remove-table-from-waiter","Admin\ManageOrdersController@assignTable");
    Route::post("admin/remove-table-from-waiter","Admin\ManageOrdersController@unassignTable");
    Route::post("admin/release-all-table-from-waiter","Admin\ManageOrdersController@releaseAllTables");    
        Route::post("admin/already-assigned","Admin\ManageOrdersController@alreadyAssignedTables");    


    Route::get("admin/major-group-managers","Admin\AdminMenuSetupController@index");
    Route::get("admin/add-major-group","Admin\AdminMenuSetupController@addMajorGroup");
    Route::post("admin/add-major-group","Admin\AdminMenuSetupController@createMajorGroup");
    Route::get("admin/major-group/delete/{id}","Admin\AdminMenuSetupController@deleteMajorGroup");
    Route::post("admin/edit-major-group/edit/{id}","Admin\AdminMenuSetupController@updateMajorGroup");
    Route::get("admin/edit-major-group/edit/{id}","Admin\AdminMenuSetupController@editMajorGroup");
     Route::get("admin/test","Admin\AdminMenuSetupController@test");

    Route::get("admin/gl-accounts","Admin\AdminMenuSetupController@GlAccount");
    Route::get("admin/add-gl-accounts","Admin\AdminMenuSetupController@addGlAccount");
    Route::post("admin/add-gl-accounts","Admin\AdminMenuSetupController@createGlAccount");
    Route::get("admin/delete-gl-accounts/{id}","Admin\AdminMenuSetupController@deleteGlAccount");
    Route::post("admin/edit-gl-accounts/{id}","Admin\AdminMenuSetupController@updateGlAccount");
    Route::get("admin/edit-gl-accounts/{id}","Admin\AdminMenuSetupController@editGlAccount");

    Route::get("admin/plu-numbers","Admin\AdminMenuSetupController@PLUNumber");
    Route::get("admin/add-plu-numbers","Admin\AdminMenuSetupController@addPLUNumber");
    Route::post("admin/add-plu-numbers","Admin\AdminMenuSetupController@createPLUNumber");
    Route::get("admin/delete-plu-numbers/{id}","Admin\AdminMenuSetupController@deletePLUNumber");
    Route::post("admin/edit-plu-numbers/{id}","Admin\AdminMenuSetupController@updatePLUNumber");
    Route::get("admin/edit-plu-numbers/{id}","Admin\AdminMenuSetupController@editPLUNumber");
    
    Route::get("admin/sub-major-groups","Admin\AdminMenuSetupController@subMajorGroups");
    Route::get("admin/add-sub-major-group","Admin\AdminMenuSetupController@addSubMajorGroup");
    Route::post("admin/add-sub-major-group","Admin\AdminMenuSetupController@createSubMajorGroup");
    Route::get("admin/sub-major-group/delete/{id}","Admin\AdminMenuSetupController@deleteSubMajorGroup");
    Route::post("admin/edit-sub-major-groups/{id}","Admin\AdminMenuSetupController@updatesubMajorGroup");
    Route::get("admin/edit-sub-major-groups/{id}","Admin\AdminMenuSetupController@editSubMajorGroup");

    Route::get("admin/offers","Admin\AdminMenuSetupController@offers");
    Route::get("admin/add-offers","Admin\AdminMenuSetupController@addOffers");
    Route::post("admin/add-offers","Admin\AdminMenuSetupController@createOffer");
    Route::get("admin/delete-offer/{id}","Admin\AdminMenuSetupController@deleteOffer");
    Route::post("admin/edit-offer/{id}","Admin\AdminMenuSetupController@updateOffer");
    Route::get("admin/edit-offer/{id}","Admin\AdminMenuSetupController@editOffer");

    Route::get("admin/menu-item-groups","Admin\AdminMenuSetupController@menuItemGroups");
    Route::get("admin/add-menu-item-groups","Admin\AdminMenuSetupController@addMenuItemGroups");
    Route::post("admin/add-menu-item-groups","Admin\AdminMenuSetupController@createMenuItemGroup");
    Route::get("admin/delete-menu-item-group/{id}","Admin\AdminMenuSetupController@deleteMenuItemGroup"); 
    Route::post("admin/edit-menu-item-group/{id}","Admin\AdminMenuSetupController@updateMenuItemGroup");
    Route::get("admin/edit-menu-item-group/{id}","Admin\AdminMenuSetupController@editMenuItemGroup");

    Route::get("admin/family-groups","Admin\AdminMenuSetupController@familyGroups");       
    Route::get("admin/add-family-groups","Admin\AdminMenuSetupController@addFamilyGroups");
    Route::post("admin/add-family-groups","Admin\AdminMenuSetupController@createFamilyGroup");
    Route::get("admin/delete-family-group/{id}","Admin\AdminMenuSetupController@deleteFamilyGroup");  
    Route::post("admin/edit-family-group/{id}","Admin\AdminMenuSetupController@updateFamilyGroup");
    Route::get("admin/edit-family-group/{id}","Admin\AdminMenuSetupController@editFamilyGroup");

    Route::get("admin/alcoholic-family-groups","Admin\AdminMenuSetupController@alcoholicFamilyGroups");       
    Route::get("admin/add-alcoholic-family-groups","Admin\AdminMenuSetupController@addAlcoholicFamilyGroups");
    Route::post("admin/add-alcoholic-family-groups","Admin\AdminMenuSetupController@createAlcoholicFamilyGroup");
    Route::get("admin/delete-alcoholic-family-group/{id}","Admin\AdminMenuSetupController@deleteAlcoholicFamilyGroup"); 
    Route::post("admin/edit-alcoholic-family-group/{id}","Admin\AdminMenuSetupController@updateAlcoholicFamilyGroup");
    Route::get("admin/edit-alcoholic-family-group/{id}","Admin\AdminMenuSetupController@editAlcoholicFamilyGroup");

    Route::get("admin/alcoholic-sub-family-groups","Admin\AdminMenuSetupController@alcoholicSubFamilyGroups");       
    Route::get("admin/add-alcoholic-sub-family-groups","Admin\AdminMenuSetupController@addAlcoholicSubFamilyGroups");
    Route::post("admin/add-alcoholic-sub-family-groups","Admin\AdminMenuSetupController@createAlcoholicSubFamilyGroup");
    Route::get("admin/delete-alcoholic-sub-family-groups/{id}","Admin\AdminMenuSetupController@deleteAlcoholicSubFamilyGroup"); 
    Route::post("admin/edit-alcoholic-sub-family-group/{id}","Admin\AdminMenuSetupController@updateAlcoholicSubFamilyGroup");
    Route::get("admin/edit-alcoholic-sub-family-group/{id}","Admin\AdminMenuSetupController@editAlcoholicSubFamilyGroup");

    
    Route::get("admin/general-items","Admin\AdminMenuSetupController@generalItems");
    Route::get("admin/add-general-items","Admin\AdminMenuSetupController@addGeneralItems");
    Route::post("admin/add-general-items","Admin\AdminMenuSetupController@createGeneralItems");
    Route::get("admin/delete-general-items/{id}","Admin\AdminMenuSetupController@deleteGeneralItems");
    Route::post("admin/edit-general-items/{id}","Admin\AdminMenuSetupController@updateGeneralItems");
    Route::get("admin/edit-general-items/{id}","Admin\AdminMenuSetupController@editGeneralItems");

    Route::get("admin/beer-and-keg-sub-major-group","Admin\AdminMenuSetupController@deliverySubMajorGroup");       
    Route::get("admin/add-beer-and-keg-sub-major-group","Admin\AdminMenuSetupController@addDeliverySubMajorGroup");
    Route::post("admin/add-beer-and-keg-sub-major-group","Admin\AdminMenuSetupController@createDeliverySubMajorGroup");
    Route::get("admin/delete-beer-and-keg-sub-major-group/{id}","Admin\AdminMenuSetupController@deleteDeliverySubMajorGroup");
    Route::post("admin/edit-beer-and-keg-sub-major-group/{id}","Admin\AdminMenuSetupController@updateDeliverySubMajorGroup");
    Route::get("admin/edit-beer-and-keg-sub-major-group/{id}","Admin\AdminMenuSetupController@editDeliverySubMajorGroup");

    Route::get("admin/delivery-family-group","Admin\AdminMenuSetupController@deliveryFamilyGroup");       
    Route::get("admin/add-delivery-family-group","Admin\AdminMenuSetupController@addDeliveryFamilyGroup");
    Route::post("admin/add-delivery-family-group","Admin\AdminMenuSetupController@createDeliveryFamilyGroup");
    Route::get("admin/delete-delivery-family-group/{id}","Admin\AdminMenuSetupController@deleteDeliveryFamilyGroup");
    Route::post("admin/edit-delivery-family-group/{id}","Admin\AdminMenuSetupController@updateDeliveryFamilyGroup");
    Route::get("admin/edit-delivery-family-group/{id}","Admin\AdminMenuSetupController@editDeliveryFamilyGroup");

    Route::get("admin/delivery-sub-family-group","Admin\AdminMenuSetupController@deliverySubFamilyGroup");       
    Route::get("admin/add-delivery-sub-family-group","Admin\AdminMenuSetupController@addDeliverySubFamilyGroup");
    Route::post("admin/add-delivery-sub-family-group","Admin\AdminMenuSetupController@createDeliverySubFamilyGroup");
    Route::get("admin/delete-delivery-sub-family-group/{id}","Admin\AdminMenuSetupController@deleteDeliverySubFamilyGroup");
    Route::post("admin/edit-delivery-sub-family-group/{id}","Admin\AdminMenuSetupController@updateDeliverySubFamilyGroup");
    Route::get("admin/edit-delivery-sub-family-group/{id}","Admin\AdminMenuSetupController@editDeliverySubFamilyGroup");

    Route::get("admin/delivery-items","Admin\AdminMenuSetupController@deliveryItems");       
    Route::get("admin/add-delivery-item","Admin\AdminMenuSetupController@addDeliveryItems");
    Route::post("admin/add-delivery-item","Admin\AdminMenuSetupController@createDeliveryItems");
    Route::get("admin/delete-delivery-item/{id}","Admin\AdminMenuSetupController@deleteDeliveryItems");
    Route::post("admin/edit-delivery-item/{id}","Admin\AdminMenuSetupController@updateDeliveryItems");
    Route::get("admin/edit-delivery-item/{id}","Admin\AdminMenuSetupController@editDeliveryItems");
   
    Route::get("admin/orders/prepaid","Admin\AdminMenuSetupController@prepaidOrders");    
    Route::get("admin/orders/completed","Admin\AdminMenuSetupController@completedOrders");       
    Route::get("admin/orders/closed","Admin\AdminMenuSetupController@closedOrders");
    Route::get("admin/orders/postpaid","Admin\AdminMenuSetupController@postpaidOrders");
    Route::post("admin/orders/postpaidreportfilter","Admin\AdminMenuSetupController@postpaidreportfilter");
    
    Route::get("admin/orders/generate-bills","Admin\ManageOrdersController@generateBills");  
    Route::post("admin/orders/generate-bills/{id}","Admin\ManageOrdersController@createBills"); 
    Route::post("admin/orders/generate-bills/{id}/{array}","Admin\ManageOrdersController@finalTotalBills");  
    Route::get("admin/orders/master-bills","Admin\ManageOrdersController@billMaster");
    Route::get("admin/orders/cash-reciept/{id}","Admin\ManageOrdersController@cashReciept");
    Route::post("admin/orders/cash-reciept/{id}","Admin\ManageOrdersController@generateCashReciept");
    Route::get("admin/orders/closed-payment-orders","Admin\ManageOrdersController@closedPaymentOrders");
    
    //////////////////////////////////////////////// ReportsRoutes /////////////////////////////
    Route::get("admin/reports/payment-sales-summary","Admin\AdminReportsController@paymentSalesSummary");    
    Route::get("admin/reports/menu-item-general-sales","Admin\AdminReportsController@menuitemgeneralsales");
    Route::get("admin/reports/menu-item-general-sales-with-plu","Admin\AdminReportsController@menuitemgeneralsaleswithplu");
    Route::get("admin/reports/menu-item-general-sales-without-plu","Admin\AdminReportsController@menuitemgeneralsaleswithoutplu");
    Route::get("admin/reports/family-group-sales","Admin\AdminReportsController@familygroupsales");
    Route::get("admin/reports/family-group-sales-with-gl","Admin\AdminReportsController@familygroupsaleswithgl");
    Route::get("admin/reports/menu-item-group-sales","Admin\AdminReportsController@menuitemgroupsales");
    Route::get("admin/reports/major-group-sales","Admin\AdminReportsController@majorgroupsales");
    Route::get("admin/reports/waiter-with-family-groups","Admin\AdminReportsController@waiterwithfamilygroup");
    Route::get("admin/reports/condiment-sales-report-with-plu","Admin\AdminReportsController@condimentsalesreportwithplu");
    Route::get("admin/reports/get-discounts-reports","Admin\AdminReportsController@discountreports");
    Route::get("admin/reports/cashier-reports","Admin\AdminReportsController@reports");
    Route::get("admin/reports/discount-reports-with-orders","Admin\AdminReportsController@discountreportswithorder");
    Route::get("admin/reports/complementary-reports-with-orders","Admin\AdminReportsController@complementaryreportswithorder");
    Route::get("admin/reports/get-cancelled-orders-reports","Admin\AdminReportsController@cancelledreports");
    Route::get("admin/reports/wallet-ledger-entries","Admin\AdminReportsController@walletledgerentries");
    
    Route::get("admin/orders/postpaid/edit/{order_id}/{total_amount}",'Admin\ManageOrdersController@addDiscountpostpaid');
    Route::get("admin/orders/billmaster/edit/{order_id}/{total_amount}",'Admin\ManageOrdersController@addDiscountbillmaster');
    
    Route::post("admin/orders/postpaid/edit/{order_id}",'Admin\ManageOrdersController@addBillDiscountPostPaid');
    Route::post("admin/orders/billmaster/edit/{order_id}",'Admin\ManageOrdersController@addBillDiscountBillMaster');

    Route::get("admin/reports/open-orders","Admin\AdminReportsController@openOrders");
    Route::post("admin/reports/open-orders-filter","Admin\AdminReportsController@openOrderFilter");
    
    Route::get("admin/reports/completed-orders","Admin\AdminReportsController@completedOrders"); 
    Route::get("admin/reports/product-wise-sales","Admin\AdminReportsController@productSales");
    
     Route::get("admin/reports/unpaid_by_waiters","Admin\AdminReportsController@unpaidByWaiters");
    Route::get("admin/reports/waiters_detailed_reports","Admin\AdminReportsController@waiterDetails");
    Route::get("admin/reports/overall_sales_report","Admin\AdminReportsController@overallSales");
    Route::get("admin/reports/discount_report","Admin\AdminReportsController@discountReport");

    Route::post("admin/reports/waiterReportFilter","Admin\AdminReportsController@waiterDetailsFilter");
    Route::post("admin/reports/unpaidwaiterReportFilter","Admin\AdminReportsController@unpaidWaitersFilter");
    
    Route::post("admin/reports/unpaidDiscountFilter","Admin\AdminReportsController@discountFilter");
    
    Route::post("admin/reports/paymentSalesReportFilter","Admin\AdminReportsController@paymentSalesFilter");
  
      
    /////////////////////////////////////////// Admin System Setup /////////////////////////////////////////////

    Route::get("admin/table-manager","Admin\AdminSystemSetupController@tables");
    Route::get("admin/table-manager/add-table","Admin\AdminSystemSetupController@addTable");
    Route::post("admin/table-manager/add-table","Admin\AdminSystemSetupController@createTable");
    Route::get("admin/table-manager/edit-table/{id}","Admin\AdminSystemSetupController@editTable");
    Route::post("admin/table-manager/edit-table/{id}","Admin\AdminSystemSetupController@updateTable");    
    Route::get("admin/table-manager/delete-table/{id}","Admin\AdminSystemSetupController@deleteTable");
    //edit table
    Route::get("admin/table-manager/edit-table/{id}","Admin\AdminSystemSetupController@EditTable");
    Route::post("admin/table-manager/Save-table","Admin\AdminSystemSetupController@EditSaveTable");
    // table status change
    Route::get("admin/tableStaus/{id}/{status}","Admin\AdminSystemSetupController@TableChangeStatus");

    Route::get("admin/advertisements","Admin\AdminSystemSetupController@index");
    Route::get("admin/add-advertisements","Admin\AdminSystemSetupController@addAdvertisements");
    Route::post("admin/add-advertisements","Admin\AdminSystemSetupController@createAdvertisements");
    Route::get("admin/edit-advertisements/{id}","Admin\AdminSystemSetupController@editAdvertisements");
    Route::post("admin/edit-advertisements/{id}","Admin\AdminSystemSetupController@updateAdvertisement");    
    Route::get("admin/delete-advertisements/{id}","Admin\AdminSystemSetupController@deleteAdvertisements");
    
    Route::get("admin/reservation-emails","Admin\AdminSystemSetupController@reservationEmail");
    Route::get("admin/add-reservation-emails","Admin\AdminSystemSetupController@addReservationEmail");
    Route::post("admin/add-reservation-emails","Admin\AdminSystemSetupController@createReservationEmail");
    Route::get("admin/delete-reservation-emails/{id}","Admin\AdminSystemSetupController@deleteReservationEmail");

    Route::get("admin/payment-methods","Admin\AdminSystemSetupController@paymentMethods");
    Route::get("admin/add-payment-methods","Admin\AdminSystemSetupController@addPaymentMethods");
    Route::post("admin/add-payment-methods","Admin\AdminSystemSetupController@createPaymentMethods");
    Route::get("admin/delete-payment-methods/{id}","Admin\AdminSystemSetupController@deletePaymentMethods");
    Route::get("admin/edit-payment-methods/{id}","Admin\AdminSystemSetupController@editPaymentMethods");    
    Route::post("admin/edit-payment-methods/{id}","Admin\AdminSystemSetupController@updatePaymentMethods");
    
    Route::get("admin/restaurants","Admin\AdminSystemSetupController@restaurants");    
    Route::get("admin/add-restaurants","Admin\AdminSystemSetupController@addRestaurants");
    Route::post("admin/add-restaurants","Admin\AdminSystemSetupController@createRestaurants");
    Route::get("admin/delete-restaurants/{id}","Admin\AdminSystemSetupController@deleteRestaurants");
    
    Route::get("admin/print-classes","Admin\AdminSystemSetupController@printClasses");    
    Route::get("admin/add-print-classes","Admin\AdminSystemSetupController@addPrintClasses");
    Route::post("admin/add-print-classes","Admin\AdminSystemSetupController@createPrintClasses");
    Route::get("admin/delete-print-classes/{id}","Admin\AdminSystemSetupController@deletePrintClasses");
    Route::get("admin/edit-print-classes/{id}","Admin\AdminSystemSetupController@editPrintClasses");    
    Route::post("admin/edit-print-classes/{id}","Admin\AdminSystemSetupController@updatePrintClasses");

    Route::get("admin/print-class-user","Admin\AdminSystemSetupController@printClassUsers");    
    Route::get("admin/add-print-class-user","Admin\AdminSystemSetupController@addprintClassUsers");
    Route::post("admin/add-print-class-user","Admin\AdminSystemSetupController@createprintClassUsers");
    Route::get("admin/delete-print-class-user/{id}","Admin\AdminSystemSetupController@deleteprintClassUsers");
    Route::get("admin/edit-print-class-user/{id}","Admin\AdminSystemSetupController@editprintClassUsers");    
    Route::post("admin/edit-print-class-user/{id}","Admin\AdminSystemSetupController@updateprintClassUsers");
    

    Route::get("admin/take-away","Admin\AdminSystemSetupController@takeAway");    
    Route::get("admin/add-take-away","Admin\AdminSystemSetupController@addTakeAway");
    Route::post("admin/add-take-away","Admin\AdminSystemSetupController@createTakeAway");
    Route::get("admin/delete-take-away/{id}","Admin\AdminSystemSetupController@deleteTakeAway");

    Route::get("admin/roles","Admin\AdminSystemSetupController@roles");    
    Route::get("admin/edit-roles/{id}","Admin\AdminSystemSetupController@editRoles");
    Route::post("admin/update-roles","Admin\AdminSystemSetupController@updateRoles");
    Route::get("admin/add-roles","Admin\AdminSystemSetupController@addRoles");
    Route::post("admin/add-roles","Admin\AdminSystemSetupController@createRoles");
    Route::get("admin/delete-roles/{id}","Admin\AdminSystemSetupController@deleteRoles");
    Route::get("admin/role-permissions/{id}","Admin\AdminSystemSetupController@rolePermissions");
    Route::post("admin/edit-permissions/{id}","Admin\AdminSystemSetupController@updatePermissions");
  
    Route::get("admin/tax-manager","Admin\AdminSystemSetupController@taxes");    
    Route::get("admin/add-tax","Admin\AdminSystemSetupController@addTax");
    Route::post("admin/add-tax","Admin\AdminSystemSetupController@createTax");
    Route::get("admin/delete-tax/{id}","Admin\AdminSystemSetupController@deleteTax");

    Route::get("admin/condiments","Admin\AdminSystemSetupController@condiments");    
    Route::get("admin/add-condiments","Admin\AdminSystemSetupController@addCondiments");
    Route::post("admin/add-condiments","Admin\AdminSystemSetupController@createCondiments");
    Route::get("admin/delete-condiments/{id}","Admin\AdminSystemSetupController@deleteCondiments");
    
    Route::get("admin/condiment-groups","Admin\AdminSystemSetupController@condimentsGroups");    
    Route::get("admin/add-condiment-groups","Admin\AdminSystemSetupController@addCondimentsGroups");
    Route::post("admin/add-condiment-groups","Admin\AdminSystemSetupController@createCondimentsGroups");
    Route::get("admin/delete-condiments-groups/{id}","Admin\AdminSystemSetupController@deleteCondimentsGroups");

    Route::get("admin/employees","Admin\AdminSystemSetupController@employees");    
    Route::get("admin/add-employee","Admin\AdminSystemSetupController@addEmployee");
    Route::post("admin/add-employee","Admin\AdminSystemSetupController@createEmployee");
    
    Route::get("admin/edit-employee/{id}","Admin\AdminSystemSetupController@editEmployee");
    Route::post("admin/edit-employee","Admin\AdminSystemSetupController@SaveEditEmployee");
   // change password from admin panel
    Route::get("admin/edit-employee-changepass/{id}","Admin\AdminSystemSetupController@ChangeEmployeePass");
    Route::post("admin/edit-employee-changepass/","Admin\AdminSystemSetupController@saveEmployeePass");
    //user change pass
    Route::get("admin/employee-changepass","Admin\AdminSystemSetupController@ChangeUserPass");
    Route::post("/admin/user-changepass1","Admin\AdminSystemSetupController@UserPass");
    // user status change
    Route::get("admin/userStaus/{id}/{status}","Admin\AdminSystemSetupController@ChangeStatus");


    Route::get("admin/delete-employee/{id}","Admin\AdminSystemSetupController@deleteEmployee");
    Route::get("admin/edit-print-class-user/{id}","Admin\AdminSystemSetupController@editprintClassUsers");    
    Route::post("admin/edit-print-class-user/{id}","Admin\AdminSystemSetupController@updateprintClassUsers");
    
    
    Route::get("admin/adjustmentlogs","Admin\AdminMenuSetupController@adjustmentlogs");
    //////////////////////////////////////////////// Admin Routes End //////////////////////////////////////////////////

Route::group(['prefix'=>'manager','as'=>'manager', 'namespace'=>'Manager'], function () {

    Route::get("dashboard","ManagerDashboardController@index");

});

Route::group(['prefix'=>'cashier','as'=>'cashier', 'namespace'=>'Cashier'], function () {

    Route::get("dashboard","CashierDashboardController@index");

});

Route::group(['prefix'=>'accounts','as'=>'accounts', 'namespace'=>'Accounts'], function () {

    Route::get("dashboard","AccountsDashboardController@index");

});

Route::group(['prefix'=>'supervisor','as'=>'supervisor', 'namespace'=>'Supervisor'], function () {

    Route::get("dashboard","SupervisorDashboardController@index");

});

Route::group(['prefix'=>'billprinter','as'=>'billprinter', 'namespace'=>'BillPrinter'], function () {

    Route::get("dashboard","BillPrinterDashboardController@index");

});

Route::group(['prefix'=>'eofici_admin_training','as'=>'eofici_admin_training', 'namespace'=>'EoficiAdminTraining'], function () {

    Route::get("dashboard","EoficiAdminTrainingDashboardController@index");

});
Route::get('/clear_cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('stock:level');
    return "Cache is cleared";
});
// bill request 
Route::get("/admin/orders/requested-order", "BillTransferRequestController@index");
Route::get("/admin/orders/requested-order/{id}/{status}", "BillTransferRequestController@statusChange");
// user profile
Route::get("/admin/userProfile", "Admin\AdminSystemSetupController@userProfile");

//save profile image
Route::post("/admin/usersave", "Admin\AdminSystemSetupController@userprofileSave");
Route::get("/admin/edit/userProfiles", "Admin\AdminSystemSetupController@userProfileShow");
//  bill transfer reports
Route::get("/admin/billTransfer", "BillTransferRequestController@reports");
Route::post("/admin/billTransfer", "BillTransferRequestController@reports1");

// test route
Route::get('xeroApi',"HomeController@xeroApi");
Route::get('/test123',"HomeController@test");


//add ingredients  page view
Route::get('/admin/ingredients',"Admin\AdminMenuSetupController@ingredients");

//save Recipe 
Route::post("/admin/add-rec", "Admin\AdminMenuSetupController@SaveRecipe");

//view all Recipies
Route::get("/admin/recepies", "Admin\AdminMenuSetupController@GetRecipes");

//edit recipie  page view
Route::get('/admin/recipe-edit/{id}',"Admin\AdminMenuSetupController@ingredientsEdit");

//Update Recipe 
Route::post("/admin/update-rec", "Admin\AdminMenuSetupController@UpdateRecipe");

//delete Recipe
Route::get('/admin/recipe-delete/{id}',"Admin\AdminMenuSetupController@RecipeDelete");

// add ingredients

Route::get('/admin/ingredients-view',"Admin\AdminMenuSetupController@rawIngredientsView");

//save ingredient 
Route::post("/admin/add-ingredient", "Admin\AdminMenuSetupController@SaveIngredient");


//get all ingredient
Route::get('/admin/manage-ingredients',"Admin\AdminMenuSetupController@manageIingredients");
Route::get('/admin/manage-stocks',"Admin\AdminMenuSetupController@managestocks");
Route::get('/admin/physical-stock-central-store',"Admin\AdminMenuSetupController@physicalStockCentralStore");
Route::get('/admin/manage-physical-stock-central-store',"Admin\AdminMenuSetupController@managePhysicalStockCentralStore");
Route::get('/admin/physical-stock-print-store',"Admin\AdminMenuSetupController@physicalStockPrintStore");
Route::get('/admin/manage-physical-stock-print-store',"Admin\AdminMenuSetupController@managePhysicalStockPrintStore");
Route::post('/admin/save-physical-stock-central-store',"Admin\AdminMenuSetupController@savePhysicalStockCentralStore");
Route::post('/admin/save-edited-physical-stock-central-store',"Admin\AdminMenuSetupController@saveEditedPhysicalStockCentralStore");
Route::post('/admin/save-physical-stock-print-store',"Admin\AdminMenuSetupController@savePhysicalStockPrintStore");
Route::post('/admin/save-edited-physical-stock-print-store',"Admin\AdminMenuSetupController@saveEditedPhysicalStockPrintStore");

//edit recipie  page view
Route::get('/admin/ingredient-edit/{id}',"Admin\AdminMenuSetupController@Editingredients");
//save edit ingredient data
Route::post('/admin/ingredient-edit',"Admin\AdminMenuSetupController@EditingredientsSave");

Route::get('/admin/ingredient-edit2/{id}',"Admin\AdminMenuSetupController@Editingredients2");
//save edit ingredient data
Route::post('/admin/ingredient-edit2',"Admin\AdminMenuSetupController@EditingredientsSave2");


Route::get('/admin/stock-edit/{id}',"Admin\AdminMenuSetupController@Editstock");
Route::get('/admin/edit-physical-stock-cs/{id}',"Admin\AdminMenuSetupController@editPhysicalStockCentralStore");
Route::get('/admin/edit-physical-stock-ps/{id}',"Admin\AdminMenuSetupController@editPhysicalStockPrintStore");

Route::post("/admin/filter/physical_stock_cs","Admin\AdminMenuSetupController@managePhysicalStockCentralStoreFilter");
Route::post("/admin/filter/physical_stock_ps","Admin\AdminMenuSetupController@managePhysicalStockPrintStoreFilter");
//save edit ingredient data
Route::post('/admin/stock-edit',"Admin\AdminMenuSetupController@EditstockSave");

//delete Ingredient
Route::get('/admin/ingredient-delete/{id}',"Admin\AdminMenuSetupController@IngredientDelete");
Route::get('/admin/stock-delete/{id}',"Admin\AdminMenuSetupController@stockDelete");
Route::get('/admin/stock-delete-ps/{id}',"Admin\AdminMenuSetupController@stockDeletePrintStore");

//admin  units add/delete


Route::get('/admin/units',"Admin\AdminMenuSetupController@Units");


Route::post('/admin/add-unit',"Admin\AdminMenuSetupController@UnitsSave");
Route::get('/admin/unit-edit/{id}',"Admin\AdminMenuSetupController@UnitsEdit");
Route::post('/admin/unit-save/{id}',"Admin\AdminMenuSetupController@UnitsEditSave");
Route::get('/admin/unit-delete/{id}',"Admin\AdminMenuSetupController@UnitsDelete");


 Route::get('delete_bill',"Admin\ManageOrdersController@test");




//enventry adjustment
// Locationwise report 
Route::get('/admin/Locationwise',"Admin\AdminReportsController@Locationwise");
Route::post('/admin/Locationwise',"Admin\AdminReportsController@Locationwise_filter");

Route::get('/admin/ingredient-adjustment/{id}',"Admin\AdminMenuSetupController@adjustment");
Route::get('/admin/stock-adjustment/{id}',"Admin\AdminMenuSetupController@stockadjustment");
Route::get('/admin/stock-transfer/{id}',"Admin\AdminMenuSetupController@stocktransfer");
Route::get('/admin/stock-transfer-central-store/{id}',"Admin\AdminMenuSetupController@stocktransfercentralstore");
Route::post('/admin/adjustments-save/{id}',"Admin\AdminMenuSetupController@adjustmentSave");
Route::post('/admin/stockadjustments-save/{id}',"Admin\AdminMenuSetupController@stockadjustmentSave");
Route::post('/admin/stocktransfer-save/{id}',"Admin\AdminMenuSetupController@stocktransferSave");
Route::post('/admin/stocktransfer-centralstore-save/{id}',"Admin\AdminMenuSetupController@stocktransfercentralstoreSave");


//add stock

Route::get('/admin/stock',"Admin\AdminMenuSetupController@stock");

Route::post('/admin/stockadd',"Admin\AdminMenuSetupController@stockAdd");



// app version control
Route::get('/admin/appversion',"Admin\AdminMenuSetupController@appversionview");
Route::post('/admin/appversion',"Admin\AdminMenuSetupController@appversion");