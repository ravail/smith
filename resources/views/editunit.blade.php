@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 ">
                <h4 class="page-title">UOM Management </h4>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12 ">
            <div class="card-box">
           
                <form action="/admin/unit-save/{{$unit->id}}" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Name</label>
                        <input class="form-control" type="text" id="name" name="name" value="{{$unit->name}}" required autofocus>
                      
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
        </div></div>
        </div>