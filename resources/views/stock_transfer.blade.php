@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 ">
                <h4 class="page-title">Station to Station Transfer</h4>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12 ">
            <div class="card-box">
           
                <form action="/admin/stocktransfer-save/{{$i->id}}" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Name</label>
                        <input class="form-control" type="text" id="name" name="name" value="{{$i->ingredient_name}}" readonly required autofocus>
                      
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Current Print Class</label>
                        <input class="form-control" type="text" id="current_print_class" name="current_print_class" value="{{$i->print_class}}" readonly required autofocus>
                      
                        @if ($errors->has('print_class'))
                        <span class="help-block">
                            <strong>{{ $errors->first('print_class') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Current Quantity</label>
                        <input class="form-control" type="text" id="current_quantity" name="current_quantity" value="{{$i->quantity_added}}" readonly required autofocus>
                      
                       
                    </div>
                    
                     <div class="form-group{{ $errors->has('classs') ? ' has-error' : '' }}" required autofocus>
                                    <label>Transfer To Print Class</label>
                                    <select class="selectpicker form-control" style="height:auto !important;" id="print_class" required name="print_class" data-live-search="true">
                                        <option value="">None</option>
                                         @foreach($classs as $key =>$value)
                                         
                                         @if($value->name != $i->print_class)
                                        <option  >{{$value->name}}</option>
                                        
                                        @endif
                                        
                                        @endforeach
                                    </select>
                    </div>
                    
                     <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Print Class Quantity</label>
                        <input class="form-control" type="text" id="current_quantity_class" name="current_quantity_class"  readonly required autofocus>
                      
                       
                    </div>
                    
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Quantity</label>
                        <input class="form-control" type="text" id="quantity" name="quantity" value="" pattern="^[+]?\d+([.]\d+)?$" title="Enter Positive values" required autofocus>
                      
                        @if ($errors->has('quantity'))
                        <span class="help-block">
                            <strong>{{ $errors->first('quantity') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                    <input class="form-control" type="hidden" id="ingredient_id" name="ingredient_id" value="{{$i->ingredient_id}}" readonly required autofocus>
                    
                    
                    
                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
        </div></div>
        </div>
        
        @section('javascript')
<script>


$(document).ready(function(){

  
  $("#print_class").change(function(){ 
      //alert($(this).val());
    
    var stock = {!!json_encode($stock);!!}
        
    var neww = $("#ingredient_id").val();
    
    for(var i=0; i<stock.length; i++){
        
        
        if(stock[i].ingredient_id == $('#ingredient_id').val() && $('#print_class').val() == stock[i].print_class){
            //alert();
            var old = stock[i].quantity_added;
           
            $('#current_quantity_class').val(old);
           
             
            
            break;
        }
        
        
    }
  });
});




</script>



</script>
@stop