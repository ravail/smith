@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

 <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-4 ">
                <h4 class="page-title">Create New Ingredient </h4>
            </div>
           
        </div>
        <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('error'))
                            <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('error') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
        <div class="card-box">
        
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/add-ingredient" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    
 <div class="row">
            <div class="col-md-6 ">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Name</label>
                        <input class="form-control" type="text" id="name" name="name" required autofocus>
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                            
                                <div class="form-group{{ $errors->has('quantity_type') ? ' has-error' : '' }}" required autofocus>
                                    <label>Unit Of Measure</label>
                                    <select class="selectpicker form-control" id="quantity_type" required name="quantity_type" data-live-search="true">
                                        <option >select</option>
                                         @foreach($unit as $key =>$value)
                                        <option value={{$value->name}}>{{$value->name}}</option>
                                        @endforeach



                                    </select>
                                </div>
                            </div>
 
            <div class="col-md-6 ">
                                <!--<div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">-->
                                <!--    <label>Quantity</label>-->
                                <!--    <input class="form-control" type="text" id="quantity" name="quantity" required autofocus>-->
                                <!--    @if ($errors->has('quantity'))-->
                                <!--    <span class="help-block">-->
                                <!--        <strong>{{ $errors->first('quantity') }}</strong>-->
                                <!--    </span>-->
                                <!--    @endif-->
                                <!--</div>-->
                           
                            
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <label>Price</label>
                        <input class="form-control" type="text" id="price" name="price" required autofocus>
                        @if ($errors->has('price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                    
                    </div>
                    </div>
                    <div class="row">
            <div class="col-md-6 ">
                    <div class="form-group{{ $errors->has('re_order_level') ? ' has-error' : '' }}">
                        <label>Stock Reorder level</label>
                        <input class="form-control" type="text" id="re_order_level" name="re_order_level" required autofocus>
                        @if ($errors->has('re_order_level'))
                        <span class="help-block">
                            <strong>{{ $errors->first('re_order_level') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                  
                    
                    
                    
                   
                      </div>
                      
                      
                    </div> 


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Create</button>
                    </div>
            </div>





            </form>
        </div>
        </div><br>
        <div class="row">
            <div class="col-md-8 ">
                <h4 class="page-title">All Ingredients </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                    <!--<a href="/dailysalesDownload.php" target="_blank" title="Export In Excel" class="btn btn-warning"-->
                    <!--    value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>-->
                    <!--<a href="/pdf/examples/product_wise.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->
                    <br><br>
                    <div class="table-responsive">
                    <!--<input id="myInput" type="text" placeholder="Search..">-->
                        <table class="table table-border custom-table m-b-0" id="myTable">
                            <thead>
                                <tr>
                                   
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Unit</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Cost</th>
                                     <th class="text-center">Reorder level</th>
                                     
                                    
                                    <th class="text-center">Action</th>
                                        

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                
                                @foreach($ingredient as $key =>$value)

                               <tr>
                                
                                   <td class="text-center">{{$value->name}}</td>
                                   <td class="text-center">{{$value->quantity_type}}</td>
                                   <td class="text-center">{{$value->quantity}}</td>
                                   <td class="text-center">{{$value->price}}</td>
                                   <td class="text-center">{{$value->re_order_level}}</td>
                                  
                                   
                                   
                                  <td class="text-center">
                                      @if(\Auth::user()->delete_ingredients =="on" || \Auth::user()->user_type =="admin")
                                       <a href="/admin/ingredient-delete/{{$value->id}}" class="btn btn-danger "  onclick="return confirm('Are you sure you want to delete this item?');"><i
                                            class="fa fa-trash-o"></i></a>
                                    @endif   
                                     @if(\Auth::user()->edit_ingredients =="on" || \Auth::user()->user_type =="admin")
                                        <a href="/admin/ingredient-edit2/{{$value->id}}" title="Edit"
                                        class="btn btn-warning custom"><i class="fa fa-pencil"></i></a>
                                        
                                        <!-- <a href="/admin/ingredient-edit/{{$value->id}}" title="Edit"-->
                                        <!--class="btn btn-success custom"><i class="fa fa-plus"></i></a>-->
                                        
                                        
                                        <!-- <a href="/admin/ingredient-adjustment/{{$value->id}}" title="Adjustment"-->
                                        <!--class="btn btn-info custom"><i class="fa fa-minus"></i></a>-->
                                        @endif   
                                    </td>
                               </tr>

                                   
                                @endforeach
                              
                            </tbody>


                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>
$(document).ready(function() {
     $('#myTable').DataTable();
});


</script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://www.olarroerp.digitalsystemsafrica.com/assets/jss/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://www.olarroerp.digitalsystemsafrica.com/assets/csss/bootstrap-datetimepicker.css">


<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<!--<script src='build/pdfmake.min.js'></script>-->
@stop