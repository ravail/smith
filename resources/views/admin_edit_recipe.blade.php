@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Update Recipe</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/update-rec" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}

                 
                 <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}">
                        <label>Number</label>
                        <input class="form-control" type="text" id="id" name="recipe_id" readonly value="{{$recipes->recipe_id}}" required autofocus>
                        @if ($errors->has('id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id') }}</strong>
                        </span>
                        @endif
                    </div>
                 
                  <div class="form-group row-fluid">
                        <label>Menu Items</label>
                        <select class="selectpicker form-control"  style="height:auto;" id="general_itemss" required name="general_itemss" data-live-search="true" disabled>
                            <option >select</option>
                            @foreach($general as $key =>$value)
                                <option value="{{$value->id}}"{{ $recipes->general_item_id == $value->id ? 'selected': ''}} >{{$value->name}}--{{$value->print_class}}</option>
                            @endforeach
                        </select>
                        
                    </div>
                    
                    
                    <input class="form-control" type="hidden" id="general_items" value="{{$recipes->general_item_id}}" name="general_items" required autofocus>

                    <!--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">-->
                    <!--    <label>Name</label>-->
                        <input class="form-control" type="hidden" id="name" value="{{$recipes->name}}" name="name" required autofocus>
                    <!--    @if ($errors->has('name'))-->
                    <!--    <span class="help-block">-->
                    <!--        <strong>{{ $errors->first('name') }}</strong>-->
                    <!--    </span>-->
                    <!--    @endif-->
                    <!--</div>-->
                    <div id="default">
                        
                        <input class="form-control" type="hidden" id="count" name="count" value="{{$recipes->ingredient_count}}" required autofocus>
                        @php 
                        $i=1;
                        @endphp
                        @foreach($recipes->ingredients as $key =>$value1)
                        <div class="row" id="row{{$i}}">
                            @php
                                $code =$value1->ingredient_id;
                                $type =$value1->quantity_type;
                            @endphp
                            <div class="col-md-4">
                               
                                    <label>Ingredients </label>
                                    <select class="selectpicker form-control ingredient1" id="ingredient{{$i}}" required name="ingredient[]" onchange="select(this)" data-live-search="true">
                                        <option >select</option>
                                        @foreach($ingredients as $key =>$value)
                                        <option data-price="{{$value->price}}"  data-code="{{$value->id}}" onchange="selected(this)" value="{{$value->name}}"  {{ $code == $value->id ? 'selected': ''}} masachusa="{{$i}}" >{{$value->name}}</option>
                                        @endforeach
                                        



                                    </select>


                            </div>
                             <input  type="hidden" value={{$value1->id}} id="row-id{{$i}}"  name="id[]"  >
                             <input  type="hidden" id="row-price{{$i}}" value={{$value1->price}}  name="price[]"  >
                             <input  type="hidden" id="row-code{{$i}}"  name="code[]" value={{$code}} >
                            <div class="col-md-3">
                                <div class="form-group{{ $errors->has('quantity_type') ? ' has-error' : '' }}"  autofocus>
                                    <label>Quantity Type </label>
                                    <input class=" form-control" id="quantity_type{{$i}}" style="height:auto !important" value=" {{$type}}"   name="quantity_type[]" readonly>
                                       
                                       
                                       



                                   
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                    <label>Quantity</label>
                                    <input class="form-control" type="text" id="quantity{{$i}}" name="quantity[]" value="{{$value1->quantity}}" required autofocus>
                                    @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2" style="margin: 30px 0px -5px ;">
                                
                                    <a class="btn btn-danger" style="float:right;" onclick=" remove({{$i}})" id="del{{$i}}">Remove</a>
                              
                            </div>

                        </div>
                        @php 
                            $i++;
                        @endphp
                        @endforeach
                    </div>
                    <div class="row" id="add-button" style="margin:auto;">
                        <div class="col-md-12" >
                            <a class="btn btn-success btn-sm" style="float:right;margin: 24px -19px;" id="add_ing">Add Ingredient</a>
                        </div>
                    </div>


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Add</button>
                    </div>
            </div>





            </form>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>

$(document).ready(function() {
    
    
    
    $("#add_ing").click(function() {
         var ingredients = {!!json_encode($ingredients);!!};
         var unit = {!!json_encode($unit);!!};
         console.log(ingredients);
         var count = parseInt($('#count').val());
         count = count + 1;
        var data=' <div class="row" id="row'+count+'"><div class="col-md-4">' +

            '<label>Ingredients </label>' +
            '<select class="selectpicker form-control" style="height:auto" id="ingredient'+count+'"  required name="ingredient[]" onchange="select(this)" data-live-search="true"><option >select</option>' ;
           for(var i=0; i<ingredients.length; i++){
               data += '<option data-price='+ingredients[i].price+' masachusa='+count+' data-code='+ingredients[i].id+' value='+ingredients[i].name+'>'+ingredients[i].name+'</option>';
           }
           
           
           data += ' </select>' +



            ' </div>' +
            '<div class="col-md-3">' +

            ' <label>Quantity Type </label>' +
             '  <input class="select form-control" style="height:auto !important" id="quantity_type'+count+'" readonly name="quantity_type[]"> ';
            
            
            data +=' </div>' +

            '<div class="col-md-3"><input  type="hidden" id="id"'+count+' value="0"  name="id[]"  >' +

            '<label>Quantity</label>' +
            ' <input class="form-control" type="text" id="quantity'+count+'" name="quantity[]" required autofocus>' +
            ' </div><div class="col-md-2" style="margin: 30px 0px -5px ;"><a class="btn btn-danger "  style="float:right;" onclick=" remove('+count+')" id="del1">Remove</a></div>'+
            '</div> <input  type="hidden" id="row-price'+count+'"   name="price[]"  ><input  type="hidden" id="row-code'+count+'"  name="code[]"  >';
            $("#default").append(data);
            $(".selectpicker").selectpicker('refresh');
            $('#count').val(count);
    });
    
    $('#ingredient').on("change",function(){
       
    });
});

function remove(id){
    
   var delete_id= parseInt($("#row-id"+id).val());
    var string = ' <input  type="hidden" value='+delete_id+'  name="delete[]"  >' ;
    $("#default").append(string);
    $("#row"+id).remove();
    var count = parseInt($('#count').val());
         count = count - 1;
         $('#count').val(count);
}



function select(ingrident){
    var price =$(':selected', ingrident).attr('data-price');
     var code =$(':selected', ingrident).attr('data-code');
     var m =$(':selected', ingrident).attr('masachusa');
    // alert(m);alert(code);
    var ingredients = {!!json_encode($ingredients);!!};
    var unit = {!!json_encode($unit);!!}
       console.log(ingredients);
    for(var i=0; i<ingredients.length; i++){
        
        if(ingredients[i].id == code){
            
            var type = ingredients[i].quantity_type;
           
            var id = '#quantity_type'+m ;
        
            $('#quantity_type'+m).val(type);
            
             $('#row-price'+m).val(price);
              $('#row-code'+m).val(code);
                console.log(m);
              
            
        }
    }
     
    // var string = ' <input  type="hidden" value='+price+'  name="price[]"  ><input  type="hidden"  name="code[]" value='+code+' >' ;
    // $("#default").append(string);
}




function selected(ingrident){
    
    var price =$(':selected', ingrident).attr('data-price');
     var code =$(':selected', ingrident).attr('data-code');
     var m =$(':selected', ingrident).attr('masachusa');
    // alert(code);
    var ingredients = {!!json_encode($ingredients);!!};
    var unit = {!!json_encode($unit);!!}
       
    for(var i=0; i<ingredients.length; i++){
        
        if(ingredients[i].id == code){
            
            var type = ingredients[i].quantity_type;
        //   alert(type);
            var id = '#quantity_type'+m ;
        
            $('#quantity_type'+m).val(type);
             $('#row-price'+m).val(price);
              $('#row-code'+m).val(code);
            
        }
    }
     
    // var string = ' <input  type="hidden" value='+price+'  name="price[]"  ><input  type="hidden"  name="code[]" value='+code+' >' ;
    // $("#default").append(string);
}
</script>


<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
@stop