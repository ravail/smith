@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        
        
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Adjustment Logs</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">

                
            </div>
        </div>
        <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
         
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                    <!--<a href="/dailysalesDownload.php" target="_blank" title="Export In Excel" class="btn btn-warning"-->
                    <!--    value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>-->
                    <!--<a href="/pdf/examples/product_wise.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->
                    <br><br>
                    <div class="table-responsive">
                    <!--<input id="myInput" type="text" placeholder="Search..">-->
                        <table class="table table-border custom-table m-b-0" id="myTable">
                            <thead>
                                <tr>
                                   
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Unit</th>
                                    <th class="text-center">Current Quantity</th>
                                    <th class="text-center">Adjusted Quantity</th>
                                     <th class="text-center">Total Quantity</th>
                                    <th class="text-center">Date of Adjustment/Time Of Adjustment</th>
                                    <th class="text-center">User</th>
                                    <th class="text-center">Remarks</th>
                                        

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                
                                @foreach($ingredient as $key =>$value)

                               <tr>
                                
                                   <td class="text-center">{{$value->name}}</td>
                                   <td class="text-center">{{$value->quantity_type}}</td>
                                   <td class="text-center">{{$value->current_quantity}}</td>
                                   <td class="text-center">{{$value->adjusted_quantity}}</td>
                                   <td class="text-center">{{$value->total_quantity}}</td>
                                   
                                  <td class="text-center">
                                      {{$value->created_at}}
                                     
                                    </td>
                                    <td class="text-center">{{$value->user}}</td>
                                     <td class="text-center">{{$value->remarks}}</td>
                                   
                               </tr>

                                   
                                @endforeach
                              
                            </tbody>


                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
@stop

@section('javascript')


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>


<script>
$(document).ready(function() {
    $('#myTable').DataTable();
} );
$(document).ready(function() {
    
   
});



///////////////////////////////
</script>

@stop