@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")


    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 ">
                <h4 class="page-title">Make Recipe</h4>
            </div>
           
        </div>
        <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
        <div class="card-box">
        
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/add-rec" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}

                 <div class="row">
            <div class="col-md-6 ">
                 <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}">
                        <label>Number</label>
                        <input class="form-control" type="text" id="id" name="id" readonly value="Rec-{{substr($id, 0, 15)}}" required autofocus>
                        @if ($errors->has('id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id') }}</strong>
                        </span>
                        @endif
                    </div>
                 
                  <div class="form-group row-fluid ">
                        <label>Menu Items</label>
                        <select class="selectpicker form-control geeral_items" style="height: auto !important;" id="geeral_items" required name="general_items" data-live-search="true">
                            <option >select</option>
                            @foreach($general as $key =>$value)
                                <option value="{{$value->id}}" >{{$value->name}}--{{$value->print_class}}</option>
                            @endforeach
                        </select>
                        
                    </div>
                     </div>
                     
                    
                    

            <div class="col-md-6 ">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <!--<label>Name</label>-->
                        <input class="form-control" type="hidden" id="name" name="name"  >
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    </div>
                    </div>
                    <div id="default">
                        <input class="form-control" type="hidden" id="count" name="count" value="1" required autofocus>
                        <div class="row" id="row1">

                            <div class="col-md-4">
                               
                                    <label>Ingredients </label>
                                    <select class="selectpicker form-control ingredient1" id="ingredient1" required name="ingredient[]" onchange="selected(this)" data-live-search="true">
                                        <option >select</option>
                                        @foreach($ingredients as $key =>$value)
                                            <option data-price="{{$value->price}}" data-code="{{$value->id}}" value="{{$value->name}}">{{$value->name}}</option>
                                        @endforeach
                                        



                                    </select>


                            </div>
                            <div class="col-md-3">
                                <div class="form-group{{ $errors->has('quantity_type') ? ' has-error' : '' }}" required autofocus>
                                    <label>Quantity Type </label>
                                    <select class=" form-control " id="quantity_type1" required name="quantity_type[]"  readonly style="height:37px !important">
                                        <option >select</option>
                                         @foreach($unit as $key =>$value)
                                        <option value="{{$value->name}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                    <label>Quantity</label>
                                    <input class="form-control" type="text" id="quantity" name="quantity[]" required autofocus>
                                    @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2" style="margin: 30px 0px -5px ;">
                                
                                    <a class="btn btn-danger" style="float:right;" onclick=" remove(1)" id="del1">Remove</a>
                              
                            </div>

                        </div>
                    </div>
                    <div class="row" id="add-button" style="margin:auto;">
                        <div class="col-md-12" >
                            <a class="btn btn-success btn-sm" style="float:right;margin: 24px -19px;" id="add_ing">Add Ingredient</a>
                        </div>
                    </div>


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Create</button>
                    </div>
            </div>





            </form>
        </div>
         </div>
        <br>
          
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-title">All Recipes</h4>
                <a href="/recipe.php" target="_blank" title="Export In Excel" class="btn btn-warning" value="xls" id="btnExport" style="
    float: right;"><i class="fa fa-file-excel-o"
                            aria-hidden="true"></i> </a>
            </div>
        </div>
        
        
        
        <div class="row">
            <div class="col-lg-12">
               
                <div class="card-box">

                   
                    <br><br>
                    <div class="table-responsive">
                    <!--<input id="myInput" type="text" placeholder="Search..">-->
                        <table class="table table-border custom-table m-b-0" id="myTable">
                            <thead>
                                <tr>
                                    <th class="text-center">Rec-Number</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">General item</th>
                                    <th class="text-center">Items count</th>
                                    <th class="text-center">Cost</th>
                                    <th class="text-center">Action</th>
                                        

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                
                                @foreach($recipes as $key =>$value)

                               <tr>
                                   <td class="text-center">{{$value->recipe_id}}</td>
                                   <td class="text-center">{{$value->name}}</td>
                                   <td class="text-center">{{$value->item_name}}</td>
                                   <td class="text-center">{{$value->ingredient_count}}</td>
                                   <td class="text-center">{{$value->cost}}</td>
                                  <td>
                                      @if(\Auth::user()->delete_recipe =="on" || \Auth::user()->user_type =="admin")
                                           <a href="/admin/recipe-delete/{{$value->recipe_id}}" class="btn btn-danger " onclick="return confirm('Are you sure you want to delete this item?');"><i
                                                class="fa fa-trash-o"></i></a>
                                            
                                        @endif
                                         @if(\Auth::user()->edit_recipe =="on" || \Auth::user()->user_type =="admin")
                                            <a href="/admin/recipe-edit/{{$value->recipe_id}}" title="Edit"
                                            class="btn btn-warning custom"><i class="fa fa-pencil"></i></a>
                                            
                                            <!-- <a href="/admin/recipe-edit/{{$value->recipe_id}}" title="Edit"-->
                                            <!--class="btn btn-success custom"><i class="fa fa-plus"></i></a>-->
                                        @endif
                                    </td>
                               </tr>

                                   
                                @endforeach
                              
                            </tbody>


                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>



//This code for the general item dropdown to name field
$(document).ready(function(){
    
    $("select.geeral_items").change(function(){
        var selectedItem = $(this).children("option:selected").text();
        $('#name').val(selectedItem); 

    });
     
});

   
 
  $("#ingredient1").change(function(){ 
      //alert($(this).val());
    var ingredients = {!!json_encode($ingredients);!!};
    var unit = {!!json_encode($unit);!!}
        //  console.log(ingredients);
    // $("#quantity_type").val('Unit');
    for(var i=0; i<ingredients.length; i++){
        
        if(ingredients[i].name == $(this).val()){
            var type = ingredients[i].quantity_type;
            $('#quantity_type  option[value='+type+']').prop("selected", true);
            $('#quantity_type').prop('disabled', true);
            
        }
    }

    
  });

    $("#add_ing").click(function() {
        
        
         var ingredients = {!!json_encode($ingredients);!!};
         var unit = {!!json_encode($unit);!!}
         var count = parseInt($('#count').val());
         count = count + 1;
         var data=' <div class="row" id="row'+count+'"><div class="col-md-4">' +

            '<label>Ingredients </label>' +
            '<select class="selectpicker form-control" style="height: auto;"  id="ingredient'+count+'" required name="ingredient[]" onchange="selected(this)" data-live-search="true"><option >select</option>'  ;
           for(var i=0; i< ingredients.length; i++){
               data += '<option data-price='+ingredients[i].price+' masachusa='+count+' data-code='+ingredients[i].id+' value='+ingredients[i].name+'>'+ingredients[i].name+'</option>';
           }
          
           data += ' </select>' +

            ' </div>' +
            '<div class="col-md-3">' +

            ' <label>Quantity Type </label>' +
            '  <select class="select form-control"  id="quantity_type'+count+'" required name="quantity_type[]" style="height:37px !important"><option >select</option>';
            for(var i=0; i<unit.length; i++){
                data +='<option value='+unit[i].name+'>'+unit[i].name+' </option>' ;
            }
            
            data +=' </select>' +

            ' </div>' +

            '<div class="col-md-3">' +

            '<label>Quantity</label>' +
            ' <input class="form-control" type="text" id="quantity"'+count+' name="quantity[]" required autofocus>' +
            ' </div><div class="col-md-2" style="margin: 30px 0px -5px ;"><a class="btn btn-danger "  style="float:right;" onclick=" remove('+count+')" id="del1">Remove</a></div>'+
            '</div>';
            $("#default").append(data);
            $(".selectpicker").selectpicker('refresh');
            $('#count').val(count);
    });
$(document).ready(function() {
    $('#myTable').DataTable();
} );
$(document).ready(function() {
    
   
});




function remove(id){
    $("#row"+id).remove();
     var count = parseInt($('#count').val());
         count = count - 1;
         $('#count').val(count);
}
function selected(ingrident){
   
    var price =$(':selected', ingrident).attr('data-price');
    var code =$(':selected', ingrident).attr('data-code');
    var m =$(':selected', ingrident).attr('masachusa');
    if (m == undefined){
        m=1;
    }
    
    var ingredients = {!!json_encode($ingredients);!!};
    var unit = {!!json_encode($unit);!!}
       
    for(var i=0; i<ingredients.length; i++){
        
        if(ingredients[i].id == code){
            
            var type = ingredients[i].quantity_type;
           
            var id = '#quantity_type'+m ;
        
            $('#quantity_type'+m+' '+ 'option[value='+type+']').prop("selected", true);
            $('#quantity_type'+m).prop('disabled', true);
            
        }
    }
     
    var string = ' <input  type="hidden" value='+price+'  name="price[]"  ><input  type="hidden"  name="code[]" value='+code+' >' ;
    
    $("#default").append(string);
}

$('#addmajorgroup').submit(function() {
    var con=parseInt($('#count').val());
    
    for(var i=1; i<=con; i++){
                $('#quantity_type'+i).removeAttr('disabled');
            }
    
    
    
    
});

</script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
@stop