@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 ">
                <h4 class="page-title">UOM Management </h4>
            </div>
        </div>
        <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
        <div class="row">
             <div class="col-md-12 ">
            <div class="card-box">
           
                <form action="/admin/add-unit" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Name</label>
                        <input class="form-control" type="text" id="name" name="name" value="" required autofocus>
                      
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Add</button>
                    </div>
            </div>





            </form>
        </div>
        </div>
        <div class="row">
            <div class="col-md-8 ">
                <h4 class="page-title">All Units </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                    <!--<a href="/dailysalesDownload.php" target="_blank" title="Export In Excel" class="btn btn-warning"-->
                    <!--    value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>-->
                    <!--<a href="/pdf/examples/product_wise.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->
                    <br><br>
                    <div class="table-responsive">
                    <!--<input id="myInput" type="text" placeholder="Search..">-->
                        <table class="table table-border custom-table m-b-0" id="myTable">
                            <thead>
                                <tr>
                                    <th class="text-center">id</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Action</th>
                                        

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                
                                @foreach($unit as $key =>$value)

                               <tr>
                                
                                   <td class="text-center">{{$value->id}}</td>
                                   <td class="text-center">{{$value->name}}</td>
                                   
                                  <td class="text-center">
                                      @if(\Auth::user()->delete_ingredients =="on" || \Auth::user()->user_type =="admin")
                                       <a href="/admin/unit-delete/{{$value->id}}" class="btn btn-danger "><i
                                            class="fa fa-trash-o"></i></a>
                                    @endif   
                                     @if(\Auth::user()->edit_ingredients =="on" || \Auth::user()->user_type =="admin")
                                        <a href="/admin/unit-edit/{{$value->id}}" title="Edit"
                                        class="btn btn-warning custom"><i class="fa fa-pencil"></i></a>
                                        
                                        <!-- <a href="/admin/unit-edit/{{$value->id}}" title="Edit"-->
                                        <!--class="btn btn-success custom"><i class="fa fa-plus"></i></a>-->
                                        @endif   
                                    </td>
                               </tr>

                                   
                                @endforeach
                              
                            </tbody>


                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>
$(document).ready(function() {
     $('#myTable').DataTable();
});


</script>
@stop