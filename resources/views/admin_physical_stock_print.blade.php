@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />



<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Enter Physical Stock Dispense Station </h4>
            </div>
        </div>
        
         <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
        
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/save-physical-stock-print-store" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

         
                                <div class="form-group col-md-12 {{ $errors->has('quantity_type') ? ' has-error' : '' }}" required autofocus>
                                    <label>Print Classes</label>
                                    <select class="selectpicker form-control quantity_type1" style="height:auto !important;" id="quantity_type2" required name="print_class_name" data-live-search="true">
                                        <option >select</option>
                                         @foreach($classes as $key =>$value)
                                        <option  >{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            
                              <input class="form-control" type="hidden" id="print_class_id" name="print_class_id"   >
                                <div class="form-group col-md-12 {{ $errors->has('quantity_type') ? ' has-error' : '' }}" required >
                                    <label>Ingredient</label>
                                    <select class="selectpicker form-control quantity_type1" style="height:auto !important;" id="quantity_type1" required name="ingredient" data-live-search="true">
                                        <option >select</option>
                                         @foreach($ingredients as $key =>$value)
                                        <option value="{{$value->id}}"  >{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                 <input class="form-control" type="hidden" id="idd" name="id"   >
                                
                             
                                
                                 
                                
                                 
                                <div class="col-md-12 ">
                                    <label>System Quantity</label>
                                    <input class="form-control" type="text" id="quan" name="quan" readonly  style="padding-right:5px; padding-left:5px; "  >
                                </div>
                                
                                
                                
                                
                                <div class="col-md-12 ">
                                    <label>Physical Quantity Counted</label>
                                    <input class="form-control" type="text" id="quantity" name="quantity" style="padding-right:5px; padding-left:5px; "  >
                                </div>

                               
                           <div class="col-md-12 ">
                                    <label>Variance</label>
                                    <input class="form-control" type="text" id="var" name="var" readonly  style="padding-right:5px; padding-left:5px; "  >
                                </div>

                                <div class="col-md-12 ">
                                    <label>Updated System Quantity</label>
                                    <input class="form-control" type="text" id="totalquantity" name="totalquantity" value="" readonly required >
                                    @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                    @endif
                                </div
                                
                                
                                
                                 
                                </div>
                                
                           
                            
                    
                       
                    


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>


$(document).ready(function(){
    var ingredients = {!!json_encode($ingredients);!!};
    //  console.log(ingredients);

  
  
  $("#quantity").change(function(){ 
      //alert(5);
    // var ingredients = {!!json_encode($ingredients);!!};
   
    // var physical = {!!json_encode($physical);!!}
    //  var stock = {!!json_encode($stock);!!}
    // console.log("Phhisal");
    // console.log(physical);
    
     var neww = $("#quantity").val();
    
    var s = $("#quan").val();
    
     
    
    if(s>0){
        
          var t= parseFloat(neww)- parseFloat(s)  ;
         
    
     $("#var").val(t);
     
      $('#totalquantity').val($(this).val());
        
        
    }else{
        
         var t= parseFloat(neww) + parseFloat(s);
    
  
    
    
     $("#var").val(t);
      $('#totalquantity').val($(this).val());
        
    }
    
    
    
    
    // for(var i=0; i<physical.length; i++){
    //     console.log($('#quantity_type1').val());
        
    //     if(physical[i].ingredient_id == $('#quantity_type1').val()){
    //         //alert();
    //         var old = physical[i].quantity;
    //         //if(old!=null){
    //             console.log(old);
    //             var  old = parseInt(old);
    //         old =parseInt(neww) + parseInt(old);
           
                
            // }else
            // {
            //     console.log(444);
            //     var  old = 0;
            // old =parseInt(neww) + parseInt(old);
            // $('#totalquantity').val(old);
                
            // }
             
            
    //         break;
    //     }else{
    //         console.log(555);
    //         var  old = 0;
    //         old =parseInt(neww) + parseInt(old);
    //         $('#totalquantity').val(old);
    //     }
        
        
    // }
    // if(physical.length==0){
    //      var  old = 0;
    //         old =parseInt(neww) + parseInt(old);
    //         $('#totalquantity').val(old);
    // }
  });
  
  
  
   $("#quantity_type1").change(function(){ 
      //alert(5);
    var ingredients = {!!json_encode($ingredients);!!};
   
    var physical = {!!json_encode($physical);!!}
     var stock = {!!json_encode($stock);!!}
    
      
     
     
     for(var j=0; j<stock.length; j++){
                  if(stock[j].ingredient_id == $(this).val() && $('#quantity_type2').val() == stock[j].print_class){
                        //alert('yes');
                        var old = stock[j].quantity_added;
                         $('#quan').val(old);
                         break;
                    }else{
                        $('#quan').val(0);
                    }
                    
             }
             
             
              
    
  
  });
  
  $("#quantity_type2").change(function(){ 
      //alert(5);
    var ingredients = {!!json_encode($ingredients);!!};
   
    var physical = {!!json_encode($physical);!!}
     var stock = {!!json_encode($stock);!!}
       var classes = {!!json_encode($classes);!!}
     
     for(var j=0; j<stock.length; j++){
                  if(stock[j].ingredient_id == $('#quantity_type1').val() && $('#quantity_type2').val() == stock[j].print_class){
                        //alert('yes');
                        var old = stock[j].quantity_added;
                         $('#quan').val(old);
                         break;
                    }else{
                        $('#quan').val(0);
                    }
                    
             }
             
             
             for(var j=0; j<classes.length; j++){
                  if(classes[j].name == $(this).val()){
                        //alert('yes');
                        var m = classes[j].id;
                         $('#print_class_id').val(m);
                         console.log($('#print_class_id').val());
                         break;
                    }else{
                       
                    }
                    
             }
   
   
    
  
  });
  
});




</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

</script>
@stop