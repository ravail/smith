@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Edit Stock </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/stock-edit" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

                    <div class="form-group{{ $errors->has('print_class') ? ' has-error' : '' }}">
                        <label>Print Class</label>
                        <select class="select" id="print_class" required name="print_class">
                                         @foreach($print_class as $key =>$value)
                                         @php $type= $stock->print_class @endphp
                                        <option value={{$value->name}} {{  $type == $value->name ? 'selected': ''}}>{{$value->name}}</option>
                                        @endforeach
                        </select>
                        
                        <input  type="hidden" id="id" name="id" value="{{$stock->id}}"  >
                       
                        
                    </div>
                    
                            
                                <div class="form-group{{ $errors->has('ingredient_id') ? ' has-error' : '' }}" required autofocus>
                                    <label>Ingredient </label>
                                    <input class="form-control" type="text" id="ingredient_id" name="ingredient_id" value="{{$stock->ingredient_id}}" readonly="readonly" required autofocus >
                                    @if ($errors->has('ingredient_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ingredient_id') }}</strong>
                                    </span>
                                    @endif
                                       



                                    
                                </div>
                                
                                 
                           

                                <div class="form-group{{ $errors->has('quantity_added') ? ' has-error' : '' }}">
                                    <label>Current Quantity</label>
                                    <input class="form-control" type="text" id="quantity_added" name="quantity_added" value="{{$stock->quantity_added}}" readonly="readonly" required autofocus >
                                    @if ($errors->has('quantity_added'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity_added') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                
                                <div class="form-group{{ $errors->has('adjustedquantity') ? ' has-error' : '' }}">
                                    <label>Adjusted Quantity</label>
                                    <input class="form-control" type="text" id="adjustedquantity" name="adjustedquantity" value="" required autofocus>
                                    @if ($errors->has('adjustedquantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adjustedquantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                
                                
                                 <div class="form-group{{ $errors->has('totalquantity') ? ' has-error' : '' }}">
                                    <label>Total Quantity</label>
                                    <input class="form-control" type="text" id="totalquantity" name="totalquantity" value=""  readonly="readonly" required autofocus >
                                    
                                    
                                    
                                    
                                    @if ($errors->has('totalquantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('totalquantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            
                                
                                
                              
                           
                            
                   
                       
                    


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>
$(document).ready(function() {
   
});


</script>
<script>
    
$("#adjustedquantity").keyup(function(){
               
    a = parseInt($('#adjustedquantity').val()),
    b = parseInt($('#quantity_added').val());
    $("#totalquantity").val(a+b);
    
});

</script>
@stop