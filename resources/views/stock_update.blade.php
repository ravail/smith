@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />



<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Issue Stock To Station </h4>
            </div>
        </div>
        
         <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
        
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/stockadd" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

                    <!--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">-->
                    <!--    <label>Name</label>-->
                    <!--    <input class="form-control" type="text" id="name" name="name" value="" required autofocus>-->
                    <!--    <input  type="hidden" id="id" name="id" value=""  >-->
                    <!--    @if ($errors->has('name'))-->
                    <!--    <span class="help-block">-->
                    <!--        <strong>{{ $errors->first('name') }}</strong>-->
                    <!--    </span>-->
                    <!--    @endif-->
                    <!--</div>-->
                    
                            <!-- <div class="col-md-12 form-group">-->
                            <!--<label>Ingredients </label>-->
                            <!--<select class="select ingredient1 form-control" id="ingredient" required name="ingredient[]" onchange="select(this)" data-live-search="true">-->
                            <!--    <option >select</option>-->
                            <!--    @foreach($ingredients as $key =>$value)-->
                            <!--    <option data-price="{{$value->price}}"  data-code="{{$value->id}}" onchange="selected(this)" value="{{$value->name}}"    >{{$value->name}}</option>-->
                            <!--    @endforeach-->
                            <!--</select>-->
                            <!--</div>-->
                    
                    
                            
                                <div class="form-group col-md-12 {{ $errors->has('quantity_type') ? ' has-error' : '' }}" required autofocus>
                                    <label>Ingredient</label>
                                    <select class="selectpicker form-control quantity_type1" style="height:auto !important;" id="quantity_type1" required name="ingredient" data-live-search="true">
                                        <option >select</option>
                                         @foreach($ingredients as $key =>$value)
                                        <option value="{{$value->id}}"  >{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                 <input class="form-control" type="hidden" id="idd" name="id"   >
                                
                                <div class="col-md-12 ">
                                    <label>Quantity Type</label>
                                    <input class="form-control" type="text" id="name" name="quantity_type" readonly style="padding-right:5px; padding-left:5px; "  >
                                </div>
                                
                                 <div class="form-group col-md-12 {{ $errors->has('print_classes') ? ' has-error' : '' }}" required autofocus>
                                    <label>Print Class</label>
                                    <select class="selectpicker form-control" style="height:auto !important;" id="print_class" required name="print_class" data-live-search="true">
                                        
                                         @foreach($print_classes as $key =>$value)
                                        <option  >{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                 
                                 <div class="col-md-12 ">
                                    <label>Current Quantity in Station</label>
                                    <input class="form-control" type="number" id="quantity_station" name="quantity_station" readonly style="padding-right:5px; padding-left:5px; "  >
                                </div>
                                
                                <div class="col-md-12 ">
                                    <label>Current Quantity in C.Store</label>
                                    <input class="form-control" type="number" id="quantity_store" name="quantity_store" readonly style="padding-right:5px; padding-left:5px; "  >
                                </div>
                                
                                
                                
                                
                                
                                <div class="col-md-12 ">
                                    <label>Quantity Added</label>
                                    <input class="form-control" type="text" id="quantityadded" name="quantity" style="padding-right:5px; padding-left:5px; "  >
                                </div>

                               
                           

                                <div class="col-md-12 ">
                                    <label>Total Quantity</label>
                                    <input class="form-control" type="number" id="totalquantity" name="totalquantity" value="" readonly required >
                                    @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                    @endif
                                </div
                                
                                 
                                </div>
                                
                           
                            
                    
                       
                    


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
    </div>
     <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                    <!--<a href="/dailysalesDownload.php" target="_blank" title="Export In Excel" class="btn btn-warning"-->
                    <!--    value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>-->
                    <!--<a href="/pdf/examples/product_wise.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->
                    <br><br>
                    <div class="table-responsive">
                    <!--<input id="myInput" type="text" placeholder="Search..">-->
                        <table class="table table-border custom-table m-b-0" id="myTable">
                            <thead>
                                <tr>
                                   
                                    <th class="text-center">Print Class</th>
                                    <th class="text-center">Quantity Added</th>
                                    <th class="text-center">Ingredient ID</th>
                                    <th class="text-center">Ingredient Name</th>
                                  
                                    <!--<th class="text-center">Action</th>-->
                                        

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                
                                @foreach($stock as $key =>$value)

                               <tr>
                                
                                   <td class="text-center">{{$value->print_class}}</td>
                                   <td class="text-center">{{$value->quantity_added}}</td>
                                   <td class="text-center">{{$value->ingredient_id}}</td>
                                   <td class="text-center">{{$value->ingredient_name}}</td>
                                  
                                  <!--<td class="text-center">-->
                                  <!--    @if(\Auth::user()->delete_ingredients =="on" || \Auth::user()->user_type =="admin")-->
                                  <!--     <a href="/admin/stock-delete/{{$value->id}}" class="btn btn-danger "><i-->
                                  <!--          class="fa fa-trash-o"></i></a>-->
                                  <!--  @endif   -->
                                  <!--   @if(\Auth::user()->edit_ingredients =="on" || \Auth::user()->user_type =="admin")-->
                                  <!--      <a href="/admin/stock-edit/{{$value->id}}" title="Edit"-->
                                  <!--      class="btn btn-warning custom"><i class="fa fa-pencil"></i></a>-->
                                        
                                  <!--      <a href="/admin/stock-edit/{{$value->id}}" title="Edit"-->
                                  <!--      class="btn btn-success custom"><i class="fa fa-plus"></i></a>-->
                                        
                                  <!--       <a href="/admin/stock-adjustment/{{$value->id}}" title="Adjustment"-->
                                  <!--      class="btn btn-info custom"><i class="fa fa-minus"></i></a>-->
                                  <!--      @endif   -->
                                  <!--  </td>-->
                               </tr>

                                   
                                @endforeach
                              
                            </tbody>


                        </table>


                    </div>
                </div>
            </div>
        </div> </div>
</div>


@stop

@section('javascript')
<script>


$(document).ready(function(){
    var ingredients = {!!json_encode($ingredients);!!};
    
    //  console.log(ingredients);
$("#quantity_type1").change(function(){ 
      //alert($(this).val());
    var ingredients = {!!json_encode($ingredients);!!};
    var unit = {!!json_encode($unit);!!}
     var stock = {!!json_encode($stock);!!}
        // console.log(ingredients);
    // $("#quantity_type").val('Unit');
    for(var i=0; i<ingredients.length; i++){
        
        if(ingredients[i].id == $(this).val()){
            console.log(ingredients[i])
            var q_store=ingredients[i].quantity;
            var  q_store = parseFloat(q_store);
            if(q_store < 0){
                q_store =0;
            }
             $('#quantity_store').val(q_store);
             for(var j=0; j<stock.length; j++){
                  if(stock[j].ingredient_id == $(this).val() && $('#print_class').val() == stock[j].print_class){
                        //alert('yes');
                        var old = stock[j].quantity_added;
                         $('#quantity_station').val(old);
                         break;
                    }else{
                        $('#quantity_station').val(0);
                    }
                    
             }
            //alert();
            var type = ingredients[i].quantity_type;
            $('#name').val(type);
            var id=ingredients[i].id;
            $('#idd').val(id);
            
            
        }
        
    }
  });
  
  $("#print_class").change(function(){ 
      //alert($(this).val());
    var ingredients = {!!json_encode($ingredients);!!};
    var unit = {!!json_encode($unit);!!}
     var stock = {!!json_encode($stock);!!}
        // console.log(ingredients);
    // $("#quantity_type").val('Unit');
    for(var i=0; i<ingredients.length; i++){
        
        
             for(var j=0; j<stock.length; j++){
                  if(stock[j].ingredient_id ==  $('#idd').val() && $('#print_class').val() == stock[j].print_class){
                        //alert('yes');
                        var old = stock[j].quantity_added;
                         $('#quantity_station').val(old);
                         break;
                    }else{
                        $('#quantity_station').val(0);
                    }
                    
             }
            //alert();
            var type = ingredients[i].quantity_type;
           
        
    }
  });
  
  
  
  $("#quantityadded").change(function(){ 
      //alert($(this).val());
    var ingredients = {!!json_encode($ingredients);!!};
    var unit = {!!json_encode($unit);!!}
    var stock = {!!json_encode($stock);!!}
        
    var neww = $("#quantityadded").val();
    
    for(var i=0; i<stock.length; i++){
        console.log($('#quantity_type1').val());
        
        if(stock[i].ingredient_id == $('#quantity_type1').val() && $('#print_class').val() == stock[i].print_class){
            //alert();
            var old = stock[i].quantity_added;
           
            $('#quantity_station').val(old);
            var  old = parseFloat(old);
            old =parseFloat(neww) + parseFloat(old);
            console.log(old)
            $('#totalquantity').val(old);
             
            
            break;
        }else{
            var  old = 0;
            old =parseFloat(neww) + parseFloat(old);
            $('#totalquantity').val(old);
        }
        
        
    }
  });
});




</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

</script>
@stop