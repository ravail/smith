@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 ">
                <h4 class="page-title">Smith App Version </h4>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12 ">
            <div class="card-box">
           
                <form action="/admin/appversion" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                  

                    <div class="form-group }}">
                        <label>Name</label>
                        <input class="form-control" type="text" id="name" name="name" value="Smith Hotels" Disabled  autofocus>
                      
                        
                    </div>
                    
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Version</label>
                        <input class="form-control" type="text" id="version" name="version" value="{{$version}}" required autofocus>
                      
                        @if ($errors->has('version'))
                        <span class="help-block">
                            <strong>{{ $errors->first('version') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
        </div></div>
        </div>