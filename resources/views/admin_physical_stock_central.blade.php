@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />



<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Physical Stock Central Store </h4>
            </div>
        </div>
        
         <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
        
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/save-physical-stock-central-store" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

         
                    
                            
                                <div class="form-group col-md-12 {{ $errors->has('quantity_type') ? ' has-error' : '' }}" required autofocus>
                                    <label>Ingredient</label>
                                    <select class="selectpicker form-control quantity_type1" style="height:auto !important;" id="quantity_type1" required name="ingredient" data-live-search="true">
                                        <option >select</option>
                                         @foreach($ingredients as $key =>$value)
                                        <option value="{{$value->id}}"  >{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                 <input class="form-control" type="hidden" id="idd" name="id"   >
                                
                             
                                
                                 
                                   
                                <div class="col-md-12 ">
                                    <label>System Quantity</label>
                                    <input class="form-control" type="text" id="example" name="example" readonly  style="padding-right:5px; padding-left:5px; "  >
                                </div>
                                
                               
                                 
                                
                                
                                
                                
                                
                                <div class="col-md-12 ">
                                    <label>Physical Quantity</label>
                                    <input class="form-control" type="text" id="quantity" name="quantity" style="padding-right:5px; padding-left:5px; "  >
                                </div>

                               
                           <div class="col-md-12 ">
                                    <label>Variance</label>
                                    <input class="form-control" type="text" id="var" name="var" readonly  style="padding-right:5px; padding-left:5px; "  >
                                </div>

                                <div class="col-md-12 ">
                                    <label>Updated System Quantity</label>
                                    <input class="form-control" type="text" id="totalquantity" name="totalquantity" value="" readonly required >
                                    @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                    @endif
                                </div
                                
                                
                                
                                 
                                </div>
                                
                           
                                 
                
                       
                    


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>


$(document).ready(function(){
    var ingredients = {!!json_encode($ingredients);!!};
     console.log(ingredients);

  
  
  $("#quantity").change(function(){ 
      //alert(5);
    var ingredients = {!!json_encode($ingredients);!!};
   
    var physical = {!!json_encode($physical);!!}
        
    var neww = $("#quantity").val();
    
    // for(var i=0; i<physical.length; i++){
    //     console.log($('#quantity_type1').val());
        
    //     if(physical[i].ingredient_id == $('#quantity_type1').val()){
    //         //alert();
    //         var old = physical[i].quantity;
    //         //if(old!=null){
    //             console.log(555);
    //             var  old = parseInt(old);
    //         old =parseInt(neww) + parseInt(old);
    //         $('#totalquantity').val(old);
                
    //         // }else
    //         // {
    //         //     console.log(444);
    //         //     var  old = 0;
    //         // old =parseInt(neww) + parseInt(old);
    //         // $('#totalquantity').val(old);
                
    //         // }
             
            
    //         break;
    //     }else{
    //         console.log(555);
    //         var  old = 0;
    //         old =parseInt(neww) + parseInt(old);
            $('#totalquantity').val(neww);
    //     }
        
        
    // }
    // if(physical.length==0){
    //      var  old = 0;
    //         old =parseInt(neww) + parseInt(old);
    //         $('#totalquantity').val(old);
    
        
    // }
    
    hello = $('#example').val()-neww;
    
    
        
        var z= hello * -1;
        
    
    
    $('#var').val(z);
    
  });
  
  
  
  
  
    $("#quantity_type1").change(function(){ 
      //alert($(this).val());
    
    var ing = {!!json_encode($ingredients);!!}
     var ps = {!!json_encode($physical);!!}
     
     var save;
        
   
    
    for(var i=0; i<ing.length; i++){
        
        
        if(ing[i].id == $('#quantity_type1').val() ){
            //alert();
             save = ing[i].quantity;
           
            $('#example').val(save);
           
             
            
            break;
        }
        
        
    }
    
   
  });
  
  
  
  
  
});




</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

</script>
@stop