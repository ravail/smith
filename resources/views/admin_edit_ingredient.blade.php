@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")
 <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Edit Ingredient </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/ingredient-edit" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Name</label>
                        <input class="form-control" type="text" id="name" name="name" value="{{$ingredient->name}}" readonly autofocus>
                        <input  type="hidden" id="id" name="id" value="{{$ingredient->id}}"  >
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                            
                                <div class="form-group{{ $errors->has('quantity_type') ? ' has-error' : '' }}" required autofocus>
                                    <label>Quantity Type </label>
                                    <select class="selectpicker form-control" id="quantity_type1" disabled="true" name="quantity_type" data-live-search="true" >
                                        <option >select</option>
                                         @foreach($unit as $key =>$value)
                                         @php $type= $ingredient->quantity_type @endphp
                                        <option value={{$value->name}} {{  $type == $value->name ? 'selected': ''}}>{{$value->name}}</option>
                                        
                                        @endforeach
                                       



                                    </select>
                                </div>
                                <input type="hidden" value="{{$ingredient->quantity_type}}" name="quantity_type">
                                 
                           

                                <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                                    <label>Current Quantity</label>
                                    <input class="form-control" type="text" id="quantity" name="quantity" value="{{$ingredient->quantity}}" readonly="readonly" required autofocus >
                                    @if ($errors->has('quantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('adjustedquantity') ? ' has-error' : '' }}">
                                    <label>Adjusted Quantity</label>
                                    <input class="form-control" type="text" id="adjustedquantity" name="adjustedquantity" value="" required autofocus  >
                                    @if ($errors->has('adjustedquantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adjustedquantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                
                                
                                
                                <div class="form-group{{ $errors->has('totalquantity') ? ' has-error' : '' }}">
                                    <label>Total Quantity</label>
                                    <input class="form-control" type="text" id="totalquantity" name="totalquantity" value=""  readonly="readonly" required autofocus >
                                    
                                    
                                    
                                    
                                    @if ($errors->has('totalquantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('totalquantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                           
                            
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                        <label>Price</label>
                        <input class="form-control" type="text" id="price" name="price" value="{{$ingredient->price}}" required autofocus>
                        @if ($errors->has('price'))
                        <span class="help-block">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                        @endif
                    </div>
                     <div class="form-group{{ $errors->has('re_order_level') ? ' has-error' : '' }}">
                        <label>Stock Reorder level</label>
                        <input class="form-control" type="text" id="re_order_level" value="{{$ingredient->re_order_level}}" name="re_order_level" required autofocus readonly>
                        @if ($errors->has('re_order_level'))
                        <span class="help-block">
                            <strong>{{ $errors->first('re_order_level') }}</strong>
                        </span>
                        @endif
                    </div>
                       
                       
                       <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                        <label>Remarks/Document Details</label>
                        <textarea class="form-control" type="text" id="remarks" value="" name="remarks"></textarea>
                        @if ($errors->has('remarks'))
                        <span class="help-block">
                            <strong>{{ $errors->first('remarks') }}</strong>
                        </span>
                        @endif
                    </div>
                    


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>
$(document).ready(function() {
   
});


</script>
<script>
    
$("#adjustedquantity").keyup(function(){
               
    a = parseFloat($('#adjustedquantity').val()),
    b = parseFloat($('#quantity').val());
    $("#totalquantity").val(a+b);
    
});

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

@stop