@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Edit Physical Stock Dispense Station </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/save-edited-physical-stock-print-store" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                    {{ csrf_field() }}
                    

                    
                    
                            
                                <div class="form-group{{ $errors->has('ingredient_id') ? ' has-error' : '' }}" required autofocus>
                                    <label>Ingredient ID </label>
                                    <input class="form-control" type="text" id="ingredient_id" name="ingredient_id" value="{{$physical->ingredient_id}}" readonly="readonly" required autofocus >
                                    @if ($errors->has('ingredient_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ingredient_id') }}</strong>
                                    </span>
                                    @endif
                                       



                                    
                                </div>
                                
                                <div class="form-group{{ $errors->has('ingredient_id') ? ' has-error' : '' }}" required autofocus>
                                    <label>Ingredient Name </label>
                                    <input class="form-control" type="text" id="ingredient_name" name="ingredient_name" value="{{$physical->ingredient_name}}" readonly="readonly" required autofocus >
                                    @if ($errors->has('ingredient_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ingredient_name') }}</strong>
                                    </span>
                                    @endif
                                       



                                    
                                </div>
                                
                                
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" required >
                                    <label>Print Class </label>
                                    <input class="form-control" type="text" id="ingredient_id" name="print_class_name" value="{{$physical->name}}" readonly="readonly" required  >
                                    @if ($errors->has('print_class_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('print_class_name') }}</strong>
                                    </span>
                                    @endif
                                       



                                    
                                </div> 
                                
                                <div class="form-group{{ $errors->has('print_class_id') ? ' has-error' : '' }}" style="display:none;" required >
                                    <label>Print Class </label>
                                    <input class="form-control" type="text" id="ingredient_id" name="print_class_id" value="{{$physical->print_class_id}}" readonly="readonly" required  >
                                    @if ($errors->has('print_class_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('print_class_id') }}</strong>
                                    </span>
                                    @endif
                                       



                                    
                                </div>

                                <div class="form-group{{ $errors->has('quantity_added') ? ' has-error' : '' }}">
                                    <label>Current Quantity</label>
                                    <input class="form-control" type="text" id="quantity_added" name="quantity_added" value="{{$physical->quantity}}" readonly="readonly" required autofocus >
                                    @if ($errors->has('quantity_added'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quantity_added') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                
                                <div class="form-group{{ $errors->has('adjustedquantity') ? ' has-error' : '' }}">
                                    <label>Adjusted Quantity</label>
                                    <input class="form-control" type="text" id="adjustedquantity" name="adjustedquantity" value="" required autofocus>
                                    @if ($errors->has('adjustedquantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adjustedquantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                
                                
                                 <div class="form-group{{ $errors->has('totalquantity') ? ' has-error' : '' }}">
                                    <label>Total Quantity</label>
                                    <input class="form-control" type="text" id="totalquantity" name="totalquantity" value=""  readonly="readonly" required autofocus >
                                    
                                    
                                    
                                    
                                    @if ($errors->has('totalquantity'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('totalquantity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            
                                
                                
                              
                           
                            
                   
                       
                    


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
            </div>





            </form>
        </div>
    </div>
</div>


@stop

@section('javascript')
<script>
$(document).ready(function() {
   
});


</script>
<script>
    
$("#adjustedquantity").keyup(function(){
               
    a = parseFloat($('#adjustedquantity').val()),
    b = parseFloat($('#quantity_added').val());
    $("#totalquantity").val(a+b);
    
});

</script>
@stop