@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        
        
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Edit Physical Stock Dispense Station</h4>
            </div>
            
        </div>
        <div class="row">
                        <div class="col-md-12">
                    
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                             @endif
                        
                        </div>
         
         </div>
         
          <form action="/admin/filter/physical_stock_ps" method="POST">
                            @csrf
                <div class="form-group row">
                        <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker form-control" placeholder="Start Date" value="{{$start_date}}" required="" readonly="" id="start_date" name="start_date" type="text">
                                                </div>
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker1 form-control" placeholder="End Date" value="{{$end_date}}" id="end_date" required="" readonly="" name="end_date" type="text">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                          
                                                </div><br>
                                           
                                         </form>
         
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                    <!--<a href="/dailysalesDownload.php" target="_blank" title="Export In Excel" class="btn btn-warning"-->
                    <!--    value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>-->
                    <!--<a href="/pdf/examples/product_wise.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->
                    <br><br>
                    <div class="table-responsive">
                    <!--<input id="myInput" type="text" placeholder="Search..">-->
                        <table class="table table-border custom-table m-b-0" id="myTable">
                            <thead>
                                <tr>
                                   
                                   
                                    
                                    <th class="text-center">Ingredient ID</th>
                                    <th class="text-center">Ingredient Name</th>
                                    <th class="text-center">Print Class Name</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Variance</th>
                                    <th class="text-center">Last Physical Quantity</th>
                                  
                                    <th class="text-center">Action</th>
                                        

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                
                                @foreach($physical as $key =>$value)

                               <tr>
                                
                                   <td class="text-center">{{$value->ingredient_id}}</td>
                                   
                                   
                                   <td class="text-center">{{$value->ingredient_name}}</td>
                                   <td class="text-center">{{$value->name}}</td>
                                   <td class="text-center">{{$value->quantity}}</td>
                                    <td class="text-center">{{$value->variance}}</td>
                                     <td class="text-center">{{$value->last_physical_quantity}}</td>
                                  
                                  
                                  <td class="text-center">
                                     
                                       <!--<a href="/admin/stock-delete-ps/{{$value->id}}" class="btn btn-danger "><i-->
                                       <!--     class="fa fa-trash-o"></i></a>-->
                                   
                                        <a href="/admin/edit-physical-stock-ps/{{$value->id}}" title="Edit"
                                        class="btn btn-warning custom"><i class="fa fa-pencil"></i></a>
                                        
                                       
                                         
                                    </td>
                               </tr>

                                   
                                @endforeach
                              
                            </tbody>


                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
@stop

@section('javascript')


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>


<script>
$(document).ready(function() {
    $('#myTable').DataTable();
} );
$(document).ready(function() {
    
    $('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
}); 
    
   
});



///////////////////////////////
</script>

@stop