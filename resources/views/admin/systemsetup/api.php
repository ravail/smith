<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mpesa extends CI_Controller
{

    public $setting = "";

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
        $this->lang->load('message', 'english');
        $this->load->library('auth');
        $this->auth->is_logged_in_parent();
        $this->setting = $this->setting_model->get();
        $this->api_config = $this->paymentsetting_model->getActiveMethod();
    }

    public function index()
    {
        $this->session->set_userdata('top_menu', 'Fees');

        $data = array();
        $data['params'] = $this->session->userdata('params');
        $data['setting'] = $this->setting;

        $this->load->view('parent/mpesa', $data);
    }

    public function checkout()
    {

        $this->form_validation->set_rules('student_fees_master_id', 'Feemaster', 'required|trim|xss_clean');
        $this->form_validation->set_rules('fee_groups_feetype_id', 'Fee Type', 'required|trim|xss_clean');
        $this->form_validation->set_rules('student_id', 'Student', 'required|trim|xss_clean');
        $this->form_validation->set_rules('total', 'Amount', 'numeric|required|greater_than[0]|trim|xss_clean');
        $this->form_validation->set_rules('account_reference', 'Account Reference', 'required|trim|xss_clean');
        $this->form_validation->set_rules('phone_number', 'Phone Number', 'required|trim|xss_clean');

        if ($this->form_validation->run() == false) {
            $data = array(
                'student_fees_master_id' => form_error('student_fees_master_id'),
                'fee_groups_feetype_id' => form_error('fee_groups_feetype_id'),
                'student_id' => form_error('student_id'),
                'mpesa_amount' => form_error('total'),
                'account_reference' => form_error('account_reference'),
                'phone_number' => form_error('phone_number'),                
                
            );
            $array = array('status' => 'fail', 'error' => $data);
            echo json_encode($array);
        } else {

            $array = array('status' => 'success', 'error' => '');
            echo json_encode($array);
        }
    }

    public function complete()
    {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $params = $this->session->userdata('params');

            $data = array();
            $data['student_fees_master_id'] = $this->input->post('student_fees_master_id');
            $data['fee_groups_feetype_id'] = $this->input->post('fee_groups_feetype_id');
            $data['student_id'] = $this->input->post('student_id');
            $data['total'] = $this->input->post('total');
            $data['phone_number'] = $this->input->post('phone_number');
            $data['account_reference'] = $this->input->post('account_reference');
            $data['symbol'] = $params['invoice']->symbol;
            $data['currency_name'] = $params['invoice']->currency_name;
            $data['name'] = $params['name'];
            $data['guardian_phone'] = $params['guardian_phone'];
            $temp_phone_number = $data['phone_number'];
            if( $temp_phone_number {0} == "0" ) {
		$temp_phone_number = "254".substr($temp_phone_number , 1);
		$data['phone_number'] = $temp_phone_number ;
	  }
	  //echo $data['phone_number'];
	  //exit;
            $url = MPESA_OAUTH_URL;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);

            $credentials = base64_encode($this->api_config->api_publishable_key.":".$this->api_config->api_secret_key);

            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials)); //setting a custom header
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $curl_response = curl_exec($curl);
            curl_close($curl);
            $token_json = json_decode($curl_response);
            //echo $curl_response;
            $timestamp =  date("Ymdhis");
            
            $password = base64_encode($this->api_config->api_username.$this->api_config->api_signature.$timestamp);
            if (!empty($token_json) && $token_json->access_token) {
                $url = MPESA_PAYMENT_PROCESS_URL;
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', "Authorization:Bearer $token_json->access_token")); //setting custom header
                $curl_post_data = array(
                    "BusinessShortCode" => $this->api_config->api_username,
                    "Password" => $password,
                    "Timestamp" => "$timestamp",
                    "TransactionType" => "CustomerPayBillOnline",
                    "Amount" => $data['total'],
                    "PartyA" => $data['phone_number'],
                    "PartyB" => $this->api_config->api_username,
                    "PhoneNumber" => $data['phone_number'],
                    "CallBackURL" =>base_url("Roseipn/mpesaCallBack"),//"https://schoolmgt-ken.000webhostapp.com/TempFeeRecord", //,
                    "AccountReference" => $data['account_reference'],
                    "TransactionDesc" => "Online fees deposit through Mpesa",
                );

                $data_string = json_encode($curl_post_data);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                $curl_response = curl_exec($curl);
                $response = json_decode($curl_response);
                if($response->ResponseCode == "0")
                {
                    $json_array = array(
                        'amount' => $params['total'],
                        'date' => date('Y-m-d'),
                        'amount_discount' => 0,
                        'amount_fine' => 0,
                        'description' => "Online fees deposit through MPESA Ref ID: " . $response->MerchantRequestID,
                        'payment_mode' => 'MPESA',
                    );
                    $save_data = array(
                        'student_fees_master_id' => $params['student_fees_master_id'],
                        'fee_groups_feetype_id' => $params['fee_groups_feetype_id'],
                        'status'=>"pending",
                        'amount_detail' => $json_array,
                        'reference_id'=>$response->MerchantRequestID,
                    );
                    $send_to = $params['guardian_phone'];
                    $inserted_id = $this->studentfeemaster_model->temp_fee_deposit($save_data, $send_to);
                    $invoice_detail = json_decode($inserted_id);
                    redirect(base_url("parent/payment/mpesainvoice/" . $invoice_detail->invoice_id . "/" . $invoice_detail->sub_invoice_id));
                    print_r($curl_response);
                }
                else {
                    //echo $curl_response;
                   // exit;
                    redirect(base_url("parent/payment/paymentfailed"));
                }
                
                
            }
            else {
                redirect(base_url("parent/payment/paymentfailed"));
            }
        }
    }
    //on call back url ...
    public function mpesaResponse()
    {
        $data = file_get_contents('php://input');
        if(!empty($data))
        {
            $this->TempFeeRecord_model->add($data);
        }
    }
    //paypal successpayment
    public function getsuccesspayment()
    {
        $params = $this->session->userdata('params');
        $data = array();
        $student_fees_master_id = $params['student_fees_master_id'];
        $fee_groups_feetype_id = $params['fee_groups_feetype_id'];
        $student_id = $params['student_id'];
        $total = $params['total'];

        $data['student_fees_master_id'] = $student_fees_master_id;
        $data['fee_groups_feetype_id'] = $fee_groups_feetype_id;
        $data['student_id'] = $student_id;
        $data['total'] = $total;
        $data['symbol'] = $params['invoice']->symbol;
        $data['currency_name'] = $params['invoice']->currency_name;
        $data['name'] = $params['name'];
        $data['guardian_phone'] = $params['guardian_phone'];
        $response = $this->paypal_payment->success($data);

        $paypalResponse = $response->getData();
        if ($response->isSuccessful()) {
            $purchaseId = $_GET['PayerID'];

            if (isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
                if ($purchaseId) {
                    $params = $this->session->userdata('params');
                    $ref_id = $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'];
                    $json_array = array(
                        'amount' => $params['total'],
                        'date' => date('Y-m-d'),
                        'amount_discount' => 0,
                        'amount_fine' => 0,
                        'description' => "Online fees deposit through Paypal Ref ID: " . $ref_id,
                        'payment_mode' => 'Paypal',
                    );
                    $data = array(
                        'student_fees_master_id' => $params['student_fees_master_id'],
                        'fee_groups_feetype_id' => $params['fee_groups_feetype_id'],
                        'amount_detail' => $json_array,
                    );
                    $send_to = $params['guardian_phone'];
                    $inserted_id = $this->studentfeemaster_model->fee_deposit($data, $send_to);
                    $invoice_detail = json_decode($inserted_id);
                    redirect(base_url("parent/payment/successinvoice/" . $invoice_detail->invoice_id . "/" . $invoice_detail->sub_invoice_id));
                }
            }
        } elseif ($response->isRedirect()) {
            $response->redirect();
        } else {
            redirect(base_url("parent/payment/paymentfailed"));
        }
    }

}
