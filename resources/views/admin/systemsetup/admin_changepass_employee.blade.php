@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Change Pasword</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <form action="/admin/edit-employee-changepass" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                
                                       
                                                <label>New Password</label>
                                                <input class="form-control" type="password" id="password1" name="new_password" value="" required autofocus>
                                                
                                            <input type="hidden" name="id" value="{{$user->id}}">
                                        
                                       
                                                <label>Confirm Password</label>
                                                <input class="form-control" type="password" onchange="Validate()" id="password2" name="confirm_password" value="" required autofocus>
                                                
                                              
                                       

                                           
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg" id="saveeee" >Save password</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

function Validate() {
        var password = document.getElementById("password1").value;
        var confirmPassword = document.getElementById("password2").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            document.getElementById("saveeee").disabled = true;
            return false;
        }else{
            document.getElementById("saveeee").disabled = false;
            return true;
        }
        
    }
	  
	  
</script>

@stop

