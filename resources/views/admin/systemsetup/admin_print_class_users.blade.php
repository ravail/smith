@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Print Class Users</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <a href="/admin/add-print-class-user" class="btn btn-primary btn-rounded pull-right" id="print_class_users_add"><i class="fa fa-plus"></i> Add Print Class User</a>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                        
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </button>
                            {{ session('message') }}
                            </div>
                     @endif
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            
                                            <th>Name</th>
                                            <th>UserName</th>
                                            <th>Restaurant</th>
                                            <th>Print Class</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                        @foreach($printClassUsers as $printClassUser)

                                        <tr>
                                            <td>{{$count}}</td>
                                            
                                            <td>
                                                {{$printClassUser->name}}
                                            </td>
                                            
                                        <td>{{$printClassUser->email}}</td>
                                        
                                        @foreach($restaurants as $restaurant)
                                            <?php if($restaurant->id == $printClassUser->restaurant){?>
                                                    
                                                <td>{{$restaurant->name}}</td>
                                            <?php break; } ?>

                                        @endforeach

                                        @foreach($printClasses as $printClass)
                                            <?php if($printClass->id == $printClassUser->print_class){?>
                                                    
                                                <td>{{$printClass->name}}</td>
                                            <?php break; } ?>

                                        @endforeach
                                                                                     
                                         
                                            <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="" id="print_class_users_edit_{{$printClassUser->id}}"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                    <a class="dropdown-item" href="" id="print_class_users_delete_{{$printClassUser->id}}"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $count++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@stop   

@section('javascript')

<script> 

var groups ={!! json_encode($printClassUsers); !!};
//console.log(groups);
if('{{\Auth::user()->print_class_users_add}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    $("#print_class_users_add").hide();
}

if('{{\Auth::user()->print_class_users_edit}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    //alert(majorGroups.length);
    for(var i = 0 ; i < groups.length ; i++){
        //alert(majorGroups.id);
        $("#print_class_users_edit_"+groups[i].id).hide();
    }
}  

if('{{\Auth::user()->print_class_users_delete}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    for(var i = 0 ; i < groups.length ; i++){

        $("#print_class_users_delete_"+groups[i].id).hide();

    }
} 

</script>

@stop