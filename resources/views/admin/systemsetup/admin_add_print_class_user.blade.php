@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Add Print Class User</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <form action="/admin/add-print-class-user" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label>Name</label>
                                <input class="form-control" type="text" id="name" name="name" placeholder="Name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('restaurant') ? ' has-error' : '' }}" required autofocus>
                                        <label>Restaurant</label>
                                        <select class="select" id="restaurant" name="restaurant">
                                            <option>Select</option>
                                           
                                            
                                        </select>
                                    </div>
                                </div>
                               
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group{{ $errors->has('print_class') ? ' has-error' : '' }}" required autofocus>
                                        <label>Print Class</label>
                                        <select class="select" id="print_class" name="print_class">
                                            <option>Select</option>
                                           
                                            
                                        </select>
                                    </div>
                                </div>
                               
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>UserName</label>
                                <input class="form-control" type="text" id="email" name="email" placeholder="UserName" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input class="form-control" type="password" id="password" name="password" placeholder="Password" required autofocus>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirm') ? ' has-error' : '' }}">
                                <label>Confirm Password</label>
                                <input class="form-control" type="password" id="password_confirm" name="password_confirm" placeholder="Confirm Password" required autofocus>
                                @if ($errors->has('password_confirm'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirm') }}</strong>
                                </span>
                                @endif
                            </div>
                              
                            <div class="m-t-20 text-center">
                                <button class="btn btn-primary btn-lg">Add</button>
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

var restaurants = {!! json_encode($restaurants); !!};
var printClass = {!! json_encode($printClasses); !!};

console.log(printClass);
var options = '<option value="">Restaurants</option>';
restaurants.forEach(element => {

        options += '<option value='+element.id+'>'+element.name+'</option>';
    });

$("#restaurant").html(options);

var options = '<option value="">Print Classes</option>';
printClass.forEach(element => {

        options += '<option value='+element.id+'>'+element.name+'</option>';
    });

$("#print_class").html(options);


    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

