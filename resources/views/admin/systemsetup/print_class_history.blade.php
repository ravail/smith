@extends('layouts.cashier_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">

                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                        <h4 class="page-title">Docket</h4>
                        <audio id="myAudio">
                                <source src="/definite.ogg" type="audio/ogg">
                                <source src="/definite.mp3" type="audio">
                                
                              </audio>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <h4 class="page-title">Dispense Station : @foreach ($printClasses as $printClass)
                                <?php if($printClass->id == $print){ ?>

                                    {{$printClass->name}}
                                <?php } ?>
                               @endforeach
                        </h4>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    
                       <div class="row">
                        <div class="col-md-12">
                                
                                 <div class="card-box">
                    <form action="/public/historyfilter" method="POST">
                            @csrf
                            
                                <div class="form-group row">

                                        <div class="col-sm-3 input-group input-append" >
                                                <input class="datepicker form-control" value="{{$start_date}}" placeholder="Start Date" required="" readonly="" id="start_date" name="start-date" type="text">
                                            </div>
                                            <div class="col-sm-3 input-group input-append" >

                                                    <input class="datepicker1 form-control" value="{{$end_date}}" placeholder="End Date" id="end_date" required="" readonly="" name="end-date" type="text">
                                                </div>
                                                <input name="id" value="{{$id}}" hidden>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                  
                                        </div>
                                     </form>
                                            <br><br>
                                            
                                    </div> </div>
                                    </div>
                    <div class="row" id="masachusa">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0" id="myTable" >
                                    <thead>
                                        <tr>

                                            <th>Order No</th>
                                            <th>Date And Time</th>
                                            <th>Table No</th>
                                            <th>No Of Guest</th>
                                            <th>Item description</th>
                                            <th>Condiments</th>
                                            <th>Waiter</th>
                                            {{-- <th>Action</th>
                                            <th>Invoice</th>
                                            <th>Docket</th> --}}
                                                
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                            @foreach ($orders as $order)

                                            <?php   $order_id = 0;
                                                    $items_description = ""; 
                                                    $items = array();
                                                    $count = 0;
                                                    $condiments = "";
                                            ?>
                                                @foreach($order_items as $order_item)
    
                                                    <?php if($order_item->post_paid_orders_id == $order->id){ 
                                                        
                                                        //alert(result.order_items[j]['name']);
                                                        $items_description .= "item: " . $order_item->name  . " " . "QTY: " . $order_item->quantity ." " ;
                                                        //alert(items_description);
                                                        $items[$count] = $order_item; 
                                                       
                                                    }
                                                    ?>
                                                    
                                                @endforeach
                                                <?php if(!empty($items)){ ?>
                                                    <?php if($order->condiments == null){ $condiments = "---"; }  ?>
                                                    <tr>
                                                        <td>{{$order->id}}</td>
                                                            
                                                            <td> {{$order->date_time}}</td>
                                                            
                                                            <td>
                                                                {{$order->table_no}}
                                                            </td>
                                                            
                                                            <td>{{$order->no_of_guests}}</td>
                                                            <td>{{$items_description}}</td>
                                                            <td>{{$condiments}} </td>
                                                            <td>{{$order->waiter}}</td> 
    
                                                            <?php } ?>        
                                                
                                                        </tr>
                                            @endforeach
                                        
                                    </tbody>
                                </table>
                               
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>

@stop 

@section('javascript')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://www.olarroerp.digitalsystemsafrica.com/assets/jss/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://www.olarroerp.digitalsystemsafrica.com/assets/csss/bootstrap-datetimepicker.css">

<script>
$(document).ready(function() {
    $('#myTable').DataTable();
} );
$(document).ready(function() {
$('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
});

</script>
@stop