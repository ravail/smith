@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Roles Permissions</h4>
            </div>
            {{-- <div class="col-sm-8 col-8 text-right m-b-20">
                            <a href="/admin/add-roles" class="btn btn-primary btn-rounded pull-right"><i class="fa fa-plus"></i> Add Roles</a>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> 
                        </div> --}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="/admin/edit-permissions/{{$id}}" method="POST" enctype="multipart/form-data"
                    id="addmajorgroup">
                    {{ csrf_field() }}
                    <div class="table-responsive">
                        <table class="table table-border custom-table m-b-0">
                            <thead>
                                <tr>
                                    <th>Module</th>

                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>DashBoard</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="dashboard_view" id="dashboard_view"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Menu Setup</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="menu_setup_view" id="ms"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Major-group-managers</td>

                                    <td>
                                        <span>
                                            <label>
                                                <input type="checkbox" name="major_groups_manager_view"
                                                    id="m_g_m_view1"> View
                                            </label>
                                        </span>

                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" class="m_g_m_view1" name="major_groups_manager_edit"
                                                    id="major_groups_manager_edit1"> Edit
                                            </label>
                                        </span>
                                    </td>

                                </tr>
         
                                <tr>
                                    <td>Sub-major-groups</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="sub_major_groups_manager_view"
                                                id="sub_major_groups_manager_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="sub_major_groups_manager_add"
                                                    id="sub_major_groups_manager_add" class="sub_major_groups_manager_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="sub_major_groups_manager_edit"
                                                    id="sub_major_groups_manager_edit" class="sub_major_groups_manager_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="sub_major_groups_manager_delete"
                                                    id="sub_major_groups_manager_delete" class="sub_major_groups_manager_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Menu-item-groups</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="menu_item_groups_view"
                                                id="menu_item_groups_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="menu_item_groups_add"
                                                    id="menu_item_groups_add1" class="menu_item_groups_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="menu_item_groups_edit"
                                                    id="menu_item_groups_edit1" class="menu_item_groups_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="menu_item_groups_delete"
                                                    id="menu_item_groups_delete1" class="menu_item_groups_view1"> Delete
                                            </label>
                                        </span>
                                    </td>

                                </tr>
                                <tr>
                                    <td>Offers</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="offers_view" id="offers_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="offers_add" id="offers_add1" class="offers_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="offers_edit" id="offers_edit1" class="offers_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="offers_delete" id="offers_delete1" class="offers_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Family-groups</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="family_groups_view" id="family_groups_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="family_groups_add" id="family_groups_add1" class="family_groups_view1">
                                                Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="family_groups_edit"
                                                    id="family_groups_edit1" class="family_groups_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="family_groups_delete"
                                                    id="family_groups_delete1" class="family_groups_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Alcoholic-family-groups</td>
                                    <td>
                                        <label>
                                            <input type="checkbox" name="alcoholic_family_groups_view"
                                                id="alcoholic_family_groups_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="alcoholic_family_groups_add"
                                                    id="alcoholic_family_groups_add1" class="alcoholic_family_groups_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="alcoholic_family_groups_edit"
                                                    id="alcoholic_family_groups_edit1" class="alcoholic_family_groups_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="alcoholic_family_groups_delete"
                                                    id="alcoholic_family_groups_delete1" class="alcoholic_family_groups_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Alcoholic-sub-family-groups</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="alcoholic_sub_family_groups_view"
                                                id="alcoholic_sub_family_groups_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="alcoholic_sub_family_groups_add"
                                                    id="alcoholic_sub_family_groups_add1" class="alcoholic_sub_family_groups_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="alcoholic_sub_family_groups_edit"
                                                    id="alcoholic_sub_family_groups_edit1" class="alcoholic_sub_family_groups_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="alcoholic_sub_family_groups_delete"
                                                    id="alcoholic_sub_family_groups_delete1" class="alcoholic_sub_family_groups_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Menu-Item</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="menu_Item_view" id="menu_Item_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>General-item</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="general_item_view" id="general_item_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="general_item_add" id="general_item_add1" class="general_item_view1">
                                                Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="general_item_edit" id="general_item_edit1" class="general_item_view1">
                                                Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="general_item_delete"
                                                    id="general_item_delete1" class="general_item_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Offer-themes-nights</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="offer_themes_nights_view"
                                                id="offer_themes_nights_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="offer_themes_nights_add"
                                                    id="offer_themes_nights_add1" class="offer_themes_nights_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="offer_themes_nights_edit"
                                                    id="offer_themes_nights_edit1" class="offer_themes_nights_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="offer_themes_nights_delete"
                                                    id="offer_themes_nights_delete1" class="offer_themes_nights_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>App-users</td>

                                    <td>
                                        <span>
                                            <label>
                                                <input type="checkbox" name="app_users_view" id="app_users_view1"> View
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="app_users_addamounttowallet"
                                                    id="app_users_addamounttowallet1" class="app_users_view1"> AddAmountToWallet
                                            </label>
                                        </span>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Manage-orders</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="manage_orders_view" id="manage_orders_view1">
                                            View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Prepaid-orders</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="prepaid_orders_view" id="prepaid_orders_view1">
                                            View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Postpaid-orders</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="postpaid_orders_view"
                                                id="postpaid_orders_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Open-orders</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="open_orders_view" id="open_orders_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="open_orders_add" id="open_orders_add1" class="open_orders_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="open_orders_delete"
                                                    id="open_orders_delete1" class="open_orders_view1"> Cancel
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="open_orders_transfer"
                                                    id="open_orders_transfer1" class="open_orders_view1"> Transfer
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>Voided Items</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="void_item_view" id="void_item_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="void_item_accept" id="void_item_accept1" class="void_item_vie"> Accept
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="void_item_reject"
                                                    id="void_item_reject1" class="void_item_vie"> Reject
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bills</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="bills_view" id="bills_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Generate-bills</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="generate_bills_view" id="generate_bills_view1">
                                            View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Master-bills</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="master_bills_view" id="master_bills_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="master_bills_close"
                                                    id="master_bills_close1" class="master_bills_view1"> Close
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="master_bills_delete"
                                                    id="master_bills_delete1" class="master_bills_view1"> Delete_bill
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="master_bills_discount"
                                                    id="master_bills_discount1" class="master_bills_view1"> Discount
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Closed-orders</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="closed_orders_view" id="closed_orders_view1">
                                            View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Closed-orders-payment</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="closed_orders_payment_view"
                                                id="closed_orders_payment_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="closed_orders_payment_reprints"
                                                    id="closed_orders_payment_reprints1" class="closed_orders_payment_view1"> Reprints
                                            </label>
                                        </span>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Complete-orders</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="complete_orders_view"
                                                id="complete_orders_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>System-setup</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="system_setup_view" id="system_setup_view1">
                                            View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Advertisements</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="advertisements_view" id="advertisements_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="advertisements_add"
                                                    id="advertisements_add1" class="advertisements_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="advertisements_edit"
                                                    id="advertisements_edit1" class="advertisements_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="advertisements_delete"
                                                    id="advertisements_delete1" class="advertisements_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Social-links</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="social_links_view" id="social_links_view1">
                                            View
                                        </label>


                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label>
                                            <input type="checkbox" name="social_links_edit" id="social_links_edit1" class="social_links_view1">
                                            Edit
                                        </label>
                                        </span>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Reservation-emails</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="reservation_email_view"
                                                id="reservation_email_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reservation_email_add"
                                                    id="reservation_email_add1" class="reservation_email_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reservation_email_edit"
                                                    id="reservation_email_edit1" class="reservation_email_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reservation_email_delete"
                                                    id="reservation_email_delete1" class="reservation_email_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Payment-methods</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="payent_method_view" id="payent_method_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="payent_method_add" id="payent_method_add1" class="payent_method_view1">
                                                Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="payent_method_edit"
                                                    id="payent_method_edit1" class="payent_method_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="payent_method_delete"
                                                    id="payent_method_delete1" class="payent_method_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>App-custom-pages</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="app_custom_pages_view"
                                                id="app_custom_pages_view1"> View
                                        </label>

                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="app_custom_pages_edit"
                                                    id="app_custom_pages_edit1" class="app_custom_pages_view1"> Edit
                                            </label>
                                        </span>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Restaurants</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="restaurants_view" id="restaurants_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="restaurants_add" id="restaurants_add1" class="restaurants_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="restaurants_edit" id="restaurants_edit1" class="restaurants_view1">
                                                Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="restaurants_delete"
                                                    id="restaurants_delete1" class="restaurants_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Print-classes</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="print_classes_view" id="print_classes_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="print_classes_add" id="print_classes_add1" class="print_classes_view1">
                                                Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="print_classes_edit"
                                                    id="print_classes_edit1" class="print_classes_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="print_classes_delete"
                                                    id="print_classes_delete1" class="print_classes_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Take-away</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="take_away_view" id="take_away_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="take_away_add" id="take_away_add1" class="take_away_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="take_away_edit" id="take_away_edit1" class="take_away_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="take_away_delete" id="take_away_delete1" class="take_away_view1">
                                                Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Employees</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="employees_view" id="employees_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="employees_add" id="employees_add1" class="employees_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="employees_edit" id="employees_edit1" class="employees_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="employees_delete" id="employees_delete1" class="employees_view1">
                                                Delete
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="employees_change_password"
                                                    id="employees_change_password1" class="employees_view1"> Change Password
                                            </label>
                                        </span>
                                         <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="employees_assign_table"
                                                    id="employees_assign_table1" class="employees_view1"> Assign Table
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Print-class-users</td>
                                    <td>
                                        <label>
                                            <input type="checkbox" name="print_class_users_view"
                                                id="print_class_users_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="print_class_users_add"
                                                    id="print_class_users_add1" class="print_class_users_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="print_class_users_edit"
                                                    id="print_class_users_edit1" class="print_class_users_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="print_class_users_delete"
                                                    id="print_class_users_delete1" class="print_class_users_view1"> Delete
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="print_class_users_change_password"
                                                    id="print_class_users_change_password1" class="print_class_users_view1"> Change Password
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Table-managers</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="table_managers_view" id="table_managers_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="table_managers_add"
                                                    id="table_managers_add1" class="table_managers_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="table_managers_edit"
                                                    id="table_managers_edit1" class="table_managers_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="table_managers_delete"
                                                    id="table_managers_delete1" class="table_managers_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tax-manager</td>
                                    <td>
                                        <label>
                                            <input type="checkbox" name="tax_manager_view" id="tax_manager_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="tax_manager_add" id="tax_manager_add1" class="tax_manager_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="tax_manager_edit" id="tax_manager_edit1" class="tax_manager_view1">
                                                Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="tax_manager_delete"
                                                    id="tax_manager_delete1" class="tax_manager_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Settings</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="settings_view" id="settings_view1"> View
                                        </label>

                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="settings_edit" id="settings_edit1" class="settings_view1"> Edit
                                            </label>
                                        </span>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Condiments</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="condiments_view" id="condiments_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Member</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="member_view" id="member_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="member_add" id="member_add1" class="member_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="member_edit" id="member_edit1" class="member_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="member_delete" id="member_delete1" class="member_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Groups</td>
                                    <td>
                                        <label>
                                            <input type="checkbox" name="groups_view" id="groups_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="groups_add" id="groups_add1" class="groups_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="groups_edit" id="groups_edit1" class="groups_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="groups_delete" id="groups_delete1" class="groups_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Roles</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="roles_view" id="roles_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="roles_add" id="roles_add1" class="roles_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="roles_edit" id="roles_edit1" class="roles_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="roles_delete" id="roles_delete1" class="roles_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Recipes</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="recipes_view" id="recipes_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>WEBACCOUNTING-ERP</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="WEBACCOUNTING_ERP_view"
                                                id="WEBACCOUNTING_ERP_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Reports</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="reports_view" id="reports_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reports_payment_sales_summary"
                                                    id="reports_payment_sales_summary1" class="reports_view1"> Product Wise Sales
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reports_menu_item_general_sales"
                                                    id="reports_menu_item_general_sales1" class="reports_view1"> Unpaid By Waiters
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reports_menu_tem_general_sales_with_plu"
                                                    id="reports_menu_tem_general_sales_with_plu1" class="reports_view1"> Waiters Detailed
                                                Reports
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox"
                                                    name="reports_menu_item_general_sales_without_plu"
                                                    id="reports_menu_item_general_sales_without_plu1" class="reports_view1"> Quantity Control
                                                Report
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reports_family_group_sales_with_gl"
                                                    id="reports_family_group_sales_with_gl1" class="reports_view1"> Discount Allowed
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reports_menu_item_group_sales"
                                                    id="reports_menu_item_group_sales1" class="reports_view1"> Open Orders
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reports_major_group_sales"
                                                    id="reports_major_group_sales1" class="reports_view1"> Complete Orders
                                            </label>
                                        </span>
                                        <span>
                                            <span>

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label>
                                                    <input type="checkbox" name="reports_void_items"
                                                        id="reports_void_items" class="reports_view1"> Void items
                                                </label>
                                            </span>
                                            <span>

                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label>
                                                    <input type="checkbox" name="reports_payment_sales_summary1"
                                                        id="reports_payment_sales_summary11" class="reports_view1"> Payment Sales Summary
                                                </label>
                                            </span>


                                    </td>
                                </tr>
                                <tr>
                                    <td>Feedback</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="feedback_view" id="feedback_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Reservations</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="reservations_view" id="reservations_view1">
                                            View
                                        </label>

                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reservations_edit" id="reservations_edit1" class="reservations_view1">
                                                Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="reservations_delete"
                                                    id="reservations_delete1" class="reservations_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Beer-delivery-and-keg-setup</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="beer_delivery_and_keg_setup_view"
                                                id="beer_delivery_and_keg_setup_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Beer-and-keg-sub-major-group</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="beer_and_keg_sub_major_group_view"
                                                id="beer_and_keg_sub_major_group_view1"> View
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Delivery-family-groups</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="delivery_family_groups_view"
                                                id="delivery_family_groups_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_family_groups_add"
                                                    id="delivery_family_groups_add1" class="delivery_family_groups_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_family_groups_edit"
                                                    id="delivery_family_groups_edit1" class="delivery_family_groups_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_family_groups_delete"
                                                    id="delivery_family_groups_delete1" class="delivery_family_groups_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Delivery-sub-family-groups</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="delivery_sub_family_groups_view"
                                                id="delivery_sub_family_groups_view1"> View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_sub_family_groups_add"
                                                    id="delivery_sub_family_groups_add1" class="delivery_sub_family_groups_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_sub_family_groups_edit"
                                                    id="delivery_sub_family_groups_edit1" class="delivery_sub_family_groups_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_sub_family_groups_delete"
                                                    id="delivery_sub_family_groups_delete1" class="delivery_sub_family_groups_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Delivery-items</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="delivery_items_view" id="delivery_items_view1">
                                            View
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_items_add"
                                                    id="delivery_items_add1" class="delivery_items_view1"> Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_items_edit"
                                                    id="delivery_items_edit1" class="delivery_items_view1"> Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delivery_items_delete"
                                                    id="delivery_items_delete1" class="delivery_items_view1"> Delete
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                
                                 <tr>
                                    <td>Stock Managment  (Ingredients)</td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="stock" id="stock2" >
                                            Recipe
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                 <input type="checkbox" name="add_ingredients"  class="stock1" id="add_ingredient1">
                                            Add
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="view_ingredients"
                                                    id="view_ingredient1" class="stock1" > View
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="edit_ingredients"
                                                    id="edit_ingredient1" class="stock1" > Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delete_ingredients"
                                                    id="delete_ingredient1" class="stock1" > Delete
                                            </label>
                                        </span>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>Recipe Managment  </td>

                                    <td>
                                        <label>
                                            <input type="checkbox" name="add_recipe" id="add_recipe1" class="stock1">
                                            Add
                                        </label>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="view_recipe"
                                                    id="view_recipe1" class="stock1" > View
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="edit_recipe"
                                                    id="edit_recipe1" class="stock1" > Edit
                                            </label>
                                        </span>
                                        <span>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="checkbox" name="delete_recipe"
                                                    id="delete_recipe1" class="stock1" > Delete
                                            </label>
                                        </span>
                                        
                                    </td>
                                </tr>



                            </tbody>
                        </table>
                    </div>
                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Update</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
@stop

@section('javascript')
    <script>
    $(document).ready(function(){
        
    
     $("#void_item_view1").click(function() {
     
         if (!this.checked) {
            $(".void_item_vie").prop("checked", false);
            $(".void_item_vie").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".void_item_vie").removeAttr('disabled');
            $(".void_item_vie").prop("checked", false);
        }
    
    });
        
    $("#sub_major_groups_manager_view1").click(function() {
     
         if (!this.checked) {
            $(".sub_major_groups_manager_view1").prop("checked", false);
            $(".sub_major_groups_manager_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".sub_major_groups_manager_view1").removeAttr('disabled');
            $(".sub_major_groups_manager_view1").prop("checked", false);
        }
    
    });
    
    $("#m_g_m_view1").click(function(){
        
        if (!this.checked) {
            $(".m_g_m_view1").prop("checked", false);
            $(".m_g_m_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".m_g_m_view1").removeAttr('disabled');
            $(".m_g_m_view1").prop("checked", false);
        }
    });
    
    $("#menu_item_groups_view1").click(function(){
        
        if (!this.checked) {
            $(".menu_item_groups_view1").prop("checked", false);
            $(".menu_item_groups_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".menu_item_groups_view1").removeAttr('disabled');
            $(".menu_item_groups_view1").prop("checked", false);
        }
    });
    
    $("#offers_view1").click(function(){
        
        if (!this.checked) {
            $(".offers_view1").prop("checked", false);
            $(".offers_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".offers_view1").removeAttr('disabled');
            $(".offers_view1").prop("checked", false);
        }
    });
    
    $("#family_groups_view1").click(function(){
        
        if (!this.checked) {
            $(".family_groups_view1").prop("checked", false);
            $(".family_groups_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".family_groups_view1").removeAttr('disabled');
            $(".family_groups_view1").prop("checked", false);
        }
    });
    
    $("#alcoholic_family_groups_view1").click(function(){
        
        if (!this.checked) {
            $(".alcoholic_family_groups_view1").prop("checked", false);
            $(".alcoholic_family_groups_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".alcoholic_family_groups_view1").removeAttr('disabled');
            $(".alcoholic_family_groups_view1").prop("checked", false);
        }
    });
    
    $("#alcoholic_sub_family_groups_view1").click(function(){
        
        if (!this.checked) {
            $(".alcoholic_sub_family_groups_view1").prop("checked", false);
            $(".alcoholic_sub_family_groups_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".alcoholic_sub_family_groups_view1").removeAttr('disabled');
            $(".alcoholic_sub_family_groups_view1").prop("checked", false);
        }
    });
    
    $("#general_item_view1").click(function(){
        
        if (!this.checked) {
            $(".general_item_view1").prop("checked", false);
            $(".general_item_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".general_item_view1").removeAttr('disabled');
            $(".general_item_view1").prop("checked", false);
        }
    });
    
    $("#offer_themes_nights_view1").click(function(){
        
        if (!this.checked) {
            $(".offer_themes_nights_view1").prop("checked", false);
            $(".offer_themes_nights_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".offer_themes_nights_view1").removeAttr('disabled');
            $(".offer_themes_nights_view1").prop("checked", false);
        }
    });
    
    $("#app_users_view1").click(function(){
        
        if (!this.checked) {
            $(".app_users_view1").prop("checked", false);
            $(".app_users_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".app_users_view1").removeAttr('disabled');
            $(".app_users_view1").prop("checked", false);
        }
    });
    
    $("#open_orders_view1").click(function(){
        
        if (!this.checked) {
            $(".open_orders_view1").prop("checked", false);
            $(".open_orders_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".open_orders_view1").removeAttr('disabled');
            $(".open_orders_view1").prop("checked", false);
        }
    });
    
     $("#master_bills_view1").click(function(){
        
        if (!this.checked) {
            $(".master_bills_view1").prop("checked", false);
            $(".master_bills_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".master_bills_view1").removeAttr('disabled');
            $(".master_bills_view1").prop("checked", false);
        }
    });
    
    $("#closed_orders_payment_view1").click(function(){
        
        if (!this.checked) {
            $(".closed_orders_payment_view1").prop("checked", false);
            $(".closed_orders_payment_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".closed_orders_payment_view1").removeAttr('disabled');
            $(".closed_orders_payment_view1").prop("checked", false);
        }
    });
    
    $("#advertisements_view1").click(function(){
        
        if (!this.checked) {
            $(".advertisements_view1").prop("checked", false);
            $(".advertisements_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".advertisements_view1").removeAttr('disabled');
            $(".advertisements_view1").prop("checked", false);
        }
    });
    
    $("#social_links_view1").click(function(){
        
        if (!this.checked) {
            $(".social_links_view1").prop("checked", false);
            $(".social_links_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".social_links_view1").removeAttr('disabled');
            $(".social_links_view1").prop("checked", false);
        }
    });
    
    $("#reservation_email_view1").click(function(){
        
        if (!this.checked) {
            $(".reservation_email_view1").prop("checked", false);
            $(".reservation_email_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".reservation_email_view1").removeAttr('disabled');
            $(".reservation_email_view1").prop("checked", false);
        }
    });
    
    $("#payent_method_view1").click(function(){
        
        if (!this.checked) {
            $(".payent_method_view1").prop("checked", false);
            $(".payent_method_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".payent_method_view1").removeAttr('disabled');
            $(".payent_method_view1").prop("checked", false);
        }
    });
    
    $("#app_custom_pages_view1").click(function(){
        
        if (!this.checked) {
            $(".app_custom_pages_view1").prop("checked", false);
            $(".app_custom_pages_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".app_custom_pages_view1").removeAttr('disabled');
            $(".app_custom_pages_view1").prop("checked", false);
        }
    });
    
    $("#restaurants_view1").click(function(){
        
        if (!this.checked) {
            $(".restaurants_view1").prop("checked", false);
            $(".restaurants_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".restaurants_view1").removeAttr('disabled');
            $(".restaurants_view1").prop("checked", false);
        }
    });
    
    $("#print_classes_view1").click(function(){
        
        if (!this.checked) {
            $(".print_classes_view1").prop("checked", false);
            $(".print_classes_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".print_classes_view1").removeAttr('disabled');
            $(".print_classes_view1").prop("checked", false);
        }
    });
    
    $("#take_away_view1").click(function(){
        
        if (!this.checked) {
            $(".take_away_view1").prop("checked", false);
            $(".take_away_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".take_away_view1").removeAttr('disabled');
            $(".take_away_view1").prop("checked", false);
        }
    });
    
    $("#employees_view1").click(function(){
        
        if (!this.checked) {
            $(".employees_view1").prop("checked", false);
            $(".employees_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".employees_view1").removeAttr('disabled');
            $(".employees_view1").prop("checked", false);
        }
    });
    
    $("#print_class_users_view1").click(function(){
        
        if (!this.checked) {
            $(".print_class_users_view1").prop("checked", false);
            $(".print_class_users_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".print_class_users_view1").removeAttr('disabled');
            $(".print_class_users_view1").prop("checked", false);
        }
    });
    
    $("#table_managers_view1").click(function(){
        
        if (!this.checked) {
            $(".table_managers_view1").prop("checked", false);
            $(".table_managers_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".table_managers_view1").removeAttr('disabled');
            $(".table_managers_view1").prop("checked", false);
        }
    });
    
    $("#tax_manager_view1").click(function(){
        
        if (!this.checked) {
            $(".tax_manager_view1").prop("checked", false);
            $(".tax_manager_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".tax_manager_view1").removeAttr('disabled');
            $(".tax_manager_view1").prop("checked", false);
        }
    });
    
    $("#settings_view1").click(function(){
        
        if (!this.checked) {
            $(".settings_view1").prop("checked", false);
            $(".settings_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".settings_view1").removeAttr('disabled');
            $(".settings_view1").prop("checked", false);
        }
    });
    
    $("#member_view1").click(function(){
        
        if (!this.checked) {
            $(".member_view1").prop("checked", false);
            $(".member_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".member_view1").removeAttr('disabled');
            $(".member_view1").prop("checked", false);
        }
    });
    
    $("#groups_view1").click(function(){
        
        if (!this.checked) {
            $(".groups_view1").prop("checked", false);
            $(".groups_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".groups_view1").removeAttr('disabled');
            $(".groups_view1").prop("checked", false);
        }
    });
    
    $("#roles_view1").click(function(){
        
        if (!this.checked) {
            $(".roles_view1").prop("checked", false);
            $(".roles_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".roles_view1").removeAttr('disabled');
            $(".roles_view1").prop("checked", false);
        }
    });
    
    $("#reports_view1").click(function(){
        
        if (!this.checked) {
            $(".reports_view1").prop("checked", false);
            $(".reports_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".reports_view1").removeAttr('disabled');
            $(".reports_view1").prop("checked", false);
        }
    });
    
    $("#reservations_view1").click(function(){
        
        if (!this.checked) {
            $(".reservations_view1").prop("checked", false);
            $(".reservations_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".reservations_view1").removeAttr('disabled');
            $(".reservations_view1").prop("checked", false);
        }
    });
    $("#stock2").click(function(){
       
        if (!this.checked) {
            
            $(".stock1").prop("checked", false);
            $(".stock1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".stock1").removeAttr('disabled');
            $(".stock1").prop("checked", false);
        }
    });
    
    $("#delivery_family_groups_view1").click(function(){
        
        if (!this.checked) {
            $(".delivery_family_groups_view1").prop("checked", false);
            $(".delivery_family_groups_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".delivery_family_groups_view1").removeAttr('disabled');
            $(".delivery_family_groups_view1").prop("checked", false);
        }
    });
    
    $("#delivery_sub_family_groups_view1").click(function(){
        
        if (!this.checked) {
            $(".delivery_sub_family_groups_view1").prop("checked", false);
            $(".delivery_sub_family_groups_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".delivery_sub_family_groups_view1").removeAttr('disabled');
            $(".delivery_sub_family_groups_view1").prop("checked", false);
        }
    });
    
    $("#delivery_items_view1").click(function(){
        
        if (!this.checked) {
            $(".delivery_items_view1").prop("checked", false);
            $(".delivery_items_view1").attr('disabled',true);
            
        }
        else if(this.checked) {
            $(".delivery_items_view1").removeAttr('disabled');
            $(".delivery_items_view1").prop("checked", false);
        }
    });
    
 
});
</script>
<script>
var user = {!!json_encode($user);!!};

console.log(user);

if (user.void_item_view == "on") {
    $("#void_item_view1").attr('checked', true);
}
else
{
     $(".void_item_vie").attr('disabled', true);
    $(".void_item_vie").prop("checked", false);
}
if (user.void_item_accept == "on") {
    $("#void_item_accept1").attr('checked', true);
}
if (user.void_item_reject == "on") {
    $("#void_item_reject1").attr('checked', true);
}

if (user.dashboard_view == "on") {
    $("#dashboard_view").attr('checked', true);
}

if (user.menu_setup_view == "on") {
    $("#ms").attr('checked', true);
}

if (user.major_groups_manager_view == "on") {

    $("#m_g_m_view1").attr('checked', true);
}
else
{
    $(".m_g_m_view1").attr('disabled', true);
    $(".m_g_m_view1").prop("checked", false);
}

if (user.major_groups_manager_edit == "on") {

    $("#major_groups_manager_edit1").attr('checked', true);
}

if (user.sub_major_groups_manager_add == "on") {

    $("#sub_major_groups_manager_add").attr('checked', true);
}
if (user.sub_major_groups_manager_delete == "on") {

    $("#sub_major_groups_manager_delete").attr('checked', true);
}
if (user.sub_major_groups_manager_edit == "on") {

    $("#sub_major_groups_manager_edit").attr('checked', true);
}
if (user.sub_major_groups_manager_view == "on") {

    $("#sub_major_groups_manager_view1").attr('checked', true);
}
else
{
    $(".sub_major_groups_manager_view1").attr('disabled', true);
    $(".sub_major_groups_manager_view1").prop("checked", false);
}

if (user.menu_item_groups_delete == "on") {

    $("#menu_item_groups_delete1").attr('checked', true);
}
if (user.menu_item_groups_add == "on") {

    $("#menu_item_groups_add1").attr('checked', true);
}
if (user.menu_item_groups_edit == "on") {

    $("#menu_item_groups_edit1").attr('checked', true);
}
if (user.menu_item_groups_view == "on") {

    $("#menu_item_groups_view1").attr('checked', true);
}
else
{
        $(".menu_item_groups_view1").attr('disabled', true);
        $(".menu_item_groups_view1").prop("checked", false);
}

if (user.offers_view == "on") {

    $("#offers_view1").attr('checked', true);
}
else
{
    $(".offers_view1").attr('disabled', true);
    $(".offers_view1").prop("checked", false);
}
if (user.offers_add == "on") {

    $("#offers_add1").attr('checked', true);
}
if (user.offers_edit == "on") {

    $("#offers_edit1").attr('checked', true);
}
if (user.offers_delete == "on") {

    $("#offers_delete1").attr('checked', true);
}

if (user.family_groups_view == "on") {

    $("#family_groups_view1").attr('checked', true);
}
else
{
    $(".family_groups_view1").attr('disabled', true);
    $(".family_groups_view1").prop("checked", false);
}
if (user.family_groups_edit == "on") {

    $("#family_groups_edit1").attr('checked', true);
}
if (user.family_groups_add == "on") {

    $("#family_groups_add1").attr('checked', true);
}
if (user.family_groups_delete == "on") {

    $("#family_groups_delete1").attr('checked', true);
}


if (user.alcoholic_family_groups_add == "on") {

    $("#alcoholic_family_groups_add1").attr('checked', true);
}
if (user.alcoholic_family_groups_edit == "on") {

    $("#alcoholic_family_groups_edit1").attr('checked', true);
}
if (user.alcoholic_family_groups_delete == "on") {

    $("#alcoholic_family_groups_delete1").attr('checked', true);
}
if (user.alcoholic_family_groups_view == "on") {

    $("#alcoholic_family_groups_view1").attr('checked', true);
}
else
{
    $(".alcoholic_family_groups_view1").attr('disabled', true);
    $(".alcoholic_family_groups_view1").prop("checked", false);
}


if (user.alcoholic_sub_family_groups_add == "on") {

    $("#alcoholic_sub_family_groups_add1").attr('checked', true);
}
if (user.alcoholic_sub_family_groups_edit == "on") {

    $("#alcoholic_sub_family_groups_edit1").attr('checked', true);
}
if (user.alcoholic_sub_family_groups_view == "on") {

    $("#alcoholic_sub_family_groups_view1").attr('checked', true);
}
else
{
    $(".alcoholic_sub_family_groups_view1").attr('disabled', true);
    $(".alcoholic_sub_family_groups_view1").prop("checked", false);
}
if (user.alcoholic_sub_family_groups_delete == "on") {

    $("#alcoholic_sub_family_groups_delete1").attr('checked', true);
}


if (user.menu_Item_view == "on") {

    $("#menu_Item_view1").attr('checked', true);
}

if (user.general_item_view == "on") {

    $("#general_item_view1").attr('checked', true);
}
else
{
    $(".general_item_view1").attr('disabled', true);
    $(".general_item_view1").prop("checked", false);
}
if (user.general_item_add == "on") {

    $("#general_item_add1").attr('checked', true);
}
if (user.general_item_edit == "on") {

    $("#general_item_edit1").attr('checked', true);
}
if (user.general_item_delete == "on") {

    $("#general_item_delete1").attr('checked', true);
}


if (user.offer_themes_nights_view == "on") {

    $("#offer_themes_nights_view1").attr('checked', true);
}
else
{
    $(".offer_themes_nights_view1").attr('disabled', true);
    $(".offer_themes_nights_view1").prop("checked", false);
}
if (user.offer_themes_nights_edit == "on") {

    $("#offer_themes_nights_edit1").attr('checked', true);
}
if (user.offer_themes_nights_add == "on") {

    $("#offer_themes_nights_add1").attr('checked', true);
}
if (user.offer_themes_nights_delete == "on") {

    $("#offer_themes_nights_delete1").attr('checked', true);
}


if (user.app_users_view == "on") {

    $("#app_users_view1").attr('checked', true);
}
else
{
    $(".app_users_view1").attr('disabled', true);
    $(".app_users_view1").prop("checked", false);
}

if (user.app_users_addamounttowallet == "on") {

    $("#app_users_addamounttowallet1").attr('checked', true);
}

if (user.manage_orders_view == "on") {

    $("#manage_orders_view1").attr('checked', true);
}

if (user.prepaid_orders_view == "on") {

    $("#prepaid_orders_view1").attr('checked', true);
}

if (user.postpaid_orders_view == "on") {

    $("#postpaid_orders_view1").attr('checked', true);
}

if (user.open_orders_view == "on") {

    $("#open_orders_view1").attr('checked', true);
}
else
{
    $(".open_orders_view1").attr('disabled', true);
    $(".open_orders_view1").prop("checked", false);
}
if (user.open_orders_add == "on") {

    $("#open_orders_add1").attr('checked', true);
}
if (user.open_orders_delete == "on") {

    $("#open_orders_delete1").attr('checked', true);
}
if (user.open_orders_transfer == "on") {

    $("#open_orders_transfer1").attr('checked', true);
}

if (user.bills_view == "on") {

    $("#bills_view1").attr('checked', true);
}
if (user.generate_bills_view == "on") {

    $("#generate_bills_view1").attr('checked', true);
}

if (user.master_bills_view == "on") {

    $("#master_bills_view1").attr('checked', true);
}
else
{
     $(".master_bills_view1").attr('disabled', true);
    $(".master_bills_view1").prop("checked", false);
}
if (user.master_bills_close == "on") {

    $("#master_bills_close1").attr('checked', true);
}
if (user.master_bills_delete == "on") {

    $("#master_bills_delete1").attr('checked', true);
}
if (user.master_bills_discount == "on") {

    $("#master_bills_discount1").attr('checked', true);
}

if (user.closed_orders_view == "on") {

    $("#closed_orders_view1").attr('checked', true);
}
if (user.closed_orders_payment_view == "on") {

    $("#closed_orders_payment_view1").attr('checked', true);
}
else
{
    $(".closed_orders_payment_view1").attr('disabled', true);
    $(".closed_orders_payment_view1").prop("checked", false);
}
if (user.closed_orders_payment_reprints == "on") {

    $("#closed_orders_payment_reprints1").attr('checked', true);
}

if (user.complete_orders_view == "on") {

    $("#complete_orders_view1").attr('checked', true);
}
if (user.system_setup_view == "on") {

    $("#system_setup_view1").attr('checked', true);
}
if (user.advertisements_view == "on") {

    $("#advertisements_view1").attr('checked', true);
}
else
{
    $(".advertisements_view1").attr('disabled', true);
    $(".advertisements_view1").prop("checked", false);
}
if (user.advertisements_edit == "on") {

    $("#advertisements_edit1").attr('checked', true);
}
if (user.advertisements_add == "on") {

    $("#advertisements_add1").attr('checked', true);
}
if (user.advertisements_delete == "on") {

    $("#advertisements_delete1").attr('checked', true);
}

if (user.social_links_view == "on") {

    $("#social_links_view1").attr('checked', true);
}
else
{
    $(".social_links_view1").attr('disabled', true);
    $(".social_links_view1").prop("checked", false);
}
if (user.social_links_edit == "on") {

    $("#social_links_edit1").attr('checked', true);
}

if (user.reservation_email_view == "on") {

    $("#reservation_email_view1").attr('checked', true);
}
else
{
    $(".reservation_email_view1").attr('disabled', true);
    $(".reservation_email_view1").prop("checked", false);
}
if (user.reservation_email_add == "on") {

    $("#reservation_email_add1").attr('checked', true);
}

if (user.reservation_email_edit == "on") {

    $("#reservation_email_edit1").attr('checked', true);
}
if (user.reservation_email_delete == "on") {

    $("#reservation_email_delete1").attr('checked', true);
}

if (user.payent_method_view == "on") {

    $("#payent_method_view1").attr('checked', true);
}
else
{
    $(".payent_method_view1").attr('disabled', true);
    $(".payent_method_view1").prop("checked", false);
}
if (user.payent_method_edit == "on") {

    $("#payent_method_edit1").attr('checked', true);
}

if (user.payent_method_add == "on") {

    $("#payent_method_add1").attr('checked', true);
}
if (user.payent_method_delete == "on") {

    $("#payent_method_delete1").attr('checked', true);
}


if (user.app_custom_pages_view == "on") {

    $("#app_custom_pages_view1").attr('checked', true);
}
else
{
    $(".app_custom_pages_view1").attr('disabled', true);
    $(".app_custom_pages_view1").prop("checked", false);
}
if (user.app_custom_pages_edit == "on") {

    $("#app_custom_pages_edit1").attr('checked', true);
}


if (user.restaurants_view == "on") {

    $("#restaurants_view1").attr('checked', true);
}
else
{
    $(".restaurants_view1").attr('disabled', true);
    $(".restaurants_view1").prop("checked", false);
}
if (user.restaurants_add == "on") {

    $("#restaurants_add1").attr('checked', true);
}
if (user.restaurants_edit == "on") {

    $("#restaurants_edit1").attr('checked', true);
}
if (user.restaurants_delete == "on") {

    $("#restaurants_delete1").attr('checked', true);
}



if (user.print_classes_view == "on") {

    $("#print_classes_view1").attr('checked', true);
}
else
{
    $(".print_classes_view1").attr('disabled', true);
    $(".print_classes_view1").prop("checked", false);
}
if (user.print_classes_add == "on") {

    $("#print_classes_add1").attr('checked', true);
}
if (user.print_classes_edit == "on") {

    $("#print_classes_edit1").attr('checked', true);
}
if (user.print_classes_delete == "on") {

    $("#print_classes_delete1").attr('checked', true);
}



if (user.take_away_view == "on") {

    $("#take_away_view1").attr('checked', true);
}
else
{
    $(".take_away_view1").attr('disabled', true);
    $(".take_away_view1").prop("checked", false);
}
if (user.take_away_edit == "on") {

    $("#take_away_edit1").attr('checked', true);
}
if (user.take_away_add == "on") {

    $("#take_away_add1").attr('checked', true);
}
if (user.take_away_delete == "on") {

    $("#take_away_delete1").attr('checked', true);
}



if (user.employees_view == "on") {

    $("#employees_view1").attr('checked', true);
}
else
{
    $(".employees_view1").attr('disabled', true);
    $(".employees_view1").prop("checked", false);
}
if (user.employees_add == "on") {

    $("#employees_add1").attr('checked', true);
}
if (user.employees_edit == "on") {

    $("#employees_edit1").attr('checked', true);
}
if (user.employees_delete == "on") {

    $("#employees_delete1").attr('checked', true);
}
if (user.employees_change_password == "on") {

    $("#employees_change_password1").attr('checked', true);
}
if (user.employees_assign_table == "on") {

    $("#employees_assign_table1").attr('checked', true);
}


if (user.print_class_users_view == "on") {

    $("#print_class_users_view1").attr('checked', true);
}
else
{
    $(".print_class_users_view1").attr('disabled', true);
    $(".print_class_users_view1").prop("checked", false);
}
if (user.print_class_users_add == "on") {

    $("#print_class_users_add1").attr('checked', true);
}
if (user.print_class_users_edit == "on") {

    $("#print_class_users_edit1").attr('checked', true);
}
if (user.print_class_users_delete == "on") {

    $("#print_class_users_delete1").attr('checked', true);
}
if (user.print_class_users_change_password == "on") {

    $("#print_class_users_change_password1").attr('checked', true);
}



if (user.table_managers_view == "on") {

    $("#table_managers_view1").attr('checked', true);
}
else
{
    $(".table_managers_view1").attr('disabled', true);
    $(".table_managers_view1").prop("checked", false);
}
if (user.table_managers_add == "on") {

    $("#table_managers_add1").attr('checked', true);
}
if (user.table_managers_edit == "on") {

    $("#table_managers_edit1").attr('checked', true);
}
if (user.table_managers_delete == "on") {

    $("#table_managers_delete1").attr('checked', true);
}



if (user.tax_manager_view == "on") {

    $("#tax_manager_view1").attr('checked', true);
}
else
{
    $(".tax_manager_view1").attr('disabled', true);
    $(".tax_manager_view1").prop("checked", false);
}
if (user.tax_manager_add == "on") {

    $("#tax_manager_add1").attr('checked', true);
}
if (user.tax_manager_edit == "on") {

    $("#tax_manager_edit1").attr('checked', true);
}
if (user.tax_manager_delete == "on") {

    $("#tax_manager_delete1").attr('checked', true);
}



if (user.settings_view == "on") {

    $("#settings_view1").attr('checked', true);
}
else
{
    $(".settings_view1").attr('disabled', true);
    $(".settings_view1").prop("checked", false);
}
if (user.settings_edit == "on") {

    $("#settings_edit1").attr('checked', true);
}



if (user.condiments_view == "on") {

    $("#condiments_view1").attr('checked', true);
}
if (user.member_view == "on") {

    $("#member_view1").attr('checked', true);
}
else
{
    $(".member_view1").attr('disabled', true);
    $(".member_view1").prop("checked", false);
}

if (user.member_add == "on") {

    $("#member_add1").attr('checked', true);
}
if (user.member_edit == "on") {

    $("#member_edit1").attr('checked', true);
}
if (user.member_delete == "on") {

    $("#member_delete1").attr('checked', true);
}



if (user.groups_view == "on") {

    $("#groups_view1").attr('checked', true);
}
else
{
    $(".groups_view1").attr('disabled', true);
    $(".groups_view1").prop("checked", false);
}

if (user.groups_add == "on") {

    $("#groups_add1").attr('checked', true);
}
if (user.groups_edit == "on") {

    $("#groups_edit1").attr('checked', true);
}
if (user.groups_delete == "on") {

    $("#groups_delete1").attr('checked', true);
}



if (user.roles_view == "on") {

    $("#roles_view1").attr('checked', true);
}
else
{
    $(".roles_view1").attr('disabled', true);
    $(".roles_view1").prop("checked", false);
}

if (user.roles_add == "on") {

    $("#roles_add1").attr('checked', true);
}
if (user.roles_edit == "on") {

    $("#roles_edit1").attr('checked', true);
}
if (user.roles_delete == "on") {

    $("#roles_delete1").attr('checked', true);
}


if (user.recipes_view == "on") {

    $("#recipes_view1").attr('checked', true);
}
if (user.WEBACCOUNTING_ERP_view == "on") {

    $("#WEBACCOUNTING_ERP_view1").attr('checked', true);
}


if (user.reports_view == "on") {

    $("#reports_view1").attr('checked', true);
}
else
{
    $(".reports_view1").attr('disabled', true);
    $(".reports_view1").prop("checked", false);
}
if (user.reports_payment_sales_summary == "on") {

    $("#reports_payment_sales_summary1").attr('checked', true);
}
if (user.reports_menu_item_general_sales == "on") {

    $("#reports_menu_item_general_sales1").attr('checked', true);
}
if (user.reports_menu_tem_general_sales_with_plu == "on") {

    $("#reports_menu_tem_general_sales_with_plu1").attr('checked', true);
}
if (user.reports_menu_item_general_sales_without_plu == "on") {

    $("#reports_menu_item_general_sales_without_plu1").attr('checked', true);
}
if (user.reports_family_group_sales_with_gl == "on") {

    $("#reports_family_group_sales_with_gl1").attr('checked', true);
}
if (user.reports_menu_item_group_sales == "on") {

    $("#reports_menu_item_group_sales1").attr('checked', true);
}
if (user.reports_major_group_sales == "on") {

    $("#reports_major_group_sales1").attr('checked', true);
}
if (user.reports_payment_sales_summary1 == "on") {

    $("#reports_payment_sales_summary11").attr('checked', true);
}
if (user.reports_void_items == "on") {

    $("#reports_void_items").attr('checked', true);
}
if (user.feedback_view == "on") {

    $("#feedback_view1").attr('checked', true);
}

if (user.reservations_view == "on") {

    $("#reservations_view1").attr('checked', true);
}
else
{
    $(".reservations_view1").attr('disabled', true);
    $(".reservations_view1").prop("checked", false);
}
if (user.reservations_delete == "on") {

    $("#reservations_delete1").attr('checked', true);
}
if (user.reservations_edit == "on") {

    $("#reservations_edit1").attr('checked', true);
}

if (user.beer_delivery_and_keg_setup_view == "on") {

    $("#beer_delivery_and_keg_setup_view1").attr('checked', true);
}
if (user.beer_and_keg_sub_major_group_view == "on") {

    $("#beer_and_keg_sub_major_group_view1").attr('checked', true);
}

if (user.delivery_family_groups_view == "on") {

    $("#delivery_family_groups_view1").attr('checked', true);
}
else
{
    $(".delivery_family_groups_view1").attr('disabled', true);
    $(".delivery_family_groups_view1").prop("checked", false);
}
if (user.delivery_family_groups_add == "on") {

    $("#delivery_family_groups_add1").attr('checked', true);
}

if (user.delivery_family_groups_edit == "on") {

    $("#delivery_family_groups_edit1").attr('checked', true);
}
if (user.delivery_family_groups_delete == "on") {

    $("#delivery_family_groups_delete1").attr('checked', true);
}



if (user.delivery_sub_family_groups_view == "on") {

    $("#delivery_sub_family_groups_view1").attr('checked', true);
}
else
{
    $(".delivery_sub_family_groups_view1").attr('disabled', true);
    $(".delivery_sub_family_groups_view1").prop("checked", false);
}
if (user.delivery_sub_family_groups_add == "on") {

    $("#delivery_sub_family_groups_add1").attr('checked', true);
}

if (user.delivery_sub_family_groups_edit == "on") {

    $("#delivery_sub_family_groups_edit1").attr('checked', true);
}
if (user.delivery_sub_family_groups_delete == "on") {

    $("#delivery_sub_family_groups_delete1").attr('checked', true);
}

if (user.delivery_items_view == "on") {

    $("#delivery_items_view1").attr('checked', true);
}
else
{
    $(".delivery_items_view1").attr('disabled', true);
    $(".delivery_items_view1").prop("checked", false);
}
if (user.delivery_items_add == "on") {

    $("#delivery_items_add1").attr('checked', true);
}

if (user.delivery_items_edit == "on") {

    $("#delivery_items_edit1").attr('checked', true);
}
if (user.delivery_items_delete == "on") {

    $("#delivery_items_delete1").attr('checked', true);
}
////////////////////////////////////////
//stock management
if (user.stock == "on") {

    $("#stock2").attr('checked', true);
}

if (user.add_ingredients == "on") {

    $("#add_ingredient1").attr('checked', true);
}
if (user.edit_ingredients == "on") {

    $("#edit_ingredient1").attr('checked', true);
}
if (user.delete_ingredients == "on") {

    $("#delete_ingredient1").attr('checked', true);
}
if (user.view_ingredients == "on") {

    $("#view_ingredient1").attr('checked', true);
}
if (user.add_recipe == "on") {

    $("#add_recipe1").attr('checked', true);
}
if (user.edit_recipe == "on") {

    $("#edit_recipe1").attr('checked', true);
}
if (user.delete_recipe == "on") {

    $("#delete_recipe1").attr('checked', true);
}
if (user.view_recipe == "on") {

    $("#view_recipe1").attr('checked', true);
}



</script>
@stop