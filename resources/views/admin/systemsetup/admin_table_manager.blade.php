@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Table Management</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">
                <a href="/admin/table-manager/add-table" class="btn btn-primary btn-rounded pull-right" id=""><i
                        class="fa fa-plus"></i> Add Table</a>
                {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if (session('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-border custom-table m-b-0" id="testTable">
                        <thead>
                            <tr>
                                <th>S.No.</th>

                                <th>Name</th>
                                <th>Restaurant</th>
                                <th>Capacity</th>
                                <th>Waiter</th>
                                <th>Booking Status</th>
                                <th>Section</th>

                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count=1; ?>
                            @foreach($tables as $table)

                            <tr>
                                <td>{{$count}}</td>

                                <td>{{$table->name}}</td>

                                @foreach($restaurants as $restaurant)
                                @if($restaurant->id == $table->restaurant)

                                <td>{{$restaurant->name}}</td>
                                @break
                                @endif
                                @endforeach

                                <td>{{$table->capacity}}</td>

                                <td>{{$table->waiter}}</td>
                                <td>{{$table->booking_status}}</td>
                                <?php $se = ''; ?>
                                @foreach($printclasses as $print)
                                <?php $se = ''; ?>
                                @if($table->section == $print->id)
                                <td>{{$print->name}}</td>
                                @break
                                @else
                                <?php $se = "N/A"; ?>
                                @endif
                                @endforeach
                                <?php if($se != ''){ ?>
                                  <td><?= $se; ?></td>
                                <?php } ?>
                                <td class="text-right">

                                    <a href="/admin/table-manager/edit-table/{{$table->id}}" title="Delete"
                                        class="btn btn-warning custom"><i class="fa fa-pencil"></i></a>
                                    <a href="/admin/table-manager/delete-table/{{$table->id}}" title="Delete"
                                        class="btn btn-danger custom"><i class="fa fa-trash"></i></a>
                                    @if($table->is_active =='active')
                                    <input class="switch" type="checkbox" id="tooglebutton"
                                        checked data-toggle="toggle" data-on="Active" data-off="Inactive"
                                        data-onstyle="success" data-offstyle="danger" data-id="{{$table->id}}">
                                    @else
                                    <input class="switch" type="checkbox" id="tooglebutton"
                                        data-toggle="toggle" data-on="Active" data-off="Inactive"
                                        data-onstyle="success" data-offstyle="danger" data-id="{{$table->id}}">
                                    @endif
                                   
                                </td>
                            </tr>
                            <?php $count++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script>

</script>
@endsection
@section('javascript')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
$(document).ready(function() {
    $('#testTable').DataTable();
} );
$(function() {
    var userId;
    var status;
    $('.switch').on('change', function() {
        userId = $(this).data('id');
       
        if ($(this).prop("checked") == true) {
            status ='active';
        } else {
            status ='inactive';
        }


        $.ajax({
            url: '/admin/tableStaus/'+userId+'/'+status,
            type: 'get',
            async: true, //NOTE THIS
           
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },

            success: function(response) {
                console.log(response)
            },
            error: function(error) {

            }
        });


    });




});
</script>
@stop