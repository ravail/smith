@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Condiment Groups</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <a href="/admin/add-condiment-groups" class="btn btn-primary btn-rounded pull-right" id="groups_add"><i class="fa fa-plus"></i> Add Condiment Groups</a>
                            {{--<div class="view-icons">
                                    <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                    <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                                </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            
                                            <th>Title</th>
                                           
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                        @foreach($condiments as $condiment)

                                        <tr>
                                            <td>{{$count}}</td>
                                             
                                            <td>
                                                {{$condiment->name}}
                                            </td>
                                            
                                           
                                            <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="" id="groups_edit_{{$condiment->id}}"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                    <a class="dropdown-item" href="" id="groups_delete_{{$condiment->id}}"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $count++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@stop 


@section('javascript')

<script> 

var groups ={!! json_encode($condiments); !!};
//console.log(groups);
if('{{\Auth::user()->groups_add}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    $("#groups_add").hide();
}

if('{{\Auth::user()->groups_edit}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    //alert(majorGroups.length);
    for(var i = 0 ; i < groups.length ; i++){
        //alert(majorGroups.id);
        $("#groups_edit_"+groups[i].id).hide();
    }
}  

if('{{\Auth::user()->groups_delete}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    for(var i = 0 ; i < groups.length ; i++){

        $("#groups_delete_"+groups[i].id).hide();

    }
} 
</script>
@stop