@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @if (session('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif

            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Employees</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">
                <a href="/admin/add-employee" class="btn btn-primary btn-rounded pull-right"><i class="fa fa-plus"></i>
                    Add Employess</a>
                {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-border custom-table m-b-0" id="testTable">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>ID No.</th>

                                <th>Badge No.</th>
                                <th>Restaurant</th>
                                <th>Role</th>

                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $count=1; ?>
                            @foreach($employee as $e)
                            @if($e->user_type != 'admin')
                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$e->name}}</td>
                                <td>{{$e->id_number}}</td>
                                <td>{{$e->badge_number}}</td>
                        <?php  if($e->restaurant_id != null || $e->restaurant_id != ''){ ?>
                                @foreach($restaurants as $r)
                                
                                <?php if($r->id == $e->restaurant_id){?>
                                
                                <?php if($r->name != ''){ ?>
                                <td>{{$r->name}}</td>
                                <?php } else { ?>
                                <td>N/A</td>
                                
                                <?php } } ?>
                                

                                @endforeach
                        <?php } else { ?>
                        <td>N/A</td>
                        <?php } ?>
                                

                                @foreach($roles as $ro)
                                <?php if($ro->id == $e->role_id){?>

                                <td>{{$ro->name}}</td>
                                <?php break; } ?>

                                @endforeach

                                <td class="text-right">


                                    <a href="/admin/edit-employee-changepass/{{$e->id}}" title="change password"
                                        class="btn btn-warning custom"><i class="fa fa-key"></i></a>
                                    <a href="/admin/edit-employee/{{$e->id}}" title="Edit"
                                        class="btn btn-warning custom"><i class="fa fa-pencil"></i></a>
                                @if(\Auth::user()->employees_assign_table =='on' || \Auth::user()->user_type =='admin')
                                    @foreach($roles as $ro)
                                    <?php if($ro->id == $e->role_id){?>

                                    <?php if($ro->name == "waiter" ){ ?>
                                    <a href="#" data-id="{{$e->id}}" data-name="{{$e->name}}" id="myBtn"
                                        onclick="hello({{$e->id}},'{{$e->name}}')" class="btn btn-warning custom"><i
                                            class="fa fa-table"></i></a>

                                    <?php } break; } ?>
                                    @endforeach
@endif

                                    <a href="/admin/delete-employee/{{$e->id}}" class="btn btn-danger "><i
                                            class="fa fa-trash-o"></i></a>
                                   
                                   @if($e->is_active =='active')
                                    <input class="switch" type="checkbox" id="tooglebutton"
                                        checked data-toggle="toggle" data-on="Active" data-off="Inactive"
                                        data-onstyle="success" data-offstyle="danger" data-id="{{$e->id}}">
                                    @else
                                    <input class="switch" type="checkbox" id="tooglebutton"
                                        data-toggle="toggle" data-on="Active" data-off="Inactive"
                                        data-onstyle="success" data-offstyle="danger" data-id="{{$e->id}}">
                                    @endif
                                   

                                    
                                </td>
                            </tr>

                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Table Assignment</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Waiter Name:<span class="waiter_name"
                                            id="waiter_name"></span></label>
                                </div>
                                <input name='w_id' id="w_id" hidden>
                                <div class="form-group release_all_tables_div" style="display:none ">
                                    <label for="exampleInputEmail1"><a href="javascript:void(0)"
                                            class="btn btn-success release_all_tables">Release All Tables</a></label>
                                </div>
                                <div class="row">

                                    <div class="col-md-6 no-padding-h">
                                        <table class="table table-bordered table-hover assignmenttable"
                                            id="create_datatable">
                                            <thead>
                                                <tr>
                                                    <th>Asigned Table</th>
                                                </tr>
                                                <tr>
                                                    <td>Table Name</td>
                                                </tr>
                                            </thead>
                                            <tbody id="already_assigned_tr">

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 no-padding-h">
                                        <table class="table table-bordered table-hover assignmenttable"
                                            id="create_datatable">
                                            <thead>
                                                <tr>
                                                    <th>Table To Be Assign</th>
                                                </tr>
                                                <tr>
                                                    <td>Section:<select class="form-control select_section"
                                                            style="width: 80%;float: right;">
                                                            <option value="">Select section</option>
                                                            <option value="1">Sekenani Station</option>
                                                            <option value="2">Bandas And Coffee Garden</option>
                                                            <option value="3">Sepeko And Gazebbo</option>
                                                            <option value="4">Main Garden</option>
                                                            <option value="5">Butchery</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_for_sections">



                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@stop

@section('javascript')

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
$(document).ready(function() {
    $('#testTable').DataTable();
} );

$(function() {
    var userId;
    var status;
    $('.switch').on('change', function() {
        userId = $(this).data('id');
       
        if ($(this).prop("checked") == true) {
            status ='active';
        } else {
            status ='inactive';
        }


        $.ajax({
              
            url: '/admin/userStaus/'+userId+'/'+status,
            type: 'get',
            async: true, //NOTE THIS
           
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },

            success: function(response) {
                console.log(response)
            },
            error: function(error) {

            }
        });


    });




});


function hello(e_id, e_name) {

    // alert(e_name);

    $("#myModal").modal();

    $("#myModal").find('.waiter_name').text(e_name);
    $("#w_id").val(e_id);

    jQuery.ajax({
        url: '/admin/already-assigned',
        type: 'POST',
        data: {
            user_slug: e_name,
            type: 'assign',
            waiter_id: e_id
        },

        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function(response) {
            console.log(response);

            var data_obj = response;
            console.log(data_obj);
            var html_obj = '';
            // /alert(response.length);
            if (response.length > 0) {
                $("#already_assigned_tr").html('');
                $.each(data_obj, function(i, v) {
                    html_obj = '<tr id="tr_' + v.id + '"><td><input onchange="assigning_uncheck(' +
                        v.id + ')" class="already_assigned_checkbox" id="already_assigned_' + v.id +
                        '" name="tableids[' + v.id + ']" data="' + v.section + '" data-title="' + v
                        .name + '" checked="" type="checkbox"> &nbsp;&nbsp;&nbsp;' + v.name +
                        '</td></tr>';
                    $("#already_assigned_tr").append(html_obj);
                });
                $(".release_all_tables_div").css('display', '');
            } else {
                $("#already_assigned_tr").html('');
                $(".release_all_tables_div").css('display', 'none');
            }


        }
    });
}

// $('#myBtn').click(function() {
//     alert("hi");
//    id = $(this).attr('data-id');
//    name = $(this).attr('data-name'); 
//    alert(name);
//    $("#myModal").modal();
// });

// $('#myModal').on('show.bs.modal', function (e) {
//     //alert('hi');
//     $(this).find('.waiter_name').text(name);
//     $("#w_id").val(id);

// });

$(".select_section").change(function() {
    //alert("hi");

    manageTableBolckSection();

});


function assigning_check(table_id) {
    //add data to check start//
    var block_section = $("#assigning_" + table_id).attr('data');
    var table_name = $("#assigning_" + table_id).attr('data-title');
    var name = $("#waiter_name").text();
    var w_id = $("#w_id").val();

    $("#tr_" + table_id).remove();
    jQuery.ajax({
        url: '/admin/assign-or-remove-table-from-waiter',
        type: 'POST',
        data: {
            table_id: table_id,
            user_slug: name,
            type: 'assign',
            waiter_id: w_id,
            block_section: block_section
        },

        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function(response) {
            console.log(response);
            var res = parseInt(response);
            if (res == 1) {
                var html_obj = '<tr id="tr_' + table_id + '"><td><input onchange="assigning_uncheck(' +
                    table_id + ')" class="already_assigned_checkbox" id="already_assigned_' + table_id +
                    '" name="tableids[' + table_id + ']" data="' + block_section + '" data-title="' +
                    table_name + '" checked="" type="checkbox"> &nbsp;&nbsp;&nbsp;' + table_name +
                    '</td></tr>';
                $("#already_assigned_tr").append(html_obj);
                $(".release_all_tables_div").css('display', '');
            } else {
                alert('Table already assigned to another waiter');
            }


        }
    });
    //add data to check end//
}

$(".release_all_tables").click(function() {


    var isconfirmed = confirm("Do you want to clear all assigned table?");
    if (isconfirmed) {

        var selected_section = $(".select_section").val();
        var name = $("#waiter_name").text();
        var w_id = $("#w_id").val();

        jQuery.ajax({
            url: '/admin/release-all-table-from-waiter',
            type: 'POST',
            data: {
                table_id: 0,
                user_slug: name,
                type: 'removeall',
                waiter_id: w_id
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },

            success: function(response) {

                console.log(response);

                if (response == 'CANNOTREMOVE') {
                    alert('Can not remove the table assignment due to some pending order');
                    $("#already_assigned_" + table_id).prop('checked', true);
                } else {
                    $(".release_all_tables_div").css('display', 'none');
                    $("#already_assigned_tr").html('');
                    manageTableBolckSection();
                }

            }
        });
    }

});

function assigning_uncheck(table_id) {
    //add data to uncheck start//
    var block_section = $("#already_assigned_" + table_id).attr('data');
    var table_name = $("#already_assigned_" + table_id).attr('data-title');
    var selected_section = $(".select_section").val();
    var name = $("#waiter_name").text();
    var w_id = $("#w_id").val();

    jQuery.ajax({
        url: '/admin/remove-table-from-waiter',
        type: 'POST',
        data: {
            table_id: table_id,
            user_slug: name,
            type: 'remove',
            waiter_id: w_id,
            block_section: block_section
        },
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },

        success: function(response) {

            if (response == 'CANNOTREMOVE') {
                alert('Can not remove the table assignment due to some pending order');
                $("#already_assigned_" + table_id).prop('checked', true);
            } else {
                $("#tr_" + table_id).remove();
                if (selected_section == block_section) {
                    var html_obj = '<tr id="tr_' + table_id + '"><td><input onChange="assigning_check(' +
                        table_id + ')" class= "assigned_checkbox" id="assigning_' + table_id +
                        '" type="checkbox" data="' + block_section + '" data-title = "' + table_name +
                        '"> &nbsp;&nbsp;&nbsp;' + table_name + '</td></tr>';
                    $("#tbody_for_sections").append(html_obj);
                }

                var total_assigned = $("#already_assigned_tr tr").length;

                if (parseInt(total_assigned) == 0) {
                    $(".release_all_tables_div").css('display', 'none');
                }
            }





        }
    });
    //add data to uncheck end//
}

function manageTableBolckSection() {
    var selected_section = $(".select_section").val();
    var id = $("#w_id").val();
    var name = $("#waiter_name").text();

    //alert(selected_section);
    if (selected_section == '') {
        $("#tbody_for_sections").html('');
    } else {
        // alert("else");
        jQuery.ajax({
            url: '/admin/employees-table-assignment-with-user-section',
            type: 'POST',
            data: {
                section: selected_section,
                user_slug: name,
                user_id: id
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(response) {
                console.log(response);
                var data_obj = response;
                console.log(data_obj);
                var html_obj = '';
                $.each(data_obj, function(i, v) {
                    html_obj += '<tr id="tr_' + v.id + '"><td><input onChange="assigning_check(' + v
                        .id + ')" class= "assigned_checkbox" id="assigning_' + v.id +
                        '" type="checkbox" data="' + v.section + '" data-title = "' + v.name +
                        '"> &nbsp;&nbsp;&nbsp;' + v.name + '</td></tr>';
                });
                $("#tbody_for_sections").html(html_obj);
            }
        });
    }
}
</script>

@stop