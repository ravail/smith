@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Add Menu Item Group</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <form action="add-menu-item-groups" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label>
                                    <input class="form-control" type="text" id="name" name="name" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('timefrom') ? ' has-error' : '' }}">
                                    <label>Available From</label>
                                    <input id="timepicker" name="timefrom" placeholder="Available From" title="timepicker" style="width: 100%;" />
                                    
                                    @if ($errors->has('timefrom'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('timefrom') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('timeto') ? ' has-error' : '' }}">
                                    <label>Available To</label>
                                    <input id="timepicker1" name="timeto" placeholder="Available To" title="timepicker" style="width: 100%;" />                                    
                                    @if ($errors->has('timeto'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('timeto') }}</strong>
                                        </span>
                                    @endif
                            </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('sub_major_groups_id') ? ' has-error' : '' }}" required autofocus>
                                                <label>Sub Major Group Name</label>
                                                <select class="select" id="sub_major_groups_id" name="sub_major_groups_id">
                                                    <option>Select</option>
                                                   
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label>Description</label>
                                       
                                        <textarea class="form-control" rows="6"  placeholder="Description" id="description" name="description"></textarea>
                                        @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                            
                                        <div class="col-md-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="is_checked"> Another Layout
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                   
                                <div class="form-group{{ $errors->has('a_image') ? ' has-error' : '' }}">
                                    <label>Product Images</label>
                                    <div>
                                        <input class="form-control" required type="file" id="a_image" name="a_image" >
                                    </div>
                                    @if ($errors->has('a_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('a_image') }}</strong>
                                    </span>
                                    @endif
                                    
                                    {{-- <div class="row">
                                        <div class="col-md-3 col-sm-3 col-4 col-lg-3 col-xl-2">
                                            <div class="product-thumbnail" id="imgDiv">
                                                <img src="" class="img-thumbnail img-fluid" alt="">
                                                <span class="product-remove" title="remove"><i class="fa fa-close"></i></span>
                                            </div>
                                        </div>
                                        
                                    </div> --}}
                                </div>
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

    $(document).ready(function () {
        // create TimePicker from input HTML element
        $("#timepicker").kendoTimePicker({
                                                
        });
    });

     $(document).ready(function () {
        // create TimePicker from input HTML element
        $("#timepicker1").kendoTimePicker({
                                                
        });
    });
                                 
    var majorGroups ={!! json_encode($groups); !!};
  
    

//console.log(data);
var options = '<option value="">Sub Major Group Name</option>';
majorGroups.forEach(element => {
        options += '<option value='+element.id+'>'+element.name+'</option>';
       
        // $('#slots').append($('<option value='+element.start_slot+'-'+element.end_slot+'>'+element.start_slot+'-'+element.end_slot+'</option>')).hide().show();
        // 
    });
    $("#sub_major_groups_id")
       .html(options);
    //$('.select-slots').selectpicker('refresh');

  

    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

