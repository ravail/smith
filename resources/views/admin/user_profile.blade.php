@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")
 <div class="page-wrapper">
    
            <div class="content container-fluid">
                <div class="row">
                    <div class="col-sm-7 col-4">
                        <h4 class="page-title">My Profile</h4>
                    </div>

                    <div class="col-sm-5 col-8 text-right m-b-30">
                        <a href="/admin/edit/userProfiles" class="btn btn-primary btn-rounded"><i class="fa fa-plus"></i> Edit Profile</a>
                    </div>
                </div>
                
                <div class="card-box">
                    <form enctype="multipart/form-data" id="fileee" action="" method="POST">
                        @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-view">
                                <div class="profile-img-wrap">
                                    <div class="profile-img">
                                        <a href="#"><img id='preview' class="avatar" 
                                        @if($user->image != null) src='{{ url("/public/img/".$user->image) }}'@else src="/assets/img/user.jpg" @endif alt=""></a>
                                        <input type="file" onchange="PreviewImage()" id="my_file" name="user_file" style="display: none;" />
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="profile-info-left">
                                                <h3 class="user-name m-t-0 m-b-0">{{$user->name}}</h3>
                                                <small class="text-muted">{{$user->user_type}}</small>
                                                <div class="staff-id">Employee ID : {{$user->id}}</div>
                                                <!--<div class="staff-msg"><a href="chat.html" class="btn btn-primary">Send Message</a></div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <ul class="personal-info">
                                                <li>
                                                    <span class="title">Phone:</span>
                                                    <span class="text"><a href="#">{{$user->phone_number}}</a></span>
                                                </li>
                                                <li>
                                                    <span class="title">Email:</span>
                                                    <span class="text"><a href="#">{{$user->email}}</a></span>
                                                </li>
                                                <li>
                                                    <span class="title">Birthday:</span>
                                                    <span class="text">{{date("F jS, Y", strtotime($user->birth_date))}}</span>
                                                </li>
                                                <li>
                                                    <span class="title">Address:</span>
                                                    <span class="text">{{$user->address}}, {{$user->state}}, {{$user->country}}, {{$user->pin_code}}</span>
                                                </li>
                                                <li>
                                                    <span class="title">Gender:</span>
                                                    <span class="text">{{$user->gender}}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    @stop 
    @section('javascript')
<script type="text/javascript">
    
};
</script>
@stop  