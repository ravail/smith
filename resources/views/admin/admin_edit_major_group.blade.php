@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Add Major Group</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <form action="/admin/edit-major-group/edit/{{$existGroup->id}}" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Group Name</label>
                                <input class="form-control" type="text" id="name" name="name" value="{{$existGroup->name}}" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('display_order') ? ' has-error' : '' }}" required autofocus>
                                                <label>Display Order</label>
                                                <select class="select" id="display_order" name="display_order">

                                                    <option>Select</option>
                                                    <option value="1"{{$existGroup && $existGroup->display_order=='1' ? 'selected': ''}}>1</option>
                                                    <option value="2"{{$existGroup && $existGroup->display_order=='2' ? 'selected': ''}}>2</option>
                                                    <option value="3"{{$existGroup && $existGroup->display_order=='3' ? 'selected': ''}}>3</option>
                                                    <option value="4"{{$existGroup && $existGroup->display_order=='4' ? 'selected': ''}}>4</option>
                                                    <option value="5"{{$existGroup && $existGroup->display_order=='5' ? 'selected': ''}}>5</option>
                                                    <option value="6"{{$existGroup && $existGroup->display_order=='6' ? 'selected': ''}}>6</option>
                                                   
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>
                                <div class="form-group{{ $errors->has('a_image') ? ' has-error' : '' }}">
                                    <label>Product Images</label>
                                    <div>
                                        <input class="form-control" type="file" id="a_image" name="a_image" value="{{$existGroup->a_image}}">
                                    </div>
                                    @if ($errors->has('a_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('a_image') }}</strong>
                                    </span>
                                    @endif
                                    
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-4 col-lg-3 col-xl-2">
                                            <div class="product-thumbnail" id="imgDiv">
                                                <img src="{{URL::asset('img/'.$existGroup->a_image)}}" class="img-thumbnail img-fluid" alt="">
                                               
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>
   

    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

