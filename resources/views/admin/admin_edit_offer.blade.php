@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Edit Offer</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <form action="/admin/edit-offer/{{$existOffer->id}}" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label>
                                    <input class="form-control" type="text" id="name" name="name" value="{{$existOffer->name}}" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label>Price</label>
                                <input class="form-control" type="text" id="price" name="price" value="{{$existOffer->price}}" required autofocus>
                                    @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('max_selection_limit') ? ' has-error' : '' }}">
                                    <label>Max Selection Limit</label>
                                <input maxlength="255" placeholder="Selection Limit" required="" value="{{$existOffer->max_selection_limit}}" class="form-control valid" min="1" name="max_selection_limit" type="number" aria-required="true" aria-invalid="false">
                                    @if ($errors->has('max_selection_limit'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('max_selection_limit') }}</strong>
                                        </span>
                                    @endif
                            </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('major_group_name') ? ' has-error' : '' }}" required autofocus>
                                                <label>Sub Major Group Name</label>
                                                <select class="select" id="sub_major_group_name" name="sub_major_group_name">
                                                    <option>Select</option>
                                                    @foreach ($groups as $group)
                                                        <option value="{{ $group->id }}"{{$group && $group->id==$existOffer->sub_major_group_name ? 'selected': ''}}>{{ $group->name }}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label>Description</label>
                                       
                                        <textarea class="form-control" rows="6"  placeholder="Description" id="description" name="description">{{$existOffer->description}}</textarea>
                                        @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group{{ $errors->has('tax') ? ' has-error' : '' }}" required autofocus>
                                                    <label>Tax</label>
                                                    <select class="select" id="tax" name="tax">
                                                        <option>Select Tax</option>
                                                        @foreach ($taxes as $tax)
                                                        <option value="{{ $tax->id }}"{{$tax && $tax->id==$existOffer->tax ? 'selected': ''}}>{{ $tax->tax_name }}</option>
                                                         @endforeach
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                           
                                        </div>
                                <div class="form-group{{ $errors->has('a_image') ? ' has-error' : '' }}">
                                    <label>Product Images</label>
                                    <div>
                                        <input class="form-control" type="file" id="a_image" name="a_image" >
                                    </div>
                                    @if ($errors->has('a_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('a_image') }}</strong>
                                    </span>
                                    @endif
                                    
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-4 col-lg-3 col-xl-2">
                                            <div class="product-thumbnail" id="imgDiv">
                                                <img src="{{URL::asset('public/img/'.$existOffer->a_image)}}" class="img-thumbnail img-fluid" alt="">
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

//console.log(data);

    //$('.select-slots').selectpicker('refresh');

  

    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

