@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">PostPaid Open Orders</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 m-b-20">
                
                <div class="card-box">
                                        
                        <form action="/admin/orders/postpaidreportfilter" method="POST">
                            @csrf
                                            <div class="form-group row">
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker form-control" value="{{$start_date}}" placeholder="Start Date"  readonly="" id="start_date" name="start-date" type="text" required>
                                                </div>
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker1 form-control" value="{{$end_date}}" placeholder="End Date" id="end_date" readonly="" name="end-date" type="text" required>
                                                </div>
                                            </div>
                           
                            <div class="row">
                                    <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                            </div><br>
                             </form>
                            {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" onclick="javascript:xport.toXLS('testTable')"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            </button> --}}
                                            <a href="/post_paid_report.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>

                                            <br><br>

                {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                                
                            </div> --}}
                @if (session('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif
            </div>

        </div>
    </div>
    <!--<input id="myInput" type="text" placeholder="Search..">-->
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-border custom-table m-b-0" id="myTable">
                    <thead>
                        <tr>

                            <th>Order No</th>
                            <th>Date And Time</th>
                            <th>Table No</th>
                       
                            <th>Item description</th>
                            <th>Customer Description</th>
                            <th>Customer Instructions</th>
                            <th>Waiter</th>
                            <th>Restaurant</th>
                            <th>Total Amount</th>
                            <th>Status</th>

                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        @foreach($orders as $order)
                        <?php  
                            
                            $items_description = ""; 
                            $customer_description= $order->item_description;
                        
                        ?>
                        @foreach($order_items as $order_item)

                        <?php if($order_item->post_paid_orders_id == $order->id){ 
                            $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity .", " ;
                            
                        }
                        
                            ?>
                        
                        @endforeach
                          
                        <tr>
                            <td>{{$order->id}}</td>

                            <td> {{$order->date_time}}</td>

                            <td>
                                {{$order->table_no}}
                            </td>

                           
                            <td>{{$items_description}}</td>
                            <td>{{$customer_description}}</td>
                            <td>{{$order->comment}}</td>
                            <!--<td>{{$order->condiments}}</td>-->
                            <td>{{$order->waiter}}</td>
                            <td>{{$order->name}}</td>
                            <td>{{$order->total_amount}}</td>
                            <td>{{$order->status}}</td>
                            <td class="text-right">
                                <div class="dropdown dropdown-action">
                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        
                                        @if(\Auth::user()->master_bills_discount == 'on' || \Auth::user()->user_type == 'admin')

                                        <a class="dropdown-item"
                                            href="/admin/orders/postpaid/edit/{{$order->id}}/{{$order->total_amount}}"><i
                                                class="fa fa-pencil m-r-5"></i> Edit</a>
                                                @endif
                                              
                                        <a class="dropdown-item" onclick="printInvoice({{$order->bill_id}})" href=""><i
                                                class="fa fa-print m-r-5"></i> Print Bill</a>
                                        <a class="dropdown-item" onclick="printBill({{$order->id}})" href=""><i
                                                class="fa fa-trash-o m-r-5"></i> Print Docket</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

</div>
@stop
@section('javascript')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>

<script>

$(document).ready(function() {
    $('#myTable').DataTable();
} );

$(document).ready(function() {
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    
    $('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
}); 
});

function printInvoice(bill_id) {
    var confirm_text = 'bill';
    //alert(bill_id);
    var isconfirmed = confirm("Do you want to print " + confirm_text + "?");
    if (isconfirmed) {
        jQuery.ajax({
            url: '/admin/orders/print-master-bills',
            type: 'POST',
            async: false, //NOTE THIS
            data: {
                bill_id: bill_id
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },

            success: function(response) {

                console.log(response);
                var divContents = response;

                var printWindow = window.open('', '', 'width=400');
                printWindow.document.write(`<html><head><title></title>
                    <style>

                        
                        @page{margin:0;size:auto}

                    </style> `);
                printWindow.document.write('</head><body >');
                printWindow.document.write(divContents);
                printWindow.document.write('</body></html>');
                printWindow.focus();

                printWindow.print();
                printWindow.document.close();
                //window.print();

            }
        });
    }
}

function printBill(bill_id) {
    // alert(bill_id);
    var confirm_text = 'bill';
    //alert(bill_id);
    var isconfirmed = confirm("Do you want to print " + confirm_text + "?");
    if (isconfirmed) {
        jQuery.ajax({
            url: '/admin/orders/print-docket',
            type: 'POST',
            async: false, //NOTE THIS
            data: {
                bill_id: bill_id
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },

            success: function(response) {

                //console.log(response);
                var divContents = response;
                //alert(response);
                var printWindow = window.open('', '', 'width=400');
                printWindow.document.write(`<html><head><title></title>
        
            <style>
                @media print {
                html, body {
                    width: auto;
                    height: auto;        
                }
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }
            @page{margin:0;size:auto}
            </style>

        `);
                printWindow.document.write('</head><body>');
                printWindow.document.write(divContents);
                printWindow.document.write('</body></html>');
                console.log(printWindow);
                printWindow.focus();
                printWindow.print();
                printWindow.document.close();


            }
        });
    }
}
</script>

@stop