{{-- <!doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta charset="utf-8">
    {{-- <title>A simple, clean, and responsive HTML invoice template</title> --}}
    
   <!-- <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head> -->

{{-- <body> --}}
    <div class="book">
        <div class="page" >
            <div class="subpage" style="text-align:center">
    
     
        <strong>Order No : {{$postOrder->id}}</strong><br>
        <strong>Table No : {{$postOrder->table_no}}</strong><br>
        <strong>Date  : {{$dd}} {{$time}} {{$dt}}</strong><br>

        @foreach($print_classes as $print_class)

            <?php if($print_class->id == $postOrder->print_class_id) {  ?>
            
                
            <strong>Print Class Name : {{$print_class->name}}</strong><br>
                

            <?php } ?>

        @endforeach

        
        <strong>Waiter Name : {{$postOrder->waiter}}</strong><br>
        <!--<table>-->
        <!--    <tbody style="text-align:center">-->
        @foreach($items as $item)

            <?php if($item->post_paid_orders_id == $postOrder->id) {  ?>
            
             <!--  <tr> <td>-->
                {{$item->quantity}} {{$item->name}} ({!! strlen($item->comments) < 15 ? $item->comments :  nl2br(wordwrap($item->comments,25,"\n")) !!})
           </br>  
                

            <?php } ?>

        @endforeach
        <tbody>
    </table>
        </div>
    </div>
    </div>
{{-- </body>

</html> --}}
