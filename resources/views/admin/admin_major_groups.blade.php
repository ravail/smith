@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                       
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Major Group Manager</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <a href="add-major-group" class="btn btn-primary btn-rounded pull-right" id='major_groups_manager_add'><i class="fa fa-plus"></i> Add Major Group</a>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                          
                        </div>
                        
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                        
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                     @endif
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Order</th>
                                           
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                        @foreach($groups as $group)

                                        <tr>
                                            <td>{{$count}}</td>
                                            
                                            <td>
                                                <div class="product-det">
                                                    <img src="{{URL::asset('public/img/'.$group->a_image)}}" alt="">
                                                    {{-- <div class="product-desc">
                                                    <h2><a href="#">{{$group->name}}</a></h2>
                                                    </div> --}}
                                                </div>
                                            </td>
                                            <td>
                                                {{$group->name}}
                                            </td>
                                            
                                        <td>{{$group->display_order}}</td>
                                            
                                            <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="/admin/edit-major-group/edit/{{$group->id}}" id="major_groups_manager_edit_{{$group->id}}"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                        <a class="dropdown-item" href="/admin/major-group/delete/{{$group->id}}" id="major_groups_manager_delete_{{$group->id}}"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $count++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop  

@section('javascript')

<script> 

var majorGroups ={!! json_encode($groups); !!};
    // /console.log(majorGroups);
if('{{\Auth::user()->major_groups_manager_add}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    $("#major_groups_manager_add").hide();
}

if('{{\Auth::user()->major_groups_manager_edit}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    //alert(majorGroups.length);
    for(var i = 0 ; i < majorGroups.length ; i++){
       // alert(majorGroups.id);
        $("#major_groups_manager_edit_"+majorGroups[i].id).hide();

    }
}  

if('{{\Auth::user()->major_groups_manager_delete}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    for(var i = 0 ; i < majorGroups.length ; i++){

        $("#major_groups_manager_delete_"+majorGroups[i].id).hide();

    }
} 

</script>

@stop