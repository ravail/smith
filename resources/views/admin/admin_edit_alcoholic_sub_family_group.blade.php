@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Edit Alcoholic Sub Family Group</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                        <form action="/admin/edit-alcoholic-sub-family-group/{{$existGroup->id}}" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label>
                                <input class="form-control" type="text" id="name" name="name" value="{{$existGroup->name}}" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('family_groups_id') ? ' has-error' : '' }}" required autofocus>
                                                <label>Alcoholic Family Group Name</label>
                                                <select class="select" id="family_groups_id" name="family_groups_id">
                                                    <option>Select</option>
                                                    @foreach ($familygroups as $familygroup)
                                                        <option value="{{ $familygroup->id }}"{{$familygroup && $familygroup->id==$existGroup->family_groups_id ? 'selected': ''}}>{{ $familygroup->name }}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label>Description</label>
                                           
                                    <textarea class="form-control" rows="6"  placeholder="Description" id="description" name="description">{{$existGroup->description}}</textarea>
                                            @if ($errors->has('description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('gl_accounts') ? ' has-error' : '' }}" required autofocus>
                                                <label>GL Account No</label>
                                                <select class="select" id="gl_accounts" name="gl_accounts">
                                                    <option>Select</option>
                                                    @foreach ($accounts as $account)
                                                        <option value="{{ $account->id }}"{{$account && $account->id==$existGroup->gl_accounts ? 'selected': ''}}>{{ $account->account_number }}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>

                                   
                                <div class="form-group{{ $errors->has('a_image') ? ' has-error' : '' }}">
                                    <label>Product Images</label>
                                    <div>
                                        <input class="form-control" type="file" id="a_image" name="a_image" >
                                    </div>
                                    @if ($errors->has('a_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('a_image') }}</strong>
                                    </span>
                                    @endif
                                    
                                    {{-- <div class="row">
                                        <div class="col-md-3 col-sm-3 col-4 col-lg-3 col-xl-2">
                                            <div class="product-thumbnail" id="imgDiv">
                                                <img src="" class="img-thumbnail img-fluid" alt="">
                                                <span class="product-remove" title="remove"><i class="fa fa-close"></i></span>
                                            </div>
                                        </div>
                                        
                                    </div> --}}
                                </div>
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

   

//console.log(data);


    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

