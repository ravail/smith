@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Void Items</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                       <div class="row">
                        <div class="col-md-12">
                    
                        @if (session('message'))
                        <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                        {{ session('message') }}
                        </div>
                 @endif
                        
                    </div>
                </div>
                     <input id="myInput" type="text" placeholder="Search..">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0" id="testTable">
                                    <thead>
                                        <tr>
                            
                                            <th>Sr.No</th>
                                            <th>Order ID</th>
                                            <th>Item Id</th>
                                            <th>Item Name</th>
                                            <th>Amount</th>
                                            <th>Quantity</th>
                                            <th>Void Reason</th>
                                            <th>Manager Comment</th>
                                            <th>Waiter</th>
                                           
                                            
                                            <th>Status</th>
                                            
                                            
                                           
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                        <?php $count = 1 ; ?>
                                        @foreach($orders as $order)
                                       <tr>
                                           <td>{{$count++}}</td>
                                       
                                      
                                            <td>{{$order->order_id}}</td>
                                            
                                        <td> {{$order->item_id}}</td>
                                            
                                            <td>
                                                    {{$order->item_name}}
                                            </td>
                                               
                                            <td>
                                                    {{$order->amount}}
                                            </td>
                                    <td>
                                                    {{$order->quantity}}
                                            </td>        
                                        <td>{{$order->comments}}</td>
                                        <td>{{$order->manager_comment}}</td>
                                        <td>{{$order->waiter_name}}</td>
                                      
                                        <td>{{$order->status}}</td>
                                @if($order->status == "Voided")        
                                  <td class="text-right">
                                             --
                                            </td>
                                @elseif($order->status == "NotVoided")
                                <td class="text-right">
                                             --
                                            </td>
                                @elseif($order->status == "Pending")
                                 <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" id="void_accept" href="/admin/orders/add-void-items/{{$order->order_id}}/{{$order->item_id}}"><i class="fa fa-pencil m-r-5"></i> Void Item</a>
                                                        <a class="dropdown-item" id="void_reject" href="/admin/orders/reject-voidItems/NotVoided/{{$order->order_id}}/{{$order->item_id}}/{{$order->item_name}}"><i class="fa fa-trash-o m-r-5"></i> Reject Void Request</a>
                                                    </div>
                                                </div>
                                            </td>
                                
                                @endif
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop  
@section('javascript')

<script> 
$(document).ready(function() {
    $('#testTable').DataTable();
} );
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

if('{{\Auth::user()->void_accept}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#void_accept").hide();
        }
if('{{\Auth::user()->void_reject}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#void_reject").hide();
        }
</script>
@stop