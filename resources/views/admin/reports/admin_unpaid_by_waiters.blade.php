@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Unpaid By Waiters</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-12">
                                    <div class="card-box">
                                        
                                        <form action="/admin/reports/unpaidwaiterReportFilter" method="post">
                                              @csrf
                                            <div class="form-group row">
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker form-control" placeholder="Start Date" required="" readonly="" id="start_date" name="start_date" type="text" value="{{$start_date}}">
                                                </div>
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker1 form-control" placeholder="End Date" id="end_date" required="" readonly="" name="end_date" type="text" value="{{$end_date}}">
                                                </div>
                                            </div>
                                    
                                            <div class="row">
                                                <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                          
                                                </div><br>
                                        </form>       
                                            {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" onclick="javascript:xport.toXLS('testTable')"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            </button> --}}
                                            <a href="/unpaid_waiter_reports.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>
                                             <a href="/pdf/examples/unpaid_waiters.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>


                                            <br><br>
                            <div class="table-responsive">
                       
                                <table class="table table-border custom-table m-b-0" id="testTable">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>	Bill No.</th>
                                            <th> Date </th>
                                            
                                            <th>	Waiter Name </th>
                                            <th> Bill Amount </th>
                                            

                                            {{-- <th>	VAT</th>
                                            <th>	CATERING LEVY</th>
                                            <th>	Taxes</th> --}}
                                            {{-- <th>	Net Sales % Of Ttl</th> --}}
                                       
                                        </tr>
                                    </thead>
                                    <tbody id="testTable1">

                                        <?php $count = 1; 
                                         $total =0;?>
                                        @foreach($bills as $bill)
                                        <tr>
                                            <td>
                                                {{$count}}
                                            </td>

                                            <td>
                                                {{$bill->id}}
                                            </td>
                                            <td>
                                                {{$bill->date}}
                                            </td>
                                            <td>
                                                {{$bill->waiter_name}}
                                            </td>
                                            <td>
                                                {{$bill->total_amount}}
                                                <? 
                                               
                                                $total =$total + $bill->total_amount;?>
                                            </td>

                                        </tr>
                                        <?php $count++; ?>
                                        @endforeach
                                        <tr><td></td><td></td><td></td><td></td><td></td></tr>
                                            <tr> <td>Total Bills Amount</td><td></td><td></td><td></td><td><span><strong>{{$total}}.00</strong></span></td> </tr>
                                    </tbody>
                                        

                                </table>
                                  
                            
                            </div>
                                    </div>
                            </div>
                    </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop    

@section('javascript')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>
<script>
$(document).ready(function() {
    $('#testTable').DataTable();
} );

$(document).ready(function() {
    $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#testTable1 tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
$('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
}); 
});   

   

function updateTable(){

//alert("hi");
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var count=1; var gross = 0.00 ;

    var c = 0 ; var previousItems = new Array();
//alert("sad");
    jQuery.ajax({
        //alert("fsd"); 
         url : '/admin/reports/unpaidwaiterReportFilter',
         type : 'POST' ,
         async : false ,   //NOTE THIS
         data : { start_date : start_date , end_date : end_date  } ,
         headers : {'X-CSRF-TOKEN': '{{ csrf_token() }}' } ,
         
        success: function (response) {

        $('#testTable>tbody').empty();

            if(response != null){
                
                //console.log(response);
                var check = 1;

                for(var i = 0 ; i < response.length ; i++ ){ 

                    tr = '<tr>' +

                        '<td>' + check + '</td>' +
                        '<td>' + response[i]['id'] + '</td>' +
                        '<td>' + response[i]['date'] + '</td>' +
                        '<td>' + response[i]['waiter_name'] + '</td>' +
                        '<td>' + response[i]['total_amount'] + '</td>' +
                        '</tr>';

                    check++; 

                    $('#testTable').append(tr);

                }
                    
            }
       }

    });

}


 
    ///////////////////////////////

var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>

@stop