@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Void Items</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-12">
                                    <div class="card-box">
                                        
                           <form action="/admin/reports/voidReportFilter" method="POST">
                            @csrf
                <div class="form-group row">
                        <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker form-control" placeholder="Start Date" value="{{$start_date}}" required="" readonly="" id="start_date" name="start_date" type="text">
                                                </div>
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker1 form-control" placeholder="End Date" value="{{$end_date}}" id="end_date" required="" readonly="" name="end_date" type="text">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                          
                                                </div><br>
                                           
                                            {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" onclick="javascript:xport.toXLS('testTable')"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            </button> --}}
                                            <a href="/void_report.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>

                                    </form>

                                            <br><br>
                                            <!--<input id="myInput" type="text" placeholder="Search..">-->
                                            <table class="table table-border custom-table m-b-0" id="testTable">
                                                <thead>
                                                    <tr>
                                        
                                                        <th>Sr.No</th>
                                                        <th>Order ID</th>
                                                        <th>Item Id</th>
                                                        <th>Item Name</th>
                                                        <th>Amount</th>
                                                        <th>Void Reason</th>
                                                        <th>Manager Comment</th>
                                                        <th>Waiter</th>
                                                       
                                                        
                                                        <th>Status</th>
                                                        <th>Voided/Rejected By</th>
                                                        
                                                        
                                                       
                                                       
                                                    </tr>
                                                </thead>
                                                <tbody id="myTable">
                                                    <?php $count = 1 ; ?>
                                                    @foreach($orders as $order)
                                                   <tr>
                                                       <td>{{$count}}</td>
                                                   
                                                  
                                                        <td>{{$order->order_id}}</td>
                                                        
                                                    <td> {{$order->item_id}}</td>
                                                        
                                                        <td>
                                                                {{$order->item_name}}
                                                        </td>
                                                           
                                                        <td>
                                                                {{$order->amount}}
                                                        </td>
                                                        
                                                    <td>{{$order->comments}}</td>
                                                    <td>{{$order->manager_comment}}</td>
                                                    <td>{{$order->waiter_name}}</td>
                                                  
                                                    <td>{{$order->status}}</td>
                                                    <td>{{$order->voided_name}}</td>
                                        
                                                    </tr>
                                                    @endforeach
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                            </div>
                    </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop    

@section('javascript')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://www.olarroerp.digitalsystemsafrica.com/assets/jss/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://www.olarroerp.digitalsystemsafrica.com/assets/csss/bootstrap-datetimepicker.css">

<script>
$(document).ready(function() {
    $('#testTable').DataTable();
} );
$(document).ready(function() {
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

$('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
}); 
});   

 

function updateTable(){

//alert("hi");
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var count=1; var gross = 0.00 ;

    var c = 0 ; var previousItems = new Array();
//alert("sad");
    jQuery.ajax({
        //alert("fsd"); 
         url : '/admin/reports/voidReportFilter',
         type : 'POST' ,
         async : false ,   //NOTE THIS
         data : { start_date : start_date , end_date : end_date  } ,
         headers : {'X-CSRF-TOKEN': '{{ csrf_token() }}' } ,
         
        success: function (response) {
console.log(response);
        $('#testTable>tbody').empty();

            if(response != null){
                
                //console.log(response);
                var check = 1;

                for(var i = 0 ; i < response.length ; i++ ){ 


                    tr = '<tr>' +

                        '<td>' + check + '</td>' +
                        '<td>' + response[i]['order_id'] + '</td>' +
                        '<td>' + response[i]['item_id'] + '</td>' +
                        '<td>' + response[i]['item_name'] + '</td>' +
                        '<td>' + response[i]['amount'] + '</td>' +
                        '<td>' + response[i]['comments'] + '</td>' +
                        '<td>' + response[i]['manager_comment'] + '</td>' +
                        '<td>' + response[i]['waiter_name'] + '</td>' +
                        '<td>' + response[i]['status'] + '</td>' +
                        
                        '</tr>';

                    check++; 

                    $('#testTable').append(tr);

                }
                    
            }
       }

    });

}


 
    ///////////////////////////////

var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>

@stop