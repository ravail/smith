@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Location Wise report</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    
                    <form action="/admin/Locationwise" method="post">
                        @csrf
                        <div class="form-group row">
                            
                            <div class="col-sm-3 input-group input-append" >
                                                <input class="datepicker form-control" placeholder="Start Date" value="{{$start_date}}" required="" readonly="" id="start_date" name="start_date" type="text">
                                            </div>
                                            <div class="col-sm-3 input-group input-append" >

                                                    <input class="datepicker1 form-control" placeholder="End Date" value="{{$end_date}}" id="end_date" required="" readonly="" name="end_date" type="text">
                                                </div>
                            
                            <div class="col-sm-3 input-group input-append">
                                <select class="form-control"  required=""  id="print_class" name="print_class">
                                      <option value="null" {{ ($class1 == null) ? 'selected':''}} >Select Station </option>
                                    @foreach($print_classes as $key => $value)
                                    <option value="{{$value->name}}"  {{ ($value->name == $class1 )  ? 'selected':''}}>{{$value->name}} </option>
                                    @endforeach
                                    <option value="Central Store" >Central Store</option>
                                </select>
                                
                            </div></div>
                        <div class="row">
                            
                            
                             

                            <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>

                        </div><br>
                    </form>

          
                   


                    <br><br>
                    <div class="table-responsive">
                      
                        <table class="table table-border custom-table m-b-0" id="testTable">
                                    <thead>
                                        <tr>
                                            <th>IG ID</th>
                                            <th>IG Name</th>
                                            <th>Unit</th>
                                            
                                            <th>System Quantity Before adjustment</th>
                                             <th>Ingredient Stock Added (C.Store)</th>
                                            <th>System Sales</th>
                                            <th>Store to Station Issues</th>
                                            <!--<th>Physical Quantity In Central Store</th>-->
                                            <th>Physical Quantity In All Locations</th>
                                            <th>Location</th>
                                            <th>Voids</th>
                                            <th>Edited Physical Stock</th>
                                            <th>Station to Station Transfer</th>
                                            <!--<th>Variance Central Store</th>-->
                                            <th>Variance</th>
                                             <th>Overall Consumption</th>
                                             <th>Current System Quantity</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody id="testTable1">
                                            <?php $count=1; ?>
                                        @foreach($stocks as $stock)
                                            
                                       
                                           
 
                                                <tr>
                                            
                                                    <td>{{$stock->ingredient_id}}</td>
                                                    <td>{{$stock->ingredient_name}}</td>
                                                    
                                                    
                                                    <td>{{$stock->info['quantity_type']}}</td>
                                                  
                                                    
                                                    
                                                    <td> @if(isset($stock->fourth_col))
                                                      {{$stock->fourth_col}} 
                                                      
                                                      @endif</td>
                                                    
                                                      <td>0</td>
                                                      <td> 
                                                      
                                                      {{$stock->system_sale}}
                                                      
                                                      </td>
                                                    <!--<td>{{$stock->info['quantity']}}</td>-->
                                                    <!--<td>-->
                                                        
                                                        
                                                        
                                                    <!--     @if(isset($stock->cs['quantity']))-->
                                                        
                                                    <!--   {{$stock->cs['quantity']}}-->
                                                        
                                                    <!--    @else-->
                                                        
                                                    <!--    ADD IN PHYSICAL STOCK-->
                                                        
                                                    <!--    @endif-->
                                                        
                                                        
                                                    <!--    </td>-->
                                                   <td>@if(isset($stock->seventh_col))
                                                    
                                                    {{$stock->seventh_col}}
                                                    
                                                    @endif</td>
                                                    
                                                    <td>@if(isset($stock->eighth_col))
                                                    
                                                    {{$stock->eighth_col}}
                                                    
                                                    @endif</td>
                                                    <td>{{$stock->print_class}}</td>
                                                    <td>@if(isset($stock->tenth_col))
                                                    
                                                    {{$stock->tenth_col}}
                                                    
                                                    @endif</td>
                                                    <td>@if(isset($stock->eleventh_col))
                                                    
                                                    {{$stock->eleventh_col}}
                                                    
                                                    @endif</td>
                                                    <!--<td>{{$stock->reduced_quantity}}</td>-->
                                                    <td>@if(isset($stock->twelfth_col))
                                                    
                                                    {{$stock->twelfth_col}}
                                                    
                                                    @endif</td>
                                                    <td>
                                                        
                                                        
                                                       
                                                        
                                                         @if(isset($stock->thirteenth_col))
                                                        
                                                         {{($stock->thirteenth_col)}}
                                                         
                                                         @endif
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        </td>
                                                   <!-- <td>
                                                        
                                                         @if(isset($stock->ps['quantity']))
                                                        
                                                        {{$stock->ps['quantity']-$stock->quantity_added}}
                                                        
                                                        @else
                                                        
                                                        ADD IN PHYSICAL STOCK
                                                        
                                                        @endif
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        </td>-->
                                                    
                                                    
                                                      <td>@if(isset($stock->fourteenth_col))
                                                        
                                                         {{$stock->fourteenth_col}}
                                                        
                                                       
                                                       
                                                        
                                                        @endif</td>
                                                      
                                                         <td>@if(isset($stock->last_col))
                                                        
                                                         {{$stock->last_col}}
                                                        
                                                       
                                                       
                                                        
                                                        @endif</td>
                                                        
                                                        
                                                
                                                </tr>
                                                <?php $count++; ?>
                                        @endforeach
                                        
                                        
                                         @foreach($ing as $in)
                                            
                                       
                                           
 
                                                <tr>
                                            
                                                    <td>{{$in->id}}</td>
                                                    <td>{{$in->name}}</td>
                                                    
                                                    
                                                    <td>{{$in->quantity_type}}</td>
                                                    <td>@if(isset($in->fourth_col))
                                                      {{$in->fourth_col}} 
                                                      
                                                      @endif</td>
                                                    
                                                    <td>@if(isset($in->fifth_col_original))
                                                    
                                                    {{$in->fifth_col_original}}
                                                    
                                                    @endif</td>
                                                    
                                                     <td> 
                                                      
                                                        
                                                      </td>
                                                   
                                                    <td>
                                                        
                                                    
                                                        
                                                        
                                                        </td>
                                                        <td>
                                                             @if(isset($in->eighth_col))
                                                    
                                                    {{$in->eighth_col}}
                                                    
                                                    @endif </td>
                                                        
                                                    <td>Central Store</td>
                                                    <td></td>
                                                    <td>
                                                        
                                                        @if(isset($in->eleventh_col))
                                                    
                                                    {{$in->eleventh_col}}
                                                    
                                                    @endif
                                                        
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        
                                                        @if(isset($in->thirteenth_col))
                                                        
                                                         {{($in->thirteenth_col)}}
                                                         
                                                         @endif
                                                        
                                                        
                                                        
                                                    </td>
                                                    <td>
                                                        
                                                        
                                                       @if(isset($in->fourteenth_col))
                                                        
                                                         {{$in->fourteenth_col}}
                                                        
                                                       
                                                       
                                                        
                                                        @endif
                                                        
                                                      
                                                        
                                                        
                                                        
                                                        </td>
                                            
                                                    
                                                    
                                                     
                                                          <td>@if(isset($in->last_col))
                                                        
                                                         {{$in->last_col}}
                                                        
                                                       
                                                       
                                                        
                                                        @endif</td>
                                                        
                                                        
                                                    
                                                        
                                                        
                                                
                                                </tr>
                                                <?php $count++; ?>
                                        @endforeach
                                        
                                        
                                    </tbody>
                                </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
@stop

@section('javascript')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://www.olarroerp.digitalsystemsafrica.com/assets/jss/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://www.olarroerp.digitalsystemsafrica.com/assets/csss/bootstrap-datetimepicker.css">


<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<!--<script src='build/pdfmake.min.js'></script>-->
 	<!--<script src='build/vfs_fonts.js'></script>-->
<script>
$(document).ready(function() {
	// Function to convert an img URL to data URL
	
	// DataTable initialisation
	$('#testTable').DataTable(
		{
			"dom": '<"dt-buttons"Bf><"clear">lirtp',
			"paging": true,
			"pageLength": 50,
			"autoWidth": true,
			"buttons": [
				{
					text: 'Export PDF',
					extend: 'pdfHtml5',
					filename: 'dt_custom_pdf',
					orientation: 'landscape', //portrait
					pageSize: 'A4', //A3 , A5 , A6 , legal , letter
					exportOptions: {
					    
						columns: ':visible',
						search: 'applied',
						order: 'applied'
					},
					customize: function (doc) {
				
						doc.content.splice(0,1);
					
				
				
						
						doc.pageMargins = [20,100,20,30];
					
						doc.defaultStyle.fontSize = 11;
					
						doc.styles.tableHeader.fontSize = 11;
						
						
			
					
						
						var objLayout = {};
						objLayout['hLineWidth'] = function(i) { return .5; };
						objLayout['vLineWidth'] = function(i) { return .5; };
						objLayout['hLineColor'] = function(i) { return '#aaa'; };
						objLayout['vLineColor'] = function(i) { return '#aaa'; };
						objLayout['paddingLeft'] = function(i) { return 4; };
						objLayout['paddingRight'] = function(i) { return 4; };
						doc.content[0].layout = objLayout;
				}
				},
				{
				    	text: 'Export CSV',
					extend: 'csv'
				}]
		});
});
$(document).ready(function() {
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#testTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $('.datepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1
    });
    $('.datepicker1').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1
    });

});

</script>

@stop