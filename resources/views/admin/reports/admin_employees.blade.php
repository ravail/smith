@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Employees</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <a href="add-major-group" class="btn btn-primary btn-rounded pull-right"><i class="fa fa-plus"></i> Add Employess</a>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Name</th>
                                            <th>ID No.</th>
                                            
                                            <th>Badge No.</th>
                                            <th>Restaurant</th>
                                            <th>Role</th>
                                            <th>Date Emp.</th>
                                          
                                            					
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                        

                                        <tr>
                                            <td>1</td>
                                            <td>Asasas </td>
                                            <td>78656</td>  
                                            <td>89807 </td>
                                            <td>The Smith Hotels</td>
                                            <td>Cashier</td>
                                            <td>21/11/2018</td>
                                            
                                            <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href=""><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                        <a class="dropdown-item" href=""><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
@stop            