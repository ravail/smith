@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Complementary Reports With Order</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                        
			 		

                                            <th>Order.No.</th>
                                            <th>Date & Time</th>
                                          
                                            <th>Item Desc</th>
                                            <th>Price</th>
                                            <th>Reason Complementary</th>
                                            <th>Employee Name</th>
                                            <th>Family Groups</th>
                                            <!-- <th class="text-right">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                       

                                        <tr>
                                           
                                            <td> 00293</td>
                                            <td>2018-08-08 04:19 PM</td>
                                            <td>tem: KUKU BROILER DRY FRY
Qty: 1 ,
Item: The Smith Famous Platter (LARGE)
Qty: 1 ,
Item: Glass of Juice
Qty: 1 ,
Item: Famous Ugali
Qty: 5 ,
Item: Kienyeji
Qty: 1 ,
Item: Mineral Water 500ml
Qty: 5 ,
Item: Spinach
Qty: 2</td>
                                            <td>6,600.00</td>
                                            <td></td>
                                            <td>SYLVIA WAMAITHA</td>
                                            <td>COFFEE, TEA & PASTRY, PLATTERS, SIDE ORDERS, MINERAL WATER, BUTCHERY</td>
                                           




                                            <!-- <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href=""><i class="fa fa-pencil m-r-5"></i> Print</a>
                                                       
                                                    </div>
                                                </div>
                                            </td> -->
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop            