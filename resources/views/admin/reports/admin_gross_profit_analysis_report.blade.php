@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Gross Profit Analysis report</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                    <form action="/admin/grossProfitAnalysis" method="post">
                        @csrf
                        <div class="form-group row">
                        <div class="col-sm-3 input-group ">
                            <input class="datepicker form-control" placeholder="Start Date" value="{{$start_date}}" required="" readonly=""
                                id="start_date2" name="start_date" type="text">
                        </div>
                        <div class="col-sm-3 input-group ">
                            <input class="datepicker1 form-control" placeholder="End Date" value="{{$end_date}}" id="end_date2" required=""
                                readonly="" name="end_date" type="text">
                        </div>
        
                    </div>
                     
                        <div class="row">
                            <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>

                        </div><br>
                    </form>
                    <a href="/download_gross_profit_report.php" target="_blank" title="Export In Excel" class="btn btn-warning" value="xls" id="btnExport"><i class="fa fa-file-excel-o"
                            aria-hidden="true"></i> </a>


                    <br><br>
                    <div class="table-responsive">
                          <input id="myInput" type="text" placeholder="Search..">
                        @php
                            $cost = 0;
                            $qty = 0;
                            $t_sale = 0;
                            $t_cost = 0;
                            $profit = 0;
                            $margin = 0;
                            $count = 0;
                        @endphp
                        <table class="table table-border  custom-table m-b-0" id="testTable">
                            <thead>
                                <tr>
                                    <th class="text-center">CODE</th>
                                    <th class="text-center">PRICE</th>
                                    <th class="text-center">COST</th>
                                    <th class="text-center">QTY SOLD</th>
                                    <th class="text-center">T: SALE</th>
                                    <th class="text-center">T: COST</th>
                                    <th class="text-center">PROFIT</th>
                                    <th class="text-center">MARGIN</th>
                                  

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                @foreach($general_items as $general_item)
                                <tr>
                                    @if (array_key_exists('simple', $general_item))
                                    <td colspan='8' class="font-weight-bold">{{$general_item['simple']['name']}}</td>
                                        
                                    @else
                                    <td colspan='8' class="font-weight-bold">{{$general_item['discount']['name']}}</td>
                                    @endif
                                          
                                </tr>
                    
                                @if (array_key_exists('simple', $general_item))
                                <tr>
                                 
                                    
                                    <td class="text-center">{{$general_item['simple']['id']}}</td>
                                    <td class="text-center">{{$general_item['simple']['price']}}</td>
                                    @if ($general_item['simple']['cost'] == null)
                                    <td class="text-center">0</td>
                                    @else
                                    <td class="text-center">{{$general_item['simple']['cost']}}</td>
                                    @endif
                                    <td class="text-center">{{$general_item['simple']['quantity_sold']}}</td>
                                    <td class="text-center">{{$general_item['simple']['total_sale']}}</td>
                                    <td class="text-center">{{$general_item['simple']['total_cost']}}</td>
                                    <td class="text-center">{{$general_item['simple']['profit']}}</td>
                                    <td class="text-center">{{$general_item['simple']['margin']}}</td>  
                                    
                                </tr>
                                   @php
                                        $cost += (double)$general_item['simple']['cost'];
                                        $qty += (double)$general_item['simple']['quantity_sold'];
                                        $t_sale += (double)$general_item['simple']['total_sale'];
                                        $t_cost += (double)$general_item['simple']['total_cost'];
                                        $profit += (double)$general_item['simple']['profit'];
                                        $margin += (double)$general_item['simple']['margin'];
                                        $count = $count+1 ;
                                    @endphp
                               
                                @endif
                                
                                @if (array_key_exists('discount', $general_item))
                                <tr>
                                 
                                    
                                    <td class="text-center">{{$general_item['discount']['id']}}</td>
                                    <td class="text-center">{{$general_item['discount']['price']}}</td>
                                    @if ($general_item['discount']['cost'] == null)
                                    <td class="text-center">0</td>
                                    @else
                                    <td class="text-center">{{$general_item['discount']['cost']}}</td>
                                    @endif
                                    <td class="text-center">{{$general_item['discount']['quantity_sold']}}</td>
                                    <td class="text-center">{{$general_item['discount']['total_sale']}}</td>
                                    <td class="text-center">{{$general_item['discount']['total_cost']}}</td>
                                    <td class="text-center">{{$general_item['discount']['profit']}}</td>
                                    <td class="text-center">{{$general_item['discount']['margin']}}</td>  
                                    
                                </tr>
                               
                                    @php
                                        $cost += (double)$general_item['discount']['cost'];
                                        $qty += (double)$general_item['discount']['quantity_sold'];
                                        $t_sale += (double)$general_item['discount']['total_sale'];
                                        $t_cost += (double)$general_item['discount']['total_cost'];
                                        $profit += (double)$general_item['discount']['profit'];
                                        $margin += (double)$general_item['discount']['margin'];
                                        $count = $count+1 ;
                                    @endphp
                                    
                                @endif
                                
                                @endforeach
                            </tbody>
                            @php
                                $qty = number_format((float)$qty, 2, '.', '');
                                $t_sale = number_format((float)$t_sale, 2, '.', '');
                                $t_cost = number_format((float)$t_cost, 2, '.', '');
                                $profit = number_format((float)$profit, 2, '.', '');
                                $margin /= $count;
                                $margin = number_format((float)$margin, 2, '.', '');
                            @endphp
                            <tfoot>
                                <tr>
                                  <td colspan='3' class="text-center font-weight-bold">TOTAL:</td>
                                  <td class="text-center">{{$qty}}</td>
                                  <td class="text-center">{{$t_sale}}</td>
                                  <td class="text-center">{{$t_cost}}</td>
                                  <td class="text-center">{{$profit}}</td>
                                  <td class="text-center">{{$margin}}</td>
                                </tr>
                            </tfoot>

                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
@stop

@section('javascript')
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>

<script>


$(document).ready(function() {
    $('#testTable').DataTable();
    
} );

$(document).ready(function() {
$("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#testTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
    $('.datepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1,
    });
    $('.datepicker1').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1,
    });

});
</script>

@stop