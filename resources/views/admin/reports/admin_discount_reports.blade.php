@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Get Discounts Reports</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                               <form action="/admin/reports/unpaidDiscountFilter" method="POST">
                                @csrf
                                <div class="form-group row">

                                        <div class="col-sm-3 input-group input-append" >
                                                <input class="datepicker form-control" placeholder="Start Date" value="{{$start_date}}" required="" readonly="" id="start_date" name="start_date" type="text">
                                            </div>
                                            <div class="col-sm-3 input-group input-append" >

                                                    <input class="datepicker1 form-control" placeholder="End Date" value="{{$end_date}}" id="end_date" required="" readonly="" name="end_date" type="text">
                                                </div>
                               
                                </div>
                                <div class="row">
                                    <div class="col-sm-1"><button class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
              
                                    </div><br>
                               </form>
                                {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" onclick="javascript:xport.toXLS('testTable')"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                </button> --}}
                                <a href="/discount.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>

                                    <!--<a href="/pdf/examples/discount.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->

                                <br><br>
                            <div class="table-responsive">
                            <!--<input id="myInput" type="text" placeholder="Search..">-->
                                <table class="table table-border custom-table m-b-0" id="testTable">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>	Discount Reason</th>
                                            <th>	Order No</th>
                                            <th>	Amount After Discount</th>
                                            <th>Discount Type</th>
                                            <th>	Discount amount</th>
                                            <th>	Discount Percent</th>
                                            <th>	Discounted Amount</th>
                                            <th>	Discount added By </th>
                                            <!-- <th class="text-right">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody id="testTable1">
                                            <?php $count=1; ?>
                                        @foreach ($orders as $order)
                                            
                                       
                                           
 
                                                <tr>
                                            
                                                    <td>{{$count}}	</td>
                                                    
                                                    <td> {{$order->discount_reason}} </td>
                                                    <td> {{$order->orders}} </td>
                                                    <td> {{$order->total_amount}} </td>
                                                    <td> {{$order->discountType}} </td>
                                                    @if(!$order->discount_amount )
                                                    <td> N/A </td>
                                                    @else
                                                    <td> {{$order->discount_amount}} </td>
                                                    @endif
                                                    @if(!$order->discount_percent )
                                                    <td> N/A </td>
                                                    @else
                                                    <td> {{$order->discount_percent}} %</td>
                                                    @endif
                                                    <td> {{$order->discounted_amount}} </td>
                                                    <td> {{$order->discountUserName}} </td>
                                                <!-- <td class="text-right">
                                                    <div class="dropdown dropdown-action">
                                                        <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href=""><i class="fa fa-pencil m-r-5"></i> Print</a>
                                                        
                                                        </div>
                                                    </div>
                                                </td> -->
                                                </tr>
                                                <?php $count++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop     

@section('javascript')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<script>
$(document).ready(function() {
    $('#testTable').DataTable();
} );

$(document).ready(function() {
    $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#testTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
$('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});

});    
   

function updateTable(){

//alert("hi");
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var count=1; var gross = 0.00 ;

    var c = 0 ; var previousItems = new Array();
//alert("sad");
    jQuery.ajax({
        //alert("fsd"); 
         url : '/admin/reports/unpaidDiscountFilter',
         type : 'POST' ,
         async : false ,   //NOTE THIS
         data : { start_date : start_date , end_date : end_date  } ,
         headers : {'X-CSRF-TOKEN': '{{ csrf_token() }}' } ,
         
        success: function (response) {

        $('#testTable>tbody').empty();

            if(response != null){
                
                console.log(response);
                var check = 1;

                for(var i = 0 ; i < response.length ; i++ ){ 

                    tr = '<tr>' +

                            '<td>' + check + '</td>' +
                            '<td>' + response[i]['discount_reason'] + '</td>' +
                            '<td>' + response[i]['orders'] + '</td>' +
                            '<td>' + response[i]['total_amount'] + '</td>' +
                            '<td>' + response[i]['discountType']  + '</td>' +
                            '<td>' + response[i]['discount_amount'] + '</td>' +
                            '<td>' + response[i]['discount_percent'] + ' %'+ '</td>' +
                            '<td>' + response[i]['discounted_amount']  + '</td>' +
                            '<td>' + response[i]['discountUserName']  + '</td>' +
                        '</tr>';
                       
                    check++; 

                    $('#testTable').append(tr);

                }       
            }
       }
    });
}



 </script>
 @stop