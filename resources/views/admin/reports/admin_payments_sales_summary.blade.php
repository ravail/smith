@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Payment Sales Summary</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="card-box">
                                        <div class="form-group row">

                                            <div class="col-sm-3 input-group input-append" >
                                                <input class="datepicker form-control" placeholder="Start Date" required="" readonly="" id="start_date" name="start-date" type="text">
                                            </div>
                                            <div class="col-sm-3 input-group input-append" >
                                                <input class="datepicker1 form-control" placeholder="End Date" id="end_date" required="" readonly="" name="start-date" type="text">
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1"><button onclick="updateTable()" class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                      
                                            </div><br>
                                       
                                        {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" onclick="javascript:xport.toXLS('testTable')"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                        </button> --}}
                                        <a href="/paymentSales.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>

                                         <!--<a href="/pdf/examples/example_011.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->

                                        <br><br>
                                            
                            <div class="table-responsive">
                            <input id="myInput" type="text" placeholder="Search..">
                                <table class="table table-border custom-table m-b-0 " id="example">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Payment Name</th>
                                            <th>No Of Transactions</th>
                                            
                                            <th>Total Payments</th>
                                           
                                            
                                            
                                            <!-- <th class="text-right">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody id="testTable">
                                        <?php $count=1; $total = 0;?>
                                        @foreach($results as $result)                                       

                                        <tr>
                                        <td>{{$count}}</td>
                                            
                                        @foreach($methods as $m)
                                        <?php if($m->id == $result->payment_method){?>
                                            <td>{{$m->name}}</td>
                                        <?php break; } ?>

                                        @endforeach
                                            
                                            <td>
                                                {{$result->count}}
                                            </td>
                                            
                                        <td>
                                            {{$result->sum}}
                                        </td>
                                 <?php $total += $result->sum ?>
                                        </tr>
                                        
                                        <?php $count++; ?>
                                        @endforeach
                                        <tr>
                                            <td>Total</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{$total}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop    

@section('javascript')

<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">


<script>

$('#pdf').on('click',function(){
    
    $("#example").tableHTMLExport({type:'pdf',filename:'report.pdf'});
});


$(document).ready(function() {
    $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#testTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
$('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});

}); 

  

function updateTable(){

//alert("hi");
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var count=1; var gross = 0.00 ;


    var c = 0 ; var previousItems = new Array();
//alert("sad");
    jQuery.ajax({
        //alert("fsd"); 
         url : '/admin/reports/paymentSalesReportFilter',
         type : 'POST' ,
         async : false ,   //NOTE THIS
         data : { start_date : start_date , end_date : end_date  } ,
         headers : {'X-CSRF-TOKEN': '{{ csrf_token() }}' } ,
         
        success: function (response) {
            
        console.log(response.methods);

        $('#testTable').empty();

            if(response != null){
                
                console.log(response.results);
                var check = 1;

                for(var i = 0 ; i < response.results.length ; i++ ){ 
                    for(var j = 0 ; j < response.methods.length ; j++ ){

                        if(response.methods[j]['id'] == response.results[i]['payment_method']){
                         
                            tr = '<tr>' +

                                '<td>' + check + '</td>' +
                                
                                '<td>' + response.methods[j]['name'] + '</td>' +
                                '<td>' + response.results[i]['count'] + '</td>' +
                                '<td>' + response.results[i]['sum'] + '</td>' +
                                '</tr>';

                            check++; 

                            $('#testTable').append(tr);
                            break;
                        }
                    }

                }
                    
            }
       }

    });

}



var restaurant ={!! json_encode($restaurants); !!};

var options = '<option value="">Restaurants</option>';
restaurant.forEach(element => {
        options += '<option value='+element.id+'>'+element.name+'</option>';
});
$("#restaurant").html(options);

</script>

@stop
