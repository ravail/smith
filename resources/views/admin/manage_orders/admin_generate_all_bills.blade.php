@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    
                    <form action="" method="POST" class="form" enctype="multipart/form-data" name="form" id="form">
                            {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">PostPaid Orders</h4>
                        </div>
                        
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                            <div class="alert" id="flash" >
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>Mark</th>
                                            <th>Order No</th>
                                            <th>Date And Time</th>
                                            <th>Table No</th>
                                            <th>No Of Guest</th>
                                            <th>Item description</th>
                                            <th>Condiments</th>
                                            <th>Waiter</th>
                                            <th>Restaurant</th>
                                            <th>Total Amount</th>
                                            <th>Status</th>
                                            
                                           
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($orders as $order)
                                       
                                        <tr>
                                            <td>
                                                <label>
                                                <input type="checkbox" name="mark" class="chk" value={{$order->id}}>
                                                </label>  </td>
                                            <td>{{$order->id}}</td>
                                            
                                        <td> {{$order->date_time}}</td>
                                            
                                            <td>
                                                    {{$order->table_no}}
                                            </td>
                                            
                                        <td>{{$order->no_of_guests}}</td>
                                        <td>{{$order->item_description}}</td>
                                        <td> {{$order->condiments}}</td>
                                        <td>{{$order->waiter}}</td>  
                                        <td>{{$order->restaurant}}</td>    
                                        <td>{{$order->total_amount}}</td>
                                        <td>{{$order->status}}</td>
                                 
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                                
                            </div>
                           
                        </div>
                       
                    </div>
                    <div class="m-t-20 text-center">
                            <button class="btn btn-primary btn-lg" type="submit" >Generate Bill</button>
                    </div>

                </form>
                </div>
               
            </div>
@stop  

@section('javascript')



<script>
    var id = {!! json_encode($id) !!};
    $( "#form" ).submit(function( event ) {
        var chkArray = [];
	
        /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
        $(".chk:checked").each(function() {
            chkArray.push($(this).val());
        });
        
        /* we join the array separated by the comma */
        var selected;
        selected = chkArray.join(',') ;
        console.log(chkArray);
        /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
        if(selected.length > 0){
            alert("You have selected " + selected);	
            $('#form').attr("method" , "POST");
            $("#form").attr("action", "/admin/orders/generate-bills/"+ id +"/"+ chkArray);
            
            return;
           
        }

    
  
 
    $("#flash").attr("class","alert alert-danger");
    $("#flash").append("Please Select Atleast one Order!");
  event.preventDefault();
});

 

</script>

@stop