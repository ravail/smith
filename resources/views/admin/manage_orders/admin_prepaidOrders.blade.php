@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Prepaid Orders</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                            
                                            <th>Order No</th>
                                            <th>Date And Time</th>
                                            <th>Table No</th>
                                            <th>No Of Guest</th>
                                            <th>Item description</th>
                                            <th>Condiments</th>
                                            <th>Waiter</th>
                                            <th>Restaurant</th>
                                            <th>Total Amount</th>
                                            <th>Status</th>
                                            
                                           
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($orders as $order)
                                       
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            
                                        <td> {{$order->date_time}}</td>
                                            
                                            <td>
                                                    {{$order->table_no}}
                                            </td>
                                            
                                        <td>{{$order->no_of_guests}}</td>
                                        <td>{{$order->item_description}}</td>
                                        <td> {{$order->condiments}}</td>
                                        <td>{{$order->waiter}}</td>  
                                        <td>{{$order->restaurant}}</td>    
                                        <td>{{$order->total_amount}}</td>
                                        <td>{{$order->status}}</td>
                                  <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href=""><i class="fa fa-pencil m-r-5"></i> Print Bill</a>
                                                        <a class="dropdown-item" href=""><i class="fa fa-trash-o m-r-5"></i> Print Docket</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop            