@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Bill Master</h4>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 m-b-20">

                {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                                
                            </div> --}}
                @if (session('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
            <input id="myInput" type="text" placeholder="Search..">
                <table class="table table-border custom-table m-b-0">
                    <thead>
                        <tr>
                            <th>Bill ID</th>
                            <th>Date</th>
                            <th>Waiter Name</th>

                            <th>Number Of Orders</th>
                            <th>Orders</th>
                            <th>Total Amount</th>
                            <th>Print</th>
                            <th>Close Bill</th>

                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id = "testTable1">


                        @foreach($orders as $order)

                        <tr>
                            <td>{{$order->id}}</td>

                            <td>{{$order->date}} </td>

                            <td>
                                {{$order->waiter_name}}
                            </td>

                            <td>{{$order->no_orders}}</td>
                            <td>{{$order->orders}}</td>
                            <td>{{$order->total_amount}}</td>

                            <td><a href="" onclick="printBill({{$order->id}})"><i class="fa fa-print fa-2x"
                                        aria-hidden="true" style=" color: blue;"></i></a></td>
                            <td><a onclick="return confirm('Do you want to close the bill?');"
                                    href="/admin/orders/cash-reciept/{{$order->id}}" class="btn btn-primary">Close
                                    Bill</a></td>
                            <td class="text-right">
                                <div class="dropdown dropdown-action">
                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                      
                                        @if(\Auth::user()->master_bills_discount == 'on' || \Auth::user()->user_type == 'admin')
                                        <a class="dropdown-item"
                                            href="/admin/orders/billmaster/edit/{{$order->id}}/{{$order->total_amount}}"><i
                                                class="fa fa-pencil m-r-5"></i> Edit</a>
                                        @endif
                                        
                                        <a class="dropdown-item"
                                            href="/admin/orders/postpaid/transfer-bill/{{$order->waiter_id}}/{{$order->waiter_name}}/{{$order->id}}"><i
                                                class="fa fa-exchange m-r-5" aria-hidden="true"></i> Transfer
                                            Bill</a>

                                        <a class="dropdown-item" href=""><i class="fa fa-trash-o m-r-5"></i>
                                            Delete</a>

                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

</div>

{{-- <iframe src="/definite.mp3" allow="autoplay" id="audio" style="display:none"></iframe>

            <audio id="player" > 
                    <source src="/definite.mp3">
                    <source src="/definite.ogg">
            </audio> --}}
@stop



@section('javascript')
<script>
// var x = document.getElementById("myAudio").autoplay;
$(document).ready(function() {
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#testTable1 tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
function myFunction() {
    window.print();
}


function printBill(bill_id) {
    var confirm_text = 'bill';
    //alert(bill_id);
    var isconfirmed = confirm("Do you want to print " + confirm_text + "?");
    if (isconfirmed) {
        jQuery.ajax({
            url: '/admin/orders/print-master-bills',
            type: 'POST',
            async: false, //NOTE THIS
            data: {
                bill_id: bill_id
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },

            success: function(response) {

                console.log(response);
                var divContents = response;


                var printWindow = window.open('', '', 'width=400');
                printWindow.document.write(`<html><head><title></title>
                    <style>

                        
                        @page{margin:0;size:auto}

                    </style> `);
                printWindow.document.write('</head><body >');
                printWindow.document.write(divContents);
                printWindow.document.write('</body></html>');
                printWindow.focus();
                printWindow.document.close();
                printWindow.print();
                //window.print();

            }
        });
    }
}
</script>
@stop