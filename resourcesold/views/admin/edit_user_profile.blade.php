@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")
<div class="page-wrapper">
            <div class="content container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Edit Profile</h4>
                    </div>
                </div>
                <form enctype="multipart/form-data" id="fileee" action="/admin/usersave" method="POST">
                    @csrf
                    <div class="card-box">
                        <h3 class="card-title">Basic Informations</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="profile-img-wrap">
                                    <img class="inline-block" id="preview_img" src='{{ url("/public/img/".$user->image) }}' alt="user image">
                                    <div class="fileupload btn">
                                        <span class="btn-text">edit</span>
                                        <input class="upload" id="file" name="user_file" onchange="PreviewImage()" type="file">
                                    </div>
                                </div>
                                <div class="profile-basic">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">First Name</label>
                                                <input type="text" class="form-control floating"  name="name" value="{{$user->name}}"  placeholder="John">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Last Name</label>
                                                <input type="text" class="form-control floating" name="name2" value="{{$user->name}}" placeholder="Doe">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-focus">
                                                <label class="focus-label">Birth Date</label>
                                                
                                                    <input class="form-control" type="date" name="birth_date" value="{{$user->birth_date}}" >
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-focus select-focus" >
                                                <label class="focus-label">Gendar</label>
                                                <select class="select form-control floating" id="gendeer" name="gender">
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-box">
                        <h3 class="card-title">Contact Informations</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Address</label>
                                    <input type="text" class="form-control floating" name="address" value="{{$user->address}}" placeholder="4487 Snowbird Lane">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-focus">
                                    <label class="focus-label">State</label>
                                    <input type="text" class="form-control floating" name="state" value="{{$user->state}}" placeholder="New York">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Country</label>
                                    <input type="text" class="form-control floating" name="country" value="{{$user->country}}" placeholder="United States">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Pin Code</label>
                                    <input type="text" class="form-control floating" name="pincode" value="{{$user->pin_code}}" placeholder="10523">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-focus">
                                    <label class="focus-label">Phone Number</label>
                                    <input type="text" class="form-control floating" name="phone_number" value="{{$user->phone_number}}" placeholder="631-889-3206">
                                </div>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center m-t-20">
                        <button class="btn btn-primary btn-lg" type="submit" name="submit">Save Changes</button>
                    </div>
                </form>
                </div>
        </div>
@stop
 @section('javascript')
<script type="text/javascript">
    
    $( document ).ready(function() {
   var user ={!! json_encode($user); !!} ;
    $('#gendeer option[value="'+user.gender+'"]').prop('selected', true)
});
    function PreviewImage() {
    // console.log($('#mDriverpic').html());
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("file").files[0]);

    oFReader.onload = function(oFREvent) {
        document.getElementById("preview_img").src = oFREvent.target.result;
    };
        
    
};
</script>
@stop  