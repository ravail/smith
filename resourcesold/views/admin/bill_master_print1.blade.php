{{-- <!doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta charset="utf-8">
    {{-- <title>A simple, clean, and responsive HTML invoice template</title> --}}
    
    {{-- <style>
            body {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 310mm;
        min-height: 297mm;
        padding: 2mm;
        margin: 2mm auto;
       
       
        background: white;
       
    }
    .subpage {
        padding-left: 6cm;
        padding-right: 4cm;
        
        height: 257mm;
       width : 190mm;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 220mm;
            height: 297mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
     --}}
  
    {{-- .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    } --}}

  
    {{-- </style> --}}
{{-- </head> --}}

{{-- <body class="aaa"> --}}
        <div class="book" id="book">
                <div class="page">
                    <div class="subpage" style="text-align:center;">
                      <div class="row">
                      <div class="col-md-4">
            
                       </div>
                       <div class="col-md-4">
                       <h6 style="margin-bottom: 2px">THE SMITH HOTELS <br>
                               
                         {{-- OLE SANE CRESCENT , MAGADI ROAD,<br>  ONGATA RONGAI(NKOROI) <br> --}}
                                           0705 625 356 MPESA TILL 928181<br>
                                           PIN : P051316693O<br>
                                           www.thesmithhotels.com | info@thesmithhotels.com<br>
                                           </h6>
                                        
                                           <span style="float:left;margin-left:10px !important;font-size:12px;">
                                           {{$bill->waiter_name}} | </span> <span style="float:left;margin-left:10px !important;font-size:12px;">Bill id : {{$bill->id}}</span>
                                        
                                        </div>
                       
                            <br><hr>
                          
                           
   
                                    <?php $total = 0; $count = 0; $c=0 ;$previous = 0; $previousItems = array(); ?>
                                    <?php for($i = 0 ; $i < $size ; $i++) { ?>
                                        @foreach($postOrders as $p)
                                            <?php if($p->id == $orders[$i]){ if($count <= 0 ){?>
                                                <table style="margin-left:10px !important;margin-top:-10px !important;">
                                                <tr>
                    
                                                    <td style="padding:0 0px 0 0px;">
                                                       <span style="font-size:12px;"> Tbl </span><span style="font-size:12px;"> {{$p->table_no}} / {{$p->no_of_guests}}</span>
                                                    </td>
                                                    <td></td>
                                                    <td style="padding:0 0px 0px 30px;">
                                                       <span style="font-size:12px;"> Chk </span> <span style="font-size:12px;"> {{$bill->id}} </span>
                                                    </td>
                                                    <td></td>
                                                    <td style="padding:0 0px 0 30px;">
                                                            <span style="font-size:12px;"> {{$bill->date}}</span>
                                                        </td>
                                                </tr>
                                            </table ><hr>
                                        <?php }?>
                                                    {{-- <span style="float:left;margin-left:10px !important;font-size:12px;margin-top:-10px !important;"><strong>EAT IN</strong></span><br> --}}
                                                    
                                                
                                                        
                                                @foreach($items as $item)
                    
                                                    <?php if($item->post_paid_orders_id == $p->id) {  ?>
                                                       <?php 

                                                            // if($previous == 0){ 

                                                            //     $previousItems[$c] = $item ;
                                             
                                                            //     $c++;

                                                            // }
                                                        
                                                            // if($previous ==  $item->general_item_id){ 

                                                            //     $previousItems[$c-1]['quantity'] +=  $item->quantity ;

                                                            // }
                                                            // else if($previous != 0 && $previous !=  $item->general_item_id){
                                                                
                                                                $flag = false;

                                                                for($j = 0 ; $j < sizeof($previousItems);$j++){

                                                                    if($previousItems[$j]->name == $item->name){
                                                                        $previousItems[$j]->quantity +=  $item->quantity ;
                                                                        $flag = true;
                                                                    }

                                                                }

                                                                if($flag == false){

                                                                    $previousItems[$c] = $item ;
                                                                    $c++;

                                                                }
                                                                
                                                            // }
                                                            
                                                        ?>
                                 
                                                    <?php  $total = ($item->quantity * $item->price) + $total; $previous = $item->general_item_id; } ?>
                                               
                                                @endforeach

                                                <?php

                                                           
                                                ?>
                                            
                                            <?php $count++; } ?>
                                       
                                        
                                        @endforeach
                                        <table style="margin-left:10px !important;margin-top:-12px !important;margin-top:1px !important">
                                    <?php } 
                                     $discount = $bill->discounted_amount;
                                            $s = sizeof($previousItems);
                                            for($i = 0 ; $i < $s ; $i++){ ?>

                                            <span style="font-size:12px;float:left !important;padding-left:12px;margin-bottom:-12px !important">{{$previousItems[$i]->quantity}}</span> <span style="font-size:12px;float:left !important;padding-left:3px;margin-bottom:-12px !important"> {{$previousItems[$i]->name}} </span>
                                                       
                                                <span style="font-size:12px;float:right !important;padding-right:18px;margin-bottom:-12px !important"> {{ $previousItems[$i]->price * $previousItems[$i]->quantity }}.00 </span>
                                                    
                                                <br>
                                            

                                            <?php } ?></table><hr>
                                    
                                    <table style="margin-left:8px !important">
                                    <tr class="total">
                                        <td></td>
                                        
                                        <td style="padding:0 0px 0 0px;">
                                            <strong><span style="font-size:12px;">Total (Incl. VAT & C.Levy)</span></strong>
                                        </td>
                                        
                                        <span style="font-size:12px;float:right !important;padding-right:18px !important;">{{$total}}.00</span>
                                    </tr></table>
 <table style="margin-left:8px !important">
                                <hr style="margin-top:20px !important">
                            <tr>
                                <td style="padding:0 0px 0 0px;">
                                    <strong><span style="font-size:12px;">Discount</span></strong>
                                </td>
                                <span style="font-size:12px;float:right !important;padding-right:18px !important;">{{$discount}}</span>
                            </tr>
                        </table>
                        <table style="margin-left:8px !important"> <tr>
                                <td style="padding:0 0px 0 0px;">
                                    <strong><span style="font-size:12px;">Total </span></strong>
                                </td>
                                
                                <span style="font-size:12px;float:right !important;padding-right:18px !important;">{{$total - $discount}}</span>
                            </tr>
                        
                        
                        </table >
                                    <span style="font-size:12px;float:left;margin-left:12px !important">This is not a receipt.</span>
                            
                                    <div style="page-break-after:always"></div>  
                           
                            </div>
                    </div>
                    
                     
                </div>
              
            </div>
 


