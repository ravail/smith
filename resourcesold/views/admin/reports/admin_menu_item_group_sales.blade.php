@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Menu Item Group Sales</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-12">
                                    <div class="card-box">
                                        <form method="GET" action="http://thesmithhotelserp.com/admin/reports/payment-sales-summary" accept-charset="UTF-8">
                        
                                            <div class="form-group row">
                                                
                                                <div class="col-sm-4">
                                                    <input class="datepicker form-control" placeholder="Start Date" readonly="" name="start-date" type="text">
                                                    
                                                </div>
                                                <div class="col-sm-4">
                                                        <input class="datepicker form-control" placeholder="End Date" readonly="" name="end-date" type="text">                                                    
                                                </div>
                                                <div class="col-sm-4">
                                                    <select class="form-control" id="restaurant" name="restaurant"><option selected="selected" value="">Restaurant</option><option value="26">RJ 14</option><option value="41">The Smith Hotels</option><option value="43">Olarro Hotel</option></select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1"><button type="submit" class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                                                         
                        
                                                    <div class="col-sm-1">
                                                    <button title="Export In Excel" type="submit" class="btn btn-warning" name="manage-request" value="xls"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                    </button>
                                                    </div>
                        
                                                    <div class="col-sm-1">
                                                        <button title="Export In PDF" type="submit" class="btn btn-warning" name="manage-request" value="pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                        
                                                    <div class="col-sm-1">
                                                        <a class="btn btn-info" href="http://thesmithhotelserp.com/admin/reports/payment-sales-summary">Clear</a>
                                                    </div>
                                                        
                        
                                                        
                                                </div><br>
                                                
                        
                                            </form>
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>	Title</th>
                                            <th>	Sales QTY</th>
                                            <th>	Gross Sales</th>
                                            <th>	Taxes</th>
                                            <th>	Net Sales % Of Ttl</th>
                                            


                                            
                                            
                                            <!-- <th class="text-right">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                       

                                        <tr>
                                            <td>1</td> 
                                            <td>Accommodation</td>
                                            
                                            <td>3588</td
                                            ><td>8,347,720.00</td>
                                            <td>-1,273,381.02</td>
                                            <td>7,074,338.98</td>
                                            
                                         
                                          

                                            <!-- <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href=""><i class="fa fa-pencil m-r-5"></i> Print</a>
                                                       
                                                    </div>
                                                </div>
                                            </td> -->
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                                    </div>
                            </div>
                    </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop            