@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Menu Item General Sales With Plu</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                                <div class="row">
                                        <div class="col-lg-12">
                                                <div class="card-box">
                                                    <form method="GET" action="http://thesmithhotelserp.com/admin/reports/payment-sales-summary" accept-charset="UTF-8">
                                    
                                                        <div class="form-group row">
                                                            
                                                            <div class="col-sm-4">
                                                                <input class="datepicker form-control" placeholder="Start Date" readonly="" name="start-date" type="text">
                                                                
                                                            </div>
                                                            <div class="col-sm-4">
                                                                    <input class="datepicker form-control" placeholder="End Date" readonly="" name="end-date" type="text">                                                    
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <select class="form-control" id="restaurant" name="restaurant"><option selected="selected" value="">Restaurant</option><option value="26">RJ 14</option><option value="41">The Smith Hotels</option><option value="43">Olarro Hotel</option></select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-1"><button type="submit" class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                                                                     
                                    
                                                                <div class="col-sm-1">
                                                                <button title="Export In Excel" type="submit" class="btn btn-warning" name="manage-request" value="xls"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                                </button>
                                                                </div>
                                    
                                                                <div class="col-sm-1">
                                                                    <button title="Export In PDF" type="submit" class="btn btn-warning" name="manage-request" value="pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                    
                                                                <div class="col-sm-1">
                                                                    <a class="btn btn-info" href="http://thesmithhotelserp.com/admin/reports/payment-sales-summary">Clear</a>
                                                                </div>
                                                                    
                                    
                                                                    
                                                            </div><br>
                                                            
                                    
                                                        </form>
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>	Item</th>
                                       
                                            
                                            <th>	Sales QTY</th>
                                            <th>	Gross Sales</th>
                                            <th>	VAT</th>
                                            <th>	CATERING LEVY</th>
                                            <th>	Taxes</th>
                                            <th>	Net Sales % Of Ttl</th>
                                            <th>	Plu no</th>
                                            <th>	Plu name</th>

                                            
                                            
                                            <!-- <th class="text-right">Action</th> -->
                                        </tr>
                                    </thead>
                                   
                                        <tbody>
                                            <?php $count=1; $gross = 0.00; $vat = 0.00; $cat = 0.00; $taxes ?>
                                            @foreach($results as $result)                                       
                                                   
                                                    <tr>
                                                         
                                                        @foreach($items as $item)
                                                            <?php if($item->id == $result->general_item_id && $item->plu_number != null){?>
                                                                <td>{{$count}}</td>
                                                                <td>{{$item->name}}</td>
                                                                
                                                                @foreach($groups as $group)
                                                                    <?php if($group->id == $item->family_groups_id){?>
                                                                        {{-- <td>{{$group->name}}</td> --}}
                                                                    <?php  
                                                                    $gross = $result->quantity*$item->price;
                                                                    $tax = $item->tax;
                                                                    if(!empty($tax)){
            
                                                                        $t=explode(',',$tax);
                                                                        $s = sizeof($t);
                                                                        if($s == 2){
                                                                    
                                                                            $vat = ($gross/100)*16;
                                                                            $cat = ($gross/100)*2;
                                                                            $taxes = $vat + $cat;
                                                                            
                                                                        }
                                                                        else if($s == 1){
            
                                                                            if($s[0] == 1){
                                                                                $vat = ($gross/100)*16;   
                                                                                $taxes = $vat;                                                                 
                                                                            }
                                                                            else{
                                                                                $cat = ($gross/100)*2;    
                                                                                $taxes = $cat;                                                                
                                                                            }
            
                                                                        }
                                                                    }
                                                                    
                                                                    //echo $t;
                                                                      break; } ?>
                                                                @endforeach
            
                                                                    <td>{{$result->quantity}}</td>
                                                                    <td>{{$gross}}</td>
                                                                      
                                                        <td>{{$vat}}</td>
                                                        <td>{{$cat}}</td>
                                                        <td>{{$taxes}}</td>
                                                        <td>{{$gross-$taxes}}</td>
                                                        <td>{{$item->plu_number}}</td>
                                                            @foreach($plues as $plu)
                                                            <?php if($plu->id == $item->plu_number){ ?>
                                                                <td>{{$plu->plu_number}}</td>
                                                                <?php break; } ?>
                                                            @endforeach
                                                                
                                                            <?php $count++; break; } ?>
                                                        @endforeach
            
                                                    </tr>
                                                    <?php $vat = 0.00;$cat = 0.00; ?>
                                                    @endforeach
                                                </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop            