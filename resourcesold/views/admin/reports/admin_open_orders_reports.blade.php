@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">

                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Open Orders</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                                
                                 <div class="card-box">
                    <form action="/admin/reports/open-orders-filter" method="POST">
                            @csrf
                            
                                <div class="form-group row">

                                        <div class="col-sm-3 input-group input-append" >
                                                <input class="datepicker form-control" value="{{$start_date}}" placeholder="Start Date" required="" readonly="" id="start_date" name="start-date" type="text">
                                            </div>
                                            <div class="col-sm-3 input-group input-append" >

                                                    <input class="datepicker1 form-control" value="{{$end_date}}" placeholder="End Date" id="end_date" required="" readonly="" name="end-date" type="text">
                                                </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-1"><button onclick="updateTable()" class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                  
                                        </div>
                                     </form>
                                         <br>
                                        <a href="/open_order_report.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>

                                            <br><br>
                                    {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" ><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                    </button> --}}

                                    {{-- <a href="/dailysalesDownload.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a> --}}

                                    <br><br>
                            <div class="table-responsive">
                            <!--<input id="myInput" type="text" placeholder="Search..">-->
                                <table class="table table-border custom-table m-b-0" id="myTable">
                                    <thead>
                                        <tr>
                            
                                            <th>Order No</th>
                                            <th>Date And Time</th>
                                            <th>Table No</th>
                                            <th>No Of Guest</th>
                                            <th>Item description</th>
                                            <!--<th>Condiments</th>-->
                                            <th>Waiter</th>
                                            <th>Restaurant</th>
                                            <th>Total Amount</th>
                                            <th>Status</th>
                                            <th>Print Class</th>
                                            
                                           
                                           
                                        </tr>
                                    </thead>
                                    
                                    <tbody idid="testTable1">
                                        
                                    <?php
                                        $total = 0;
                        foreach($data['details'] as $key=>$detail)
                            {
                                    $items_description = '';
                                    $print_class = '';
                                foreach($data['order_it'][$key] as $k => $order_itm)
                                    {
                                    $items_description .= "item: " . $order_itm->name .  ' ' . "QTY: " . $order_itm->quantity .", " ;
                                    $print_class = $order_itm->print_class_name;
                                    }
                                    
                            ?>
                              <tr>
                                            <td>{{$detail->id}}</td>
                                        <td> {{$detail->date_time}}</td>
                                            <td>
                                            {{$detail->table_no}}
                                            </td>
                                        <td>{{$detail->no_of_guests}}</td>
                                        <td>{{$items_description}}</td>
                                      
                                        <td>{{$detail->waiter}}</td>  
                                        <td>{{$detail->name}}</td>    
                                        <td>{{$detail->total_amount}}</td>
                                        <td>{{$detail->status}}</td>
                                        <td>{{$print_class}}</td>
                                        </tr>
                        <?php    $total += $detail->total_amount;
                            }
                                    ?>
                                   
                             <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                     <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                             
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><span><strong>{{$total}}.00</strong></span></td>
                                </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop  

@section('javascript')

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>

<script>

$(document).ready(function() {
    $('#myTable').DataTable();
} );



$(document).ready(function() {
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#testTable1 tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

$('.datepicker').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
$('.datepicker1').datetimepicker({
    format: 'yyyy-mm-dd hh:ii:00',
    minuteStep:1,
});
//     $('#datePicker')
//         .datepicker({
//             format: 'mm/dd/yyyy'
//         });
// });
       
// $(document).ready(function() {
//     $('#datePicker1')
//         .datepicker({
//             format: 'mm/dd/yyyy'
//         });
});    


///////////////////////////////

var xport = {
  _fallbacktoCSV: true,  
  toXLS: function(tableId, filename) {   
    this._filename = (typeof filename == 'undefined') ? tableId : filename;
    
    //var ieVersion = this._getMsieVersion();
    //Fallback to CSV for IE & Edge
    if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
      return this.toCSV(tableId);
    } else if (this._getMsieVersion() || this._isFirefox()) {
      alert("Not supported browser");
    }

    //Other Browser can download xls
    var htmltable = document.getElementById(tableId);
    var html = htmltable.outerHTML;

    this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
  },
  toCSV: function(tableId, filename) {
    this._filename = (typeof filename === 'undefined') ? tableId : filename;
    // Generate our CSV string from out HTML Table
    var csv = this._tableToCSV(document.getElementById(tableId));
    // Create a CSV Blob
    var blob = new Blob([csv], { type: "text/csv" });

    // Determine which approach to take for the download
    if (navigator.msSaveOrOpenBlob) {
      // Works for Internet Explorer and Microsoft Edge
      navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
    } else {      
      this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
    }
  },
  _getMsieVersion: function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
    }

    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }

    var edge = ua.indexOf("Edge/");
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
    }

    // other browser
    return false;
  },
  _isFirefox: function(){
    if (navigator.userAgent.indexOf("Firefox") > 0) {
      return 1;
    }
    
    return 0;
  },
  _downloadAnchor: function(content, ext) {
      var anchor = document.createElement("a");
      anchor.style = "display:none !important";
      anchor.id = "downloadanchor";
      document.body.appendChild(anchor);

      // If the [download] attribute is supported, try to use it
      
      if ("download" in anchor) {
        anchor.download = this._filename + "." + ext;
      }
      anchor.href = content;
      anchor.click();
      anchor.remove();
  },
  _tableToCSV: function(table) {
    // We'll be co-opting `slice` to create arrays
    var slice = Array.prototype.slice;

    return slice
      .call(table.rows)
      .map(function(row) {
        return slice
          .call(row.cells)
          .map(function(cell) {
            return '"t"'.replace("t", cell.textContent);
          })
          .join(",");
      })
      .join("\r\n");
  }
};


</script>

@stop