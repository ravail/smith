@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Waiters Detailed Report</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-12">
                                    <div class="card-box">

                                            <div class="form-group row">
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker form-control" placeholder="Start Date" required="" readonly="" id="start_date" name="start-date" type="text">
                                                </div>
                                                <div class="col-sm-3 input-group input-append" >

                                                        <input class="datepicker1 form-control" placeholder="End Date" id="end_date" required="" readonly="" name="start-date" type="text">
                                                    </div>
                                            </div>
                                            
                                            {{-- <div class="form-group row">
                                                <div class="col-sm-3 input-group input-append date" id="datePicker">
                                                    <input type="text" class="form-control" id="start_date" name="date" placeholder="Start Date" />
                                                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                               
                                                <div class="col-sm-3 input-group input-append date" id="datePicker1">
                                                    <input type="text" class="form-control" name="date1" id="end_date" placeholder="End Date" />
                                                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                                
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="startTimeSearch" value="" name="startTimeSearch" placeholder="Start Time">
                                                </div>
    
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="endTimeSearch" value="" name="endTimeSearch" placeholder="End Time">
                                                </div>

                                            </div> --}}
                                            <div class="row">
                                                <div class="col-sm-1"><button onclick="updateTable()" class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                          
                                                </div><br>
                                           
                                            {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" href="download.php"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            </button> --}}

                                            <a href="/waiterDownload.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>
                                            {{-- <button onclick="javascript:demoFromHTML();">PDF</button> --}}
                                            <br><br>
                        
                            <div id="customers" class="table-responsive">
                                <table class="table table-border custom-table m-b-0" id="testTable">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Waiter</th>
                                            <th>	Description</th>
                                            <th> QTY </th>
                                            
                                            <th>	Total Sales </th>
                                        
                                     
                                        </tr>
                                    </thead>
                                    <tbody>
                       @php 
                       
                        $count = 1;
                       
                       @endphp
                      @foreach($previousItems as $previousItem)
                                <tr>
                                    <td>{{$count}}</td>
                                    
                                    <td>{{$previousItem->waiter}}</td>
                                    <td>
                                        {{$previousItem->name}}
                                </td>
                                <td>
                                        {{$previousItem->quantity}}
                                </td>
                                <td>
                                        {{$previousItem->quantity * $previousItem->price}}
                                </td>
                               
                                            
                                </tr>
                           @php 
                       
                        $count ++;
                       
                       @endphp                 
                    @endforeach              
                                    </tbody>
                                        

                                </table>
                                  
                            
                            </div>
                                    </div>
                            </div>
                    </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop    

@section('javascript')

<!--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>-->


<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>

<script>


$(document).ready(function() {
    $('#testTable').DataTable();
} );

$(document).ready(function() {

    $('.datepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1,
    });
    $('.datepicker1').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1,
    });

});
               
function updateTable(){


    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var count=1; var gross = 0.00 ;

    // var startTimeSearch = $('#startTimeSearch').val();
    // var endTimeSearch = $('#endTimeSearch').val();

    var c = 0 ; var previousItems = new Array();

    jQuery.ajax({
      
         url : '/admin/reports/waiterReportFilter',
         type : 'POST' ,
         async : true ,   //NOTE THIS
         data : { start_date : start_date , end_date : end_date } ,
         headers : {'X-CSRF-TOKEN': '{{ csrf_token() }}' } ,
         
       success: function (response) {
        
        console.log(response);

        $('#testTable>tbody').empty();

            if(response != null){
                
               

                    var check = 1;
                    //console.log(previousItems);
                    for(var i = 0 ; i < response.length ; i++ ){ 


                        tr = '<tr>' +
                                '<td>' + check + '</td>' +
                                '<td>' + response[i]['waiter'] + '</td>' +
                                '<td>' + response[i]['name'] + '</td>' +
                                '<td>' + response[i]['quantity'] + '</td>' +
                                '<td>' + response[i]['quantity'] * response[i]['price'] + '</td>' +
                                '</tr>';
           
                        check++; 
                        $('#testTable').append(tr);
                    }
                   
                    
       }
       }
     });

}

            



</script>

@stop
