@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Productwise Sales Report</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">

                {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">

                    <form action="/admin/reports/filter_sales_products" method="POST">
                        @csrf
                    <div class="form-group row">
                        <div class="col-sm-3 input-group input-append">
                            <input class="datepicker form-control" placeholder="Start Date" value="{{$start_date}}" required="" readonly=""
                                id="start_date" name="start_date" type="text">
                        </div>
                        <div class="col-sm-3 input-group input-append">
                            <input class="datepicker1 form-control" placeholder="End Date" value="{{$end_date}}" id="end_date" required=""
                                readonly="" name="end_date" type="text">
                        </div>
        
                    </div>

                    <div class="row">
                        <div class="col-sm-1"><button class="btn btn-success"
                                name="manage-request" value="filter">Filter</button></div>

                    </div><br>
                            </form>
                    {{-- <button title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport" ><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            </button> --}}

                    <a href="/dailysalesDownload.php" target="_blank" title="Export In Excel" class="btn btn-warning"
                        value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>
<!--<a href="/pdf/examples/product_wise.php" class="btn btn-danger" title="PDF [new window]" target="_blank">PDF</a>-->
                    <br><br>
                    <div class="table-responsive">
                    <!--<input id="myInput" type="text" placeholder="Search..">-->
                        <table class="table table-border custom-table m-b-0" id="myTable">
                            <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th> Item</th>


                                    <th> Sales QTY</th>
                                    <th> Sales</th>
                                    {{-- <th>	VAT</th>
                                            <th>	CATERING LEVY</th>
                                            <th>	Taxes</th> --}}
                                    {{-- <th>	Net Sales % Of Ttl</th> --}}

                                </tr>
                            </thead>
                            <tbody id="testTable1">
                                <?php $count=1; $gross = 0.00; $vat = 0.00; $cat = 0.00; $taxes ;$total = 0; $tt = 0;$tot = 0; ?>
                                @foreach($results as $result)

                                <tr>

                                    @foreach($items as $item)
                                    <?php if($item->id == $result->general_item_id ){?>
                                    <td>{{$count}}</td>
                                    <td>{{$item->name}}</td>

                                    <!--@foreach($groups as $group)-->
                                    <?php if($group->id == $item->family_groups_id){?>
                                    {{-- <td>{{$group->name}}</td> --}}
                                    <?php  
                                                            $gross = $result->quantity*$item->price;
                                                            $tt = $result->qpsum;
                                                            $tot += $result->qpsum;
                                                            $total += $gross;
                                                            $tax = $item->tax;
                                                            if(!empty($tax)){
    
                                                                $t=explode(',',$tax);
                                                                $s = sizeof($t);
                                                                if($s == 2){
                                                            
                                                                    $vat = ($gross/100)*16;
                                                                    $cat = ($gross/100)*2;
                                                                    $taxes = $vat + $cat;
                                                                    
                                                                }
                                                                else if($s == 1){
    
                                                                    if($s[0] == 1){
                                                                        $vat = ($gross/100)*16;   
                                                                        $taxes = $vat;                                                                 
                                                                    }
                                                                    else{
                                                                        $cat = ($gross/100)*2;    
                                                                        $taxes = $cat;                                                                
                                                                    }
    
                                                                }
                                                            }
                                                            
                                                            //echo $t;
                                                            //   break;
                                                              } ?>
                                    <!--@endforeach-->

                                    <td>{{$result->quantity}}</td>
                                    <td>{{$tt}}</td>

                                    {{-- <td>{{$vat}}</td>
                                    <td>{{$cat}}</td>
                                    <td>{{$taxes}}</td> --}}
                                    {{-- <td>{{$gross-$taxes}}</td> --}}

                                    <?php $count++; break; } ?>
                                    @endforeach





                                </tr>
                                <?php $vat = 0.00;$cat = 0.00; ?>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Total Sales</td>
                                    <td></td>
                                    <td></td>
                                    <td><span><strong>{{$tot}}.00</strong></span></td>
                                </tr>
                            </tbody>


                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
@stop

@section('javascript')


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>


<script>
$(document).ready(function() {
    $('#myTable').DataTable();
} );
$(document).ready(function() {
    $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#testTable1 tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
    $('.datepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep: 1,
    });
    $('.datepicker1').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep: 1,
    });
    var items = {!!json_encode($items);!!};
    // console.log(items);
    var groups = {!!json_encode($groups) !!};
    var taxes = {!!json_encode($taxes) !!};
    var tr = null;
});

function updateTable() {
    var tr = null;
    var start_date1 = $('#start_date').val();
    var end_date1 = $('#end_date').val();

    var count = 1;
    var gross = 0.00;;
    var total = 0
    jQuery.ajax({
        url: '/admin/reports/filter_sales_products',
        type: 'POST',
        async: false, //NOTE THIS
        data: {
            start_date: start_date1,
            end_date: end_date1
        },
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },

        success: function(response) {

            console.log(response);
            // console.log(response.items);
            var items =response.items;
            $('#testTable>tbody').empty();

            if (response != null) {

                for (var i = 0; i < response.results.length; i++) {

                    for (var j = 0; j < items.length; j++) {

                        if (items[j]['id'] == response.results[i]['general_item_id']) {

                            gross = response.results[i]['quantity'] * items[j]['price'];
                            total += gross;

                            tr = '<tr>' +
                                '<td>' + count + '</td>' +
                                '<td>' + response.results[i]['name'] + '</td>' +
                                '<td>' + response.results[i]['quantity'] + '</td>' +
                                '<td>' + gross + '</td>' +
                                '</tr>';

                            count++;
                            break;
                        }

                    }
                    $('#testTable').append(tr);

                }
                tr1 = '<tr>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    +'</tr>';
                $('#testTable').append(tr1);

                tr2 = '<tr>' +
                    '<td>Total Sales</td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td>' + total + ".00" + '</td>' +
                    +'</tr>';
                $('#testTable').append(tr2);

            }
        }
    });

}


///////////////////////////////
</script>

@stop