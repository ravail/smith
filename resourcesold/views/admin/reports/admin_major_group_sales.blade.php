@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Major Group Sales</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>	Title</th>
                                            <th>	Sales QTY</th>
                                            <th>	Gross Sales</th>
                                            <th>	Taxes</th>
                                            <th>	Net Sales % Of Ttl</th>
                                            


                                            
                                            
                                            <!-- <th class="text-right">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                       

                                        <tr>
                                            <td>1</td> 
                                            <td> Food & beverage	</td>
                                            
                                            <td>   73917</td
                                            ><td>34,158,677.00</td>
                                            <td>-5,210,587.89</td>
                                            <td>28,948,089.11</td>
                                            
                                        			
                                          

                                            <!-- <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href=""><i class="fa fa-pencil m-r-5"></i> Print</a>
                                                       
                                                    </div>
                                                </div>
                                            </td> -->
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop            