@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">GL Accounts</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <a href="/admin/add-gl-accounts" class="btn btn-primary btn-rounded pull-right" id="gl_add"><i class="fa fa-plus"></i> Add GL Account</a>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>

                    <div class="row">
                            <div class="col-md-12">
                        
                            @if (session('message'))
                            <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                            {{ session('message') }}
                            </div>
                     @endif
                            
                        </div>
                    </div>
                    <input id="myInput" type="text" placeholder="Search..">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                           
                                            <th>GL Account Number</th>
                                           
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                        <?php $count=1; ?>
                                        @foreach($accounts as $account)

                                        <tr>
                                            <td>{{$count}}</td>
                                            
                                            <td>
                                                {{$account->account_number}}
                                            </td>
                                         
                                            <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="/admin/edit-gl-accounts/{{$account->id}}" id="gl_edit_{{$account->id}}"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                    <a class="dropdown-item" href="/admin/delete-gl-accounts/{{$account->id}}" id="gl_delete_{{$account->id}}"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $count++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@stop 

@section('javascript')

<script> 

$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

var accounts ={!! json_encode($accounts); !!};
//    / console.log(majorGroups);
if('{{\Auth::user()->gl_add}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    $("#gl_add").hide();
}

if('{{\Auth::user()->gl_edit}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    //alert(majorGroups.length);
    for(var i = 0 ; i < accounts.length ; i++){
       //alert(majorGroups.id);
        $("#gl_edit_"+accounts[i].id).hide();

    }
}  

if('{{\Auth::user()->gl_delete}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    for(var i = 0 ; i < accounts.length ; i++){

        $("#gl_delete_"+accounts[i].id).hide();

    }
} 

</script>

@stop