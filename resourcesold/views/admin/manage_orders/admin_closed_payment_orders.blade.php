@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Closed Payment Orders</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">

                {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                           
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-12">
                <div class="card-box">
                    <form action="/admin/closed-orders-filter" method="post">
                        @csrf
                                                <div class="form-group row">
                                                <div class="col-sm-3 input-group input-append" >
                                                    <input class="datepicker form-control" placeholder="Start Date" required="" readonly="" id="start_date" name="start_date" type="text">
                                                </div>
                                                <div class="col-sm-3 input-group input-append" >

                                                        <input class="datepicker1 form-control" placeholder="End Date" id="end_date" required="" readonly="" name="end_date" type="text">
                                                    </div>
                                            </div>
                                       
                                            <div class="row">
                                                <div class="col-sm-1"><button type="submit" class="btn btn-success" name="manage-request" value="filter">Filter</button></div>
                                                <br>
                                                 
                          
                                                </div><br>
                                            <a href="/closed_payment.php" target="_blank" title="Export In Excel"  class="btn btn-warning"  value="xls" id="btnExport"><i class="fa fa-file-excel-o" aria-hidden="true"></i> </a>
                                         
</form>
                                           
                                            <br><br>
                <div class="table-responsive">
               
                    <table class="table table-border custom-table m-b-0" id="testTable">
                        <thead>
                            <tr style="width:80%;height:100px; ">

                                <th>Receipt ID</th>
                                <th>Waiter Name</th>
                                <th>Cashier Name</th>
                                <th>Date</th>

                                <th>Number Of Orders</th>
                                <th>Methods : Payment : Naration</th>
                                <th>Total Amount</th>

                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody id="testTable1">
                            
                            @foreach($orders as $order)
                            <?php $paymentsArray = ""; ?>
                            
                             

                            <tr style="width:80%;height:100px; ">
                                <td>{{$order->bill_id}}</td>
                                <td>{{$order->waiter_name}}</td>
                                <td>{{$order->cashier_name}}</td>
                                <td> {{$order->created_at}}</td>
                                <td>{{$order->no_orders}}</td>
                               
                                @if(!empty($order->narration))
                                <td  style="width:400px;height:200px;"> 
                                
                                {!! nl2br(e($order->narration)) !!}</td>
                               
                                @else
                                
                                <td  style="width:400px;height:200px;"> 
                                
                                ----</td>
                               
                                
                                @endif
                                <td>{{$order->total_amount}}</td>

                                <td class="text-right">
                                    <a onclick="aa({{$order->bill_id}})" id="view" data-toggle="modal"
                                        data-target="#myModal" class="btn btn-warning custom" title="View Receipt">
                                        <i class="fa fa-eye "></i></a>
                                    <a onclick="print_etr({{$order->bill_id}})" class="btn btn-info "
                                        title="Print Receipt">
                                        <i class="fa fa-file "></i></a>


                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Payment Summary For Receipt</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                        <input type="text" name="bill_id" id="bill_id" hidden>


                        <table id="classTable" class="table table-bordered">
                            <thead>
                                <tr>

                                    <th>S.n.</th>
                                    <th>Narration</th>
                                    <th>Payment Mode</th>

                                    <th>Amount</th>

                                </tr>
                            </thead>
                            <tbody id="modalTable">
                                <?php $count = 1; ?>


                                <tr>
                                    <td>Total </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>


@stop

@section('javascript')
<link rel="stylesheet" href="https://cbegroup.ca/assets/css/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cbegroup.ca/assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
<script>
$(document).ready(function() {

    $('.datepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1,
    });
    $('.datepicker1').datetimepicker({
        format: 'yyyy-mm-dd hh:ii:00',
        minuteStep:1,
    });

});
        
$(document).ready(function() {
    $('#testTable').DataTable();
} );
// $(document).ready(function() {
//     $("#myInput").on("keyup", function() {
//         var value = $(this).val().toLowerCase();
//         $("#testTable1 tr").filter(function() {
//             $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//         });
//     });
// });


function aa(id) {
    //alert(id);
    var payments = {!!json_encode($payments);!!};
    var methods = {!!json_encode($methods);!!};

    //$("#bill_id").val(id);
    var check = 1;
    $('#modalTable').empty();
    for (var i = 0; i < payments.length; i++) {
        if (payments[i]['bill_id'] == id) {
            for (var j = 0; j < methods.length; j++) {
                if (methods[j]['id'] == payments[i]['payment_method']) {

                    tr = '<tr>' +

                        '<td>' + check + '</td>' +

                        '<td>' + payments[i]['narration'] + '</td>' +


                        '<td>' + methods[j]['name'] + '</td>' +



                        '<td>' + payments[i]['total_amount'] + '</td>' +
                        '</tr>';

                    check++;

                    $('#modalTable').append(tr);
                    break;
                }
            }
        }
    }


}

function print_etr(bill_id) {
    var confirm_text = 'bill';
    //alert(bill_id);
    //   var isconfirmed=confirm("Do you want to print "+confirm_text+"?");
    //   if (isconfirmed) 
    //   {
    $.ajax({
        url: '/admin/orders/print-master-bills-etr',
        type: 'POST',

        //NOTE THIS

        data: {
            bill_id: bill_id
        },
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },

        success: function(response) {
            popup(response);


        }
    });


}

function popup(response) {

    //alert('hi');
    var divContents = response;

    var printWindow = window.open('', '', 'width=400');
    printWindow.document.write(`<html><head><title></title>
        <style>

           @page{margin:0;size:auto}

        </style> `);
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');

    printWindow.print();
    printWindow.document.close();
    $("#addmajorgroup").submit();

}
</script>

@stop