@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Completed Orders</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">

                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>Order No</th>
                                            <th>Date And Time</th>
                                            <th>Table No</th>
                                            <th>No Of Guest</th>
                                            <th>Item description</th>
                                            <th>Condiments</th>
                                            <th>Waiter</th>
                                            <th>Restaurant</th>
                                            <th>Total Amount</th>
                                            <th>Status</th>
                                            <th>Order By</th>                                            
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                       

                                        <tr>
                                            <td>{{$count}}</td>
                                            
                                            <td> 03/11/2018 02:50 PM</td>
                                            
                                            <td>
                                                    87
                                            </td>
                                            
                                        <td>1</td>
                                        <td>Item: Ben 10 Meat Balls(Do Not Dispense)
                                                Qty: 1</td>
                                        <td> </td>
                                        <td>Dennis Omito</td>  
                                        <td>The Smith Hotels</td>    
                                        <td>112.50</td>
                                        <td>NEW ORDER</td>
                                        <td>Waiter Name:CAROL NYAMBURA</td>

                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
@stop            