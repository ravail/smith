@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")



<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h4 class="page-title">Add Discount</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form action="/admin/orders/billmaster/edit/{{$order_id}}" method="POST" enctype="multipart/form-data"
                    id="addmajorgroup">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Bill ID</label>
                        <input class="form-control" type="text" id="id" name="id" value="{{$order_id}}" required
                            autofocus readonly>
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Bill Total Amount</label>
                        <input class="form-control" type="text" id="amount" name="amount" value="{{$total_amount}}"
                            required autofocus readonly>
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label> Discount Type</label>
                        <select name="discountType" class="form-control"  onchange="chooseType(this)" id="discountType">
                        <option  value="Select">Select </option>
                        <option value="percent">Percent </option>
                        <option value="fixed">Fixed </option>
                        </select>
                    </div>
                    <div class="form-group fixed">
                    <label> Amount Of Discount</label>
                    <input class="form-control" type="text" id="amountToDiscount" name="amountToDiscount"
                            value="" >
                    </div>
                    <div class="form-group percent">
                        <label>Discount(%)</label>
                        <input placeholder="Discount" class="form-control valid" min="1" max="100"
                            name="discount_percent" id="discount_percent" type="number" aria-required="true"
                            aria-invalid="false">
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Amount after discount</label>
                        <input class="form-control" type="text" id="amount_after_discount" name="amount_after_discount"
                            value="" required autofocus readonly>
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Discount Reason</label>
                        <input class="form-control" type="text" id="discount_reason" name="discount_reason" required
                            autofocus>
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>


                    <div class="m-t-20 text-center">
                        <button class="btn btn-primary btn-lg">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@stop

@section('javascript')

<script>
$(function() {
    $('.percent').hide();
    $('.fixed').hide();
});
</script>
<script>
function chooseType(type) {
    var value = $(type).find(':selected').val();
    if (value == 'fixed') {
        $('.percent').hide();
        $('.fixed').show();
    } else {
        $('.fixed').hide();
        $('.percent').show();
    }
}
$("#discount_percent").on("change", function() {

    var total = $('#amount').val();
    var discount = $('#discount_percent').val();

    var amount = (100 - discount) / 100;

    var total_amount = (amount * total);
    //alert(discount);
    //alert(total_amount);

    $('#amount_after_discount').val(total_amount);

});
$("#amountToDiscount").on("change", function() {

var total = $('#amount').val();
var discount = $('#amountToDiscount').val();

var amount = (total - discount);

var total_amount = amount ;


$('#amount_after_discount').val(total_amount);

});


function showprofile(input) {
    var file, img;
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
            img = new Image();
            img.src = e.target.result;;
            img.onload = function() {
                //alert(this.width + " " + this.height);
                if (this.width < 200) {
                    alert('image width must be greater than 200');
                    $('#addmajorgroup').trigger("reset");
                    $('#imgDiv').attr('src', '');
                    return;
                } else if (this.height < 200) {
                    alert('image height must be greater than 200');
                    $('#addmajorgroup').trigger("reset");
                    $('#imgDiv').attr('src', '');
                    return;
                }
                $('#imgDiv').attr('src', e.target.result);
            };

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#a_image").change(function() {
    //   /alert("change");
    showprofile(this);
});
</script>

@stop