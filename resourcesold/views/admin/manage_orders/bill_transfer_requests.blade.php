@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-sm-4 col-4">
                <h4 class="page-title">Bill Transfer Requests</h4>
            </div>
            <div class="col-sm-8 col-8 text-right m-b-20">

                {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if (session('message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
                @endif

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-border custom-table m-b-0">
                        <thead>
                            <tr>

                                <th>Sr.No</th>
                                <th>Bill ID</th>
                                <th>From Waiter</th>
                                <th>To Waiter</th>
                                <th>status</th>


                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 1;?>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{$count}}</td>


                                <td>{{$order->bill_master_id}}</td>

                                <td> {{$order->from_waiter_name}}</td>

                                <td>
                                    {{$order->to_waiter_name}}
                                </td>

                                <td>{{$order->status}}</td>

                                <td class="text-right">


                                    <a onclick="aa({{$order}})" id="view" data-toggle="modal" data-target="#myModal"
                                        class="btn btn-warning custom" title="View Receipt">
                                        <i class="fa fa-eye "></i></a>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Payment Summary For Receipt</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                        <input type="text" name="bill_id" id="bill_id" hidden>


                        <table id="classTable" class="table table-bordered">
                            <thead>
                                <tr>

                                    <th>Bill ID</th>
                                    <th>Order Ids</th>
                                    <th>Total amount</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="modalTable">



                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>

    </div>

</div>

<script>
function aa(id) {
    //alert(id);
    var bills = {!!json_encode($bills);!!};
    console.log(bills);
    //$("#bill_id").val(id);
    var check = 1;
    $('#modalTable').empty();

var tr ='';
    for (var i in bills) {
        if (bills[i].id == id['bill_master_id']) {
            tr = '<tr>' +
                ' <td> ' + bills[i].id + '</td>' +
                '<td>' + bills[i].orders + '</td>' +
                '<td>' + bills[i].total_amount + '</td>' +
                '<td>' + bills[i].status + '</td>';
        }
    }

    tr += '</tr>' +
        '<td colspan="2"></td><td colspan="2" style="padding: 2px;">' +
        ' <a class="btn btn-warning custom" href="/admin/orders/requested-order/' + id['id'] +
        '/Accepted"><i class="fa fa-pencil"></i> Accept</a> ' +

        '<a class="btn btn-warning custom" href="/admin/orders/requested-order/' + id['id'] +
        '/Rejected"><i class="fa fa-trash-o"></i> Reject</a>' +
        '</td></tr>';
    $('#modalTable').append(tr);



}
</script>
@stop