@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")
<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            {{-- <div class="col-sm-12">
                    <h4 class="page-title">Basic Inputs</h4>
                </div> --}}
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    {{-- <h4 class="card-title">Basic Inputs</h4> --}}
                    <form action="/admin/orders/cash-reciept/{{$bill->id}}" method="POST" enctype="multipart/form-data"
                        id="addmajorgroup">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 control-label">Bill No:</label>
                            <div class="col-sm-10">
                                {{$bill->id}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 control-label">Amount:</label>
                            <div class="col-sm-10">
                                {{$bill->total_amount}}
                            </div>
                            <input id="total_unbilled_amount" name="total_amount" type="hidden"
                                value="{{$bill->total_amount}}">
                        </div>
                        <div class="form-group row">

                            <label for="inputEmail3" class="col-sm-1 control-label">S.n</label>
                            <label for="inputEmail3" class="col-sm-3 control-label">Payment Mode</label>
                            <label for="inputEmail3" class="col-sm-3 control-label">Amount</label>
                            <label for="inputEmail3" class="col-sm-5 control-label">Narration</label>

                        </div>
                        <?php $count = 1 ?>
                        @foreach($payments as $payment)
                        <div class="form-group row">
                            <div class="col-sm-1 ">{{$count}}</div>
                            <div class="col-sm-3 ">{{$payment->name}}</div>
                            <div class="col-sm-3 ">

                                <input type="text" name="billing_info[{{$payment->id}}][amount]" id="billing_amount_1"
                                    class="billing_amount form-control digitsonly" placeholder="Amount"
                                    autocomplete="off">

                            </div>
                            <div class="col-sm-1 ">

                                <input type="text" id="pay" name="billing_info[{{$payment->id}}][pay]"
                                    value="{{$payment->id}}" hidden>

                            </div>
                            <div class="col-sm-3 ">

                                <textarea type="text" name="billing_info[{{$payment->id}}][narration]"
                                    id="billing_narration_1" class="billing_narration form-control valid"
                                    aria-invalid="false"> </textarea>
                            </div>
                        </div>
                        @endforeach

                        <div class="form-group row">

                            <label for="inputEmail3" class="col-sm-3 control-label">Due Amount</label>

                            <div class="col-sm-9 ">


                                <span id="due_amount">{{$bill->total_amount}}</span>
                            </div>


                        </div>



                    </form>

                    <div class="m-t-20 text-center">
                        <button type="submit" id="close_the_bill" data-id="{{$bill->id}}" class="btn btn-primary btn-lg close_the_bill"
                            >Update</button>
                    </div>

                </div>

            </div>
        </div>
    </div>

</div>
@stop

@section('javascript')
<script>



$(".billing_amount").keyup(function(event) {
    //alert($(this).val());
    var billed_amount_total = gettotalamount();
    var unbilled_amount_total = $("#total_unbilled_amount").val();
    var due_amount = parseFloat(unbilled_amount_total) - parseFloat(billed_amount_total);
    //alert(due_amount);
    if (due_amount < 0) {
        alert('Total bill amount is exceeding Now');
        document.getElementById("close_the_bill").disabled = true;
       
    }else if(due_amount > 0){
  
        document.getElementById("close_the_bill").disabled = true;
       
   
    }else{
        document.getElementById("close_the_bill").disabled = false;
    }
    $("#due_amount").html(parseFloat(due_amount));
});

$(".close_the_bill").click(function() {
    var bill = $(this).data('id');
    
    var billed_amount_total = gettotalamount();
    var unbilled_amount_total = $("#total_unbilled_amount").val();
    var due_amount = parseFloat(parseFloat(unbilled_amount_total) - parseFloat(billed_amount_total));
    if (due_amount == 0) {
        
        print_etr(bill);
    }else{
        alert('please enter payment methods');
        return false;
    }
    

});

function gettotalamount() {
    var filled_amount = [];
    $(".billing_amount").each(function(index) {
        filled_amount.push(parseFloat($(this).val()));
    });
    var total = 0;
    for (var i = 0; i < filled_amount.length; i++) {
        total += parseFloat(filled_amount[i]) || 0;
    }
    return total;
}






function print_etr(bill_id) {

    var confirm_text = 'bill';
    
    alert(bill_id);
    //alert(bill_id);
    //   var isconfirmed=confirm("Do you want to print "+confirm_text+"?");
    //   if (isconfirmed) 
    //   {
    $.ajax({
        url: '/admin/orders/print-master-bills-etr',
        type: 'POST',

        //NOTE THIS

        data: {
            bill_id: bill_id
        },
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },

        success: function(response) {

            popup(response);


        }
    });


}

function popup(response) {

    //alert('hi');
    var divContents = response;

    var printWindow = window.open('', '', 'width=400');
    printWindow.document.write(`<html><head><title></title>
                    <style>

                       @page{margin:0;size:auto}

                    </style> `);
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');

    printWindow.print();
    printWindow.document.close();
    $("#addmajorgroup").submit();

}
</script>
@stop