@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Generate Bills</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                        <form action="/admin/orders/postpaid/transfer-bill" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                            <div class="row">
                                
                                    <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label>Bill ID</label>
                                                    <input class="form-control" type="text" id="bill_id" name="bill_id" value="{{$id}}"  required autofocus readonly>
                                                    @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label>Waiter ID</label>
                                                    <input class="form-control" type="text" id="waiter_id" name="waiter_id" value="{{$waiter_id}}"  required autofocus readonly>
                                                    @if ($errors->has('name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                        <label>Waiter Name</label>
                                                        <input class="form-control" type="text" id="waiter_name" name="waiter_name" value="{{$waiter_name}}"  required autofocus readonly>
                                                        @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                        <div class="form-group{{ $errors->has('waiter') ? ' has-error' : '' }}" required autofocus>
                                            <label>Waiter To Transfer Bill</label>
                                            <select class="select" id="waiter" name="waiter" onchange="actionValue()">
                                                <option>Select Waiter</option>

                                                @foreach ($orders as $order)
                                                    @if($order->id != $waiter_id)
                                                        <option value="{{ $order->id }}">{{ $order->name }}</option>
                                                    @endif
                                                @endforeach
                                                
                                            </select>
                                        </div>
                                    </div>
                                   
                                </div>
                        
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

// var orders ={!! json_encode($orders); !!};
// //console.log(data);
// var options = '<option value="">Select Waiter</option>';
// orders.forEach(element => {

//     options += '<option value='+element.id+'>'+element.waiter+'</option>';

// });$("#waiter").html(options);
// function actionValue(){

//   var id = $( "#waiter option:selected" ).val();
//   //alert(id);
//   $("#addmajorgroup").attr("action", "/admin/orders/generate-bills/" + id);

// }

    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

