@extends('layouts.admin_layout') @section('title', 'Dashboard') @section("body-content")

<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div class="dash-widget dash-widget5">
                    <span class="dash-widget-icon bg-success"><i class="fa fa-money" aria-hidden="true"></i></span>
                    <div class="dash-widget-info">
                        <h3>$998</h3>
                        <span>Revenue</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div class="dash-widget dash-widget5">
                    <span class="dash-widget-icon bg-info"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    <div class="dash-widget-info">
                        <h3>1072</h3>
                        <span>Users</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div class="dash-widget dash-widget5">
                    <span class="dash-widget-icon bg-warning"><i class="fa fa-files-o"></i></span>
                    <div class="dash-widget-info">
                        <h3>72</h3>
                        <span>Projects</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xl-3">
                <div class="dash-widget dash-widget5">
                    <span class="dash-widget-icon bg-danger"><i class="fa fa-tasks" aria-hidden="true"></i></span>
                    <div class="dash-widget-info">
                        <h3>618</h3>
                        <span>Tasks</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div id="bar-example"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">Project Status</h4>
                        <div id="donutChart" class="rad-chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">Product Status</h4>
                        <div id="areaChart" class="rad-chart"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">Task Status</h4>
                        <div id="area-chart" class="rad-chart"></div>
                    </div>
                </div>
            </div>
        </div>
       
        {{-- <div class="themes">
            <div class="themes-icon"><i class="fa fa-cog"></i></div>
            <div class="themes-body">
                <ul id="theme-change" class="theme-colors">
                    <li><a href="https://dreamguys.co.in/preadmin/light/index.html"><span class="theme-light"></span></a></li>
                    <li><a href="index.html"><span class="theme-orange"></span></a></li>
                    <li><a href="https://dreamguys.co.in/preadmin/purple/index.html"><span class="theme-purple"></span></a></li>
                    <li><a href="https://dreamguys.co.in/preadmin/blue/index.html"><span class="theme-blue"></span></a></li>
                    <li><a href="https://dreamguys.co.in/preadmin/dark/index.html"><span class="theme-dark"></span></a></li>
                    <li><a href="https://dreamguys.co.in/preadmin/rtl/index.html"><span class="theme-rtl">RTL</span></a></li>
                </ul>
            </div>
        </div> --}}
    </div>
    
</div>
@stop