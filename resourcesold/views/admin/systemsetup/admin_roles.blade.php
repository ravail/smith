@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                            <h4 class="page-title">Roles</h4>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <a href="/admin/add-roles" class="btn btn-primary btn-rounded pull-right" id="roles_add"><i class="fa fa-plus"></i> Add Roles</a>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0" id="testTable">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            
                                            <th>Name</th>
                                        
                                           
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $count=1; ?>
                                        @foreach($roles as $role)

                                        <tr>
                                            <td>{{$count}}</td>
                                            
                                           
                                            <td>
                                                {{$role->name}}
                                            </td>
                                       
                                            <td class="text-right">
                                                <div class="dropdown dropdown-action">
                                                    <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                     <a class="dropdown-item" href="/admin/edit-roles/{{$role->id}}" id="roles_edit_{{$role->id}}"><i class="fa fa-pencil m-r-5"></i> Edit</a> 
                                                    <a class="dropdown-item" href="/admin/role-permissions/{{$role->id}}" id="roles_permissions_{{$role->id}}"><i class="fa fa-pencil m-r-5"></i> Permissions</a>                                                        
                                                        <a class="dropdown-item" href="/admin/delete-roles/{{$role->id}}" id="roles_delete_{{$role->id}}"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php $count++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
@stop 

@section('javascript')

<script> 
$(document).ready(function() {
    $('#testTable').DataTable();
} );
var groups ={!! json_encode($roles); !!};
//console.log(groups);
if('{{\Auth::user()->roles_add}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    $("#roles_add").hide();
}

if('{{\Auth::user()->roles_edit}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    //alert(majorGroups.length);
    for(var i = 0 ; i < groups.length ; i++){
        //alert(majorGroups.id);
        $("#roles_edit_"+groups[i].id).hide();
    }
}  

if('{{\Auth::user()->roles_delete}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    for(var i = 0 ; i < groups.length ; i++){

        $("#roles_delete_"+groups[i].id).hide();

    }
} 
if('{{\Auth::user()->roles_permissions}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
    //alert('{{\Auth::user()->sub_major_groups_manager_add}}');
    for(var i = 0 ; i < groups.length ; i++){

        $("#roles_permissions_"+groups[i].id).hide();

    }
}
</script>

@stop
         