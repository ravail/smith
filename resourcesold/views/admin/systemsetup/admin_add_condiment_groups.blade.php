@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Add Condiment Group</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <form action="/admin/add-condiment-groups" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Title</label>
                                    <input class="form-control" type="text" id="name" name="name" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('max_selection_limit') ? ' has-error' : '' }}">
                                    <label>Max Selection Limit</label>
                                    <input maxlength="255" placeholder="Selection Limit" required="" class="form-control" min="1" name="max_selection_limit" type="number" aria-required="true">
                                    @if ($errors->has('max_selection_limit'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('max_selection_limit') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('condiments') ? ' has-error' : '' }}" required autofocus>
                                                <label>Condiments</label>
                                                <select class="select" id="condiments" name="condiments">
                                                    <option>Select</option>
                                                
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>
                                
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>
   
var condiments ={!! json_encode($condiments); !!};

var options = '<option value="">Select Condiments</option>';
condiments.forEach(element => {
    options += '<option value='+element.id+'>'+element.name+'</option>';
    
});
$("#condiments").html(options);

    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

