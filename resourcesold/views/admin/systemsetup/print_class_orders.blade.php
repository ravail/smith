@extends('layouts.cashier_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">

                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-sm-4 col-4">
                        <h4 class="page-title">Docket</h4>
                        <audio id="myAudio">
                                <source src="/definite.ogg" type="audio/ogg">
                                <source src="/definite.mp3" type="audio">
                                
                              </audio>
                        </div>
                        <div class="col-sm-8 col-8 text-right m-b-20">
                            <h4 class="page-title">Dispense Station : @foreach ($printClasses as $printClass)
                                <?php if($printClass->id == $print){ ?>

                                    {{$printClass->name}}
                                <?php } ?>
                               @endforeach
                        </h4>
                            {{-- <div class="view-icons">
                                <a href="products.html" class="grid-view btn btn-link"><i class="fa fa-th"></i></a>
                                <a href="products-list.html" class="list-view btn btn-link active"><i class="fa fa-bars"></i></a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row" id="masachusa">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-border custom-table m-b-0" id="tbl" >
                                    <thead>
                                        <tr>

                                            <th>Order No</th>
                                            <th>Date And Time</th>
                                            <th>Table No</th>
                                            <th>No Of Guest</th>
                                            <th>Item description</th>
                                            <th>Condiments</th>
                                            <th>Waiter</th>
                                            <th>Action</th>
                                            <th>Invoice</th>
                                            <th>Docket</th>
                                                
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($orders != null && $order_items != null){ 
                                            ?>
                                        @foreach ($orders as $order)

                                                <?php   $order_id = 0;
                                                        $items_description = ""; 
                                                        $items = array();
                                                        $count = 0;
                                                        $condiments = "";
                                                ?>

                                            @foreach($order_items as $order_item)

                                                <?php if($order_item->post_paid_orders_id == $order->id){ 
                                                    
                                                    //alert(result.order_items[j]['name']);
                                                    $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity ." " ;
                                                    //alert(items_description);
                                                    $items[$count] = $order_item; 
                                                   
                                                }
                                                
                                            ?>
                                                
                                            @endforeach
                                            <?php if(!empty($items)){ ?>
                                                <?php if($order->condiments == null){ $condiments = "---"; }  ?>
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                        
                                                        <td> {{$order->date_time}}</td>
                                                        
                                                        <td>
                                                            {{$order->table_no}}
                                                        </td>
                                                        
                                                        <td>{{$order->no_of_guests}}</td>
                                                        <td>{{$items_description}}</td>
                                                        <td>{{$condiments}} </td>
                                                        <td>{{$order->waiter}}</td> 

                                                       

                                                            <?php if($items[0]->cooking_status =="NEW"){  
                                                                    ?> 

                                                            <td><a class="btn btn-danger" href="/admin/orders/status/NEW/{{$items[0]['print_class_id']}}/{{$items[0]['post_paid_orders_id']}}">{{$items[0]['cooking_status']}}</a></td>
                                                                        <td><a class="btn btn-success" onclick="printInvoice({{$order->bill_id}})">Print Invoice</a></td>
                                                            
                                                            <?php }elseif($items[0]->cooking_status =="PREPARATION"){ 
                                                                ?>
                                                                        <td><a class="btn btn-primary" href="/admin/orders/status/PREPARATION/{{$items[0]['print_class_id']}}/{{$items[0]['post_paid_orders_id']}}">{{$items[0]['cooking_status']}}</a></td>
                                                                        <td>...</td>
                                                                    
                                                            <?php }elseif($items[0]->cooking_status =="READY TO PICK"){
                                                            ?>     
                                                                    <td><a class="btn btn-info" href="/admin/orders/status/READY TO PICK/{{$items[0]['print_class_id']}}/{{$items[0]['post_paid_orders_id']}}">{{$items[0]['cooking_status']}}</a></td>
                                                                    <td>...</td>
                                                                
                                                            <?php } ?>
                                                            <td><a class="btn btn-success" onclick="printBill({{$order->id}},{{$items[0]->print_class_id}})">Print Docket</a></td>
                                                        
                                                        <?php } ?>        
                                            
                                                    </tr>
                                        @endforeach
                                                <?php } ?>
                                    </tbody>
                                </table>
                               
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>

      
            <iframe src="/definite.mp3" allow="autoplay" id="audio" style="display:none"></iframe>
            {{-- <audio id="myAudio" > 
                    <source src="/definite.mp3">
                    <source src="/definite.ogg">
            </audio> --}}
@stop 

@section('javascript')

<script>
// var aud = document.getElementById("myAudio");
// aud.play();

// var sound = document.getElementById('audio');
// sound.play();
var orders ={!! json_encode($orders); !!} ;
var order_items = {!! json_encode($order_items) !!} ;   
console.log(order_items);
var id = 0;
var x=0;
var items_description = ""; 
var condiments = "";
var b;
var tr=null;
var auto_docket = 0;



if(orders != null && order_items !=null){
$(document).ready(function(){
    
    setInterval(function(){ 
        //code goes here that will be run every 5 seconds.    
        $.ajax({
            type: "GET",
            url: "/public/print-class-orders-update",
            success: function(result) {
                console.log(result);
                $('#tbl>tbody').empty();
                console.log(result.orders);
                console.log("items");
                console.log(result.order_items);
                if(result.orders !=null && result.order_items !=null){
                    //alert(result.order_items);
                //     console.log("masachusa");
                // for(var index=1;index < result.orders.length;){
                //     console.log(result.orders);
                //     if(result.orders[index]['order_id']==result.orders[index-1]['order_id']){
                //         result.orders.splice(index,1);
                       
                //     }else{
                //         index++;
                //     }
                // }
               
                var orderLength = result.orders.length;
                console.log(orderLength);
                var orderItemsLength = result.order_items.length;
                for(var i = 0; i < orderLength ; i++){
                   var flag = false;
                   var items = [];
                   var count = 0;
                   var auto_invoice = result.orders[i]['auto_invoice'];
                   

                   for(var j = 0; j < orderItemsLength ; j++){
                    if(result.order_items[j]['post_paid_orders_id'] == result.orders[i]['id'] ){

                        if(result.order_items[j]['cooking_status'] == "NEW"){
                            var aud = document.getElementById("myAudio");
                            aud.play();
                        }
                        if(result.order_items[j]['do_print'] == "yes"){
                            flag = true;
                        }
                        //alert(result.order_items[j]['name']);
                        items_description += "item: " + result.order_items[j]['name'] + "<br>" + "QTY: " + result.order_items[j]['quantity'] + "<br>" ;
                        //alert(items_description);
                        items[count] = result.order_items[j];
                        
                      
                        count++;
                    }

                   }
                   if(result.orders[i]['condiments'] ==null){
                        condiments = "---";
                   }
                   if(items.length != 0){
                    if(items[0]['cooking_status'] == "NEW"){

                        tr = '<tr class="trTblPendingHrResponse">' +
                                
                        '<td>' + result.orders[i]['id'] + '</td>' +
                        '<td>' + result.orders[i]['date_time'] + '</td>' +
                        '<td>' + result.orders[i]['table_no'] + '</td>' +
                        '<td>' + result.orders[i]['no_of_guests'] + '</td>' +
                        '<td>' + items_description + '</td>' +
                        '<td>' + condiments + '</td>' +
                        '<td>' + result.orders[i]['waiter'] + '</td> ' +
                        '<td><a class="btn btn-danger" href=/admin/orders/status/NEW/'+ items[0]['print_class_id'] + '/' + items[0]['post_paid_orders_id'] + '>' + items[0]['cooking_status'] + '</a></td>' +
                        '<td>---</td>' +
                        // '<td><a class="btn btn-success" onclick="printInvoice('+result.orders[i]['bill_id']+')">Print </a></td>' +
                       
                        
                        '<td><a class="btn btn-success" onclick="printBill('+ result.orders[i]['id'] +',' + items[0]['print_class_id'] +')">Print Docket</a></td>' 
                        '</tr>';
                            ;
                           
                            
                            if(items[0]['auto_docket'] == 0){
                                jQuery.ajax({
                                            url: '/admin/orders/print-docket',
                                            type: 'POST',
                                            async:false,   //NOTE THIS
                                            data:{bill_id:result.orders[i]['id'] ,print_class_id:items[0]['print_class_id']},
                                            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                                            
                                        success: function (response) {

                                            console.log(response);
                                            var divContents = response;
                                            //alert(response);
                                            var printWindow = window.open('', '', 'width=400');
                                            printWindow.document.write(`<html><head><title></title>
                                            
                                                <style>
                                                    @media print {
                                                    html, body {
                                                        width: auto;
                                                        height: auto;        
                                                    }
                                                    .page {
                                                        margin: 0;
                                                        border: initial;
                                                        border-radius: initial;
                                                        width: initial;
                                                        min-height: initial;
                                                        box-shadow: initial;
                                                        background: initial;
                                                        page-break-after: always;
                                                    }
                                                }
                                                @page{margin:0;size:auto}
                                                </style>

                                            `);
                                            printWindow.document.write('</head><body>');
                                            printWindow.document.write(divContents);
                                            printWindow.document.write('</body></html>');
                                        //console.log(printWindow);
                                        printWindow.focus();
                                            printWindow.print();
                                            printWindow.document.close();
                                            

                                    }
                                });

                                jQuery.ajax({
                                    url: '/admin/orders/updatePrint',
                                    type: 'POST',
                                    //async:false,   //NOTE THIS
                                    data:{order_id:result.orders[i]['id'],auto_invoice:0,auto_docket:1,print_class_id:items[0]['print_class_id']},
                                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                                                
                                        success: function (response) {
                                            console.log(response);
                                        }
                                });

                                jQuery.ajax({
                                    url: '/admin/orders/status/NEW/'+ + items[0]['print_class_id'] + '/' + items[0]['post_paid_orders_id'],
                                    type: 'GET',
                                    //async:false,   //NOTE THIS
                                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                                                
                                        success: function (response) {
                                            console.log(response);
                                        }
                                });

                            }
                            if(flag == true && auto_invoice == 0){
                                //alert("in asd");
                                jQuery.ajax({
                                        
                                    url: '/admin/orders/print-master-bills',
                                    type: 'POST',
                                    async:false,   //NOTE THIS
                                    data:{bill_id:result.orders[i]['bill_id']},
                                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                                                
                                    success: function (response) {

                                                //console.log(response);
                                        var divContents = response;
                                                
                                        var printWindow = window.open('', '', 'width=400');
                                        printWindow.document.write(`<html><head><title></title>
                                            <style>

                                                @page{margin:0;size:auto}

                                            </style> `);
                                        printWindow.document.write('</head><body >');
                                        printWindow.document.write(divContents);
                                        printWindow.document.write('</body></html>');
                                        printWindow.focus();                
                                                
                                        printWindow.print();
                                        printWindow.document.close();
                                                //window.print();

                                    }
                                });

                                        jQuery.ajax({
                                            url: '/admin/orders/updatePrint',
                                            type: 'POST',
                                            //async:false,   //NOTE THIS
                                            data:{order_id:result.orders[i]['id'],auto_invoice:1,auto_docket:1,print_class_id:items[0]['print_class_id']},
                                            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                                                        
                                                success: function (response) {
                                                    console.log(response);
                                                }
                                        });

                                }

                    }
                    else if(items[0]['cooking_status'] == "PREPARATION"){
                                //alert(result.orders[i]['id'] );
                                //alert(items[0]['post_paid_orders_id']);
                         tr = '<tr class="trTblPendingHrResponse">' +
                                
                                '<td>' + result.orders[i]['id'] + '</td>' +
                                '<td>' + result.orders[i]['date_time'] + '</td>' +
                                '<td>' + result.orders[i]['table_no'] + '</td>' +
                                '<td>' + result.orders[i]['no_of_guests'] + '</td>' +
                                '<td>' + items_description + '</td>' +
                                '<td>' + condiments + '</td>' +
                                '<td>' + result.orders[i]['waiter'] + '</td> ' +
        
                                '<td><a class="btn btn-primary" href=/admin/orders/status/PREPARATION/'+ items[0]['print_class_id'] + '/' + items[0]['post_paid_orders_id'] + '>' + items[0]['cooking_status'] + '</a></td>' +
                                '<td>...</td>' +                                   
                                '<td><a class="btn btn-success" onclick="printBill('+ result.orders[i]['id'] +',' + items[0]['print_class_id']  +')">Print Docket</a></td>' 
                            '</tr>' ;
                    }
                    else if(items[0]['cooking_status'] == "READY TO PICK"){
                        tr = '<tr class="trTblPendingHrResponse">' +
                                
                                '<td>' + result.orders[i]['id'] + '</td>' +
                                '<td>' + result.orders[i]['date_time'] + '</td>' +
                                '<td>' + result.orders[i]['table_no'] + '</td>' +
                                '<td>' + result.orders[i]['no_of_guests'] + '</td>' +
                                '<td>' + items_description + '</td>' +
                                '<td>' + condiments + '</td>' +
                                '<td>' + result.orders[i]['waiter'] + '</td> ' +
        
                                '<td><a class="btn btn-info" href=/admin/orders/status/READY/'+ items[0]['print_class_id'] + '/' + items[0]['post_paid_orders_id'] + '>' + items[0]['cooking_status'] +'</a></td>' +
                                '<td>...</td>' +                                                                       
                                '<td><a class="btn btn-success" onclick="printBill('+ result.orders[i]['id'] +',' + items[0]['print_class_id']  +')">Print Docket</a></td>' 
                            '</tr>' ;
                    }
                   }
               
                        $('#tbl').append(tr);
                        
                        items_description = "";
                        
                    }
                   
                }
                }
                
            
        });
    }, 9000);
});
}

 function printInvoice(bill_id)
       {
          var confirm_text = 'bill';
          //alert(bill_id);
          var isconfirmed=confirm("Do you want to print "+confirm_text+"?");
          if (isconfirmed) 
          {
            jQuery.ajax({
                url: '/admin/orders/print-master-bills',
                type: 'POST',
                async:false,   //NOTE THIS
                data:{bill_id:bill_id},
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                
              success: function (response) {

                console.log(response);
                var divContents = response;
                
                var printWindow = window.open('', '', 'width=400');
                printWindow.document.write(`<html><head><title></title>
                    <style>

                        
                        @page{margin:0;size:auto}

                    </style> `);
                printWindow.document.write('</head><body >');
                printWindow.document.write(divContents);
                printWindow.document.write('</body></html>');
                printWindow.focus();                
                
                printWindow.print();
                printWindow.document.close();
                //window.print();

              }
            });
          }
       }

function printBill(bill_id,print_class_id)
{
  // alert(bill_id);
   var confirm_text = 'bill';
   //alert(bill_id);
   var isconfirmed=confirm("Do you want to print "+confirm_text+"?");
   if (isconfirmed) 
   {
     jQuery.ajax({
         url: '/admin/orders/print-docket',
         type: 'POST',
         async:false,   //NOTE THIS
         data:{bill_id:bill_id,print_class_id:print_class_id},
         headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
         
       success: function (response) {

        //console.log(response);
        var divContents = response;
        //alert(response);
        var printWindow = window.open('', '', 'width=400');
        printWindow.document.write(`<html><head><title></title>
        
            <style>
                @media print {
                html, body {
                    width: auto;
                    height: auto;        
                }
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }
            @page{margin:0;size:auto}
            </style>

        `);
        printWindow.document.write('</head><body>');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
       console.log(printWindow);
       printWindow.focus();
        printWindow.print();
        printWindow.document.close();
        

       }
     });
   }
}
</script>
@stop