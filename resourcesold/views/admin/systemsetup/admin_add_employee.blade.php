@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Add Employee</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <form action="/admin/add-employee" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label>
                                    <input class="form-control" type="text" id="name" name="name" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('restaurant_id') ? ' has-error' : '' }}" required autofocus>
                                                <label>Restaurant</label>
                                                <select class="select" id="restaurant_id" name="restaurant_id" required="">
                                                    <option>Select</option>
                                                   
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}" required autofocus>
                                                <label>Roles</label>
                                                <select class="select" id="role_id" name="role_id">
                                                    <option>Select</option>
                                                   
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label>Email</label>
                                            <input class="form-control" type="email" id="email" name="email" required autofocus>
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label>Password</label>
                                                <input class="form-control" type="password" id="password" name="password" required autofocus>
                                                @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                            </div>   
                                        
                                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                                <label>Phone Number</label>
                                                <input class="form-control" type="text" id="phone_number" name="phone_number" required autofocus>
                                                @if ($errors->has('phone_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                </span>
                                                @endif
                                            </div> 
                                        <div class="form-group{{ $errors->has('badge_number') ? ' has-error' : '' }}">
                                            <label>Badge Number</label>
                                            <input class="form-control" type="text" id="badge_number" name="badge_number" required autofocus>
                                                @if ($errors->has('badge_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('badge_number') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('id_number') ? ' has-error' : '' }}">
                                            <label>Id Number</label>
                                            <input class="form-control" type="text" id="id_number" name="id_number" required autofocus>
                                            @if ($errors->has('id_number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('id_number') }}</strong>
                                            </span>
                                            @endif
                                        </div> 
                                        {{-- <div class="form-group{{ $errors->has('date_employeed') ? ' has-error' : '' }}">
                                            <label>Date Employeed</label>
                                            <input maxlength="255" placeholder="Date employeed" required="" class="form-control datepicker" readonly="" name="date_employeed" type="text" aria-required="true">
                                                @if ($errors->has('date_employeed'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('date_employeed') }}</strong>
                                                </span>
                                            @endif
                                        </div> --}}
                                        
                                        <div class="form-group{{ $errors->has('complementary_number') ? ' has-error' : '' }}">
                                                <label>Complementary Number</label>
                                                <input class="form-control" type="text" id="complementary_number" name="complementary_number" required autofocus>
                                                @if ($errors->has('complementary_number'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('complementary_number') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group{{ $errors->has('complementary_amount') ? ' has-error' : '' }}">
                                                    <label>Complementary Amount</label>
                                                    <input class="form-control" type="text" id="complementary_amount" name="complementary_amount" required autofocus>
                                                    @if ($errors->has('complementary_amount'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('complementary_amount') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
                                                        <label>Max Discount For Pay Later Order(%)</label>
                                                        <input class="form-control valid" placeholder="Value" min="0" max="100" name="discount" type="number" value="0" aria-invalid="false">
                                                        @if ($errors->has('discount'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('discount') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>    
                                   
                                <div class="form-group{{ $errors->has('a_image') ? ' has-error' : '' }}">
                                    <label>Product Images</label>
                                    <div>
                                        <input class="form-control" type="file" id="a_image" name="a_image" >
                                    </div>
                                    @if ($errors->has('a_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('a_image') }}</strong>
                                    </span>
                                    @endif
                                    
                                    {{-- <div class="row">
                                        <div class="col-md-3 col-sm-3 col-4 col-lg-3 col-xl-2">
                                            <div class="product-thumbnail" id="imgDiv">
                                                <img src="" class="img-thumbnail img-fluid" alt="">
                                                <span class="product-remove" title="remove"><i class="fa fa-close"></i></span>
                                            </div>
                                        </div>
                                        
                                    </div> --}}
                                </div>
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

    var restaurants = {!! json_encode($restaurants); !!};
    var roles = {!! json_encode($roles) !!};

//console.log(data);
var options = '<option value="">Restaurants</option>';
restaurants.forEach(element => {
    options += '<option value='+element.id+'>'+element.name+'</option>';
      
});
$("#restaurant_id").html(options);
   
var options = '<option value="">Roles</option>';
roles.forEach(element => {
    options += '<option value='+element.id+'>'+element.name+'</option>';
    
});
$("#role_id").html(options);
   

    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

