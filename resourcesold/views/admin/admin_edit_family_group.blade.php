@extends('layouts.admin_layout') @section('title', 'Major Group Manager') @section("body-content")

        <div class="page-wrapper">
                <div class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <h4 class="page-title">Edit Family Group</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                        <form action="/admin/edit-family-group/{{$existGroup->id}}" method="POST" enctype="multipart/form-data" id="addmajorgroup">
                            {{ csrf_field() }}
                             
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label>
                                <input class="form-control" type="text" id="name" name="name" value="{{$existGroup->name}}" required autofocus>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('menu_item_groups_id') ? ' has-error' : '' }}" required autofocus>
                                                <label>Menu Item Group Name</label>
                                                <select class="select" id="menu_item_groups_id" name="menu_item_groups_id">
                                                    <option>Select</option>
                                                    @foreach ($groups as $group)
                                                        <option value="{{ $group->id }}"{{$group && $group->id==$existGroup->menu_item_groups_id ? 'selected': ''}}>{{ $group->name }}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('gl_account') ? ' has-error' : '' }}" required autofocus>
                                                <label>GL Account No</label>
                                                <select class="select" id="gl_account" name="gl_account">
                                                    <option>Select</option>
                                                    @foreach ($glAccounts as $glAccount)
                                                        <option value="{{ $glAccount->id }}"{{$glAccount && $glAccount->id==$existGroup->gl_account ? 'selected': ''}}>{{ $glAccount->account_number }}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>

                                   
                                <div class="form-group{{ $errors->has('a_image') ? ' has-error' : '' }}">
                                    <label>Product Images</label>
                                    <div>
                                        <input class="form-control" type="file" id="a_image" name="a_image" >
                                    </div>
                                    @if ($errors->has('a_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('a_image') }}</strong>
                                    </span>
                                    @endif
                                    
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-4 col-lg-3 col-xl-2">
                                            <div class="product-thumbnail" id="imgDiv">
                                                <img src="{{URL::asset('img/'.$existGroup->a_image)}}" class="img-thumbnail img-fluid" alt="">
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                               
                              
                                <div class="m-t-20 text-center">
                                    <button class="btn btn-primary btn-lg">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               
            </div>
@stop

@section('javascript')


<script>

   

    function showprofile(input) {
		var file, img;
		if (input.files && input.files[0]) {
			
		  var reader = new FileReader();
	  
		  reader.onload = function(e) {
			img = new Image();
			img.src=e.target.result;;
			img.onload = function () {
				//alert(this.width + " " + this.height);
				if(this.width < 200)
					{
						alert('image width must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					else if(this.height < 200)
					{
						alert('image height must be greater than 200');
						$('#addmajorgroup').trigger("reset");
						$('#imgDiv').attr('src', '');
						return;
					}
					$('#imgDiv').attr('src', e.target.result);
			};
			
		  }
	  
		  reader.readAsDataURL(input.files[0]);
		}
	  }
	  
	  $("#a_image").change(function() {
        //   /alert("change");
		showprofile(this);
	  });
</script>

@stop

