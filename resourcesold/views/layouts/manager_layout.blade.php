<!DOCTYPE html>
<html>


<!-- Mirrored from dreamguys.co.in/preadmin/orange/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Nov 2018 05:38:01 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets/img/favicon.png')}}">
    <title>Preadmin - Bootstrap Admin Template</title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/fullcalendar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/plugins/morris/morris.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/style.css')}}">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <style>
    
    
.sidebar-menu li a {
    font-size: 12px;
}
#sidebar-menu ul ul a {
    display: block;
    padding: 0 20px 0 31px;
}
#sidebar-menu ul ul ul a {
    padding-left: 33px;
}

    </style>
</head>

<body>
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="index.html" class="logo">
                    <img src="{{URL::asset('assets/img/logo.png')}}" width="40" height="40" alt="">
                </a>
            </div>
            <div class="page-title-box pull-left">
                <h3>Preadmin</h3>
            </div>
            <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <ul class="nav user-menu pull-right">
                <li class="nav-item dropdown d-none d-sm-block">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge badge-pill bg-primary pull-right">3</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="notification-list">
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">
												<img alt="John Doe" src="{{URL::asset('assets/img/user.jpg')}}" class="img-fluid">
											</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
												<p class="noti-time"><span class="notification-time">4 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">V</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
												<p class="noti-time"><span class="notification-time">6 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">L</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
												<p class="noti-time"><span class="notification-time">8 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">G</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
												<p class="noti-time"><span class="notification-time">12 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">V</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
												<p class="noti-time"><span class="notification-time">2 days ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="activities.html">View all Notifications</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown d-none d-sm-block">
                    <a href="javascript:void(0);" id="open_msg_box" class="hasnotifications nav-link"><i class="fa fa-comment-o"></i> <span class="badge badge-pill bg-primary pull-right">8</span></a>
                </li>
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
							<img class="rounded-circle" src="{{URL::asset('assets/img/user.jpg')}}" width="40" alt="Admin">
							<span class="status online"></span>
						</span>
						<span>Admin</span>
                    </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="profile.html">My Profile</a>
						<a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
						<a class="dropdown-item" href="settings.html">Settings</a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu pull-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="profile.html">My Profile</a>
                    <a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
                    <a class="dropdown-item" href="settings.html">Settings</a>
                    <a class="dropdown-item" href="login.html">Logout</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">Main Menu</li>
                        <li class="active">
                            <a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                       
                        <li class="submenu">
                            <a href="#"><i class="fa fa-video-camera camera" aria-hidden="true"></i> <span> Menu Setup</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li><a href="voice-call.html">Major-Groups-Manager</a></li>
                                <li><a href="video-call.html">Sub-Major-Groups</a></li>
                                <li><a href="incoming-call.html">Menu Item Groups</a></li>
                                <li><a href="incoming-call.html">Offers</a></li>
                                <li><a href="incoming-call.html">Family Groups</a></li>
                                <li><a href="incoming-call.html">Alcoholic Family Groups</a></li>
                                <li><a href="incoming-call.html">Alcoholic Sub Family Groups</a></li>

                                <a href="#"><i class="fa fa-video-camera camera" aria-hidden="true"></i> <span> Manu Items</span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled" style="display: none;">

                                <li><a href="incoming-call.html">General Item</a></li>
                                <li><a href="incoming-call.html">Offers/Theme Nights</a></li>
                                

                                </ul>
                                
                            </ul>
                        </li>
                       
                        <li>
                            <a href="contacts.html"><i class="fa fa-address-card" aria-hidden="true"></i> App Users</a>
                        </li>
                      
                        <li class="submenu">
                            <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i> <span> Manage Orders</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li><a href="blog.html">Prepaid Orders</a></li>
                                
                                <a href="#"><span> Postpaid Orders</span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled" style="display: none;">
                                    <li><a href="blog.html">Open Orders</a></li>
                                    <li><a href="blog.html">Bills</a></li>
                                    
                                    <li><a href="blog.html">Closed Orders</a></li>
                                    <li><a href="blog.html">Closed Orders Payments</a></li>
                                    
                                    
                                </ul>

                                <li><a href="add-blog.html">Completed Orders</a></li>
                                
                            </ul>
                        </li>
                        
                        <li class="submenu">
                            <a href="#"><i class="fa fa-laptop" aria-hidden="true"></i> <span> System Setup</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                
                                <li><a href="tabs.html">Employee</a></li>
                              
                                <li><a href="tabs.html">Table-Manager</a></li>
                                
                            </ul>
                        </li>
                        <li>
                            <a href="widgets.html"><i class="fa fa-th" aria-hidden="true"></i> Recipes</a>
                        </li>
                       
                        <li class="submenu">
                            <a href="#"><i class="fa fa-edit" aria-hidden="true"></i> <span> Reports</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li><a href="form-basic-inputs.html">Payment Sales Summary</a></li>
                                <li><a href="form-input-groups.html">Menu Item General Sales</a></li>
                                <li><a href="form-horizontal.html">Menu Item Sales with Plu</a></li>
                                <li><a href="form-vertical.html">Menu Item Sales without Plu</a></li>
                                <li><a href="form-input-groups.html">Family Group Sales</a></li>
                                <li><a href="form-input-groups.html">Family Group Sales With GI</a></li>
                                <li><a href="form-input-groups.html">Menu Item Group Sales</a></li>
                                <li><a href="form-input-groups.html">Major Group Sales</a></li>
                                <li><a href="form-input-groups.html">Waiter With Family Group</a></li>
                                <li><a href="form-input-groups.html">Condiments Sales Plu Report</a></li>
                                <li><a href="form-input-groups.html">Discounts Reports</a></li>
                                <li><a href="form-input-groups.html">Cashier Reports</a></li>
                                <li><a href="form-input-groups.html">Discounts Reports With Orders</a></li>
                                <li><a href="form-input-groups.html">Complementary Reports</a></li>
                                <li><a href="form-input-groups.html">Cancelled Orders Reports</a></li>
                                <li><a href="form-input-groups.html">Wallet Ledger Entries</a></li>
                                
                                
                            </ul>
                        </li>
                   
                                                
                    </ul>
                </div>
            </div>
        </div>
        
    </div>
    @yield("body-content")

    <div class="sidebar-overlay" data-reff=""></div>
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery-3.2.1.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('assets/js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.slimscroll.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/plugins/morris/morris.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/app.js')}}"></script>
</body>


<!-- Mirrored from dreamguys.co.in/preadmin/orange/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Nov 2018 05:38:25 GMT -->
</html>