<!DOCTYPE html>
<html>


<!-- Mirrored from dreamguys.co.in/preadmin/orange/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Nov 2018 05:38:01 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets/img/favicon.png')}}">
    <meta name="_token" content="{{csrf_token()}}" />


    <title>The Smith Hotels</title>
    
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/fullcalendar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/plugins/morris/morris.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/style.css')}}">
    
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.3.1017/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.3.1017/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.3.1017/styles/kendo.material.mobile.min.css" />
    
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap-clockpicker.min.css')}}">

   
<!------ Include the above in your HEAD tag ---------->
 
    <style>
    #sidebar-menu ul ul a {
    display: block;
    padding: 0 9px 0 17px !important;
    line-height: 30px;
    min-height: 30px;
}
    
.sidebar-menu li a {
    font-size: 12px;
   
}
#sidebar-menu ul ul a {
    display: block;
    padding: 0 20px 0 31px;
}
#sidebar-menu ul ul ul a {
    padding-left: 33px;
}
.product-det > img {
    top: 0;
    width: 73px;
    position: relative !important;
   
    left: 0;
}

    </style>
</head>

<body>
    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="/admin/dashboard" class="logo">
                    <img src="{{URL::asset('assets/img/logo.png')}}" width="40" height="40" alt="">
                </a>
            </div>
            <div class="page-title-box pull-left">
                <h3>Smith-Hotels</h3>
            </div>
            <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <ul class="nav user-menu pull-right">
                {{-- <li class="nav-item dropdown d-none d-sm-block">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge badge-pill bg-primary pull-right">3</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="notification-list">
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">
												<img alt="John Doe" src="{{URL::asset('assets/img/user.jpg')}}" class="img-fluid">
											</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
												<p class="noti-time"><span class="notification-time">4 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">V</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
												<p class="noti-time"><span class="notification-time">6 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">L</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
												<p class="noti-time"><span class="notification-time">8 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">G</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
												<p class="noti-time"><span class="notification-time">12 mins ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-message">
                                    <a href="activities.html">
                                        <div class="media">
											<span class="avatar">V</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
												<p class="noti-time"><span class="notification-time">2 days ago</span></p>
											</div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="activities.html">View all Notifications</a>
                        </div>
                    </div>
                </li> --}}
                {{-- <li class="nav-item dropdown d-none d-sm-block">
                    <a href="javascript:void(0);" id="open_msg_box" class="hasnotifications nav-link"><i class="fa fa-comment-o"></i> <span class="badge badge-pill bg-primary pull-right">8</span></a>
                </li> --}}
                <li class="nav-item dropdown has-arrow">
                    <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                        <span class="user-img">
							<img class="rounded-circle" src="{{URL::asset('assets/img/user.jpg')}}" width="40" alt="Admin">
							<span class="status online"></span>
						</span>
						<span>{{\Auth::user()->name}}</span>
                    </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="/admin/userProfile">My Profile</a>
						<a class="dropdown-item" href="/admin/roles">Settings</a>
						<a class="dropdown-item" href="/admin/employee-changepass">Change Password</a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
					</div>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu pull-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="profile.html">My Profile</a>
                    <a class="dropdown-item" href="edit-profile.html">Edit Profile</a>
                    <a class="dropdown-item" href="settings.html">Settings</a>
                    <a class="dropdown-item" href="login.html">Logout</a>
                </div>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="menu-title">Main Menu</li>
                        <li class="{{ Request::segment(1) === '/admin/dashboard' ? 'active' : null }}" id="dash">
                            <a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                       
                        <li class="submenu">
                            <a href="#" id="menu_setup_view"><i class="fa fa-share" aria-hidden="true"></i><span> Menu Setup</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li class="{{ Request::segment(1) === 'admin/major-group-managers' ? 'active' : null }}" id='major_groups_manager_view'><a href="/admin/major-group-managers"><i class="fa fa-circle-o" aria-hidden="true"></i>Major-Groups-Manager</a></li>
                                <li class="{{ Request::segment(1) === 'admin/sub-major-groups' ? 'active' : null }}" id='sub_major_groups_manager_view'><a href="/admin/sub-major-groups"><i class="fa fa-circle-o" aria-hidden="true"></i>Sub-Major-Groups</a></li>
                                <li class="{{ Request::segment(1) === 'admin/menu-item-groups' ? 'active' : null }}" id='menu_item_groups_view'><a href="/admin/menu-item-groups"><i class="fa fa-circle-o" aria-hidden="true"></i>Menu Item Groups</a></li>
                                <li class="{{ Request::segment(1) === 'admin/offers' ? 'active' : null }}" id='offers_view'><a href="/admin/offers"><i class="fa fa-circle-o" aria-hidden="true"></i>Offers</a></li>
                                <li class="{{ Request::segment(1) === 'admin/gl-accounts' ? 'active' : null }}" id="gl_view"><a href="/admin/gl-accounts" ><i class="fa fa-circle-o" aria-hidden="true"></i>GL Accounts</a></li>   
                                <li class="{{ Request::segment(1) === 'admin/plu-numbers' ? 'active' : null }}" id='plu_view'><a href="/admin/plu-numbers"><i class="fa fa-circle-o" aria-hidden="true"></i>PLU Number</a></li>                                                                                             
                                <li class="{{ Request::segment(1) === 'admin/family-groups' ? 'active' : null }}" id='family_groups_view'><a href="/admin/family-groups"><i class="fa fa-circle-o" aria-hidden="true"></i>Family Groups</a></li>
                                <li class="{{ Request::segment(1) === 'admin/alcoholic-family-groups' ? 'active' : null }}" id='alcoholic_family_groups_view'><a href="/admin/alcoholic-family-groups"><i class="fa fa-circle-o" aria-hidden="true"></i>Alcoholic Family Groups</a></li>
                                <li class="{{ Request::segment(1) === 'admin/alcoholic-sub-family-groups' ? 'active' : null }}" id='alcoholic_sub_family_groups_view'><a href="/admin/alcoholic-sub-family-groups"><i class="fa fa-circle-o" aria-hidden="true"></i>Alcoholic Sub Family Groups</a></li>

                                <a href="#" id='menu_Item_view'><i class="fa fa-share" aria-hidden="true"></i> <span> Menu Items</span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled" style="display: none;">

                                <li class="{{ Request::segment(1) === 'admin/general-items' ? 'active' : null }}" id='general_item_view'><a href="/admin/general-items"><i class="fa fa-circle-o" aria-hidden="true"></i>General Item</a></li>
                                <li id='offer_themes_nights_view'><a href=""><i class="fa fa-circle-o" aria-hidden="true"></i>Offers/Theme Nights</a></li>
                                

                                </ul>
                                
                            </ul>
                        </li>
                        <li class="submenu">
                            <a href="" id='beer_delivery_and_keg_setup_view'><i class="fa fa-share" aria-hidden="true"></i> <span> Beer & Keg Setup</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li class="{{ Request::segment(1) === 'admin/beer-and-keg-sub-major-group' ? 'active' : null }}" id='beer_and_keg_sub_major_group_view'><a href="/admin/beer-and-keg-sub-major-group"><i class="fa fa-circle-o" aria-hidden="true"></i>Delivery Sub Major Group</a></li>
                                <li class="{{ Request::segment(1) === 'admin/delivery-family-group' ? 'active' : null }}" id='delivery_family_groups_view'><a href="/admin/delivery-family-group"><i class="fa fa-circle-o" aria-hidden="true"></i>Delivery Family Group</a></li>
                                <li class="{{ Request::segment(1) === 'admin/delivery-sub-family-group' ? 'active' : null }}" id='delivery_sub_family_groups_view'><a href="/admin/delivery-sub-family-group"><i class="fa fa-circle-o" aria-hidden="true"></i>Delivery Sub Family Group</a></li>
                                <li class="{{ Request::segment(1) === 'admin/delivery-items' ? 'active' : null }}" id='delivery_items_view'><a href="/admin/delivery-items"><i class="fa fa-circle-o" aria-hidden="true"></i>Delivery Items</a></li>
                                
                            </ul>
                        </li>
                        <li id='app_users_view'>
                            <a href=""><i class="fa fa-share" aria-hidden="true"></i> App Users</a>
                        </li>
                      
                        <li class="submenu">
                            <a href="" id='manage_orders_view'><i class="fa fa-share" aria-hidden="true"></i> <span> Manage Orders</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li class="{{ Request::segment(1) === 'admin/orders/prepaid' ? 'active' : null }}" id='prepaid_orders_view'><a href="/admin/orders/prepaid"><i class="fa fa-circle-o" aria-hidden="true"></i>Prepaid Orders</a></li>
                                
                                <a href="#" id="postpaid_orders_view"><i class="fa fa-share" aria-hidden="true"></i> <span> Postpaid Orders</span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled" style="display: none;">
                                    <li class="{{ Request::segment(1) === 'admin/orders/postpaid' ? 'active' : null }}" id='open_orders_view'><a href="/admin/orders/postpaid"><i class="fa fa-circle-o" aria-hidden="true"></i>Open Orders</a></li>
                                    <a href="#" id='bills_view'><i class="fa fa-share" aria-hidden="true"></i>  <span> Bills</span> <span class="menu-arrow"></span></a>
                                    <ul class="list-unstyled" style="display: none;">
    
                                        <li class="{{ Request::segment(1) === 'admin/orders/generate-bills' ? 'active' : null }}" id="generate_bills_view"><a href="/admin/orders/generate-bills"><i class="fa fa-circle-o" aria-hidden="true"></i>Generate Bills</a></li>
                                        <li class="{{ Request::segment(1) === 'admin/orders/master-bills' ? 'active' : null }}" id="master_bills_view"><a href="/admin/orders/master-bills"><i class="fa fa-circle-o" aria-hidden="true"></i>Bill Master</a></li>
                                    
    
                                    </ul>                            
                                    <li class="{{ Request::segment(1) === 'admin/orders/closed' ? 'active' : null }}" id='closed_orders_view'><a href="/admin/orders/closed"><i class="fa fa-circle-o" aria-hidden="true"></i>Closed Orders</a></li>
                                    <li class="{{ Request::segment(1) === 'admin/orders/closed-payment-orders' ? 'active' : null }}" id='closed_orders_payment_view'><a href="/admin/orders/closed-payment-orders"><i class="fa fa-circle-o" aria-hidden="true"></i>Closed Orders Payments</a></li>
                                                                   
                                  
                                </ul>

                                <li class="{{ Request::segment(1) === 'admin/orders/completed' ? 'active' : null }}" id='complete_orders_view'><a href="/admin/orders/completed"><i class="fa fa-circle-o" aria-hidden="true"></i>Completed Orders</a></li>
                                 <li class="{{ Request::segment(1) === 'admin/orders/void-items' ? 'active' : null }}" id='prepaid_orders_view2'><a href="/admin/orders/void-items"><i class="fa fa-circle-o" aria-hidden="true"></i>Void Items</a></li>
                                  <li class="{{ Request::segment(1) === '/admin/orders/requested-order' ? 'active' : null }}" id='prepaid_orders_view'><a href="/admin/orders/requested-order"><i class="fa fa-circle-o" aria-hidden="true"></i>Bill Transfer Requests </a></li>
                                
                            </ul>
                        </li>
                        
                        <li class="submenu">
                            <a href="#" id="system_setup_view"><i class="fa fa-share" aria-hidden="true"></i> <span> System Setup</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li class="{{ Request::segment(1) === 'admin/advertisements' ? 'active' : null }}" id="advertisements_view"><a href="/admin/advertisements"><i class="fa fa-circle-o" aria-hidden="true"></i>Advertisements</a></li>
                                <li id="social_links_view"><a href=""><i class="fa fa-circle-o" aria-hidden="true"></i>Social Links</a></li>
                                <li class="{{ Request::segment(1) === 'admin/reservation-emails' ? 'active' : null }}" id="reservation_email_view"><a href="/admin/reservation-emails"><i class="fa fa-circle-o" aria-hidden="true"></i>Reservation Emails</a></li>
                                
                                <li class="{{ Request::segment(1) === 'admin/payment-methods' ? 'active' : null }}" id="payent_method_view"><a href="/admin/payment-methods"><i class="fa fa-circle-o" aria-hidden="true"></i>Payment Methods</a></li>
                                <li id="app_custom_pages_view"><a href=""><i class="fa fa-circle-o" aria-hidden="true" ></i>App Custom Pages</a></li>
                                <li class="{{ Request::segment(1) === 'admin/restaurants' ? 'active' : null }}" id="restaurants_view"><a href="/admin/restaurants"><i class="fa fa-circle-o" aria-hidden="true"></i>Restaurants</a></li>
                                <li class="{{ Request::segment(1) === 'admin/print-classes' ? 'active' : null }}" id="print_classes_view"><a href="/admin/print-classes"><i class="fa fa-circle-o" aria-hidden="true"></i>Print Classes</a></li>
                                <li id="take_away_view"><a href=""><i class="fa fa-circle-o" aria-hidden="true"></i>Take Away</a></li>
                                <li id="employees_view" class="{{ Request::segment(1) === 'admin/employees' ? 'active' : null }}"><a href="/admin/employees"><i class="fa fa-circle-o" aria-hidden="true"></i>Employee</a></li>
                                <li class="{{ Request::segment(1) === 'admin/print-class-user' ? 'active' : null }}" id="print_class_users_view"><a href="/admin/print-class-user"><i class="fa fa-circle-o" aria-hidden="true"></i>Print Class User</a></li>
                                <li class="{{ Request::segment(1) === 'admin/table-manager' ? 'active' : null }}" id="table_managers_view"><a href="/admin/table-manager"><i class="fa fa-circle-o" aria-hidden="true"></i>Table-Manager</a></li>
                                <li class="{{ Request::segment(1) === 'admin/tax-manager' ? 'active' : null }}" id="tax_manager_view"><a href="/admin/tax-manager"><i class="fa fa-circle-o" aria-hidden="true"></i>Tax Manager</a></li>
                                <li id="settings_view"><a href=""><i class="fa fa-circle-o" aria-hidden="true"></i>Settings</a></li>
                                <li class="{{ Request::segment(1) === 'admin/roles' ? 'active' : null }}" id="roles_view"><a href="/admin/roles"><i class="fa fa-circle-o" aria-hidden="true"></i>Roles</a></li>
                                
                                <li class="submenu">
                                    <a href="#" id="condiments_view"><i class="fa fa-share" aria-hidden="true"></i> <span> Condiments</span> <span class="menu-arrow"></span></a>
                                    <ul class="list-unstyled" style="display: none;">
                                        <li class="{{ Request::segment(1) === 'admin/condiments' ? 'active' : null }}" id="member_view"><a href="/admin/condiments"><i class="fa fa-circle-o" aria-hidden="true"></i>Member</a></li>
                                        <li class="{{ Request::segment(1) === 'admin/condiment-groups' ? 'active' : null }}" id="groups_view"><a href="/admin/condiment-groups"><i class="fa fa-circle-o" aria-hidden="true"></i>Groups</a></li>
                                        
                                    </ul>                                    
                                </li>
                                
                            </ul>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-share" aria-hidden="true"></i> Recipes</a>
                        </li>
                        <li id='WEBACCOUNTING_ERP_view'>
                            <a href=""><i class="fa fa-share" aria-hidden="true"></i> WEBACCOUNTING-ERP</a>
                        </li>
                        <li class="submenu">
                            <a href="#" id="reports"><i class="fa fa-share" aria-hidden="true"></i> <span> Reports</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                
                                 <li id="reports_product-wise-sales"><a href="/admin/reports/product-wise-sales"><i class="fa fa-circle-o" aria-hidden="true"></i>Product Wise Sales</a></li>
                                <li id="reports_unpaid_by_waiters"><a href="/admin/reports/unpaid_by_waiters"><i class="fa fa-circle-o" aria-hidden="true"></i>Unpaid By Waiters</a></li>
                                <li id="reports_void_reports"><a href="/admin/reports/void_reports"><i class="fa fa-circle-o" aria-hidden="true"></i>Void Items</a></li>

                                <li id="reports_waiters_detailed_reports"><a href="/admin/reports/waiters_detailed_reports"><i class="fa fa-circle-o" aria-hidden="true"></i>Waiters Detailed Reports</a></li>
                                <li id="reports_overall_sales_report"><a href="/admin/reports/overall_sales_report"><i class="fa fa-circle-o" aria-hidden="true"></i>Quantity control Report</a></li>
                                <li id="reports_discount_report"><a href="/admin/reports/discount_report"><i class="fa fa-circle-o" aria-hidden="true"></i>Discount Allowed</a></li>
                                <li  class="{{ Request::segment(1) === 'admin/reports/payment-sales-summary' ? 'active' : null }}" id='reports_payment_sales_summary'><a href="/admin/reports/payment-sales-summary"><i class="fa fa-circle-o" aria-hidden="true"></i>Payment Sales Summary</a></li>
                                <!--<li id="reports_menu_item_general_sales"><a href="/admin/reports/menu-item-general-sales"><i class="fa fa-circle-o" aria-hidden="true"></i>Menu Item General Sales</a></li>-->
                                <!--<li id='reports_menu_tem_general_sales_with_plu'><a href="/admin/reports/menu-item-general-sales-with-plu"><i class="fa fa-circle-o" aria-hidden="true"></i>Menu Item Sales with Plu</a></li>-->
                                <!--<li id="reports_menu_item_general_sales_without_plu"><a href="/admin/reports/menu-item-general-sales-without-plu"><i class="fa fa-circle-o" aria-hidden="true"></i>Menu Item Sales without Plu</a></li>-->
                                <!--<li id="reports_family_group_sales"><a href="/admin/reports/family-group-sales"><i class="fa fa-circle-o" aria-hidden="true"></i>Family Group Sales</a></li>-->
                                <!--<li id="reports_family_group_sales_with_gl"><a href="/admin/reports/family-group-sales-with-gl"><i class="fa fa-circle-o" aria-hidden="true"></i>Family Group Sales With GI</a></li>-->
                                <!--<li id="reports_menu_item_group_sales"><a href="/admin/reports/menu-item-group-sales"><i class="fa fa-circle-o" aria-hidden="true"></i>Menu Item Group Sales</a></li>-->
                                <!--<li id="reports_major_group_sales"><a href="/admin/reports/major-group-sales"><i class="fa fa-circle-o" aria-hidden="true"></i>Major Group Sales</a></li>-->
                                <!--<li id="reports_waiter_with_family_groups"><a href="/admin/reports/waiter-with-family-groups"><i class="fa fa-circle-o" aria-hidden="true"></i>Waiter With Family Group</a></li>-->
                                <!--<li id="reports_condiments_sales_with_plu"><a href="/admin/reports/condiment-sales-report-with-plu"><i class="fa fa-circle-o" aria-hidden="true"></i>Condiments Sales Plu Report</a></li>-->
                                <!--<li id="reports_get_discount_reports"><a href="/admin/reports/get-discounts-reports"><i class="fa fa-circle-o" aria-hidden="true"></i>Discounts Reports</a></li>-->
                                <!--<li id="reports_get_cashier_reports"><a href="/admin/reports/cashier-reports"><i class="fa fa-circle-o" aria-hidden="true"></i>Cashier Reports</a></li>-->
                                <!--<li id="get_discount_reports_with_orders"><a href="/admin/reports/discount-reports-with-orders"><i class="fa fa-circle-o" aria-hidden="true"></i>Discounts Reports With Orders</a></li>-->
                                <!--<li id="reports_get_complementary_reports_with_orders"><a href="/admin/reports/complementary-reports-with-orders"><i class="fa fa-circle-o" aria-hidden="true"></i>Complementary Reports</a></li>-->
                                <!--<li id="reports_get_cancelled_orders_reports"><a href="/admin/reports/get-cancelled-orders-reports"><i class="fa fa-circle-o" aria-hidden="true"></i>Cancelled Orders Reports</a></li>-->
                                <!--<li id="reports_wallet_ledger_entries"><a href="/admin/reports/wallet-ledger-entries"><i class="fa fa-circle-o" aria-hidden="true"></i>Wallet Ledger Entries</a></li>-->
                                <!--<li id="reports_wallet_ledger_entries"><a href="/admin/reports/product-wise-sales"><i class="fa fa-circle-o" aria-hidden="true"></i>Product Wise Sales</a></li>-->
                                 <li id="reportsopen-orders"><a href="/admin/reports/open-orders"><i class="fa fa-circle-o" aria-hidden="true"></i>Open Orders</a></li>                                
                                <li id="reports_completed-orders"><a href="/admin/reports/completed-orders"><i class="fa fa-circle-o" aria-hidden="true"></i>Completed Orders</a></li>  
                                 <li id="reports_request-orders"><a href="/admin/billTransfer"><i class="fa fa-circle-o" aria-hidden="true"></i>Bill Transfer </a></li>  
                                
                                
                            </ul>
                        </li>
                        <li class="submenu">
                            <a href="#" id="feedback_view"><i class="fa fa-share" aria-hidden="true"></i> <span> Feedbacks</span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled" style="display: none;">
                                <li id='feedback_orders'><a href=""><i class="fa fa-circle-o" aria-hidden="true"></i>Orders</a></li>
                                <li id='feedback_restaurants'><a href=""><i class="fa fa-circle-o" aria-hidden="true"></i>Restaurants</a></li>
                            </ul>
                        </li>
                        
                        <li class="submenu" id="reservations_view">
                            <a href=""><i class="fa fa-share" aria-hidden="true"></i> <span>Reservations</span> </a>
                            
                        </li>
                                                
                    </ul>
                </div>
            </div>
        </div>
        
    </div>
    @yield("body-content")

    <div class="sidebar-overlay" data-reff=""></div>
   
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery-3.2.1.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('assets/js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery.slimscroll.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/plugins/morris/morris.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/plugins/raphael/raphael-min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/app.js')}}"></script>
      <script type="text/javascript" src="{{URL::asset('assets/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
    <!--<script type="text/javascript" src="{{URL::asset('assets/js/ion.sound.js')}}"></script>-->
    
    
    <script src="https://kendo.cdn.telerik.com/2018.3.1017/js/kendo.all.min.js"></script>


    <script>
          
        if('{{\Auth::user()->void_item_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#prepaid_orders_view2").hide();
        }
        
        if('{{\Auth::user()->dashboard_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#dash").hide();
        }

        if('{{\Auth::user()->menu_setup_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#menu_setup_view").hide();
        }

        if('{{\Auth::user()->major_groups_manager_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#major_groups_manager_view").hide();
        }

        if('{{\Auth::user()->sub_major_groups_manager_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#sub_major_groups_manager_view").hide();
        }

        if('{{\Auth::user()->menu_item_groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#menu_item_groups_view").hide();
        }

        if('{{\Auth::user()->offers_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#offers_view").hide();
        }
        if('{{\Auth::user()->family_groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#family_groups_view").hide();
        }
        if('{{\Auth::user()->alcoholic_family_groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#alcoholic_family_groups_view").hide();
        }
        if('{{\Auth::user()->alcoholic_sub_family_groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#alcoholic_sub_family_groups_view").hide();
        }
        if('{{\Auth::user()->menu_Item_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#menu_Item_view").hide();
        }
        if('{{\Auth::user()->general_item_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#general_item_view").hide();
        }
        if('{{\Auth::user()->offer_themes_nights_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#offer_themes_nights_view").hide();
        }
        if('{{\Auth::user()->gl_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#gl_view").hide();
        }
        if('{{\Auth::user()->plu_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#plu_view").hide();
        }
        if('{{\Auth::user()->beer_delivery_and_keg_setup_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#beer_delivery_and_keg_setup_view").hide();
        }
        if('{{\Auth::user()->beer_and_keg_sub_major_group_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#beer_and_keg_sub_major_group_view").hide();
        }
        if('{{\Auth::user()->delivery_family_groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#delivery_family_groups_view").hide();
        }
        if('{{\Auth::user()->delivery_family_groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#delivery_family_groups_view").hide();
        }
        if('{{\Auth::user()->delivery_sub_family_groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#delivery_sub_family_groups_view").hide();
        }
        if('{{\Auth::user()->delivery_items_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#delivery_items_view").hide();
        }
        if('{{\Auth::user()->app_users_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#app_users_view").hide();
        }
        if('{{\Auth::user()->manage_orders_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#manage_orders_view").hide();
        }
      
        if('{{\Auth::user()->complete_orders_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#complete_orders_view").hide();
        }
        if('{{\Auth::user()->closed_orders_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#closed_orders_view").hide();
        }
        if('{{\Auth::user()->open_orders_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#open_orders_view").hide();
        }
        if('{{\Auth::user()->closed_orders_payment_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#closed_orders_payment_view").hide();
        }
        if('{{\Auth::user()->bills_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#bills_view").hide();
        }
        if('{{\Auth::user()->generate_bills_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#generate_bills_view").hide();
        }
        if('{{\Auth::user()->master_bills_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#master_bills_view").hide();
        }
        if('{{\Auth::user()->WEBACCOUNTING_ERP_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#WEBACCOUNTING_ERP_view").hide();
        }    
        if('{{\Auth::user()->reservations_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reservations_view").hide();
        }
        if('{{\Auth::user()->feedback_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#feedback_view").hide();
        }
        if('{{\Auth::user()->feedback_orders}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#feedback_orders").hide();
        }
        if('{{\Auth::user()->feedback_restaurants}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#feedback_restaurants").hide();
        }
        if('{{\Auth::user()->reports_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports").hide();
        }
        if('{{\Auth::user()->reports_payment_sales_summary1}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_payment_sales_summary").hide();
        }
        if('{{\Auth::user()->reports_payment_sales_summary}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_product-wise-sales").hide();
        }
        if('{{\Auth::user()->reports_menu_item_general_sales}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_unpaid_by_waiters").hide();
        }
////////////////////////
        if('{{\Auth::user()->reports_menu_tem_general_sales_with_plu}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_waiters_detailed_reports").hide();
        }
        if('{{\Auth::user()->reports_menu_item_general_sales_without_plu}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_overall_sales_report").hide();
        }
        if('{{\Auth::user()->reports_family_group_sales_with_gl}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_discount_report").hide();
        }
        if('{{\Auth::user()->reports_menu_item_group_sales}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reportsopen-orders").hide();
        }
        if('{{\Auth::user()->reports_major_group_sales}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_completed-orders").hide();
        }
        if('{{\Auth::user()->reports_waiter_with_family_groups}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_waiter_with_family_groups").hide();
        }
        if('{{\Auth::user()->reports_condiments_sales_with_plu}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_condiments_sales_with_plu").hide();
        }
        if('{{\Auth::user()->reports_get_discount_reports}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_get_discount_reports").hide();
        }
        if('{{\Auth::user()->reports_get_cashier_reports}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_get_cashier_reports").hide();
        }
        if('{{\Auth::user()->get_discount_reports_with_orders}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#get_discount_reports_with_orders").hide();
        }
        if('{{\Auth::user()->reports_get_complementary_reports_with_orders}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_get_complementary_reports_with_orders").hide();
        }
        if('{{\Auth::user()->reports_get_cancelled_orders_reports}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_get_cancelled_orders_reports").hide();
        }
        if('{{\Auth::user()->reports_family_group_sales}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_family_group_sales").hide();
        }
        if('{{\Auth::user()->reports_wallet_ledger_entries}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_wallet_ledger_entries").hide();
        }
        if('{{\Auth::user()->postpaid_orders_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#postpaid_orders_view").hide();
        }
        if('{{\Auth::user()->reports_void_items}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reports_void_reports").hide();
        }
        

        ////////////////////////////////////// System Setup ///////////////////////////////////////////

        if('{{\Auth::user()->system_setup_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#system_setup_view").hide();
        }

        if('{{\Auth::user()->advertisements_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#advertisements_view").hide();
        }
        if('{{\Auth::user()->social_links_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#social_links_view").hide();
        }
        if('{{\Auth::user()->reservation_email_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#reservation_email_view").hide();
        }
        if('{{\Auth::user()->payent_method_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#payent_method_view").hide();
        }
        if('{{\Auth::user()->app_custom_pages_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#app_custom_pages_view").hide();
        }
        if('{{\Auth::user()->restaurants_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#restaurants_view").hide();
        }
        if('{{\Auth::user()->print_classes_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#print_classes_view").hide();
        }
        if('{{\Auth::user()->take_away_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#take_away_view").hide();
        }
        if('{{\Auth::user()->employees_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#employees_view").hide();
        }
        if('{{\Auth::user()->print_class_users_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#print_class_users_view").hide();
        }
        if('{{\Auth::user()->table_managers_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#table_managers_view").hide();
        }
        if('{{\Auth::user()->tax_manager_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#tax_manager_view").hide();
        }
        if('{{\Auth::user()->settings_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#settings_view").hide();
        }
        if('{{\Auth::user()->condiments_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#condiments_view").hide();
        }
        if('{{\Auth::user()->member_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#member_view").hide();
        }
        if('{{\Auth::user()->groups_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#groups_view").hide();
        }
        if('{{\Auth::user()->roles_view}}' == 'off' && '{{\Auth::user()->user_type}}' != 'admin'){
            $("#roles_view").hide();
        }
      
            </script>

    @yield('javascript')
</body>


<!-- Mirrored from dreamguys.co.in/preadmin/orange/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Nov 2018 05:38:25 GMT -->
</html>