<!DOCTYPE html>
<html>


<!-- Mirrored from dreamguys.co.in/preadmin/orange/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Nov 2018 05:38:30 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets/img/favicon.png')}}">
    <title>The Smith Hotels</title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/style.css')}}">
    <!--[if lt IE 9]>
		<script src="{{URL::asset('assets/js/html5shiv.min.js')}}"></script>
		<script src="{{URL::asset('assets/js/respond.min.js')}}"></script>
	<![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <div class="account-page">
            <div class="container">
                <h3 class="account-title">Login</h3>
                <div class="account-box">
                    <div class="account-wrapper">
                        {{-- <div class="account-logo">
                            <a href="index.html"><img src="assets/img/logo2.png" alt="Preadmin"></a>
                        </div> --}}
                        <form method="POST" action="/public/print-class/login">
                            {{ csrf_field() }}
                            <div class="form-group form-focus{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="focus-label">Username or Email</label>
                                <input class="form-control floating" type="email" id="email" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group form-focus{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="focus-label">Password</label>
                                <input class="form-control floating" type="password" id="password" name="password" required>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group text-center">
                                <button class="btn btn-primary btn-block account-btn" type="submit">Login</button>
                            </div>
                            {{-- <div class="text-center">
                                <a href="forgot-password.html">Forgot your password?</a>
                            </div> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{URL::asset('assets/js/jquery-3.2.1.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::asset('assets/js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('assets/js/app.js')}}"></script>
</body>


<!-- Mirrored from dreamguys.co.in/preadmin/orange/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Nov 2018 05:38:31 GMT -->
</html>