<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItemsGroup extends Model
{
    protected $fillable = [
        'name', 'sub_major_groups_id', 'is_checked','timefrom','timeto','description' , 'a_image',
    ];

    public function subMajorGroups()
    {
        return $this->belongsTo(\App\SubMajorGroups::class);
    }

    public function alcoholicGroups(){

        return $this->hasMany(\App\AlcoholicFamilyGroup::class);
    }

    public function familyGroups(){

        return $this->hasMany(\App\FamilyGroup::class);
    }
}
