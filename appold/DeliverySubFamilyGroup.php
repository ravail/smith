<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliverySubFamilyGroup extends Model
{
    protected $fillable = [
        'name', 'delivery_family_group_name', 'description' , 'a_image',
    ];
}
