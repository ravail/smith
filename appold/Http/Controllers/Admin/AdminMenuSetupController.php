<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddMajorGroupRequest as AddMajorGroupRequest;
use App\MajorGroup;
use App\SubMajorGroups;
use App\Tax;
use DB;
use App\OrderItems;

use Session;
use App\PrepaidOrders;
use App\PostPaidOrders;
use App\GLAccount;
use App\Offer;
use App\FamilyGroup;
use App\PLUNumber;
use App\GeneralItems;
use App\DeliveryItems;
use App\MenuItemsGroup;
use App\AlcoholicFamilyGroup;
use App\AlcoholicSubFamilyGroup;
use App\DeliverySubMajorGroup;
use App\DeliveryFamilyGroup;
use App\DeliverySubFamilyGroup;
use App\SystemSetup\PrintClasses;
use App\SystemSetup\CondimentGroup;
use Illuminate\Support\Facades\Validator;
use Image;

class AdminMenuSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function index()
    {
        $groups=MajorGroup::all();
        //return $groups;exit;

        return view('admin.admin_major_groups',compact('groups'));
    }

   

    public function addMajorGroup(){

        return view('admin.admin_add_major_group');
    }

    public function createMajorGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=MajorGroup::create($data);

        Session::flash('message', 'We have sent you a verification email!');
       
        return redirect()->to('/admin/major-group-managers')->with('message','Major Group Created Successfully!');
        
    }

    public function deleteMajorGroup($id){

        $delete = MajorGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Major Group Deleted Successfully');

        }
    }

    public function editMajorGroup($id){

        $existGroup = MajorGroup::where('id', $id)->first();
        //return $existGroup;exit;
        return view('admin.admin_edit_major_group',compact('existGroup'));
    }

    public function updateMajorGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = MajorGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            
            if($data['a_image']!=""){

                $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                if ($image_uploaded) {
                
                    $data['a_image'] = $imageName;
                
                }
            }

            $profile=$existGroup->update($data);

            $profile = MajorGroup::where('id', $id)->get();

            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/major-group-managers')->with('message','Major Group Updated!');

        }
    }
    
    ///////////////////////////////// GL Account ////////////////////////////////////////////////

    public function GlAccount()
    {
        $accounts=GLAccount::all();
        //return $groups;exit;

        return view('admin.admin_gl_accounts',compact('accounts'));
    }

    public function addGlAccount(){
        return view('admin.admin_add_glaccount');
    }

    public function createGlAccount(Request $request){

        $data=$request->all();
      
        $add=GLAccount::create($data);

        Session::flash('message', 'We have sent you a verification email!');        
        return redirect()->to('/admin/gl-accounts')->with('message','GL Account Created!');
        
    }
    public function deleteGlAccount($id){

        $delete = GLAccount::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'GL Account Deleted Successfully!');
            
            return back()->with('message', 'GL Account Deleted Successfully!');

        }
    }

    public function editGlAccount($id){

        $existGl = GLAccount::where('id', $id)->first();
        //return $existGroup;exit;
        return view('admin.admin_edit_glaccount',compact('existGl'));
    }

    public function updateGlAccount(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = GLAccount::where('id', $id)->first();
       
        if (!empty($existGroup)) {

           
            $profile=$existGroup->update($data);
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/gl-accounts')->with('message','GL Account Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ////////////////////////////////// PLU Numbers //////////////////////////////////////////////

    public function PLUNumber()
    {
        $accounts=PLUNumber::all();
        //return $groups;exit;

        return view('admin.admin_plu_numbers',compact('accounts'));
    }

    public function addPLUNumber(){
        return view('admin.admin_add_plu_number');
    }

    public function createPLUNumber(Request $request){

        $data=$request->all();
      
        $add=PLUNumber::create($data);
        Session::flash('message', 'GL Account Deleted Successfully!');
        
        return redirect()->to('/admin/plu-numbers')->with('message','PLU Number Deleted Successfully!');
        
    }
    public function deletePLUNumber($id){

        $delete = PLUNumber::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'GL Account Deleted Successfully!');
            
            return back()->with('message', 'PLU Number Deleted Successfully!');

        }
    }

    public function editPLUNumber($id){

        $existGl = PLUNumber::where('id', $id)->first();
        //return $existGroup;exit;
        return view('admin.admin_edit_plu_number',compact('existGl'));
    }

    public function updatePLUNumber(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = PLUNumber::where('id', $id)->first();
       
        if (!empty($existGroup)) {
         
            $profile=$existGroup->update($data);
            Session::flash('message', 'We have sent you a verification email!');

            return redirect()->to('/admin/plu-numbers')->with('message','PLU Number Updated!');

        }
    }


    /////////////////////////////////Sub Major Groups////////////////////////////////////////////

    public function subMajorGroups(){

        $main_groups=MajorGroup::all();
        
        $groups=SubMajorGroups::all();
        
        return view('admin.admin_sub_major_groups',compact('groups','main_groups'));
    }
    public function addSubMajorGroup(){
        $groups=MajorGroup::all();
        
        return view('admin.admin_add_sub_major_groups',compact('groups'));
    }
    public function createSubMajorGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=SubMajorGroups::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/sub-major-groups')->with('message', 'Sub Major Group created Successfully!');
    }

    public function deleteSubMajorGroup($id){

        $delete = SubMajorGroups::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Sub Major Group Deleted Successfully');

        }
    }

    public function editSubMajorGroup($id){

        $existGroup = SubMajorGroups::where('id', $id)->first();
        $groups=MajorGroup::all();
        
        //return $existGroup;exit;
        
        return view('admin.admin_edit_sub_major_group',compact('existGroup','groups'));
    }

    public function updateSubMajorGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = SubMajorGroups::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }
           
            $profile=$existGroup->update($data);

            $profile = SubMajorGroups::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/sub-major-groups')->with('message','Sub Major Group Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function offers(){

        $offers=Offer::all();
        $groups=SubMajorGroups::all();
        
        
        return view('admin.admin_offers',compact('offers','groups'));
    }

    public function addOffers(){
        
        $groups=SubMajorGroups::all();
        $tax=Tax::all();
        
        return view('admin.admin_add_offers',compact('groups','tax'));
    }

    public function createOffer(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=Offer::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('admin/offers')->with('message', 'Offer Created Successfully!');

    }

    public function deleteOffer($id){

        $delete = Offer::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Offer Deleted Successfully');

        }
    }

    public function editOffer($id){

        $existOffer = Offer::where('id', $id)->first();
        
        $groups=SubMajorGroups::all();
        $taxes=Tax::all();
 
        return view('admin.admin_edit_offer',compact('existOffer','groups','taxes'));
    }

    public function updateOffer(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = Offer::where('id', $id)->first();
       
        if (!empty($existGroup)) {

           
            $profile=$existGroup->update($data);

            $profile = Offer::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/offers')->with('message','Offer Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    //////////////////////////////////General Items /////////////////////////////////////////

    public function generalItems(){

        //$groups = FamilyGroup::all();
        $groups = DB::select('SELECT id,check_flag,name FROM `family_groups`
                    UNION DISTINCT
                SELECT id,check_flag,name FROM `alcoholic_sub_family_groups`');
        $items = GeneralItems::all();
        // return $groups;
        return view('admin.admin_general_items',compact('groups','items'));
    }

    public function addGeneralItems(){
        
        // $family_groups = FamilyGroup::all();
        $family_groups = DB::select('SELECT id,check_flag,name FROM `family_groups`
        UNION DISTINCT
        SELECT id,check_flag,name FROM `alcoholic_sub_family_groups`');
        
        $tax = Tax::all();
        $print_classes = PrintClasses::all();
        $plu_numbers = PLUNumber::all();
        $family_groups = FamilyGroup::all();
        return view('admin.admin_add_menu_general_items',compact('family_groups','tax','print_classes','plu_numbers'));
    }

    public function createGeneralItems(Request $request){

        $data=$request->all();
        
        $size = sizeof($data['tax']);
  
        if($size > 1){
            
            $data['tax'] = $data['tax'][0] .",".$data['tax'][1];
          
        }else{
            $data['tax'] = $data['tax'][0];
        }

        // return $data['tax']; 

        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=GeneralItems::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/general-items')->with('message','General Item Created!');
        
    }

    public function deleteGeneralItems($id){

        $delete = GeneralItems::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'General Item Deleted Successfully!');

        }
    }

    public function editGeneralItems($id){

        $existGeneralItem = GeneralItems::where('id', $id)->first();
        $family_groups = DB::select('SELECT id,check_flag,name FROM `family_groups`
        UNION DISTINCT
        SELECT id,check_flag,name FROM `alcoholic_sub_family_groups`');
        $taxes = Tax::all();
        $print_classes = PrintClasses::all();
        $plu_numbers = PLUNumber::all();
        $condiments_groups = CondimentGroup::all();
        return view('admin.admin_edit_general_items',compact('existGeneralItem','family_groups','taxes','print_classes','plu_numbers','condiments_groups'));
    }

    public function updateGeneralItems(Request $request ,$id){

        $data=$request->all();
        $size = sizeof($data['tax']);
  
        if($size > 1){
            
            $data['tax'] = $data['tax'][0] .",".$data['tax'][1];
          
        }else{
            $data['tax'] = $data['tax'][0];
        }
         if(! $request->has('is_show_menu') ){
            $data['is_show_menu']= 'off';
        }
        
        $existGroup = GeneralItems::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = GeneralItems::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/general-items')->with('message','General Item Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ///////////////////////////////// Delivery Items /////////////////////////////////////

    public function deliveryItems(){

        $groups = FamilyGroup::all();
        $items = DeliveryItems::all();
        return view('admin.beer_&_keg_setup.admin_delivery_items',compact('groups','items'));
    }

    public function addDeliveryItems(){
        
        $family_groups = FamilyGroup::all();
        $tax = Tax::all();
       
        $plu_numbers = PLUNumber::all();

        return view('admin.beer_&_keg_setup.admin_add_delivery_items',compact('family_groups','tax','plu_numbers'));
    }

    public function createDeliveryItems(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=DeliveryItems::create($data);
        
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/delivery-items')->with('message', 'Delivery Item created Successfully!');
    }

    public function deleteDeliveryItems($id){

        $delete = DeliveryItems::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'Delivery Item Deleted Successfully!');

        }
    }

    public function editDeliveryItems($id){

        $existDeliveryItem = DeliveryItems::where('id', $id)->first();
        $family_groups = FamilyGroup::all();
        $taxes = Tax::all();
       
        $plu_numbers = PLUNumber::all();

        return view('admin.beer_&_keg_setup.admin_edit_delivery_items',compact('existDeliveryItem','family_groups','taxes','plu_numbers'));
    }

    public function updateDeliveryItems(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliveryItems::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliveryItems::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/delivery-items')->with('message','Delivery Item Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ///////////////////////////////// Menu Items Groups //////////////////////////////////

    public function menuItemGroups(){
        
        $menus=MenuItemsGroup::all();
        $groups=SubMajorGroups::all();
        
        
        return view('admin.admin_menu_item_groups',compact('menus','groups'));
    }

    public function addMenuItemGroups(){
        
        $groups=SubMajorGroups::all();
        $tax=Tax::all();
        
        return view('admin.admin_add_menu_item_group',compact('groups','tax'));
    }

    public function createMenuItemGroup(Request $request){

        $data=$request->all();
      //return $data;
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=MenuItemsGroup::create($data);
        
        Session::flash('message', 'We have sent you a verification email!');        
        return redirect()->to('/admin/menu-item-groups')->with('message','Menu Item Group Created Successfully!');
        
    }

    public function deleteMenuItemGroup($id){

        $delete = MenuItemsGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Menu Item Group Deleted Successfully!');
        }
    }

    public function editMenuItemGroup($id){

        
        $groups = SubMajorGroups::all();
       
        $existGroup = MenuItemsGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_menu_item_group',compact('existGroup','groups'));
    }

    public function updateMenuItemGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = MenuItemsGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = MenuItemsGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/menu-item-groups')->with('message','Menu Item Group Updated!');


        }
    }

    ///////////////////////////// Family Groups //////////////////////////////////

    public function familyGroups(){
        $groups=FamilyGroup::paginate(200);
        //return $groups;exit;

        return view('admin.admin_family_groups',compact('groups'));
    }

    public function addFamilyGroups(){

        $groups=MenuItemsGroup::all();

        $glAccounts=GLAccount::all();


        return view('admin.admin_add_family_group',compact('groups','glAccounts'));
    }

    public function createFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=FamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/family-groups')->with('message', 'Family Group created Successfully!');
    }

    public function deleteFamilyGroup($id){

        $delete = FamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Family Group Deleted Successfully');

        }
    }

    public function editFamilyGroup($id){

        
    
        $groups=MenuItemsGroup::all();

        $glAccounts=GLAccount::all();
       
        $existGroup = FamilyGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_family_group',compact('existGroup','groups','glAccounts'));
    }

    public function updateFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = FamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            
            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = FamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/family-groups')->with('message','Family Group Updated!');

        }
    }

    //////////////////////////// Alcoholic Family Group //////////////////////////////////

    public function alcoholicFamilyGroups(){

        $menus=MenuItemsGroup::all();
        $fgroup=AlcoholicFamilyGroup::all();        
        //return $groups;exit;

        return view('admin.admin_alcoholic_family_group',compact('fgroup','menus'));
    }

    public function addAlcoholicFamilyGroups(){

        
        $menus=MenuItemsGroup::all();

        return view('admin.admin_add_alcoholic_family_group',compact('menus'));
    }

    public function createAlcoholicFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=AlcoholicFamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/alcoholic-family-groups')->with('message', 'Alcoholic Family Group created Successfully!');
    }

    public function deleteAlcoholicFamilyGroup($id){

        $delete = AlcoholicFamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Alcoholic Group Deleted Successfully!');

        }
    }
    public function editAlcoholicFamilyGroup($id){

        
    
        $groups=MenuItemsGroup::all();
       
        $existGroup = AlcoholicFamilyGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_alcoholic_family_groups',compact('existGroup','groups'));
    }

    public function updateAlcoholicFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = AlcoholicFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            
            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = AlcoholicFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/alcoholic-family-groups')->with('message','Alcoholic Family Group Updated!');


        }
    }

    ////////////////////////////////////////// Alcoholic Sub Family Group //////////////////////////////

    public function alcoholicSubFamilyGroups(){

        $familygroups=AlcoholicFamilyGroup::all();
        $subFamilyGroups=AlcoholicSubFamilyGroup::all();
        return view('admin.admin_alcoholic_sub_family_groups',compact('familygroups','subFamilyGroups'));
    }

    public function addAlcoholicSubFamilyGroups(){
 
        $familygroups=AlcoholicFamilyGroup::all();
        $account=GLAccount::all();  

        return view('admin.admin_add_alcoholic_sub_family_group',compact('familygroups','account'));
    }

    public function createAlcoholicSubFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=AlcoholicSubFamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/alcoholic-sub-family-groups')->with('message', 'Alcoholic Sub Family Group created Successfully!');
    }

    public function deleteAlcoholicSubFamilyGroup($id){

        $delete = AlcoholicSubFamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Alcoholic Sub Family Group Deleted Successfully!');

        }
    }

    public function editAlcoholicSubFamilyGroup($id){

        $familygroups=AlcoholicFamilyGroup::all();
        $accounts=GLAccount::all();  
    
        $existGroup = AlcoholicSubFamilyGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_alcoholic_sub_family_group',compact('existGroup','familygroups','accounts'));
    }

    public function updateAlcoholicSubFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = AlcoholicSubFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

 if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = AlcoholicSubFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/alcoholic-sub-family-groups')->with('message','Alcoholic Sub Family Group Updated!');


        }
    }


    //////////////////////////////// Beer & Keg (1) ////////////////////////////////

    public function deliverySubMajorGroup(){

        $main_groups=MajorGroup::all();
        
        $groups=DeliverySubMajorGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_delivery_sub_major_group',compact('groups','main_groups'));
    }

    public function addDeliverySubMajorGroup(){
        
        $groups=MajorGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_add_delivery_sub_major_group',compact('groups'));
    }

    public function createDeliverySubMajorGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=DeliverySubMajorGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('/admin/beer-and-keg-sub-major-group')->with('success_message', 'Delivery Sub Major Group created Successfully!');
    }

    public function deleteDeliverySubMajorGroup($id){

        $delete = DeliverySubMajorGroup::where('id', $id)->delete();
        if($delete){
        Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'Delivery Sub Major Group Deleted Successfully!');

        }
    }
    public function editDeliverySubMajorGroup($id){

        $groups=MajorGroup::all(); 
    
        $existGroup = DeliverySubMajorGroup::where('id', $id)->first();
        
        return view('admin.beer_&_keg_setup.admin_edit_delivery_sub_major_group',compact('existGroup','groups'));
    }

    public function updateDeliverySubMajorGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliverySubMajorGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliverySubMajorGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/beer-and-keg-sub-major-group')->with('message','Delivery Sub Major Group Updated!');


        }
    }

    ///////////////////////////////////////// Beer & Keg (2) ////////////////////////////////////////

    public function deliveryFamilyGroup(){

        $del_sub_groups=DeliverySubMajorGroup::all();
        
        $del_family_groups=DeliveryFamilyGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_delivery_family_group',compact('del_sub_groups','del_family_groups'));
    }

    public function addDeliveryFamilyGroup(){
        
        $groups=DeliverySubMajorGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_add_delivery_family_group',compact('groups'));
    }

    public function createDeliveryFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=DeliveryFamilyGroup::create($data);
        
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('/admin/delivery-family-group')->with('message','Delivery Family Group Created!');
    }

    public function deleteDeliveryFamilyGroup($id){

        $delete = DeliveryFamilyGroup::where('id', $id)->delete();
        if($delete){
        Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'Delivery Family Group Deleted Successfully');

        }
    }

    public function editDeliveryFamilyGroup($id){

        $groups=DeliverySubMajorGroup::all(); 
    
        $existGroup = DeliveryFamilyGroup::where('id', $id)->first();
        
        return view('admin.beer_&_keg_setup.admin_edit_delivery_family_group',compact('existGroup','groups'));
    }

    public function updateDeliveryFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliveryFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliveryFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/delivery-family-group')->with('message','Delivery Family Group Updated!');


        }
    }

/////////////////////////////////////////////// Beer & Keg (3) ///////////////////////////////////////////////

    public function deliverySubFamilyGroup(){

        //$del_sub_groups=DeliverySubMajorGroup::all();
        
        $del_family_groups=DeliveryFamilyGroup::all();

        $del_sub_family_group=DeliverySubFamilyGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_delivery_sub_family_group',compact('del_sub_family_group','del_family_groups'));
    }

    public function addDeliverySubFamilyGroup(){
        
        $groups=DeliveryFamilyGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_add_delivery_sub_family_group',compact('groups'));
    }

    public function createDeliverySubFamilyGroup(Request $request){

        $data=$request->all();
    
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
        
            $data['a_image'] = $imageName;
        
        }

        $add=DeliverySubFamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('/admin/delivery-sub-family-group')->with('message','Delivery Sub Family Group Created!');
    }

    public function deleteDeliverySubFamilyGroup($id){

        $delete = DeliverySubFamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('success_message', 'Deleted Successfully');

        }
    }

    public function editDeliverySubFamilyGroup($id){

        $groups=DeliveryFamilyGroup::all(); 
    
        $existGroup = DeliverySubFamilyGroup::where('id', $id)->first();
        
        return view('admin.beer_&_keg_setup.admin_edit_delivery_sub_family_group',compact('existGroup','groups'));
    }

    public function updateDeliverySubFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliverySubFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliverySubFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/delivery-sub-family-group')->with('message','Delivery Sub Family Group Updated!');


        }
    }

////////////////////////////////////////////////// Manage Ordeer //////////////////////////////
    public function prepaidOrders(){

        $orders = PrepaidOrders::all();
        return view('admin.manage_orders.admin_prepaidOrders',compact('orders'));
    }

    public function completedOrders(){

        return view('admin.manage_orders.admin_completeOrders');
        
    }
    public function closedOrders(){
        return view('admin.manage_orders.admin_closedOrders');
        
    }

    public function postpaidOrders(){
        
        $orders = DB::select("select post_paid_orders.*,restaurants.name from post_paid_orders join restaurants on post_paid_orders.restaurant = restaurants.id where Date(post_paid_orders.created_at) = '".date("Y-m-d")."'");

        //$orders = PostPaidOrders::where(date('created_at'),'=',date("Y-m-d"))->get();
        //$order_items = OrderItems::where('cooking_status','=',"DELIVERED")->get();
        $order_items = DB::select("select * from order_items where cooking_status = 'DELIVERED' and Date(created_at) = '".date("Y-m-d")."'");


        $start_date = '';
        $end_date = '';
        
        $file = fopen("post_paid_report.csv","w");
        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('','','','','','','Post paid Report'));

        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('Today Report','','','','','',''));
        fputcsv($file, array('ID','Date_Time','Table NO','Item Description','Waiter','Restaurant','Total Amount','Status'));
        
             $total = 0;
        foreach($orders as $order){
            
            $items_description = "";
        foreach($order_items as $order_item){
                
         if($order_item->post_paid_orders_id == $order->id){
                //alert(result.order_items[j]['name']);
                $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity .", " ;
                           
                }
            
            }
          
          fputcsv($file, array($order->id,$order->date_time,$order->table_no,$items_description,$order->waiter,$order->name,$order->total_amount,$order->status));
                $total += $order->total_amount;
         
        }
        fputcsv($file, array('','','','','','Total',$total));
        fclose($file);
        
        return view('admin.manage_orders.admin_postpaidOrders',compact('orders','order_items','start_date','end_date'));
        
    }
     public function postpaidreportfilter(Request $request){
            
        $data=$request->all();
        $start_date = $data['start-date'];
        $end_date = $data['end-date'];
        $orders = DB::select("select post_paid_orders.*,restaurants.name from post_paid_orders join restaurants on post_paid_orders.restaurant = restaurants.id where post_paid_orders.created_at between '".$data['start-date']."' and '".$data['end-date']."'");
   
        //$orders = PostPaidOrders::where(date('created_at'),'=',date("Y-m-d"))->get();
        //$order_items = OrderItems::where('cooking_status','=',"DELIVERED")->get();
         
        $order_items = DB::select("select * from order_items where cooking_status = 'DELIVERED' and created_at between '".$data['start-date']."' and '".$data['end-date']."'");
        
        $file = fopen("post_paid_report.csv","w");
        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('','','','','','','Post paid Report'));

        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('Dated','From',$start_date,'To',$end_date,'',''));
        fputcsv($file, array('ID','Date_Time','Table NO','Item Description','Waiter','Restaurant','Total Amount','Status'));
        
             $total = 0;
        foreach($orders as $order){
            
            $items_description = "";
        foreach($order_items as $order_item){
                
         if($order_item->post_paid_orders_id == $order->id){
                //alert(result.order_items[j]['name']);
                $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity .", " ;
                           
                }
            
            }
          
          fputcsv($file, array($order->id,$order->date_time,$order->table_no,$items_description,$order->waiter,$order->name,$order->total_amount,$order->status));
                $total += $order->total_amount;
         
        }
        fputcsv($file, array('','','','','','Total',$total));
        fclose($file);
        
        return view('admin.manage_orders.admin_postpaidOrders',compact('orders','order_items','start_date','end_date'));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
