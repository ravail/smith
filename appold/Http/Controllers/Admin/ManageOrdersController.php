<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ManageOrders\BillMaster;
use App\ManageOrders\ClosedOrders;
use App\ManageOrders\Payments;
use App\OrderItems;
use App\PostPaidOrders;
use App\SystemSetup\PaymentMethod;
use App\SystemSetup\PrintClasses;
use App\SystemSetup\Tables;
use App\User;
use App\VoidItems;
use Illuminate\Http\Request;
use Session;
use Auth;
use DB;

class ManageOrdersController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('admin');
    }

    public function employeeTable(Request $request)
    {

        $name = $request->user_slug;
        $id = $request->user_id;
        $sel = $request->section;

        $tables = Tables::where('section', $sel)->where('waiter_id', 0)->where('booking_status', 'Free')->where('is_active', 'active')->get();

        return $tables;
    }

    public function alreadyAssignedTables(Request $request)
    {

        $name = $request->user_slug;
        $id = $request->waiter_id;
        $type = $request->type;

        $tables = Tables::where('waiter_id', $id)->where('booking_status', 'assign')->get();

        return $tables;
    }

    public function assignTable(Request $request)
    {

        $name = $request->user_slug;

        $table_id = $request->table_id;
        $status = $request->type;
        $waiter_id = $request->waiter_id;
        $block_section = $request->block_section;

        if ($status == "assign") {
            $table = Tables::where('section', $block_section)->where('id', $table_id)->where('waiter_id', 0)->where('booking_status', 'Free')->first();

            if (!empty($table)) {

                $table->booking_status = $status;
                $table->waiter_id = $waiter_id;
                $table->waiter = $name;

                $table->save();
                return 1;
            } else {
                return 2;
            }
        } elseif ($status == "remove") {

            $postOrders = PostPaidOrders::where('table_no', $table_id)->where('waiter_id', $waiter_id)->first();
            if ($postOrders->status != "Completed") {
                $s = "CANNOTREMOVE";
                return $s;
            } else {

                $table = Tables::where('section', $block_section)->where('id', $table_id)->where('waiter_id', $waiter_id)->first();
                $table->booking_status = 'Free';
                $table->waiter_id = 0;
                $table->waiter = 'N/A';

                $table->save();
                $s = "Remove";
                return $s;
            }
        }

    }

    public function unassignTable(Request $request)
    {

        $name = $request->user_slug;

        $table_id = $request->table_id;
        $status = $request->type;
        $waiter_id = $request->waiter_id;
        $block_section = $request->block_section;

        $postOrders = PostPaidOrders::where('table_no', $table_id)->where('waiter_id', $waiter_id)->first();
        if (!empty($postOrder)) {
            if ($postOrders->status != "Completed") {
                $s = "CANNOTREMOVE";
                return $s;
            } else {

                $table = Tables::where('section', $block_section)->where('id', $table_id)->where('waiter_id', $waiter_id)->first();
                $table->booking_status = 'Free';
                $table->waiter_id = 0;
                $table->waiter = 'N/A';

                $table->save();
                $s = "Remove";
                return $s;
            }
        } else {
            $table = Tables::where('section', $block_section)->where('id', $table_id)->where('waiter_id', $waiter_id)->first();
            $table->booking_status = 'Free';
            $table->waiter_id = 0;
            $table->waiter = 'N/A';

            $table->save();
            $s = "Remove";
            return $s;
        }
    }
    public function releaseAllTables(Request $request)
    {

        $name = $request->user_slug;

        $table_id = $request->table_id;
        $status = $request->type;
        $waiter_id = $request->waiter_id;
        $block_section = $request->block_section;

        $postOrders = PostPaidOrders::where('waiter_id', $waiter_id)->get();
        if (!empty($postOrders)) {
            foreach ($postOrders as $order) {
                if ($order->status != "Completed") {
                    $s = "CANNOTREMOVE";
                    return $s;
                }
            }

            foreach ($postOrders as $order) {

                $table = Tables::where('section', $order->section)->where('id', $order->table_no)->where('waiter_id', $waiter_id)->first();
                $table->booking_status = 'Free';
                $table->waiter_id = 0;
                $table->waiter = 'N/A';
                $table->save();
            }
            $s = "REMOVE";
            return $s;
        } else {

            $tables = Tables::where('waiter_id', $waiter_id)->get();
            return $tables;
            foreach ($tables as $table) {

                $table = Tables::where('section', $table->block_section)->where('waiter_id', $waiter_id)->first();
                $table->booking_status = 'Free';
                $table->waiter_id = 0;
                $table->waiter = 'N/A';
                $table->save();

            }

            $s = "REMOVE";
            return $s;
        }

    }

    function print(Request $request) {

        $bill_id = $request->bill_id;
        $bill = BillMaster::where('id', $bill_id)->first();

        $postOrders = PostPaidOrders::where('bill_id', $bill_id)->get();
        //$items = DB::select("SELECT * FROM order_items WHERE post_paid_orders_id IN(SELECT id FROM post_paid_orders WHERE bill_id = ".$bill_id.")");
       
        $items = DB:: select("SELECT * FROM `order_items` JOIN post_paid_orders ON order_items.post_paid_orders_id = post_paid_orders.id JOIN bill_masters ON post_paid_orders.bill_id= bill_masters.id WHERE bill_masters.id =".$bill_id);
        // $query = DB::getQueryLog();
       


        $orders = explode(',', $bill->orders);
        $size = sizeof($orders);
        
        return view('admin.bill_master_print1', compact('bill', 'postOrders', 'items', 'orders', 'size'));

    }

    public function printEtr(Request $request)
    {

        $bill_id =$request->bill_id;
        $bill = BillMaster::where('id', $bill_id)->first();
        $postOrders = PostPaidOrders::where('bill_id', $bill_id)->get();
        //$postOrders = PostPaidOrders::all();
         //$items = OrderItems::all();
         $items = DB:: select("SELECT * FROM `order_items` JOIN post_paid_orders ON order_items.post_paid_orders_id = post_paid_orders.id JOIN bill_masters ON post_paid_orders.bill_id= bill_masters.id WHERE bill_masters.id =".$bill_id);
        $orders = explode(',', $bill->orders);
        $size = sizeof($orders);
        // return $size;
        return view('admin.admin_etr_invoice', compact('bill', 'postOrders', 'items', 'orders', 'size'));

    }

    public function cashReciept($id)
    {

        $bill = BillMaster::where('id', $id)->first();
        $payments = PaymentMethod::all();
        return view('admin.manage_orders.admin_cash_reciept', compact('bill', 'payments'));

    }

    public function generateCashReciept(Request $request, $id)
    {

        //return $id;
        $payments = PaymentMethod::all();
        $data = $request->all();

        //return $request;

        foreach ($request->get('billing_info') as $name) {

            $arr['bill_id'] = $id;
            $arr['total_amount'] = $name['amount'];
            $arr['payment_method'] = $name['pay'];
            $arr['narration'] = $name['narration'];
            //return $arr['payment_method'];
            if ($arr['total_amount'] != null) {
                $add = Payments::create($arr);

            }

        }
        $bill = BillMaster::where('id', $id)->first();
        $bill->status = 'Paid';
        $bill->save();

        $user = Auth::user();
        $data1['bill_id'] = $id;
        $data1['date'] = $bill->date;
        $data1['waiter_id'] = $bill->waiter_id;
        $data1['waiter_name'] = $bill->waiter_name;
        $data1['no_orders'] = $bill->no_orders;
        $data1['total_amount'] = $bill->total_amount;
        $data1['cashier_id'] = $user->id;
        $data1['cashier_name'] = $user->name;

        $add = ClosedOrders::create($data1);

        // print_r($data);
        return redirect()->to('admin/orders/master-bills')->with('message', 'Order Closed!');

    }

    public function closedPaymentOrders()
    {

        $orders = DB::select('select * from closed_orders where created_at >= curdate()');
         $payments = DB::select('select * from payments where created_at >= curdate()');
        // return $payments;
        // $payments = Payments::all();
        $methods = PaymentMethod::all();
        $paymentsArray = "";
        //  $orders = DB::select('SELECT bill_id,created_at as date, narration, SUM(total_amount) AS total_amount, payment_method, ( SELECT waiter_name FROM closed_orders WHERE payments.bill_id = closed_orders.bill_id ORDER BY bill_id LIMIT 1 ) as waiter_name, ( SELECT cashier_name FROM closed_orders WHERE payments.bill_id = closed_orders.bill_id ORDER BY bill_id LIMIT 1 ) as cashier_name,( SELECT no_orders FROM closed_orders WHERE payments.bill_id = closed_orders.bill_id ORDER BY bill_id LIMIT 1 ) as no_of_orders, ( SELECT name FROM payment_methods WHERE payments.payment_method = payment_methods.id ORDER BY payment_methods.id LIMIT 1 ) as method_name FROM payments GROUP BY bill_id');
        //return  $orders;
         $final = array();
         $prev_total = 0;
         $prev_item = 0;
         $j=0;
        //  for($i = 0; $i < sizeof($orders) ; $i++){
             
        //      if(empty($final)){
        //         array_push($final,$orders[$i]);
        //         $j++;
        //      }
        //      else{
                 
        //         if($final[$j-1]->bill_id == $orders[$i]->bill_id){
                   
        //             $final[$j-1]->total_amount += $orders[$i]->total_amount; 
        //         }
        //         else{
        //             array_push($final,$orders[$i]);
        //             $j++;
        //         }
                 
        //      }
             
        //  }
        //  return $final;
        $file = fopen("closedPayment.csv","w");
        fputcsv($file, array('Closed Order Payments Report','',''));
        // fputcsv($file, array('','','','','','',''));

        // fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('All Time Report','',''));
        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('Bill_id ','Waiter','Cashier','Date','Methods Used','Total Amount'));
        
        foreach($orders as $order){
             $paymentsArray = "";
         foreach($payments as $payment){
            
            if($order->bill_id == $payment->bill_id){
                                    
            foreach($methods as $method){
                if($method->id == $payment->payment_method){
                                           
                $paymentsArray .=  ' (' . $method->name .  ' : '.$payment->total_amount.' : '. (strlen($payment->narration)> 13 ?  substr($payment->narration, 0, 13) : $payment->narration).'), ' ;
                                           
            }
         }
         $order->narration = $paymentsArray;
         fputcsv($file, array( $order->bill_id , $order->waiter_name , $order->cashier_name,$order->created_at,$order->narration,$order->total_amount ));
         
        }
         }
        }
        fclose($file);
                                 
        // return $orders;                      

        return view('admin.manage_orders.admin_closed_payment_orders', compact('orders', 'payments', 'methods'));

    }
    
    public function closedOrdersFilter(Request $request){
        
        //return $request;
         $start_date = $request->start_date;
//return $start_date;
        $end_date = $request->end_date;
        $methods = PaymentMethod::all();
        
        //  $a = "select * from closed_orders where created_at BETWEEN '" . $start_date . "' AND '". $end_date ."'";
        //  return $a;
        $orders = DB::select("select * from closed_orders where created_at BETWEEN '" . $start_date . "' AND '". $end_date ."'");
        //return $orders;
        $payments = DB::select("select * from payments where created_at BETWEEN '" . $start_date . "' AND '". $end_date ."' ");
        
         $file = fopen("closedPayment.csv","w");
        fputcsv($file, array('Closed Order Payments Report','',''));
        // fputcsv($file, array('','','','','','',''));

        // fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('All Time Report','',''));
        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('Bill_id ','Waiter','Cashier','Date','Methods Used','Total Amount'));
        
        foreach($orders as $order){
             $paymentsArray = "";
         foreach($payments as $payment){
            
            if($order->bill_id == $payment->bill_id){
                                    
            foreach($methods as $method){
                if($method->id == $payment->payment_method){
                                           
                $paymentsArray .=  ' (' . $method->name .  ' : '.$payment->total_amount.' : '. (strlen($payment->narration)> 13 ?  substr($payment->narration, 0, 13) : $payment->narration).'), ' ;
                                           
            }
         }
         $order->narration = $paymentsArray;
          fputcsv($file, array( $order->bill_id , $order->waiter_name , $order->cashier_name,$order->created_at,$order->narration,$order->total_amount ));
         
        }
         }
        }
         fclose($file);
        
        return view('admin.manage_orders.admin_closed_payment_orders', compact('orders', 'payments', 'methods','start_date','end_date'));
        
    }

    public function printDocket(Request $request)
    {

        $bill_id = $request->bill_id;
        $print_class_id = $request->print_class_id;
        $data = $request->all();
        //return $data;
        $postOrder = PostPaidOrders::where('id', $bill_id)->first();
        $items = OrderItems::where('print_class_id', $print_class_id)->where('post_paid_orders_id', $bill_id)->get();
        //print_r($postOrder);exit;
        $print_classes = PrintClasses::all();
        $d = explode(" ", $postOrder->date_time);
        //print_r($d);exit;
        //print_r($postOrder->date_time);exit;
        $dt = $d[2];
        $time = $d[1];
        //print_r($dt);exit;
        $dates = date_create($d[0]);
        //print_r($dates);exit;
        $dd = date_format($dates, "j-M-Y");
        //print_r($dd);exit;
        return view('admin.print_docket1', compact('postOrder', 'items', 'print_classes', 'dt', 'dd', 'time'));

    }

    public function reciept($bill_id)
    {

        $bill = BillMaster::where('id', $bill_id)->first();

        $postOrders = PostPaidOrders::all();

        $items = OrderItems::all();

        $orders = explode(',', $bill->orders);

        $size = sizeof($orders);
        //return $size;
        return view('admin.bill_master_print', compact('bill', 'postOrders', 'items', 'orders', 'size'));

    }

    public function generateBills()
    {

        $orders = PostPaidOrders::all();
        return view('admin.manage_orders.admin_generate_bills', compact('orders'));

    }

    public function createBills($id)
    {

        $orders = PostPaidOrders::where('status', "Completed")->get();
        return view('admin.manage_orders.admin_generate_all_bills', compact('orders', 'id'));

    }

    public function finalTotalBills($id, $array)
    {

        $arr = array();
        $arr = $array;
        $str = explode(',', $array);
        //return $str;
        $amount = 0;

        $order = array();

        for ($i = 0; $i < sizeof($str); $i++) {
            $j = 0;
            $orders = PostPaidOrders::where('id', $str[$i])->get();
            $date = explode(" ", $orders[$j]->date_time);
            $data['date'] = $date[0];
            $data['waiter_id'] = $orders[$j]->waiter_id;
            $data['waiter_name'] = $orders[$j]->waiter;

            $amount = $amount + $orders[$j]->total_amount;
            $data['total_amount'] = $amount;

            array_push($order, $orders[$j]->id);

        }

        $o = json_encode($order);
        $first = explode('[', $o);
        $second = explode(']', $first[1]);
        // /echo $second[0];
        $data['no_orders'] = sizeof($str);
        $data['orders'] = $second[0];

        $add = BillMaster::create($data);

        return redirect()->to('admin/orders/generate-bills')->with('message', 'Bill Generated Successfully!');
    }

    public function billMaster()
    {

        $orders = BillMaster::where('status', 'Un-paid')->get();
        return view('admin.manage_orders.admin_bill_master', compact('orders'));
    }

    public function updateOrderStatus($status, $id, $order_id)
    {

        //return $status;
        $s = "";
        $orders = OrderItems::where('print_class_id', $id)->where('post_paid_orders_id', $order_id)->get();
        //return $orders;
        foreach ($orders as $order) {
            if ($status == 'NEW') {

                $order->cooking_status = "PREPARATION";
                $order->save();
                $s = "updated";
            } elseif ($status == "PREPARATION") {
                $order->cooking_status = "READY TO PICK";
                $order->save();
                $s = "updated";
            } elseif ($status == "READY TO PICK") {
                $order->cooking_status = "DELIVERED";
                $order->save();
                $s = "updated";

            }

        }

        if ($s == "updated") {
            return back()->with('success_message', 'Status Updated Successfully');
        }
        return back()->with('error_message', 'Status not Updated');
    }

    public function updatePrint(Request $request)
    {

        $s = "";
        $auto_invoice = $request->auto_invoice;
        $auto_docket = $request->auto_docket;
        $order_id = $request->order_id;
        $print_class_id = $request->print_class_id;
        if ($auto_invoice == 0) {
            $orders = OrderItems::where('print_class_id', $print_class_id)->where('post_paid_orders_id', $order_id)->get();
            foreach ($orders as $order) {

                $order->auto_docket = $auto_docket;
                $order->save();
                $s = "updated";

            }
        } else {
            $post_order = PostPaidOrders::where('id', $order_id)->first();
            //return $post_order;exit;
            $post_order->auto_invoice = $auto_invoice;
            $post_order->save();
            $s = "updated";
        }
        if ($s == "updated") {
            return back()->with('success_message', 'Status Updated Successfully');
        }
        return back()->with('error_message', 'Status not Updated');

    }

    public function addDiscountpostpaid($order_id, $total_amount)
    {

        return view('admin.manage_orders.add_discount', compact('order_id', 'total_amount'));

    }
    public function addDiscountbillmaster($order_id, $total_amount)
    {

        return view('admin.manage_orders.add_discount_billMaster', compact('order_id', 'total_amount'));

    }
    public function addBillDiscountPostPaid(Request $request)
    {

        $data = $request->all();
        $post_order = PostPaidOrders::where('id', $request->id)->first();

        $post_order->discountType = $request->discountType;
        if ($request->discountType == "fixed") {
            $post_order->discount_amount = $request->amountToDiscount;
        } else {
            $post_order->discount_percent = $request->discount_percent;
        }
        $post_order->discount_reason = $request->discount_reason;
        $post_order->discounted_amount = $request->amount - $request->amount_after_discount;
        $post_order->total_amount = $request->amount_after_discount;
        $post_order->save();
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('admin/orders/postpaid')->with('message', 'Discount Added!');

    }

    public function addBillDiscountBillMaster(Request $request)
    {

        $data = $request->all();
        $order = BillMaster::where('id', $request->id)->first();

        $order->discountUserName = \Auth::user()->name;
        $order->discountType = $request->discountType;
        if ($request->discountType == "fixed") {
            $order->discount_amount = $request->amountToDiscount;
        } else {
            $order->discount_percent = $request->discount_percent;
        }
        $order->discount_reason = $request->discount_reason;
        $order->discounted_amount = $request->amount - $request->amount_after_discount;
        $order->total_amount = $request->amount_after_discount;
        $order->save();
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('admin/orders/master-bills')->with('message', 'Discount Added!');

    }

    public function voidItems()
    {

        $orders = VoidItems::all();
        //return $orders;
        return view('admin.manage_orders.admin_void_items', compact('orders'));

    }

    public function voidStatus(Request $request, $status)
    {

        //return $request;

        if ($status == 'Voided') {
                
            $user = \Auth::user();
            // $item = OrderItems::where('post_paid_orders_id',$order_id)->where('name',$item_name)->delete();
            //return $item;exit;
            $itemCheck = OrderItems::where('post_paid_orders_id', $request->order_id)->where('name', $request->item_name)->first();
            if (!$itemCheck)
            {
                 return redirect()->to('/admin/orders/void-items')->with('message', '');
            }
            $amount = $request->item_amount*$itemCheck->quantity;
            $or = PostPaidOrders::where('id', $request->order_id)->first();
            $or->total_amount = $or->total_amount - $amount;
            $or->save();
            
            $bil = BillMaster::where('id', $or->bill_id)->first();
            $bil->total_amount = $bil->total_amount - $amount;
            if($bil->total_amount < 0){
                $bil->total_amount = 0;
                $bil->discounted_amount = 0;
                $bil->discountType = "";
                $bil->discount_reason = '';
            }
            $bil->save();

            $item = OrderItems::where('post_paid_orders_id', $request->order_id)->where('name', $request->item_name)->delete();

            $void = VoidItems::where('order_id', $request->order_id)->where('item_name', $request->item_name)->first();
            $void->status = $status;
            $void->voided_id = $user->id;
            $void->voided_name = $user->name;
            $void->manager_comment = $request->void_reason;
            $void->save();

            Session::flash('message', 'We have sent you a verification email!');

            return redirect()->to('/admin/orders/void-items')->with('message', 'Item Voided Successfully');

        } else {
 $user = \Auth::user();
            $void = VoidItems::where('order_id', $order_id)->where('item_name', $item_name)->first();
            $void->status = $status;
            $void->voided_id = $user->id;
            $void->voided_name = $user->name;
            $void->save();
           

            Session::flash('message', 'We have sent you a verification email!');

            return redirect()->to('/admin/orders/void-items')->with('message', 'Item Void Request Rejected Successfully');

        }

    }
    
     public function rejectVoidStatus($status,$order_id,$item_id,$item_name)
    {
$user = \Auth::user();
        $void = VoidItems::where('order_id', $order_id)->where('item_name', $item_name)->first();
            $void->status = $status;
            $void->voided_id = $user->id;
            $void->voided_name = $user->name;
            $void->save();
 $updateItem = OrderItems::where('post_paid_orders_id', $order_id)->where('name', $item_name)->first();
    
    $updateItem->void_request = "no";
    $updateItem->save();
            Session::flash('message', 'We have sent you a verification email!');

            return redirect()->to('/admin/orders/void-items')->with('message', 'Item Void Request Rejected Successfully');

    }

    public function addVoidItems($order_id,$item_id)
    {
              $void = VoidItems::where('order_id', $order_id)->where('item_id', $item_id)->first();
            $item_name = $void->item_name;
            $amount = $void->amount;
        return view('admin.manage_orders.add_void_item_comment', compact('order_id', 'item_name', 'amount'));

    }

    public function transferBill($waiter_id, $waiter_name, $id)
    {

        $orders = User::where('user_type', 'waiter')->get();

        return view('admin.manage_orders.admin_transfer_bill', compact('orders', 'waiter_id', 'waiter_name', 'id'));

    }

    public function billTransfer(Request $request)
    {

        $user = User::where('id', $request->waiter)->first();
        $bill = BillMaster::where('id', $request->bill_id)->first();

        $bill->waiter_id = $user->id;
        $bill->waiter_name = $user->name;
        $bill->save();

        Session::flash('message', 'Bill Transfered Successfully!');

        return redirect()->to('admin/orders/master-bills')->with('message', 'Bill Transfered Successfully');

    }

}
