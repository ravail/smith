<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectPath()
    {
        if (\Auth::user()->user_type =='waiter') {
          
            return response('Please try later.', 401);
            // or return route('routename');
        }
        // else if(\Auth::user()->user_type=='admin')
        // {
        //     $redirectTo = '/admin/dashboard';
        //     return $redirectTo;
        // }
        // elseif(\Auth::user()->user_type=='Cashier'){
        //     $redirectTo = '/admin/dashboard';
        //     return $redirectTo;
        // }
        // elseif(\Auth::user()->user_type=='accounts'){
        //     $redirectTo = '/accounts/dashboard';
        //     return $redirectTo;
        // }
        // elseif(\Auth::user()->user_type=='supervisor'){
        //     $redirectTo = '/supervisor/dashboard';
        //     return $redirectTo;
        // }
        // elseif(\Auth::user()->user_type=='billprinter'){
        //     $redirectTo = '/billprinter/dashboard';
        //     return $redirectTo;
        // }
        // elseif(\Auth::user()->user_type=='eofici_admin_training'){
        //     $redirectTo = '/eofici_admin_training/dashboard';
        //     return $redirectTo;
        // }
        else
        {
              $redirectTo = '/admin/dashboard';
            return $redirectTo;
        }
    
        //return "/account";
        // or return route('routename');
    }
    protected function logout() {
        if(\Auth::user()->user_Type == 'print_class_user' ){
            return redirect('public/print-class/login')->with(\Auth::logout());
        }else{
            return redirect('/')->with(\Auth::logout());
        }
    }
    protected function logoutPrintClassUser() {
        
        return redirect('public/print-class/login')->with(\Auth::logout());
        
    }
}
