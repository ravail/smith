<?php

namespace App\Http\Controllers;
use App\PrintClassUser;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\SystemSetup\PrintClasses;
use App\PostPaidOrders;
use App\OrderItems;
use DB;

use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    
    public function index()
    {
        return view('home');
    }
    public function userLogin(){
        return view('partials.login');
        
    }
    public function printClassLogin(){
        return view('partials.print_class_login');
        
    }

    public function printClassUserLogin(Request $request){

       //return $request;
        $data = $request->all();
        $login_validator= Validator::make($data, [
            
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            

        ]);
        $user=PrintClassUser::where('email',$data['email'])->first();
        if(!empty($user)){

        

        if (Hash::check($data['password'], $user->password)) {

          Session::put('print_class' , $user->print_class);
          return redirect()->to('/public/print-class-orders');

        }
        else{

            Session::flash('message', 'We have sent you a verification email!');

            return back()->with('message', 'Invalid Login!');
            
        }
    }else{
            return back()->with('message', 'Invalid Login!');
        }
}

    // public function orders(){

    //     $print = Session::get('print_class');
    //     //return $print;
    //     $order_items = OrderItems::where('print_class_id',$print)->get();
    //     $station = OrderItems::where('post_paid_orders_id',17)->orderBy('id', 'DESC')->first();
        
    //     //return $order_items;
    //     $printClasses=PrintClasses::all();
    //     $orders=PostPaidOrders::where('cooking_status','!=',"Delivered")->get();
        
    //     return view('admin.systemsetup.print_class_orders',compact('print','printClasses','orders','order_items','station'));

    // }
    
    public function orders(){

        $print = Session::get('print_class');
        //return $print;
        $orders = array();
        $order_items = OrderItems::where('print_class_id',$print)->where('cooking_status','!=',"DELIVERED")->get();
        
        $selected_order_id = "";
       $i=0;
    //   echo "<pre>";
        foreach($order_items as $item){
        //   echo $item->post_paid_orders_id;
            $selectedOrder2=PostPaidOrders::where('id',$item->post_paid_orders_id)->first();
        //   if($i == 33)
        //   {
        //       print_r( $selectedOrder2);
        //   exit;
              
        //   } 
            
            if(sizeof($orders) == 0){
                
                array_push($orders,$selectedOrder2);
                $selected_order_id = $selectedOrder2->id ;
                
            }else{
          
                
                    
                    if($selectedOrder2->id == $selected_order_id){
                        
                        $selected_order_id=$selectedOrder2->id;
                        
                    } else{
                        array_push($orders,$selectedOrder2);
                        $selected_order_id = $selectedOrder2->id ;
                    }
                    
              
                    
               
                
            }
            
            $i++;
        }
    //return $orders;
        $printClasses=PrintClasses::all();
        
        
        return view('admin.systemsetup.print_class_orders',compact('print','printClasses','orders','order_items'));

    }

    public function ordersUpdate(){

        $print = Session::get('print_class');
        //return $print;
        $orders = array();
        $order_items = OrderItems::where('print_class_id',$print)->where('cooking_status','!=',"DELIVERED")->get();
       
        foreach($order_items as $item){
           
            $selectedOrder=PostPaidOrders::where('id',$item->post_paid_orders_id)->first();
            //return $selectedOrder;
             
              if(sizeof($orders) == 0){
                
                array_push($orders,$selectedOrder);
                $selected_order_id = $selectedOrder->id ;
                
            }else{
          
                
                    
                    if($selectedOrder->id == $selected_order_id){
                        
                        $selected_order_id=$selectedOrder->id;
                        
                    } else{
                        array_push($orders,$selectedOrder);
                        $selected_order_id = $selectedOrder->id ;
                    }
                    
              
                    
               
                
            }
            
            
         }
      
       //print_r($orders);
        $printClasses=PrintClasses::all();
        //$orders=PostPaidOrders::where('cooking_status','!=',"Delivered")->get();

        $array['print'] = $print;
        $array['order_items'] = $order_items;
        $array['printClasses'] = $printClasses;
        $array['orders'] = $orders;

        return $array;

    }
    
    public function orderHistory($id){
//return $id;
        $order_items = DB::select("select * from order_items where print_class_id = '".$id."' and cooking_status = 'DELIVERED' and created_at >= curdate() order by id desc");
        
        //return $order_items;
    $start_date = "";
        $end_date = "";
        //$order_items=OrderItems::where('print_class_id',$id)->where('cooking_status','DELIVERED')->get();
        //$orders = PostPaidOrders::all();
        $orders = DB::select('select * from post_paid_orders where created_at >= curdate() order by id desc');
        $printClasses=PrintClasses::all();
        $print = Session::get('print_class');        
    
        return view('admin.systemsetup.print_class_history',compact('order_items','orders','printClasses','print','print','id','start_date','end_date'));

    }
    
    public function orderHistoryFilter(Request $request)
    {
        $data = $request->all();
        $start_date = $data['start-date'];
        $end_date = $data['end-date'];
        $id = $data['id'];
        
        $order_items = DB::select("select * from order_items where print_class_id = '".$id."' and cooking_status = 'DELIVERED' and created_at between '".$start_date."' and '".$end_date."' order by id desc");
        $orders = DB::select("select * from post_paid_orders where created_at between '".$start_date."' and '".$end_date."' order by id desc");
        $printClasses=PrintClasses::all();
        $print = Session::get('print_class');
        
        return view('admin.systemsetup.print_class_history',compact('order_items','orders','printClasses','print','id','start_date','end_date'));
    }
}
