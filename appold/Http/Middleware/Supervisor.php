<?php

namespace App\Http\Middleware;

use Closure;

class Supervisor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next , $guard=null)
    {
        if (\Auth::guard($guard)->check()) {
            if(\Auth::guard($guard)->user()->user_type=='supervisor')
            {
                return $next($request);
            }
            
            else
            {
                $error_heading = "Oops ! Unauthorized ";
                $error_description = "This page does not exit or You are not authorized to view this page.";
                //return response(view('partials.error-page',compact('error_heaing','error_description')));
               return $error_heading;
            }
           
        }
        return $next($request);
    }
}
