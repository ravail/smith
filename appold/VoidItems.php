<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoidItems extends Model
{
    protected $fillable = [

        'order_id', 'waiter_id','item_id','item_name','waiter_name','status','comments','amount','manager_comment','voided_name'

    ];
}
