<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrepaidOrders extends Model
{
    protected $fillable = [
        'date_time','table_no','no_of_guests','item_description','condiments','waiter','restaurant','total_amount','status',
    ];
}
