<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_type','restaurant_id','role_id','image', 'birth_date', 'gender', 'address', 'state', 'pin_code', 'country','employees_assign_table','phone_number','badge_number','id_number','date_employeed','complementary_number','complementary_amount','discount','a_image','dashboard_view','menu_setup_view','major_groups_manager_view','major_groups_manager_add','major_groups_manager_delete','major_groups_manager_edit','sub_major_groups_manager_view','sub_major_groups_manager_edit','sub_major_groups_manager_add','sub_major_groups_manager_delete','menu_item_groups_edit','menu_item_groups_view','menu_item_groups_add','menu_item_groups_delete','offers_view','offers_edit','offers_add','offers_delete','family_groups_view','family_groups_edit','family_groups_add','family_groups_delete','alcoholic_family_groups_view','alcoholic_family_groups_edit','alcoholic_family_groups_add','alcoholic_family_groups_delete','alcoholic_sub_family_groups_view','alcoholic_sub_family_groups_edit','alcoholic_sub_family_groups_add','alcoholic_sub_family_groups_delete','menu_Item_view','general_item_view','general_item_edit','general_item_add','general_item_delete','offer_themes_nights_view','offer_themes_nights_edit','offer_themes_nights_add','offer_themes_nights_delete','app_users_view','app_users_addamounttowallet','manage_orders_view','prepaid_orders_view','postpaid_orders_view','open_orders_view','open_orders_transfer','open_orders_add','open_orders_delete','bills_view','generate_bills_view','master_bills_view','master_bills_close','master_bills_discount','master_bills_delete','closed_orders_view','closed_orders_payment_view','closed_orders_payment_reprints','complete_orders_view','system_setup_view','advertisements_view','advertisements_edit','advertisements_add','advertisements_delete','social_links_view','social_links_edit','reservation_email_edit','reservation_email_view','reservation_email_add','reservation_email_delete','payent_method_view','payent_method_add','payent_method_edit','payent_method_delete','app_custom_pages_view','app_custom_pages_edit','restaurants_view','restaurants_edit','restaurants_add','restaurants_delete','print_classes_view','print_classes_edit','print_classes_add','print_classes_delete','take_away_view','take_away_edit','take_away_add','take_away_delete','employees_view','employees_edit','employees_add','employees_delete','print_class_users_view','print_class_users_add','print_class_users_edit','print_class_users_delete','print_class_users_change_password','employees_change_password','table_managers_view','table_managers_edit','table_managers_add','table_managers_delete','tax_manager_view','tax_manager_add','tax_manager_edit','tax_manager_delete','settings_view','settings_edit','condiments_view','member_view','member_edit','member_add','member_delete','groups_view','groups_edit','groups_add','groups_delete','roles_view','roles_edit','roles_add','roles_delete','roles_permissions','recipes_view','WEBACCOUNTING_ERP_view','feedback_view','reservations_view','reservations_edit','reservations_delete','beer_delivery_and_keg_setup_view','beer_delivery_and_keg_setup_edit','beer_delivery_and_keg_setup_add','beer_delivery_and_keg_setup_delete','beer_and_keg_sub_major_group_view','beer_and_keg_sub_major_group_edit','beer_and_keg_sub_major_group_add','beer_and_keg_sub_major_group_delete','delivery_family_groups_view','delivery_family_groups_edit','delivery_family_groups_add','delivery_family_groups_delete','delivery_sub_family_groups_view','delivery_sub_family_groups_edit','delivery_sub_family_groups_add','delivery_sub_family_groups_delete','delivery_items_view','delivery_items_edit','delivery_items_add','delivery_items_delete','reports_view','reports_payment_sales_summary','reports_menu_item_general_sales','reports_menu_tem_general_sales_with_plu','reports_menu_item_general_sales_without_plu','reports_family_group_sales_with_gl','reports_menu_item_group_sales','reports_major_group_sales','reports_waiter_with_family_groups','reports_condiments_sales_with_plu','reports_get_discount_reports','reports_get_cashier_reports','get_discount_reports_with_orders','reports_get_complementary_reports_with_orders','reports_get_cancelled_orders_reports','reports_wallet_ledger_entries','gl_view','plu_view','reports_payment_sales_summary1','reports_void_items',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
     public function orders(){
        return $this->hasMany(\App\PostPaidOrders::class,'waiter_id')->with('orderItems');
    }
}
