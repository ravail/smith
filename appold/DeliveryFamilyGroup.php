<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryFamilyGroup extends Model
{
    protected $fillable = [
        'name', 'delivery_sub_major_group_name', 'description' , 'a_image',
    ];
}
