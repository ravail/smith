<?php

namespace App\SystemSetup;

use Illuminate\Database\Eloquent\Model;

class Condiments extends Model
{
    protected $fillable = [
        'name','plu_number','a_image'
    ];
}
