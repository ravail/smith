<?php

namespace App\SystemSetup;

use Illuminate\Database\Eloquent\Model;

class ReservationEmail extends Model
{
    protected $fillable = [
        'email', 'phone',
    ];
}
