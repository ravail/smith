<?php

namespace App\SystemSetup;

use Illuminate\Database\Eloquent\Model;

class Advertisements extends Model
{
    protected $fillable = [
        'name', 'display_order', 'a_image',
    ];
}
