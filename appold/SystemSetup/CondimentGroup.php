<?php

namespace App\SystemSetup;

use Illuminate\Database\Eloquent\Model;

class CondimentGroup extends Model
{
    protected $fillable = [
        'name','max_selection_limit','condiments'
    ];
}
