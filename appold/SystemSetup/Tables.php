<?php

namespace App\SystemSetup;

use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    protected $fillable = [
        'name', 'restaurant', 'section','capacity',
    ];
}
