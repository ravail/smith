<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyGroup extends Model
{
    protected $fillable = [
        'name', 'menu_item_groups_id', 'gl_account','a_image',
    ];

    public function menuItems()
    {
        return $this->belongsTo(\App\MenuItemsGroup::class);
    }
}
