<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PLUNumber extends Model
{
    protected $fillable = [
        'plu_number',
    ];
}
