<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubMajorGroups extends Model
{
    protected $fillable = [
        'name', 'major_group_name', 'description' , 'a_image',
    ];

    public function menuItems(){
        return $this->hasMany(\App\MenuItemsGroup::class);
    }
    
}
