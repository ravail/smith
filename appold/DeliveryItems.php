<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryItems extends Model
{
    protected $fillable = [
        'name','price' ,'family_groups_id','description','plu_number','tax','is_show_menu' ,'a_image',
    ];
}
