<?php

namespace App\Listeners;

use App\Events\OrderSended;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderToStation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderSended  $event
     * @return void
     */
    public function handle(OrderSended $event)
    {
        //
    }
}
