<?php

namespace App\ManageOrders;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $fillable = [
        'bill_id','narration','payment_method','total_amount',
    ];
}
