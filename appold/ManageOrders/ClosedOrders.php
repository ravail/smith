<?php

namespace App\ManageOrders;

use Illuminate\Database\Eloquent\Model;

class ClosedOrders extends Model
{
    protected $fillable = [
        'date','bill_id','waiter_id','waiter_name','no_orders','total_amount',
    'cashier_id','cashier_name'];
}
