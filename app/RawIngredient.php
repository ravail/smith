<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawIngredient extends Model
{
    protected $fillable = [
       'name', 'price', 'quantity_type', 'quantity','re_order_level','print_class',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $table = 'raw_ingredient';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
