<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostPaidOrders extends Model
{
    protected $fillable = [
        'date_time','section','bill_id','table_no','no_of_guests','item_description','condiments','waiter_id','waiter','restaurant','total_amount','status',
    ];

    public function orderItems(){
        return $this->hasMany(\App\OrderItems::class,'post_paid_orders_id');
    }
}
