<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrintClassUser extends Model
{
    protected $fillable = [
        'name', 'user_Type','email','restaurant','print_class','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
