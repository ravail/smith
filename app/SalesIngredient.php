<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesIngredient extends Model
{
    protected $fillable = [
       'ingredient_id', 'price', 'quantity','print_class_name','void'
    ];
    protected $table='sales_ingredient';
}
