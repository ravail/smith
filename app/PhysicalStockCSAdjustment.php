<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhysicalStockCSAdjustment extends Model
{
    protected $fillable = [
       'ingredient_id', 'ingredient_name','system_quantity','variance','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $table = 'physical_stock_central_store_adjustments';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
