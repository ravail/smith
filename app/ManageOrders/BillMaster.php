<?php

namespace App\ManageOrders;

use Illuminate\Database\Eloquent\Model;

class BillMaster extends Model
{
    protected $fillable = [
        'date','waiter_id','waiter_name','no_orders','xero_invoice_id','orders','total_amount','discounted_amount',
    ];
}
