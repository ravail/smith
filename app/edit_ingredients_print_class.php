<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class edit_ingredients_print_class extends Model
{
    protected $fillable = [
       'name', 'current_quantity', 'print_class_id','adjusted_quantity','print_class_name','ingredient_id','transferred_from'
    ];
     public $table = 'edit_ingredients_print_class';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}

