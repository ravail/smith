<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    protected $fillable = [
      'name','recipe_id' ,'ingredient_id','price', 'quantity_type', 'quantity','general_item_id','print_class_id'
    ];
    protected $table = 'ingredients';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
