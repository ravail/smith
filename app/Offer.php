<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'name','price' ,'max_selection_limit','sub_major_group_name', 'description' ,'tax' ,'a_image',
    ];
}
