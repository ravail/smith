<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhysicalStockPSAdjustment extends Model
{
    protected $fillable = [
       'ingredient_id', 'ingredient_name', 'print_class_id','print_class_name','system_quantity','variance','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $table = 'physical_stock_print_store_adjustments';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
