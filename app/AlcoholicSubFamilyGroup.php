<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlcoholicSubFamilyGroup extends Model
{
    protected $fillable = [
        'name', 'family_groups_id','description', 'gl_accounts','a_image',
    ];

    public function alcoholicGroups()
    {
        return $this->belongsTo(\App\AlcoholicFamilyGroup::class);
    }
}
