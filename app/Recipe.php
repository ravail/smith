<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = [
      'recipe_id','general_item_id','name','cost', 'ingredient_count',
    ];
    
    public $table = 'recipe';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
