<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class edit_ingredients extends Model
{
    protected $fillable = [
       'name', 'price', 'current_quantity', 'quantity_type','adjusted_quantity','total_quantity','user','remarks','ingredient_id'
    ];
}
