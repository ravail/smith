<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhysicalStockCS extends Model
{
    protected $fillable = [
       'ingredient_id', 'quantity', 'ingredient_name','variance','last_physical_quantity'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $table = 'physical_stock_central_store';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
