<?php

namespace App\SystemSetup;

use Illuminate\Database\Eloquent\Model;

class Restaurants extends Model
{
    protected $fillable = [
        'name', 'opentime', 'closetime','location','a_image' , 'f_image',
    ];
}
