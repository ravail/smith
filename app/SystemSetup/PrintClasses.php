<?php

namespace App\SystemSetup;

use Illuminate\Database\Eloquent\Model;

class PrintClasses extends Model
{
    protected $fillable = [
        'name',
    ];
}
