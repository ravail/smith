<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'name'
    ];
    // Table name
  protected $table = 'units';

  //Primary key
  public $primaryKey = 'id';

  //timestamps
  public $timestamps = true;
   
   
}