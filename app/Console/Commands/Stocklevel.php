<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\RawIngredient;
use App\User;
use DB;
use Carbon\Carbon;
use Mail;
class Stocklevel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:level';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stock Reorder Level';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ingredients = RawIngredient::all();
        foreach($ingredients as $key => $value1){
            if($value1->quantity < $value1->re_order_level ){
                $user = User::where('user_type','admin')->get();
                
    foreach($user as $key=>$value){
        $Text = "Alert\n Stock level goes Down For Item  \nName : ".$value1->name  ."\nitem_id :". $value1->id."\nItem Quantity :".$value1->quantity."\nLast Updated :".$value1->updated_at;
             $email=$value->email;
             Mail::raw($Text, function ($message) use ($email) {
              $message->from('info@thesmitherp.com', 'Smith Hotels');
      
              $message->to($email);
              $message->subject('Stock Re Order Level Alert');
         });
    }
                
            }
        }
    }
}
