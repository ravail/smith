<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stock extends Model
{
    protected $fillable = [
       'ingredient_id', 'print_class', 'quantity_added', 'ingredient_name','voided','reduced_quantity',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $table = 'stock';
    //Primary key
    protected $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
