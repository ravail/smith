<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class xero extends Model
{
  
  protected $table = 'xero';

  //Primary key
  public $primaryKey = 'id';

  //timestamps
  public $timestamps = false;
}
