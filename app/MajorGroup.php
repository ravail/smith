<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MajorGroup extends Model
{
    protected $fillable = [
        'name', 'display_order', 'a_image',
    ];
}
