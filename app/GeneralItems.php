<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralItems extends Model
{
    protected $fillable = [
        'name','price','purchase_price','quantity' ,'family_groups_id','description','print_class_id','condiments_groups_id','plu_number','tax','is_show_menu' ,'a_image',
    ];
}
