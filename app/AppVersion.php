<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppVersion extends Model
{
    protected $fillable = [
        'version'
    ];
    // Table name
  protected $table = 'app_setting';

  //Primary key
  public $primaryKey = 'id';

  //timestamps
  public $timestamps = true;
}

