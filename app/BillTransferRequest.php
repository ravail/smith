<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillTransferRequest extends Model
{
    protected $fillable = [
        'bill_master_id', 'from_waiter_id', 'from_waiter_name', 'to_waiter_id', 'to_waiter_name', 'status'
    ];
    // Table name
  protected $table = 'bill_transfer_request';

  //Primary key
  public $primaryKey = 'id';

  //timestamps
  public $timestamps = true;
}

