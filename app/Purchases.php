<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    protected $fillable = [
       'ingredient_id', 'price', 'quantity',
    ];
}
