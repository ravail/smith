<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GLAccount extends Model
{
    protected $fillable = [
        'account_number',
    ];
}
