<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $fillable = [
        'name','general_item_id','do_print','post_paid_orders_id', 'quantity','price' ,'print_class_id','image','comments'
    ];

    public function orders()
    {
        return $this->belongsTo(\App\PostPaidOrders::class);
    }
}
