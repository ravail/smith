<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlcoholicFamilyGroup extends Model
{
    protected $fillable = [
        'name', 'menu_items_group_id', 'a_image',
    ];

    public function menuItems()
    {
        return $this->belongsTo(\App\MenuItemsGroup::class);
    }

    public function subAlcoholicGroups(){

        return $this->hasMany(\App\AlcoholicSubFamilyGroup::class);
    }
}
