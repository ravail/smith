<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliverySubMajorGroup extends Model
{
    protected $fillable = [
        'name', 'major_group_name', 'description' , 'a_image',
    ];
}
