<?php

namespace App\Http\Controllers;
use App\BillTransferRequest;
use App\ManageOrders\BillMaster;
use Illuminate\Http\Request;
use DB;
class BillTransferRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index(){
        $orders = BillTransferRequest::all();
        $bills = BillMaster::all();
        return view('admin.manage_orders.bill_transfer_requests',compact('orders','bills'));

    }
    public function statusChange($id,$status){
        $billTransferInfo =BillTransferRequest::where('id',$id)->first();
        if($status == 'Accepted'){
            $billInfo = BillMaster::where('id',$billTransferInfo->bill_master_id)->first();
            $billInfo->waiter_id =$billTransferInfo->to_waiter_id;
            $billInfo->waiter_name =$billTransferInfo->to_waiter_name;
            $billInfo->save();
            $billTransferInfo->status="Accepted";
            $billTransferInfo->save();
            return redirect()->to('/admin/orders/requested-order')->with('message','Bill trasfer Accepted!');
           
        }else{
            $billTransferInfo->status="Rejected";
            $billTransferInfo->save();
            return redirect()->to('/admin/orders/requested-order')->with('message','Bill trasfer Rejected!');
        }
        
    }
    public function reports(){
          $orders = BillTransferRequest::all();
           $file = fopen("BillTransferReport.csv","w");
            fputcsv($file, array('','','Bill Transfer Requests Report','',''));
            // fputcsv($file, array('','','','','','',''));
    
            // fputcsv($file, array('','','','','','',''));
            fputcsv($file, array('','','','All Time Report','',''));
            fputcsv($file, array('Sr.No',	'bill ID',	'From Waiter',	'To Waiter',	'status',	'Date'));
            $id=1;
            foreach( $orders as $detail){
                
                 fputcsv($file, array($id,$detail->bill_master_id,$detail->from_waiter_name,$detail->to_waiter_name,$detail->status,$detail->created_at));
                $id++;
                
            }
            fclose($file);
            $start_date = NULL;
        $end_date = NULL;
           return view('admin.reports.bill_transfer_report',compact('orders','start_date','end_date'));
    }
    
public function reports1(Request $request){
    $start_date = $request->start_date;
        $end_date = $request->end_date;
        
          $orders = DB::select("select * FROM bill_transfer_request where created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "' ");
           $file = fopen("BillTransferReport.csv","w");
            fputcsv($file, array('','','Bill Transfer Requests Report','',''));
            // fputcsv($file, array('','','','','','',''));
    
            // fputcsv($file, array('','','','','','',''));
            fputcsv($file, array('','','','All Time Report','',''));
            fputcsv($file, array('Sr.No',	'bill ID',	'From Waiter',	'To Waiter',	'status',	'Date'));
            $id=1;
            foreach( $orders as $detail){
                
                 fputcsv($file, array($id,$detail->bill_master_id,$detail->from_waiter_name,$detail->to_waiter_name,$detail->status,$detail->created_at));
                $id++;
                
            }
            fclose($file);
           return view('admin.reports.bill_transfer_report',compact('orders','start_date','end_date'));
    }
    
}
