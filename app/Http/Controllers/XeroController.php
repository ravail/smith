<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\storage;
use XeroAPI\XeroPHP\AccountingObjectSerializer;
use XeroPHP\Remote\Exception\BadRequestException;
use Session;
use App\xero;
class XeroController extends Controller
{
    public function index()
    {

        ini_set('display_errors', 'On');
  require '../vendor/autoload.php';
 

        // $storage = new storage();

        // session_start();
      
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => '7926ACDEA0224C1585A35CD1DCB79BF9',   
            'clientSecret'            => '2wLASnD9-NfmiQBIQ_YGZikLexT-SN-qcgGXKtDPNemzUXFS',
            'redirectUri'             => 'https://smith.test/callback', 
          'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
          'urlAccessToken'          => 'https://identity.xero.com/connect/token',
          'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);
      
        // Scope defines the data your app has permission to access.
        // Learn more about scopes at https://developer.xero.com/documentation/oauth2/scopes
        $options = [
            'scope' => ['openid email profile offline_access accounting.settings accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments']
        ];
      
        // This returns the authorizeUrl with necessary parameters applied (e.g. state).
        $authorizationUrl = $provider->getAuthorizationUrl($options);
      
        // Save the state generated for you and store it to the session.
        // For security, on callback we compare the saved state with the one returned to ensure they match.
        // Session::put('oauth2state', $provider->getState());
        $xero=xero::where('id',1)->update(array('oauth2state' => $provider->getState()));;
        
    //   dd(Session::all());
        // Redirect the user to the authorization URL.
        header('Location: ' . $authorizationUrl);
        exit();
    }
    public function callback()
    {

//         ini_set('display_errors', 'On');
//    require '../vendor/autoload.php';
 
  $xero=xero::find(1);
    $a= $xero->oauth2state;

  $provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                => '7926ACDEA0224C1585A35CD1DCB79BF9',   
    'clientSecret'            => '2wLASnD9-NfmiQBIQ_YGZikLexT-SN-qcgGXKtDPNemzUXFS',
    'redirectUri'             => 'https://smith.test/callback', 
    'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
    'urlAccessToken'          => 'https://identity.xero.com/connect/token',
    'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
  ]);
   
  // If we don't have an authorization code then get one
  if (!isset($_GET['code'])) {
    echo "Something went wrong, no authorization code found";
    exit("Something went wrong, no authorization code found");

    $a=null;
    $a->save();
   
  // Check given state against previously stored one to mitigate CSRF attack
  } elseif (empty($_GET['state']) || ($_GET['state'] !== $a)) {
    echo "Invalid State";
    // unset($_SESSION['oauth2state']);
    exit('Invalid state');
  } else {
  
    try {
      // Try to get an access token using the authorization code grant.
      $accessToken = $provider->getAccessToken('authorization_code', [
        'code' => $_GET['code']
      ]);
           
      $config = \XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( $accessToken->getToken() );
      $identityApi = new \XeroAPI\XeroPHP\Api\IdentityApi(
        new \GuzzleHttp\Client(),
        $config
      );
       
      $result = $identityApi->getConnections();

      // Save my tokens, expiration tenant_id
     
      $xero=xero::where('id',1)->update(array('token' => $accessToken->getToken(),'expires' => $accessToken->getExpires(),'tenant_id'=>$result[0]->getTenantId(),'refresh_token'=>$accessToken->getRefreshToken(),'id_token'=>$accessToken->getValues()["id_token"]));
   
      header('Location: ' . '/authorisedResource');
      exit();
     
    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
      echo "Callback failed";
      exit();
    }
  }
    }


    public function authorisedResource(){




  // Use this class to deserialize error caught
 
  // Storage Classe uses sessions for storing token > extend to your DB of choice

  // $res = mysqli_query($con,"SELECT * FROM xero_tokens LIMIT 1") ;
  // $row = mysqli_fetch_array($res);


$xero=xero::find(1);
  $xeroTenantId = $xero->tenant_id;


//   if ($storage->getHasExpired()) {
  
  $provider = new \League\OAuth2\Client\Provider\GenericProvider([
    'clientId'                => '7926ACDEA0224C1585A35CD1DCB79BF9',   
    'clientSecret'            => '2wLASnD9-NfmiQBIQ_YGZikLexT-SN-qcgGXKtDPNemzUXFS',
    'redirectUri'             => 'https://smith.test/callback', 
    'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
    'urlAccessToken'          => 'https://identity.xero.com/connect/token',
    'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
  ]);

        // echo "xero tendent id"."<br>";
        // echo $xeroTenantId."<br>";
        $refresh_token = $xero->refresh_token;
        // $refresh_token = $row['refresh_token'];
       
        // echo "refresh token"."<br>";
        // echo $refresh_token."<br>";
        // echo $storage->getRefreshToken()."<br>";
        // echo "new access token"."<br>";
        
    $newAccessToken = $provider->getAccessToken('refresh_token', [
      'refresh_token' => $refresh_token,
    ]);
    
  $config = \XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( $newAccessToken );
  $apiInstance = new \XeroAPI\XeroPHP\Api\AccountingApi(
      new \GuzzleHttp\Client(),
      $config
  );


    }
    
}
