<?php

namespace App\Http\Controllers;
use App\PrintClassUser;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\SystemSetup\PrintClasses;
use App\PostPaidOrders;
use App\OrderItems;
// use XeroPrivate;
use App\xero;
use DB;
use DateTime;

use Illuminate\Support\Facades\Hash;
// use XeroPHP\Models\Accounting\Item;

class HomeController extends Controller
{
    public function test(){

        // $arr_invoices=[];
        // $lineArray=[];

        // $invoice = new \XeroAPI\XeroPHP\Models\Accounting\Invoice;
        // $contact = new \XeroAPI\XeroPHP\Models\Accounting\Contact;
        
        //     //$date1 = explode(" ", $order->date_time);
            
        //      //set  contact for invoices
        //     $contact->setContactStatus(\XeroAPI\XeroPHP\Models\Accounting\Contact::CONTACT_STATUS_ACTIVE);
        //     $contact->setName('test');
        //     $contact->setEmailAddress('test@test.com');
        //     $invoice->setContact($contact);
        //     $invoice->setType(\XeroAPI\XeroPHP\Models\Accounting\Invoice::TYPE_ACCREC);
        //     $dateInstance = new DateTime();
        //     $dateInstance3 = new DateTime();
        //     $invoice->setDate($dateInstance);
        //     $dateInstance1 = $dateInstance3;
        //     $invoice->setDueDate($dateInstance1);
        //     $invoice->setCurrencyCode(\XeroAPI\XeroPHP\Models\Accounting\CurrencyCode::KES);
        //     $invoice->setStatus(\XeroAPI\XeroPHP\Models\Accounting\Invoice::STATUS_SUBMITTED);

       
         
        //           $line= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
               
        //         $line->setDescription('testin');
        //         $item_id = 'item-2025';
        //         $line->setQuantity(1);
        //         $line->setItemCode($item_id);
        //         // $line->setAccountCode('001');
        //         $line->setUnitAmount(5);
        //         $line->setTaxType('NONE');
        //         $line->setTaxAmount(0);
        //         $amount= 5.00;
        //         $line->setLineAmount($amount);
        //         $line->setDiscountRate(0); // Percentage
        //         // // Add the line to the order
        //         array_push($lineArray, $line);

        //         $linee= new \XeroAPI\XeroPHP\Models\Accounting\LineItem;
        //         $linee->setDescription('testin');
        //         $item_id = 'item-2026';
        //         $linee->setQuantity(1);
        //         $linee->setItemCode($item_id);
        //         // $line->setAccountCode('001');
        //         $linee->setUnitAmount(5);
        //         $linee->setTaxType('NONE');
        //         $linee->setTaxAmount(0);
        //         $amount= 5.00;
        //         $linee->setLineAmount($amount);
        //         $linee->setDiscountRate(0);
                

        //         array_push($lineArray, $linee);

               
        //          $invoice->setLineItems($lineArray);
                 
                
                 

        //          try{
        //             $dataa= $this->xeroInstance();
        //             $new = $dataa['api_instance']->updateInvoice($dataa['xero_tenant_id'],'721c67da-ee5f-48b8-af59-a6b5f26e6bcf',$invoice);
        //         } catch (\XeroAPI\XeroPHP\ApiException $exception) {
        //             print_r($exception);
        //             exit();
        //         } 

        //         dd($new->getInvoices()[0]->getInvoiceId());

        
        
        $banktransaction_1 = new \XeroAPI\XeroPHP\Models\Accounting\BankTransaction;
$banktransaction_1->setReference('Ref-' . $this->getRandNum())
	->setDate(new DateTime('2021-5-07'))
	
	->setType("PAY")
	->setLineAmountTypes(\XeroAPI\XeroPHP\Models\Accounting\LineAmountTypes::EXCLUSIVE)
	->setBankAccount($bankAccount)
	->setContact($contact);
array_push($arr_banktransactions, $banktransaction_1);

		
$banktransactions = new \XeroAPI\XeroPHP\Models\Accounting\BankTransactions;
$banktransactions->setBankTransactions($arr_banktransactions);


          try{
                    $dataa= $this->xeroInstance();
                    $result = $dataa['api_instance']->createBankTransactions($dataa['xero_tenant_id'], $banktransactions); 

                    // $new = $dataa['api_instance']->updateInvoice($dataa['xero_tenant_id'],'721c67da-ee5f-48b8-af59-a6b5f26e6bcf',$invoice);
                } catch (\XeroAPI\XeroPHP\ApiException $exception) {
                    print_r($exception);
                    exit();
                } 



    }
    
    public function index()
    {
        return view('home');
    }
    public function userLogin(){
        return view('partials.login');
        
    }
    public function printClassLogin(){
        return view('partials.print_class_login');
        
    }

    public function printClassUserLogin(Request $request){

       //return $request;
        $data = $request->all();
        $login_validator= Validator::make($data, [
            
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            

        ]);
        $user=PrintClassUser::where('email',$data['email'])->first();
        if(!empty($user)){

        

        if (Hash::check($data['password'], $user->password)) {
            
          

          Session::put('print_class' , $user->print_class);
          return redirect()->to('/public/print-class-orders');

        }
        else{

            Session::flash('message', 'We have sent you a verification email!');

            return back()->with('message', 'Invalid Login!');
            
        }
    }else{
            return back()->with('message', 'Invalid Login!');
        }
}

    // public function orders(){

    //     $print = Session::get('print_class');
    //     //return $print;
    //     $order_items = OrderItems::where('print_class_id',$print)->get();
    //     $station = OrderItems::where('post_paid_orders_id',17)->orderBy('id', 'DESC')->first();
        
    //     //return $order_items;
    //     $printClasses=PrintClasses::all();
    //     $orders=PostPaidOrders::where('cooking_status','!=',"Delivered")->get();
        
    //     return view('admin.systemsetup.print_class_orders',compact('print','printClasses','orders','order_items','station'));

    // }
    
    public function orders(){

        $print = Session::get('print_class');
        //return $print;
        $orders = array();
         $order_items = OrderItems::where('print_class_id',$print)->where('cooking_status','!=',"DELIVERED")->orderBy('id', 'DESC')->skip(0)->take(200)->get();
       // $order_items = DB::select("select * from order_items where print_class_id = '".$print."' and cooking_status != 'DELIVERED' order by id desc limit 200");
        
        $selected_order_id = "";
       $i=0;
    //   echo "<pre>";
        foreach($order_items as $item){
        //   echo $item->post_paid_orders_id;
            $selectedOrder2=PostPaidOrders::where('id',$item->post_paid_orders_id)->first();
        //   if($i == 33)
        //   {
        //       print_r( $selectedOrder2);
        //   exit;
              
        //   } 
            
            if(sizeof($orders) == 0){
                
                array_push($orders,$selectedOrder2);
                $selected_order_id = $selectedOrder2->id ;
                
            }else{
          
                
                    
                    if($selectedOrder2->id == $selected_order_id){
                        
                        $selected_order_id=$selectedOrder2->id;
                        
                    } else{
                        array_push($orders,$selectedOrder2);
                        $selected_order_id = $selectedOrder2->id ;
                    }
                    
              
                    
               
                
            }
            
            $i++;
        }
    //return $orders;
        $printClasses=PrintClasses::all();
        
        // dd($order_items);exit;
        
        
        
        return view('admin.systemsetup.print_class_orders',compact('print','printClasses','orders','order_items'));

    }

    public function ordersUpdate(){

        $print = Session::get('print_class');
        //return $print;
        $orders = array();
        $order_items = OrderItems::where('print_class_id',$print)->where('cooking_status','!=',"DELIVERED")->get();
       
        foreach($order_items as $item){
           
            $selectedOrder=PostPaidOrders::where('id',$item->post_paid_orders_id)->first();
            //return $selectedOrder;
             
              if(sizeof($orders) == 0){
                
                array_push($orders,$selectedOrder);
                $selected_order_id = $selectedOrder->id ;
                
            }else{
          
                
                    
                    if($selectedOrder->id == $selected_order_id){
                        
                        $selected_order_id=$selectedOrder->id;
                        
                    } else{
                        array_push($orders,$selectedOrder);
                        $selected_order_id = $selectedOrder->id ;
                    }
                    
              
                    
               
                
            }
            
            
         }
      
       //print_r($orders);
        $printClasses=PrintClasses::all();
        //$orders=PostPaidOrders::where('cooking_status','!=',"Delivered")->get();

        $array['print'] = $print;
        $array['order_items'] = $order_items;
        $array['printClasses'] = $printClasses;
        $array['orders'] = $orders;

        return $array;

    }
    
    public function orderHistory($id){
//return $id;
        $order_items = DB::select("select * from order_items where print_class_id = '".$id."' and cooking_status = 'DELIVERED' and created_at >= curdate() order by id desc");
        
        //return $order_items;
    $start_date = "";
        $end_date = "";
        //$order_items=OrderItems::where('print_class_id',$id)->where('cooking_status','DELIVERED')->get();
        //$orders = PostPaidOrders::all();
        $orders = DB::select('select * from post_paid_orders where created_at >= curdate() order by id desc');
        $printClasses=PrintClasses::all();
        $print = Session::get('print_class');        
    
        return view('admin.systemsetup.print_class_history',compact('order_items','orders','printClasses','print','print','id','start_date','end_date'));

    }
    
    public function orderHistoryFilter(Request $request)
    {
        $data = $request->all();
        $start_date = $data['start-date'];
        $end_date = $data['end-date'];
        $id = $data['id'];
        
        $order_items = DB::select("select * from order_items where print_class_id = '".$id."' and cooking_status = 'DELIVERED' and created_at between '".$start_date."' and '".$end_date."' order by id desc");
        $orders = DB::select("select * from post_paid_orders where created_at between '".$start_date."' and '".$end_date."' order by id desc");
        $printClasses=PrintClasses::all();
        $print = Session::get('print_class');
        
        return view('admin.systemsetup.print_class_history',compact('order_items','orders','printClasses','print','id','start_date','end_date'));
    }
    
    // public function xeroApi(){
        
    //     $invoices2 = XeroPrivate::load('Accounting\\Item')->execute();
       
        
        
        
       
        
    //     foreach($invoices2 as $invoi){
        
    //          //$code= $invoi->SalesDetails->AccountCode;
             
        
    //     echo $invoi->Code;
    //     echo '\n';
    //     }
       
    //    exit;
    //     //$invoices = XeroPrivate::load(Contact::class)->execute();
    //     // $contact = XeroPrivate::load('Accounting\\Contact')->execute();
    //     $invoices = XeroPrivate::load('Accounting\\Invoice')->where('InvoiceID=GUID("e8d71763-a50b-4977-923c-f7f9d147e701")')->execute();
    //     foreach($invoices as $invoice){
    //          if($invoice->Status !='PAID') {
    //              $invoice1 = new \XeroPHP\Models\Accounting\Invoice();
    //               $invoice1->setStatus('SUBMITTED');
    //               $invoice1->setInvoiceID('f94f2a54-3f3d-4eba-a229-ffd2b2dd2bf1');
    //             $posted_in_invoice = XeroPrivate::save($invoice1);
                 
    //          }
    //     }
       
    //      return($invoices) ;
    //      exit;
    //     // dd($contact);
    //     //$contact = XeroPrivate::load(Contact::class)->execute();
    //     // foreach($contact as $contact){
    //     //     $id= $contact->ContactID;
    //     //     break;
    //     // } 
        
    //     // return $items;

       
    //      $invoice = new \XeroPHP\Models\Accounting\Invoice();
    //      $contact = new \XeroPHP\Models\Accounting\Contact();
    //      $line1 = new \XeroPHP\Models\Accounting\Invoice\LineItem();
    //     // $line2 = new  \XeroPHP\Models\Accounting\Invoice\LineItem();
        
    //     // // Set up the invoice contact
    //      //$contact->setAccountNumber('201');
    //      $contact->setContactStatus('ACTIVE');
    //      $contact->setName('Muhammad Kashif');
    //      $contact->setEmailAddress('muhammadkashif70000@gmail.com');
    //    // $contact->ContactID($id);
    //     // $contact->setDefaultCurrency('GBP');

    //     // // Assign the contact to the invoice
    //      $invoice->setContact($contact);

    //     // // Set the type of invoice being created (ACCREC / ACCPAY)
    //      $invoice->setType('ACCREC');

    //      $dateInstance = new DateTime();
    //      $invoice->setDate($dateInstance);
    //      $dateInstance1 = $dateInstance->modify('+1 weeks');
    //      $invoice->setDueDate($dateInstance1);
    //     // $invoice->setInvoiceNumber('DMA-00001');
    //     // $invoice->setReference('DMA-00001');

    //     // // Provide an URL which can be linked to from Xero to view the full order
    //     // $invoice->setUrl('http://yourdomain/fullpathtotheorder');

    //      $invoice->setCurrencyCode('KES');
    //      $invoice->setStatus('SUBMITTED');

    //     // // Create some order lines

    //      $line1->setDescription('Blue tshirt');

    //      $line1->setQuantity(1);
    //      $line1->setItemCode('0002');
    //      $line1->setAccountCode('310');
    //      $line1->setUnitAmount(99.99);
    //      $line1->setTaxType('NONE');
    //      $line1->setTaxAmount(0);
    //      $line1->setLineAmount(99.99);
    //      $line1->setDiscountRate(0); // Percentage

    //     // // Add the line to the order
    //      $invoice->addLineItem($line1);

    //     // // Repeat for all lines...
    //     $posted_payment = XeroPrivate::save($invoice);
        
    //     $sxml = simplexml_load_string($posted_payment->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
        
        
    //     echo $sxml->Invoices->Invoice->InvoiceID;
        
        
      
    //         }

    public function xeroInstance(){


        $xero=xero::find(1);
        $xeroTenantId = $xero->tenant_id;
      
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
          'clientId'                => '7926ACDEA0224C1585A35CD1DCB79BF9',   
          'clientSecret'            => '2wLASnD9-NfmiQBIQ_YGZikLexT-SN-qcgGXKtDPNemzUXFS',
          'redirectUri'             => 'https://smith.test/callback', 
          'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
          'urlAccessToken'          => 'https://identity.xero.com/connect/token',
          'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);
      
              // echo "xero tendent id"."<br>";
              // echo $xeroTenantId."<br>";
              $refresh_token = $xero->refresh_token;
             
              
          $newAccessToken = $provider->getAccessToken('refresh_token', [
            'refresh_token' => $refresh_token,
          ]);
          
        $config = \XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( $newAccessToken );
         $apiInstance = new \XeroAPI\XeroPHP\Api\AccountingApi(
            new \GuzzleHttp\Client(),
            $config
        );

        return array('xero_tenant_id'=>$xeroTenantId,'api_instance'=>$apiInstance);

    }
}

