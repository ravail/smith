<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\xero;
use App\Http\Requests\AddMajorGroupRequest as AddMajorGroupRequest;
use App\MajorGroup;
use App\SubMajorGroups;
use App\Tax;
use App\AppVersion;
use App\Unit;
use DB;
use App\OrderItems;
use Session;
use App\PrepaidOrders;
use App\PostPaidOrders;
use App\edit_ingredients;
use App\edit_ingredients_print_class;
use App\GLAccount;
use App\Ingredients;
use App\Offer;

use XeroAPI\XeroPHP\ApiException;
use XeroAPI\XeroPHP\AccountingObjectSerializer;
use XeroAPI\XeroPHP\PayrollAuObjectSerializer;
use App\FamilyGroup;
use App\PLUNumber;
use App\GeneralItems;
use App\DeliveryItems;
use App\MenuItemsGroup;
use App\AlcoholicFamilyGroup;
use App\AlcoholicSubFamilyGroup;
use App\DeliverySubMajorGroup;
use App\DeliveryFamilyGroup;
use App\DeliverySubFamilyGroup;
use App\SystemSetup\PrintClasses;
use App\SystemSetup\CondimentGroup;
use Illuminate\Support\Facades\Validator;
use Image;
use XeroPrivate;
use DateTime;
use App\Recipe;
use App\RawIngredient;
use App\PhysicalStockCS;
use App\PhysicalStockPS;
use App\PhysicalStockCSAdjustment;
use App\PhysicalStockPSAdjustment;
use App\stock;
use App\Purchases;
use Illuminate\Support\Str;

class AdminMenuSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function index()
    {
        $groups=MajorGroup::all();
        //return $groups;exit;

        return view('admin.admin_major_groups',compact('groups'));
    }
    
    public function test(){
        
        $items = RawIngredient::all();
        
       
        $print_classes=PrintClasses::all();
        
       
       
                    foreach($items as $key =>$value1){
                         
                        
                        
                    foreach($print_classes as $key => $value){
                        
                        $stock = array(
                            'ingredient_id' => $value1->id,
                            'print_class' => $value->name,
                            'quantity_added' => 0,
                            'ingredient_name' => $value1->name,
                            
                            );
                            
                            stock::create($stock);
                        
                        
                        
                    }
                                   
                     
                        
                        
                        
                        
                    }

              
        
        
    }

   

    public function addMajorGroup(){

        return view('admin.admin_add_major_group');
    }

    public function createMajorGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=MajorGroup::create($data);

        Session::flash('message', 'We have sent you a verification email!');
       
        return redirect()->to('/admin/major-group-managers')->with('message','Major Group Created Successfully!');
        
    }

    public function deleteMajorGroup($id){

        $delete = MajorGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Major Group Deleted Successfully');

        }
    }

    public function editMajorGroup($id){

        $existGroup = MajorGroup::where('id', $id)->first();
        //return $existGroup;exit;
        return view('admin.admin_edit_major_group',compact('existGroup'));
    }

    public function updateMajorGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = MajorGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            
            if($data['a_image']!=""){

                $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                if ($image_uploaded) {
                
                    $data['a_image'] = $imageName;
                
                }
            }

            $profile=$existGroup->update($data);

            $profile = MajorGroup::where('id', $id)->get();

            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/major-group-managers')->with('message','Major Group Updated!');

        }
    }
    
    ///////////////////////////////// GL Account ////////////////////////////////////////////////

    public function GlAccount()
    {
        $accounts=GLAccount::all();
        //return $groups;exit;

        return view('admin.admin_gl_accounts',compact('accounts'));
    }

    public function addGlAccount(){
        return view('admin.admin_add_glaccount');
    }

    public function createGlAccount(Request $request){

        $data=$request->all();
      
        $add=GLAccount::create($data);

        Session::flash('message', 'We have sent you a verification email!');        
        return redirect()->to('/admin/gl-accounts')->with('message','GL Account Created!');
        
    }
    public function deleteGlAccount($id){

        $delete = GLAccount::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'GL Account Deleted Successfully!');
            
            return back()->with('message', 'GL Account Deleted Successfully!');

        }
    }

    public function editGlAccount($id){

        $existGl = GLAccount::where('id', $id)->first();
        //return $existGroup;exit;
        return view('admin.admin_edit_glaccount',compact('existGl'));
    }

    public function updateGlAccount(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = GLAccount::where('id', $id)->first();
       
        if (!empty($existGroup)) {

           
            $profile=$existGroup->update($data);
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/gl-accounts')->with('message','GL Account Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ////////////////////////////////// PLU Numbers //////////////////////////////////////////////

    public function PLUNumber()
    {
        $accounts=PLUNumber::all();
        //return $groups;exit;

        return view('admin.admin_plu_numbers',compact('accounts'));
    }

    public function addPLUNumber(){
        return view('admin.admin_add_plu_number');
    }

    public function createPLUNumber(Request $request){

        $data=$request->all();
      
        $add=PLUNumber::create($data);
        Session::flash('message', 'GL Account Deleted Successfully!');
        
        return redirect()->to('/admin/plu-numbers')->with('message','PLU Number Deleted Successfully!');
        
    }
    public function deletePLUNumber($id){

        $delete = PLUNumber::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'GL Account Deleted Successfully!');
            
            return back()->with('message', 'PLU Number Deleted Successfully!');

        }
    }

    public function editPLUNumber($id){

        $existGl = PLUNumber::where('id', $id)->first();
        //return $existGroup;exit;
        return view('admin.admin_edit_plu_number',compact('existGl'));
    }

    public function updatePLUNumber(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = PLUNumber::where('id', $id)->first();
       
        if (!empty($existGroup)) {
         
            $profile=$existGroup->update($data);
            Session::flash('message', 'We have sent you a verification email!');

            return redirect()->to('/admin/plu-numbers')->with('message','PLU Number Updated!');

        }
    }


    /////////////////////////////////Sub Major Groups////////////////////////////////////////////

    public function subMajorGroups(){

        $main_groups=MajorGroup::all();
        
        $groups=SubMajorGroups::all();
        
        return view('admin.admin_sub_major_groups',compact('groups','main_groups'));
    }
    public function addSubMajorGroup(){
        $groups=MajorGroup::all();
        
        return view('admin.admin_add_sub_major_groups',compact('groups'));
    }
    public function createSubMajorGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=SubMajorGroups::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/sub-major-groups')->with('message', 'Sub Major Group created Successfully!');
    }

    public function deleteSubMajorGroup($id){

        $delete = SubMajorGroups::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Sub Major Group Deleted Successfully');

        }
    }

    public function editSubMajorGroup($id){

        $existGroup = SubMajorGroups::where('id', $id)->first();
        $groups=MajorGroup::all();
        
        //return $existGroup;exit;
        
        return view('admin.admin_edit_sub_major_group',compact('existGroup','groups'));
    }

    public function updateSubMajorGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = SubMajorGroups::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }
           
            $profile=$existGroup->update($data);

            $profile = SubMajorGroups::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/sub-major-groups')->with('message','Sub Major Group Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function offers(){

        $offers=Offer::all();
        $groups=SubMajorGroups::all();
        
        
        return view('admin.admin_offers',compact('offers','groups'));
    }

    public function addOffers(){
        
        $groups=SubMajorGroups::all();
        $tax=Tax::all();
        
        return view('admin.admin_add_offers',compact('groups','tax'));
    }

    public function createOffer(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=Offer::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('admin/offers')->with('message', 'Offer Created Successfully!');

    }

    public function deleteOffer($id){

        $delete = Offer::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Offer Deleted Successfully');

        }
    }

    public function editOffer($id){

        $existOffer = Offer::where('id', $id)->first();
        
        $groups=SubMajorGroups::all();
        $taxes=Tax::all();
 
        return view('admin.admin_edit_offer',compact('existOffer','groups','taxes'));
    }

    public function updateOffer(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = Offer::where('id', $id)->first();
       
        if (!empty($existGroup)) {

           
            $profile=$existGroup->update($data);

            $profile = Offer::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/offers')->with('message','Offer Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    //////////////////////////////////General Items /////////////////////////////////////////

    public function generalItems(){

        //$groups = FamilyGroup::all();
        $groups = DB::select('SELECT id,check_flag,name FROM `family_groups`
                    UNION DISTINCT
                SELECT id,check_flag,name FROM `alcoholic_sub_family_groups`');
        $items = GeneralItems::all();
        // return $groups;
        return view('admin.admin_general_items',compact('groups','items'));
    }

    public function addGeneralItems(){
        //  $account = new \XeroPHP\Models\Accounting\Account();
       
        $data= $this->xeroInstance();
       

      
           
        $accounts = $data['api_instance']->getAccounts($data['xero_tenant_id'], null, null, null);

       
        $taxes = $data['api_instance']->getTaxRates($data['xero_tenant_id'], null, null, null);
        //  dd($taxes);
                     
                                   
                                       
        // $family_groups = FamilyGroup::all();
        $family_groups = DB::select('SELECT id,check_flag,name FROM `family_groups`
        UNION DISTINCT
        SELECT id,check_flag,name FROM `alcoholic_sub_family_groups`');
        
        $tax = Tax::all();
        $print_classes = PrintClasses::all();
        $plu_numbers = PLUNumber::all();
        $family_groups = FamilyGroup::all();
        return view('admin.admin_add_menu_general_items',compact('family_groups','tax','print_classes','plu_numbers','accounts','taxes'));
    }

    public function createGeneralItems(Request $request){
       
        $item = new \XeroAPI\XeroPHP\Models\Accounting\Item;
        $purchase = new \XeroAPI\XeroPHP\Models\Accounting\Purchase;
        $sale = new \XeroAPI\XeroPHP\Models\Accounting\Purchase;
    

        $data=$request->all();
        // dd($data);
        $size = sizeof($data['tax']);
  
        if($size > 1){
            
            $data['tax'] = $data['tax'][0] .",".$data['tax'][1];
          
        }else{
            $data['tax'] = $data['tax'][0];
        }

        // return $data['tax']; 
        if($request->hasFile('a_image')){
            $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
            $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
            if ($image_uploaded) {
               
                $data['a_image'] = $imageName;
              
            }
        }
        $add=GeneralItems::create($data)->id;
        $add=$add+5000;
        $date =new DateTime;
        $item_array=array();
        //setting purchases details
        $purchase->setUnitPrice($data['purchase_price'])
                 ->setAccountCode($request->purchase_account)
                 
                 ->setTaxType($request->purchase_tax_rate);
        //setting sales details
        $sale->setUnitPrice($data['price'])
                 ->setAccountCode($request->sale_account)
                 
                 ->setTaxType($request->sale_tax_rate);
        $item->setName($data['name'])
			->setCode('item-'.$add)
			->setDescription($data['description'])
			->setIsTrackedAsInventory(true)
			->setQuantityOnHand($data['quantity'])
			->setSalesDetails($sale)
			->setPurchaseDetails($purchase)	
			->setIsPurchased(true)	
			->setIsSold(true);
		
            array_push($item_array, $item);
        
            //  print_r($item);exit;
            try{
                $data= $this->xeroInstance();
                $new = $data['api_instance']->createItems($data['xero_tenant_id'],$item,true,4);
            } catch (\XeroAPI\XeroPHP\ApiException $exception) {
                print_r($exception);
               
            } 
// 		  try {

//             $data= $this->xeroInstance();
//             $new = $data['api_instance']->createItems($data['xero_tenant_id'],$item,true,4);

// } catch (Exception $e) {

    
// }
		
// 
		//stock management for xero
// 		 $invoice = new \XeroPHP\Models\Accounting\Invoice();
// 		 $contact = new \XeroPHP\Models\Accounting\Contact();
// 		 $lineItem= new \XeroPHP\Models\Accounting\Invoice\LineItem();
// 		 $lineItem1= new \XeroPHP\Models\Accounting\Invoice\LineItem();
// 		 $account = new \XeroPHP\Models\Accounting\Account();
//          $account->setCode('102');
// 		 $user = \Auth::user();
		 
// 		     //set  contact for invoices
//             $contact->setContactStatus('ACTIVE');
//             $contact->setName($user->name);
//             $contact->setEmailAddress($user->email);
//             $invoice->setContact($contact);
//             $invoice->setType('ACCPAY');
//             $dateInstance = new DateTime();
//             $invoice->setDate($dateInstance);
//             $dateInstance1 = $dateInstance->modify('+1 weeks');
//             $invoice->setDueDate($dateInstance1);
//             $invoice->setCurrencyCode('KES');
//             $invoice->setStatus('AUTHORISED');
//             //line item for increase quantity 
//             $lineItem->setDescription($data['name']);
            
//             $lineItem->setQuantity($data['quantity']);
//             $lineItem->setItemCode('item-'.$add);
//             $lineItem->setAccountCode('310');
//             $lineItem->setUnitAmount($data['purchase_price']);
//             $lineItem->setTaxType('NONE');
//             $lineItem->setTaxAmount(0);
            
//             // $lineItem1->setDescription('Inventory Opening Balance');
//             // $lineItem1->setQuantity($data['quantity']);
//             // $lineItem1->setAccountCode('411');
//             // $lineItem1->setUnitAmount(-$data['purchase_price']);
//             // // Add the line to the order
//              $invoice->addLineItem($lineItem);
//             //  $invoice->addLineItem($lineItem1);
             
// 		    $invoice = XeroPrivate::save($invoice);	
// 		    $sxml = simplexml_load_string($invoice->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
		    
// 		    $invoiceid = $sxml->Invoices->Invoice->InvoiceID;
		    
// 		    //$invoice1 = new \XeroPHP\Models\Accounting\Invoice();
// 		    $invoice1 = XeroPrivate::loadByGUID('Accounting\\Invoice', $invoiceid);
		   // $invoice1->setInvoiceID($invoiceid);
		  //  print_r($invoice1);
		  //  exit;
		  //  $newPayment = new \XeroPHP\Models\Accounting\Payment();
    //         $dateInstance = new DateTime();
    //         $TOTAL= $data['quantity']*$data['purchase_price'];
    //         $newPayment->setInvoice($invoice1)
    //                     ->setAccount($account)
    //                     ->setDate($dateInstance)
    //                     ->setAmount($TOTAL)
    //                     ->setIsReconciled(true);
    //         $posted_payment = XeroPrivate::save($newPayment);
        
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/general-items')->with('message','General Item Created!');
        
    }

    public function deleteGeneralItems($id){

        $delete = GeneralItems::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'General Item Deleted Successfully!');

        }
    }

    public function editGeneralItems($id){
         
        $data= $this->xeroInstance();
       

      
           
        $accounts = $data['api_instance']->getAccounts($data['xero_tenant_id'], null, null, null);

        $d=$id+5000;
        $unitdp = 4;
        $taxes1 = $data['api_instance']->getTaxRates($data['xero_tenant_id'], null, null, null);
         $xeroitem= $data['api_instance']->getItem($data['xero_tenant_id'], 'item-'.$d, $unitdp);
        $existGeneralItem = GeneralItems::where('id', $id)->first();
       
        
        $family_groups = DB::select('SELECT id,check_flag,name FROM `family_groups`
        UNION DISTINCT
        SELECT id,check_flag,name FROM `alcoholic_sub_family_groups`');
        $taxes = Tax::all();
        $print_classes = PrintClasses::all();
        $plu_numbers = PLUNumber::all();
        $condiments_groups = CondimentGroup::all();
        return view('admin.admin_edit_general_items',compact('taxes1','xeroitem','accounts','existGeneralItem','family_groups','taxes','print_classes','plu_numbers','condiments_groups'));
    }

    public function updateGeneralItems(Request $request ,$id){
       
        
       
        $purchase = new \XeroAPI\XeroPHP\Models\Accounting\Purchase;
        $sale = new \XeroAPI\XeroPHP\Models\Accounting\Purchase;
        $data=$request->all();
        $size = sizeof($data['tax']);
  
        if($size > 1){
            
            $data['tax'] = $data['tax'][0] .",".$data['tax'][1];
          
        }else{
            $data['tax'] = $data['tax'][0];
        }
         if(! $request->has('is_show_menu') ){
            $data['is_show_menu']= 'off';
        }
        
        $existGroup = GeneralItems::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            $date =new DateTime;
            $profile=$existGroup->update($data);
            $dataa= $this->xeroInstance();
             $d=$id+5000;
        $unitdp = 4;
      
         $item= $dataa['api_instance']->getItem($dataa['xero_tenant_id'], 'item-'.$d, $unitdp);
            // print_r($item);
            
            



            $purchase->setUnitPrice($data['purchase_price'])
            ->setAccountCode($request->purchase_account)
            
            ->setTaxType($request->purchase_tax_rate);
   //setting sales details
   $sale->setUnitPrice($data['price'])
            ->setAccountCode($request->sale_account)
            
            ->setTaxType($request->sale_tax_rate);
   $item[0]->setName($data['name'])
       
       ->setDescription($data['description'])
       ->setIsTrackedAsInventory(true)
       ->setQuantityOnHand($data['quantity'])
       ->setSalesDetails($sale)
       ->setPurchaseDetails($purchase)	
       ->setIsPurchased(true)	
       ->setIsSold(true);
   
    //    array_push($item_array, $item);
   
       //  print_r($item);exit;
       try{
          
           $new = $dataa['api_instance']->updateItem($dataa['xero_tenant_id'],'item-'.$d,$item,4);
       } catch (\XeroAPI\XeroPHP\ApiException $exception) {
           print_r($exception);
           exit();
       } 


             
    //         $purchase->setUnitPrice($data['purchase_price'])
    //              ->setAccountCode($request->purchase_account)
    //              ->setTaxType($request->purchase_tax_rate)
    //              ->setUpdatedDateUTC($date);
    //         //setting sales details
    //         $sale->setUnitPrice($data['price'])
    //                  ->setAccountCode($request->sale_account)
    //                  ->setTaxType($request->sale_tax_rate)
    //                  ->setUpdatedDateUTC($date);
    //         $item->setName($data['name'])
    // // 			->setItemID($code)
    // 			->setDirty('Code')
	// 		->setDescription($data['description'])
	// 		->setQuantityOnHand($data['quantity'])
	// 		->setSalesDetails($sale)
	// 		->setPurchaseDetails($purchase)	
	// 		->setIsPurchased(true)	
	// 		->setIsSold(true);
// 			echo"<pre>ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff<br>";
			
// 		print_r($item);
// 		exit;
	
	
// 			  try {

//   $itemss = XeroPrivate::save($item);	
	

// } catch (Exception $e) {

    
// }
	
	
// 	echo "<pre>";
// 		print_r($itemss) ;exit;
//	->setSalesDetails($sale)
		//	->setPurchaseDetails($purchase)	
// 		->setQuantityOnHand($data['quantity'])
		
// 			->setIsPurchased(true)	
// 			->setIsSold(true);
		//stock management for xero
// 		 $invoice = new \XeroPHP\Models\Accounting\Invoice();
// 		 $contact = new \XeroPHP\Models\Accounting\Contact();
// 		 $lineItem= new \XeroPHP\Models\Accounting\Invoice\LineItem();
// 		 $lineItem1= new \XeroPHP\Models\Accounting\Invoice\LineItem();
// 		 $account = new \XeroPHP\Models\Accounting\Account();
//          $account->setCode('102');
// 		 $user = \Auth::user();
		 
// 		     //set  contact for invoices
//             $contact->setContactStatus('ACTIVE');
//             $contact->setName($user->name);
//             $contact->setEmailAddress($user->email);
//             $invoice->setContact($contact);
//             $invoice->setType('ACCPAY');
//             $dateInstance = new DateTime();
//             $invoice->setDate($dateInstance);
//             $dateInstance1 = $dateInstance->modify('+1 weeks');
//             $invoice->setDueDate($dateInstance1);
//             $invoice->setCurrencyCode('KES');
//             $invoice->setStatus('AUTHORISED');
//             //line item for increase quantity 
//             $lineItem->setDescription($data['name']);
            
//             $lineItem->setQuantity($data['quantity']);
//             $lineItem->setItemCode('item-'.$id);
//             $lineItem->setAccountCode('310');
//             $lineItem->setUnitAmount($data['purchase_price']);
//             $lineItem->setTaxType('NONE');
//             $lineItem->setTaxAmount(0);
            
//             // // Add the line to the order
//              $invoice->addLineItem($lineItem);
//             //  $invoice->addLineItem($lineItem1);
             
// 		    $invoice = XeroPrivate::save($invoice);	
// 		    $sxml = simplexml_load_string($invoice->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
		    
// 		    $invoiceid = $sxml->Invoices->Invoice->InvoiceID;
		    
// 		    //$invoice1 = new \XeroPHP\Models\Accounting\Invoice();
// 		    $invoice1 = XeroPrivate::loadByGUID('Accounting\\Invoice', $invoiceid);
// 		   // $invoice1->setInvoiceID($invoiceid);
// 		  //  print_r($invoice1);
// 		  //  exit;
// 		    $newPayment = new \XeroPHP\Models\Accounting\Payment();
//             $dateInstance = new DateTime();
//             $TOTAL= $data['quantity']*$data['purchase_price'];
//             $newPayment->setInvoice($invoice1)
//                         ->setAccount($account)
//                         ->setDate($dateInstance)
//                         ->setAmount($TOTAL)
//                         ->setIsReconciled(true);
//             $posted_payment = XeroPrivate::save($newPayment);

//             $profile = GeneralItems::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/general-items')->with('message','General Item Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ///////////////////////////////// Delivery Items /////////////////////////////////////

    public function deliveryItems(){

        $groups = FamilyGroup::all();
        $items = DeliveryItems::all();
        return view('admin.beer_&_keg_setup.admin_delivery_items',compact('groups','items'));
    }

    public function addDeliveryItems(){
        
        $family_groups = FamilyGroup::all();
        $tax = Tax::all();
       
        $plu_numbers = PLUNumber::all();

        return view('admin.beer_&_keg_setup.admin_add_delivery_items',compact('family_groups','tax','plu_numbers'));
    }

    public function createDeliveryItems(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=DeliveryItems::create($data);
        
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/delivery-items')->with('message', 'Delivery Item created Successfully!');
    }

    public function deleteDeliveryItems($id){

        $delete = DeliveryItems::where('id', $id)->delete();
        if($delete){
            Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'Delivery Item Deleted Successfully!');

        }
    }

    public function editDeliveryItems($id){

        $existDeliveryItem = DeliveryItems::where('id', $id)->first();
        $family_groups = FamilyGroup::all();
        $taxes = Tax::all();
       
        $plu_numbers = PLUNumber::all();

        return view('admin.beer_&_keg_setup.admin_edit_delivery_items',compact('existDeliveryItem','family_groups','taxes','plu_numbers'));
    }

    public function updateDeliveryItems(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliveryItems::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliveryItems::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            
            return redirect()->to('/admin/delivery-items')->with('message','Delivery Item Updated!');

            //return back()->with('success_message', 'Data Updated Successfully.');

        }
    }

    ///////////////////////////////// Menu Items Groups //////////////////////////////////

    public function menuItemGroups(){
        
        $menus=MenuItemsGroup::all();
        $groups=SubMajorGroups::all();
        
        
        return view('admin.admin_menu_item_groups',compact('menus','groups'));
    }

    public function addMenuItemGroups(){
        
        $groups=SubMajorGroups::all();
        $tax=Tax::all();
        
        return view('admin.admin_add_menu_item_group',compact('groups','tax'));
    }

    public function createMenuItemGroup(Request $request){

        $data=$request->all();
      //return $data;
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=MenuItemsGroup::create($data);
        
        Session::flash('message', 'We have sent you a verification email!');        
        return redirect()->to('/admin/menu-item-groups')->with('message','Menu Item Group Created Successfully!');
        
    }

    public function deleteMenuItemGroup($id){

        $delete = MenuItemsGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Menu Item Group Deleted Successfully!');
        }
    }

    public function editMenuItemGroup($id){

        
        $groups = SubMajorGroups::all();
       
        $existGroup = MenuItemsGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_menu_item_group',compact('existGroup','groups'));
    }

    public function updateMenuItemGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = MenuItemsGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = MenuItemsGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/menu-item-groups')->with('message','Menu Item Group Updated!');


        }
    }

    ///////////////////////////// Family Groups //////////////////////////////////

    public function familyGroups(){
        $groups=FamilyGroup::paginate(200);
        //return $groups;exit;

        return view('admin.admin_family_groups',compact('groups'));
    }

    public function addFamilyGroups(){

        $groups=MenuItemsGroup::all();

        $glAccounts=GLAccount::all();


        return view('admin.admin_add_family_group',compact('groups','glAccounts'));
    }

    public function createFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=FamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/family-groups')->with('message', 'Family Group created Successfully!');
    }

    public function deleteFamilyGroup($id){

        $delete = FamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Family Group Deleted Successfully');

        }
    }

    public function editFamilyGroup($id){

        
    
        $groups=MenuItemsGroup::all();

        $glAccounts=GLAccount::all();
       
        $existGroup = FamilyGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_family_group',compact('existGroup','groups','glAccounts'));
    }

    public function updateFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = FamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            
            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = FamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/family-groups')->with('message','Family Group Updated!');

        }
    }

    //////////////////////////// Alcoholic Family Group //////////////////////////////////

    public function alcoholicFamilyGroups(){

        $menus=MenuItemsGroup::all();
        $fgroup=AlcoholicFamilyGroup::all();        
        //return $groups;exit;

        return view('admin.admin_alcoholic_family_group',compact('fgroup','menus'));
    }

    public function addAlcoholicFamilyGroups(){

        
        $menus=MenuItemsGroup::all();

        return view('admin.admin_add_alcoholic_family_group',compact('menus'));
    }

    public function createAlcoholicFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=AlcoholicFamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/alcoholic-family-groups')->with('message', 'Alcoholic Family Group created Successfully!');
    }

    public function deleteAlcoholicFamilyGroup($id){

        $delete = AlcoholicFamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Alcoholic Group Deleted Successfully!');

        }
    }
    public function editAlcoholicFamilyGroup($id){

        
    
        $groups=MenuItemsGroup::all();
       
        $existGroup = AlcoholicFamilyGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_alcoholic_family_groups',compact('existGroup','groups'));
    }

    public function updateAlcoholicFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = AlcoholicFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {
            
            if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = AlcoholicFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/alcoholic-family-groups')->with('message','Alcoholic Family Group Updated!');


        }
    }

    ////////////////////////////////////////// Alcoholic Sub Family Group //////////////////////////////

    public function alcoholicSubFamilyGroups(){

        $familygroups=AlcoholicFamilyGroup::all();
        $subFamilyGroups=AlcoholicSubFamilyGroup::all();
        return view('admin.admin_alcoholic_sub_family_groups',compact('familygroups','subFamilyGroups'));
    }

    public function addAlcoholicSubFamilyGroups(){
 
        $familygroups=AlcoholicFamilyGroup::all();
        $account=GLAccount::all();  

        return view('admin.admin_add_alcoholic_sub_family_group',compact('familygroups','account'));
    }

    public function createAlcoholicSubFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=AlcoholicSubFamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        
        return redirect()->to('/admin/alcoholic-sub-family-groups')->with('message', 'Alcoholic Sub Family Group created Successfully!');
    }

    public function deleteAlcoholicSubFamilyGroup($id){

        $delete = AlcoholicSubFamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('message', 'Alcoholic Sub Family Group Deleted Successfully!');

        }
    }

    public function editAlcoholicSubFamilyGroup($id){

        $familygroups=AlcoholicFamilyGroup::all();
        $accounts=GLAccount::all();  
    
        $existGroup = AlcoholicSubFamilyGroup::where('id', $id)->first();
        
        return view('admin.admin_edit_alcoholic_sub_family_group',compact('existGroup','familygroups','accounts'));
    }

    public function updateAlcoholicSubFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = AlcoholicSubFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

 if($data['a_image']){
                 $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                    $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                    if ($image_uploaded) {
           
                            $data['a_image'] = $imageName;
          
                }
            }

            $profile=$existGroup->update($data);

            $profile = AlcoholicSubFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/alcoholic-sub-family-groups')->with('message','Alcoholic Sub Family Group Updated!');


        }
    }


    //////////////////////////////// Beer & Keg (1) ////////////////////////////////

    public function deliverySubMajorGroup(){

        $main_groups=MajorGroup::all();
        
        $groups=DeliverySubMajorGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_delivery_sub_major_group',compact('groups','main_groups'));
    }

    public function addDeliverySubMajorGroup(){
        
        $groups=MajorGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_add_delivery_sub_major_group',compact('groups'));
    }

    public function createDeliverySubMajorGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=DeliverySubMajorGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('/admin/beer-and-keg-sub-major-group')->with('success_message', 'Delivery Sub Major Group created Successfully!');
    }

    public function deleteDeliverySubMajorGroup($id){

        $delete = DeliverySubMajorGroup::where('id', $id)->delete();
        if($delete){
        Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'Delivery Sub Major Group Deleted Successfully!');

        }
    }
    public function editDeliverySubMajorGroup($id){

        $groups=MajorGroup::all(); 
    
        $existGroup = DeliverySubMajorGroup::where('id', $id)->first();
        
        return view('admin.beer_&_keg_setup.admin_edit_delivery_sub_major_group',compact('existGroup','groups'));
    }

    public function updateDeliverySubMajorGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliverySubMajorGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliverySubMajorGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/beer-and-keg-sub-major-group')->with('message','Delivery Sub Major Group Updated!');


        }
    }

    ///////////////////////////////////////// Beer & Keg (2) ////////////////////////////////////////

    public function deliveryFamilyGroup(){

        $del_sub_groups=DeliverySubMajorGroup::all();
        
        $del_family_groups=DeliveryFamilyGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_delivery_family_group',compact('del_sub_groups','del_family_groups'));
    }

    public function addDeliveryFamilyGroup(){
        
        $groups=DeliverySubMajorGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_add_delivery_family_group',compact('groups'));
    }

    public function createDeliveryFamilyGroup(Request $request){

        $data=$request->all();
      
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
           
            $data['a_image'] = $imageName;
          
        }

        $add=DeliveryFamilyGroup::create($data);
        
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('/admin/delivery-family-group')->with('message','Delivery Family Group Created!');
    }

    public function deleteDeliveryFamilyGroup($id){

        $delete = DeliveryFamilyGroup::where('id', $id)->delete();
        if($delete){
        Session::flash('message', 'We have sent you a verification email!');
            
            return back()->with('message', 'Delivery Family Group Deleted Successfully');

        }
    }

    public function editDeliveryFamilyGroup($id){

        $groups=DeliverySubMajorGroup::all(); 
    
        $existGroup = DeliveryFamilyGroup::where('id', $id)->first();
        
        return view('admin.beer_&_keg_setup.admin_edit_delivery_family_group',compact('existGroup','groups'));
    }

    public function updateDeliveryFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliveryFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliveryFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/delivery-family-group')->with('message','Delivery Family Group Updated!');


        }
    }

/////////////////////////////////////////////// Beer & Keg (3) ///////////////////////////////////////////////

    public function deliverySubFamilyGroup(){

        //$del_sub_groups=DeliverySubMajorGroup::all();
        
        $del_family_groups=DeliveryFamilyGroup::all();

        $del_sub_family_group=DeliverySubFamilyGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_delivery_sub_family_group',compact('del_sub_family_group','del_family_groups'));
    }

    public function addDeliverySubFamilyGroup(){
        
        $groups=DeliveryFamilyGroup::all();
        
        return view('admin.beer_&_keg_setup.admin_add_delivery_sub_family_group',compact('groups'));
    }

    public function createDeliverySubFamilyGroup(Request $request){

        $data=$request->all();
    
        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {
        
            $data['a_image'] = $imageName;
        
        }

        $add=DeliverySubFamilyGroup::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('/admin/delivery-sub-family-group')->with('message','Delivery Sub Family Group Created!');
    }

    public function deleteDeliverySubFamilyGroup($id){

        $delete = DeliverySubFamilyGroup::where('id', $id)->delete();
        if($delete){
            return back()->with('success_message', 'Deleted Successfully');

        }
    }

    public function editDeliverySubFamilyGroup($id){

        $groups=DeliveryFamilyGroup::all(); 
    
        $existGroup = DeliverySubFamilyGroup::where('id', $id)->first();
        
        return view('admin.beer_&_keg_setup.admin_edit_delivery_sub_family_group',compact('existGroup','groups'));
    }

    public function updateDeliverySubFamilyGroup(Request $request ,$id){

        $data=$request->all();
     
        $existGroup = DeliverySubFamilyGroup::where('id', $id)->first();
       
        if (!empty($existGroup)) {

            $profile=$existGroup->update($data);

            $profile = DeliverySubFamilyGroup::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/delivery-sub-family-group')->with('message','Delivery Sub Family Group Updated!');


        }
    }

////////////////////////////////////////////////// Manage Ordeer //////////////////////////////
    public function prepaidOrders(){

        $orders = PrepaidOrders::all();
        return view('admin.manage_orders.admin_prepaidOrders',compact('orders'));
    }

    public function completedOrders(){

        return view('admin.manage_orders.admin_completeOrders');
        
    }
    public function closedOrders(){
        return view('admin.manage_orders.admin_closedOrders');
        
    }

    public function postpaidOrders(){
        
        $orders = DB::select("select post_paid_orders.*,restaurants.name from post_paid_orders join restaurants on post_paid_orders.restaurant = restaurants.id where Date(post_paid_orders.created_at) = '".date("Y-m-d")."'");

        //$orders = PostPaidOrders::where(date('created_at'),'=',date("Y-m-d"))->get();
        //$order_items = OrderItems::where('cooking_status','=',"DELIVERED")->get();
        $order_items = DB::select("select * from order_items where cooking_status = 'DELIVERED' and Date(created_at) = '".date("Y-m-d")."'");


        $start_date = '';
        $end_date = '';
        
        $file = fopen("post_paid_report.csv","w");
        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('','','','','','','Post paid Report'));

        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('Today Report','','','','','',''));
        fputcsv($file, array('ID','Date_Time','Table NO','Item Description','Customer Description','Waiter','Restaurant','Total Amount','Status'));
        
             $total = 0;
        foreach($orders as $order){
            
            
             $ord = DB::select("select * from order_items where '".$order->id."' = post_paid_orders_id");
             
             $order->comment=$ord[0]->comments;
            $items_description = "";
            $customer_description="";
        foreach($order_items as $order_item){
                
                
         if($order_item->post_paid_orders_id == $order->id){
                //alert(result.order_items[j]['name']);
                $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity .", " ;
                $customer_description = $order_item->comments;
                           
                }
            
            }
          
          fputcsv($file, array($order->id,$order->date_time,$order->table_no,$items_description,$customer_description,$order->waiter,$order->name,$order->total_amount,$order->status));
                $total += $order->total_amount;
         
        }
        fputcsv($file, array('','','','','','Total',$total));
        fclose($file);
        
        return view('admin.manage_orders.admin_postpaidOrders',compact('orders','order_items','start_date','end_date'));
        
    }
     public function postpaidreportfilter(Request $request){
            
        $data=$request->all();
        $start_date = $data['start-date'];
        $end_date = $data['end-date'];
        $orders = DB::select("select post_paid_orders.*,restaurants.name from post_paid_orders join restaurants on post_paid_orders.restaurant = restaurants.id where post_paid_orders.created_at between '".$data['start-date']."' and '".$data['end-date']."'");
   
        //$orders = PostPaidOrders::where(date('created_at'),'=',date("Y-m-d"))->get();
        //$order_items = OrderItems::where('cooking_status','=',"DELIVERED")->get();
         
        $order_items = DB::select("select * from order_items where cooking_status = 'DELIVERED' and created_at between '".$data['start-date']."' and '".$data['end-date']."'");
        
        $file = fopen("post_paid_report.csv","w");
        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('','','','','','','Post paid Report'));

        fputcsv($file, array('','','','','','',''));
        fputcsv($file, array('Dated','From',$start_date,'To',$end_date,'',''));
        fputcsv($file, array('ID','Date_Time','Table NO','Item Description','Customer Description','Waiter','Restaurant','Total Amount','Status'));
        
             $total = 0;
        foreach($orders as $order){
            
            $items_description = "";
            $customer_description="";
        foreach($order_items as $order_item){
                
         if($order_item->post_paid_orders_id == $order->id){
                //alert(result.order_items[j]['name']);
                $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity .", " ;
                $customer_description = $order_item->comments;
                           
                }
            
            }
          
          fputcsv($file, array($order->id,$order->date_time,$order->table_no,$items_description,$customer_description,$order->waiter,$order->name,$order->total_amount,$order->status));
                $total += $order->total_amount;
         
        }
        fputcsv($file, array('','','','','','Total',$total));
        fclose($file);
        
        return view('admin.manage_orders.admin_postpaidOrders',compact('orders','order_items','start_date','end_date'));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function ingredients()
    {
        
        $items = RawIngredient::all();
        
       
$unit=Unit::all();
        $uuid = Str::uuid()->toString();
        $general =GeneralItems::all();
        foreach($general as $key=>$value){
          $print = PrintClasses::where('id',$value->print_class_id)->first();
          if( !empty($print)){
              $value->print_class=$print->name;
          }else{
              $value->print_class='';
          }
          
        }
         $recipes=Recipe::get();
        foreach ( $recipes as $key =>$value){
          $value->item_name = GeneralItems::where('id',$value->general_item_id)->value('name');
          $value->ingre= Ingredients::where('recipe_id',$value->recipe_id)->get();
        }
        // print_r($recipes);exit;
        
        $file = fopen("recipe.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'The Smith Hotels',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '0705625356'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Recipes'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
       
        
        foreach ( $recipes as $key =>$value){
            fputcsv($file, array(
            '',
            
            ));
            fputcsv($file, array(
            '',
            
            ));
            fputcsv($file, array(
            '',
            
            ));
            
              fputcsv($file, array(
            '',
            $value->name
            ));
            $i=1;
            
            foreach($value->ingre as $keyy=>$valuee){
                
                  fputcsv($file, array(
            '',
            '',
            '',
            $i,
            $valuee->name,
            $valuee->quantity,
            $valuee->quantity_type
            ));
              $i++;  
                
            }
        
            
           
          
        }
        
        fclose($file);
        
      
       
        return view ('admin_add_recipe',['ingredients'=>$items,'id'=>$uuid,'unit'=>$unit,'general'=>$general,'recipes'=>$recipes]);
        
    }
     public function SaveRecipe(Request $request){
        //  echo "<pre>";
        //  print_r($request->all());
        //  exit;
        $recipies=Recipe::where('general_item_id',$request->general_items)->get();
        
        
        $print_class_id = GeneralItems::where('id',$request->general_items)->value('print_class_id');
       
        
        if(sizeof($recipies) == 0){
           
            $data=array();
        $ingredient = $request->ingredient;
        $quantity_type = $request->quantity_type;
        $quantity = $request->quantity;
        $price = $request->price;
        $code = $request->code;
        $cost=0;
        //dd($ingredient);exit;
        
        for($j=0;$j<sizeof($price);$j++){
           $cost += $price[$j] * $quantity[$j];
        }
        $recipe=array(
            'recipe_id'=>$request->id,
            'name'=>$request->name,
            'general_item_id'=>$request->general_items,
            'cost'=>$cost,
            'ingredient_count'=>$request->count,
            );
        Recipe::create($recipe)->id;
        
      
        for($i=0 ;$i<$request->count ;$i++){
            $data['name']= $ingredient[$i];
            $data['quantity_type']= $quantity_type[$i];
            $data['quantity']= $quantity[$i];
            $data['price']= $price[$i];
            $data['ingredient_id']= $code[$i];
            $data['recipe_id']= $request->id;
            $data['general_item_id']=$request->general_items;
            $data['print_class_id']=$print_class_id;
            Ingredients::create($data);
        }
        
        
        
       return redirect()->to('/admin/ingredients')->with('message','Recipe Created successfully!');
        }else{
            
           return redirect()->to('/admin/ingredients')->with('message','Recipe  Already Created!'); 
        }
        

        
    }
    public function GetRecipes(){
        $recipes=Recipe::get();
        foreach ( $recipes as $key =>$value){
          $value->item_name = GeneralItems::where('id',$value->general_item_id)->value('name');
        }
       
        return view('admin_manage_recipe',['recipes'=>$recipes]);
        
        
    }
    public function ingredientsEdit($recipe_id){
        $recipe= Recipe::where('recipe_id',$recipe_id)->first();
        $recipe->ingredients =Ingredients::where('recipe_id',$recipe_id)->get();
        $uuid = Str::uuid()->toString();
        $general =GeneralItems::all();
        foreach($general as $key=>$value){
          $print = PrintClasses::where('id',$value->print_class_id)->first();
          if( !empty($print)){
              $value->print_class=$print->name;
          }else{
              $value->print_class='';
          }
        }
        $unit =Unit::all();
        $items = RawIngredient::all();
        // XeroPrivate::load('Accounting\\Item')->execute();
        // $items= array();
        // foreach($ingredients as $key =>$value){
            
        
        //      if( strpos( $value->Code, 'Ing-' ) !== false){
                
        //         array_push($items,$ingredients[$key]);
        //      }
            
        // }
        
        // dd($recipe->ingredients);exit;
        
        return view('admin_edit_recipe',['ingredients'=>$items,'unit'=>$unit,'recipes'=>$recipe,'id'=>$uuid,'general'=>$general]);
        
    }
    public function UpdateRecipe(Request $request){
        $data=array();
        //dd($request->all());exit;
        
        // dd($request->all());exit;
        
        $ingredient = $request->ingredient;
        $quantity_type = $request->quantity_type;
        $quantity = $request->quantity;
        $price = $request->price;
        $id = $request->id;
        $code = $request->code;
        $cost=0;
        
        
        //$delete = $request->delete;
        //if(sizeof($delete) >0){
        
        
            
            Ingredients::where('recipe_id',$request->recipe_id)->delete();
            
        
             
        //}
        
        //return $request->all();
        
        for($j=0;$j<sizeof($price);$j++){
            $cost += $price[$j] *$quantity[$j];
        }
        $recipe=array(
            'recipe_id'=>$request->recipe_id,
            'name'=>$request->name,
            'general_item_id'=>$request->general_items,
            'cost'=>$cost,
            'ingredient_count'=>$request->count,
            );
        Recipe::where('recipe_id',$request->recipe_id)->update($recipe);
    

        for($i=0 ;$i<sizeof($quantity_type) ;$i++){
            

           
           
                $data['name']= $ingredient[$i];
                $data['quantity_type']= $quantity_type[$i];
                $data['quantity']= $quantity[$i];
                $data['price']= $price[$i];
                $data['ingredient_id']= $code[$i];
                $data['recipe_id']= $request->recipe_id;
               $ing= Ingredients::create($data);  
            
                   
            
        }
        
        return redirect()->to('/admin/ingredients')->with('message','Recipe Updated successfully!');
    }
    public function RecipeDelete($id){
        $succ=Ingredients::where('recipe_id',$id)->delete();
        $eee    = Recipe::where('recipe_id',$id)->delete();
        if($succ && $eee){
              return redirect()->to('/admin/recepies')->with('message','Recipe Deleted successfully!');
        }else{
             return redirect()->to('/admin/recepies')->with('message','Recipe Deleted Failed!');
        }
        
    }
      public function rawIngredientsView(){
          $unit=Unit::all();
           $data = RawIngredient::all();
           
         $print = PrintClasses::all();
         return view ('admin_add_ingredient',['ingredient'=> $data,'unit'=>$unit,'print_class'=>$print]);
    
    }
    public function SaveIngredient(Request $request){
        $data= $request->all();
       


  try {
            $data = RawIngredient::create($data);
        } catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == '1062'){
                 return redirect()->to('admin/ingredients-view')->with('error','Name Already Exists!');
            }
        }
       
        
        $print_classes=PrintClasses::all();
        
        foreach($print_classes as $key => $value){
                        
                        $stock = array(
                            'ingredient_id' => $data->id,
                            'print_class' => $value->name,
                            'quantity_added' => 0,
                            'ingredient_name' => $data->name,
                            
                            );
                            
                            stock::create($stock);
                        
                        
                        
        }
        
       return redirect()->to('admin/ingredients-view')->with('message','Ingredient Save Successfully!');
        
        
    }
    public function manageIingredients(){
         
       
        $data = RawIngredient::all();
        
        
        return view('admin_manage_ingredient',['ingredient'=> $data]);
    }
    public function managestocks(){
         
       
        $data = stock::all();
        $ing = RawIngredient::all();
        
        
        return view('admin_manage_stock',['stocks'=> $data,'ing'=>$ing]);
    }
    public function physicalStockCentralStore(){
         
       
        
        $ing = RawIngredient::all();
        $physical= PhysicalStockCS::all();
        
        
        return view('admin_physical_stock_central',['ingredients'=>$ing,'physical'=>$physical]);
    }
    
    public function physicalStockPrintStore(){
         
       
        
        $ing = RawIngredient::all();
        $physical= PhysicalStockPS::all();
        $print_classes = PrintClasses::all();
        $stock= stock::all();
        
        
        return view('admin_physical_stock_print',['ingredients'=>$ing,'physical'=>$physical,'classes'=>$print_classes,'stock'=>$stock]);
    }
    public function managePhysicalStockCentralStore(){
         
       
        
        
        $physical= PhysicalStockCS::all();
        
         $start_date = '';
        $end_date = '';
        
        return view('physical_stock_cs',['physical'=>$physical, 'start_date'=>$start_date, 'end_date'=>$end_date]);
    }
    
    public function managePhysicalStockCentralStoreFilter(Request $request){
         
       
       $start_date = $request->start_date;
        
        $end_date = $request->end_date;
        
         $physical = DB::select("select * FROM physical_stock_central_store where updated_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
        
        
        // $physical= PhysicalStockCS::all();
        
        //  $start_date = '';
        // $end_date = '';
        
        return view('physical_stock_cs',['physical'=>$physical, 'start_date'=>$start_date, 'end_date'=>$end_date]);
    }
    public function managePhysicalStockPrintStore(){
         
         
         $physical=PhysicalStockPS::all();
         
         foreach($physical as $key =>$value1){
             
             $print=PrintClasses::find($value1->print_class_id);
             
             $value1->name=$print->name;
             
         }
         
           $start_date = '';
        $end_date = '';
       
        // $query = "Select * from physical_stock_central_store as p leftjoin print_classes as o on p.print_class_id=o.id";
        
        // $physical = DB::select($query);
      
        // $physical= PhysicalStockPS::all();
        
        
        
        return view('physical_stock_ps',['physical'=>$physical, 'start_date'=>$start_date, 'end_date'=>$end_date]);
    }
    
     public function managePhysicalStockPrintStoreFilter(Request $request){
         
         
           $start_date = $request->start_date;
        
        $end_date = $request->end_date;
        
         $physical = DB::select("select * FROM physical_stock_print_store where updated_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
         
         foreach($physical as $key =>$value1){
             
             $print=PrintClasses::find($value1->print_class_id);
             
             $value1->name=$print->name;
             
         }
         
         
       
        // $query = "Select * from physical_stock_central_store as p leftjoin print_classes as o on p.print_class_id=o.id";
        
        // $physical = DB::select($query);
      
        // $physical= PhysicalStockPS::all();
        
        
        
        return view('physical_stock_ps',['physical'=>$physical, 'start_date'=>$start_date, 'end_date'=>$end_date]);
    }
    public function editPhysicalStockCentralStore($id){
         
       
        
        
        $physical=PhysicalStockCS::where('id',$id)->first();
        
       
        
        
        return view('edit_physical_stock_cs',['physical'=>$physical]);
    }
    public function editPhysicalStockPrintStore($id){
         
       
        // $query = "SELECT `physical_stock_print_store`.*, `print_classes`.`name` FROM `physical_stock_print_store`, `print_classes` WHERE `print_classes`.`id` = `physical_stock_print_store`.`print_class_id` AND `physical_stock_print_store`.`id` = $id LIMIT 1";
        
        // $physical = DB::select($query);
        
       
        
         $physical=PhysicalStockPS::where('id',$id)->first();
         
        
             
             $print=PrintClasses::find($physical->print_class_id);
             
             $physical->name=$print->name;
             
         
        
        
        return view('edit_physical_stock_ps',['physical'=>$physical]);
    }
    public function savePhysicalStockCentralStore(Request $request){
         
      $data= $request->all();
   
        
        $plus=0;
        
        $ing= RawIngredient::where('id',$data['ingredient'])->first();
        $z=$ing->quantity;
        $data1= PhysicalStockCS::where('ingredient_id',$data['ingredient'])->first();
    
        
        
        
        if(sizeof($data1)>0){
            
            
            $plus=$data['quantity'];
            
        
            
        
         $stock = array(
            'ingredient_id' => $data['ingredient'],
           
            'quantity' => $plus,
            'ingredient_name' => $ing->name,
            'variance'=>$data['var'],
            'last_physical_quantity'=>$data['quantity']
            );
            
    
    
            
            $data1->update($stock);
            
            $ing->quantity=$data['quantity'];
            $ing->save();
            
        }else
        {
            $ing= RawIngredient::where('id',$data['ingredient'])->first();
            
           
            $stock = array(
            'ingredient_id' => $data['ingredient'],
            
            'quantity' => $data['quantity'],
            'ingredient_name' => $ing->name, 'variance'=>$data['var'],
            'last_physical_quantity'=>$data['quantity']
            
            
            );
            
            

        PhysicalStockCS::create($stock);
         $ing->quantity=$data['quantity'];
            $ing->save();
            
        }
        
        
          $adjustment = array(
            'ingredient_id' => $data['ingredient'],
            
            'system_quantity' => $data['quantity'],
            'ingredient_name' => $ing->name,
             'variance'=>$data['var']
            
            
            );
            
            
            $d=$z-$data['quantity'];
            
            
                $d=$d*-1;
            
        
        
        
        $adjustment=PhysicalStockCSAdjustment::create($adjustment);
        
          $ingredient = array(
            'name' => $ing->name,
            'quantity_type' => $ing->quantity_type,
            'current_quantity' => $z,
            'adjusted_quantity' => $d,
            'total_quantity' => $ing->quantity+$d,
            
            'ingredient_id'=>$ing->id
            
            );
            edit_ingredients::create($ingredient);
        
        
        
        
        
         return redirect()->to('/admin/physical-stock-central-store')->with('message','Added Successfully!');
        
        
       
    }
    public function savePhysicalStockPrintStore(Request $request){
         
      $data= $request->all();
      
    //   dd($data);
    //   exit;
      
        
        $plus=0;
        
        $ing= RawIngredient::where('id',$data['ingredient'])->first();
        
        $data1= PhysicalStockPS::where('ingredient_id',$data['ingredient'])->where('print_class_id',$data['print_class_id'])->first();
        
       
            
        
        $t= stock::where('ingredient_id',$data['ingredient'])->where('print_class',$data['print_class_name'])->first();
        $z=$t->quantity_added;
        
         $fifth_coll = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$data['ingredient']."' AND print_class_name='" .$data['print_class_name']."'  ");
        
        // echo $z;exit;
        // echo $fifth_coll[0]->quantity;exit;
        if($fifth_coll[0]->quantity < $z){
            
            
            
            $dd=$z-$fifth_coll[0]->quantity;
            //  $dd=$dd*-1;
       // }
       
            
            
                
        
            $ingredient1 = array(
            'name' => $ing->name,
            'print_class_name' => $data['print_class_name'],
            'current_quantity' => $z,
            'adjusted_quantity' =>$dd ,
            'ingredient_id'=>$ing->id,
            'transferred_from'=>'physical'
            
            );
            edit_ingredients_print_class::create($ingredient1);
            
            
            
            
        }elseif($fifth_coll[0]->quantity > $z){
            
            
              $dd=$z-$fifth_coll[0]->quantity;
              
            //   if($fifth_coll[0]->quantity>0){
                  
            //       $dd=$dd*-1;
                  
            //   }
             
       // }
       
            
            
                
        
            $ingredient1 = array(
            'name' => $ing->name,
            'print_class_name' => $data['print_class_name'],
            'current_quantity' => $z,
            'adjusted_quantity' =>$dd ,
            'ingredient_id'=>$ing->id,
            'transferred_from'=>'physical'
            
            );
            edit_ingredients_print_class::create($ingredient1);
            
            
            
            
        }
        
        $t->quantity_added=$data['totalquantity'];
        
        $t->save();
        
        if(sizeof($data1)>0){
            
            
            $plus=$data['totalquantity'];
            
        
            
        
         $stock = array(
            'ingredient_id' => $data['ingredient'],
           'print_class_id' => $data['print_class_id'],
            'quantity' => $plus,
            'ingredient_name' => $ing->name,
            'variance'=>$data['var'],
            'last_physical_quantity'=>$data['quantity']
            
            );
            
    
            
            $data1->update($stock);
            
            
        }else
        {
            $ing= RawIngredient::where('id',$data['ingredient'])->first();
            
           
            $stock = array(
            'ingredient_id' => $data['ingredient'],
           'print_class_id' => $data['print_class_id'],
            'quantity' => $data['totalquantity'],
            'ingredient_name' => $ing->name,
              'variance'=>$data['var'],
              'last_physical_quantity'=>$data['quantity']
            
            );
            
          
            

        PhysicalStockPS::create($stock);
        
          
            
        }
        
        
         $adjustment = array(
            'ingredient_id' => $data['ingredient'],
            
            'system_quantity' => $data['quantity'],
            'ingredient_name' => $ing->name,
            'print_class_name' =>$data['print_class_name'],
            'print_class_id' => $data['print_class_id'],
             'variance'=>$data['var'],
            
            
            );
            
        
        
        
        $adjustment=PhysicalStockPSAdjustment::create($adjustment);
        
        // if($z<0){
            
        //     $d=$data['quantity'];
            
        // }else{
             $d=$z-$data['quantity'];
             $d=$d*-1;
       // }
       
            
            
                
        
            $ingredient1 = array(
            'name' => $ing->name,
            'print_class_name' => $data['print_class_name'],
            'current_quantity' => $z,
            'adjusted_quantity' =>$d ,
            'ingredient_id'=>$ing->id,
            'transferred_from'=>'physical'
            
            );
            edit_ingredients_print_class::create($ingredient1);
        
        
        
        
        
        
        
         return redirect()->to('/admin/physical-stock-print-store')->with('message','Added Successfully!');
        
        
       
    }
    public function Editingredients($id){
        $unit=Unit::all();
         $data = RawIngredient::where('id',$id)->first();
         $print = PrintClasses::all();
         
         return view ('admin_edit_ingredient',['ingredient'=> $data,'unit'=>$unit,'print_class'=>$print]);
    }
    
    
    public function Editingredients2($id){
        $unit=Unit::all();
         $data = RawIngredient::where('id',$id)->first();
         $print = PrintClasses::all();
         
         return view ('admin_edit_ingredient2',['ingredient'=> $data,'unit'=>$unit,'print_class'=>$print]);
    }
    
        public function EditingredientsSave2(Request $request){
        
        $user = \Auth::user();
        
        $data= $request->all();
        unset($data['_token']);
        
        $data1=array( 
            'price' => $data['price'],
             'name' => $data['name'],
            're_order_level' => $data['re_order_level'],
            'quantity_type'=>$data['quantity_type'],
            
            );
        
            
            
           
        $data1 = RawIngredient::where('id',$data['id'])->update($data1);
         return redirect()->to('admin/ingredients-view')->with('message','Ingredient Save Successfully!');
        
    }
    
    public function Editstock($id){
       
         $data = stock::where('id',$id)->first();
         $print = PrintClasses::all();
        
         
         return view ('admin_edit_stock',['stock'=> $data,'print_class'=>$print]);
    }
    
    
    public function EditingredientsSave(Request $request){
        
        $user = \Auth::user();
        
        $data= $request->all();
        unset($data['_token']);
        $purchase = array(
            'ingredient_id' => $data['id'],
            'price' => $data['price'],
            'quantity' => $data['adjustedquantity'],
            
            );
        Purchases::create($purchase);
        $Purchase_price =$data['price']*$data['adjustedquantity'];
        $Purchase_quantity =$data['adjustedquantity'];
        $all_stock = RawIngredient::where('id',$data['id'])->first();
        if($all_stock->quantity > 0){
            $ttt = $all_stock->quantity;
        }else{
            $ttt = $all_stock->quantity * -1;
        }
        $stock_price= $all_stock->price * $ttt;
        $stock_quantity= $ttt;
       
        $total_quantity =$stock_quantity+$Purchase_quantity;
        $data1=array( 
            'name' => $data['name'],

            
            'quantity' => $total_quantity,
            're_order_level' => $data['re_order_level'],
            'quantity_type'=>$data['quantity_type'],
            
            
            
            );
        $ingredient = array(
            'name' => $data['name'],
            'quantity_type' => $data['quantity_type'],
            'current_quantity' => $data['quantity'],
            'adjusted_quantity' => $data['adjustedquantity'],
            'total_quantity' => $data['totalquantity'],
            'user' => $user->name,
            'remarks'=>$data['remarks'],
            'ingredient_id'=>$data['id']
            
            );
            edit_ingredients::create($ingredient);
            
            
           
        $data1 = RawIngredient::where('id',$data['id'])->update($data1);
         return redirect()->to('admin/manage-ingredients')->with('message','Ingredient Save Successfully!');
        
    }
    
        public function EditstockSave(Request $request){
        
        
        
        $data= $request->all();
        $plus=0;
        
        
         
            $ing= RawIngredient::where('id',$data['ingredient_id'])->first();
            $y=$ing->quantity;
            $ing->quantity=$ing->quantity-$data['adjustedquantity'];
            
            $save=array(
                'name'=>$ing->name,
                'price'=>$ing->price,
                'quantity_type'=>$ing->quantity_type,
                'quantity'=>$ing->quantity,
                're_order_level'=>$ing->re_order_level,
    );
    
    $ing->update($save);
    
    
       $ingredient = array(
            'name' => $ing->name,
            'quantity_type' => $ing->quantity_type,
            'current_quantity' => $y,
            'adjusted_quantity' => (-1*$data['adjustedquantity']),
            'total_quantity' => $ing->quantity,
            'ingredient_id'=>$ing->id
            
            );
            edit_ingredients::create($ingredient);
    
    
    
    
    $stk= stock::where('id',$data['id'])->first();
    $plus=$stk->quantity_added+$data['adjustedquantity'];
    
    
    $ingredient1 = array(
            'name' => $ing->name,
            'print_class_name' => $data1->print_class,
            'current_quantity' => $stk->quantity_added,
            'adjusted_quantity' =>$data['adjustedquantity'] ,
            'ingredient_id'=>$ing->id
             ,
            
            );
            edit_ingredients_print_class::create($ingredient1);
    
    
    $stock = array(
            'ingredient_id' => $data['ingredient_id'],
            'print_class' => $data['print_class'],
            'quantity_added' => $plus,
            
            );
            
            $stk->update($stock);
            
            
    
    
    
         return redirect()->to('admin/manage-stocks')->with('message','Stock Edited Successfully!');
        
    }
    
        public function saveEditedPhysicalStockCentralStore(Request $request){
        
        
        
        $data= $request->all();
        
        $plus=0;
        
         
        
        
    $stk= PhysicalStockCS::where('ingredient_id',$data['ingredient_id'])->first();
   
   $var = $stk->variance + $data['adjustedquantity'];
    
    
    
    $stock = array(
            'ingredient_id' => $data['ingredient_id'],
            
            'quantity' =>$data['totalquantity'],
             'variance' => $var
            
            );
            
            $stk->update($stock);
            
            
               $ing= RawIngredient::where('id',$data['ingredient_id'])->first();
               $z=$ing->quantity;
            $ing->quantity=$data['totalquantity'];
            $ing->save();
            
              
         $adjustment = array(
            'ingredient_id' => $data['ingredient_id'],
            
            'system_quantity' => $data['totalquantity'],
            'ingredient_name' => $ing->name,
             'variance'=>$data['adjustedquantity'],
             'type'=>'edit'
           
            
            
            );
            
            
        
        
        
        $adjustment=PhysicalStockCSAdjustment::create($adjustment);
        
          $d=$z-$data['totalquantity'];
            
            
                $d=$d*-1;
        
        $ingredient = array(
            'name' => $ing->name,
            'quantity_type' => $ing->quantity_type,
            'current_quantity' => $z,
            'adjusted_quantity' => $d,
            'total_quantity' => $ing->quantity+$d,
            
            'ingredient_id'=>$ing->id
            
            );
            edit_ingredients::create($ingredient);
             
            
    
    
    
         return redirect()->to('/admin/manage-physical-stock-central-store')->with('message','Stock Edited Successfully!');
    
    }
    public function saveEditedPhysicalStockPrintStore(Request $request){
        
        
        
        $data= $request->all();
    
        
        $plus=0;
        
        
        
    $stk= PhysicalStockPS::where('ingredient_id',$data['ingredient_id'])->where('print_class_id',$data['print_class_id'])->first();
   
   $var = $stk->variance + $data['adjustedquantity'];
    
    
    
    $stock = array(
            'ingredient_id' => $data['ingredient_id'],
            'print_class_id' => $data['print_class_id'],
            'quantity' =>$data['totalquantity'],
            'variance' => $var
            
            );
            
            $stk->update($stock);
            
            
            $t= stock::where('ingredient_id',$data['ingredient_id'])->where('print_class',$data['print_class_name'])->first();
            $z=$t->quantity_added;
        
        $t->quantity_added=$data['totalquantity'];
        
        $t->save();
        
        
          $adjustment = array(
            'ingredient_id' => $data['ingredient_id'],
            
            'system_quantity' => $data['totalquantity'],
            'ingredient_name' => $t->ingredient_name,
            'print_class_name' =>$data['print_class_name'],
            'print_class_id' => $data['print_class_id'],
            'variance'=>$data['adjustedquantity'],
            'type'=>'edit'
            
            
            );
            
        
        
        
        $adjustment=PhysicalStockPSAdjustment::create($adjustment);
        
       
        
            $ingredient1 = array(
            'name' => $t->ingredient_name,
            'print_class_name' => $data['print_class_name'],
            'current_quantity' => $z,
            'adjusted_quantity' =>$data['adjustedquantity'] ,
            'ingredient_id'=>$t->ingredient_id,
            'transferred_from'=>'edit_physical'
            
            );
            edit_ingredients_print_class::create($ingredient1);
            
    
    
    
         return redirect()->to('/admin/manage-physical-stock-print-store')->with('message','Stock Edited Successfully!');
        
    }
     public function adjustmentlogs()
    {

        $data = edit_Ingredients::all();
        return view('admin_adjustment_logs',['ingredient'=> $data]);


    }
    public function IngredientDelete($id){
        
        $da=RawIngredient::find($id);
        
          $del= edit_Ingredients::where('ingredient_id',$id)->delete();
          $del= edit_Ingredients_print_class::where('ingredient_id',$id)->delete();
        
        $delete = RawIngredient::where('id',$id)->delete();
        $delete = stock::where('ingredient_id',$id)->delete();
        
       
        
        if($delete){
            return redirect()->to('/admin/manage-ingredients')->with('message','Ingredient Deleted Successfully!');
        }else{
            return redirect()->to('/admin/manage-ingredients')->with('message','Ingredient Deleted Failed!');
        }
    }
    
      public function stockDelete($id){
        $delete = stock::where('id',$id)->delete();
        
        if($delete){
            return redirect()->to('/admin/manage-stocks')->with('message','Stock Deleted Successfully!');
        }else{
            return redirect()->to('/admin/manage-stocks')->with('message','Stock Deleted Failed!');
        }
    }
    public function stockDeletePrintStore($id){
        $delete = PhysicalStockPS::where('id',$id)->delete();
        
        if($delete){
            return redirect()->to('/admin/manage-physical-stock-print-store')->with('message','Stock Deleted Successfully!');
        }else{
            return redirect()->to('/admin/manage-physical-stock-print-store')->with('message','Stock Deleted Failed!');
        }
    }
    
    public function Units(){
        $unit= Unit::all();
        return view('unit',['unit'=>$unit]);
    }
    public function UnitsSave(Request $request){
        
        
        $this->validate($request, [
            'name' => 'required|max:255|unique:units',
        ]);
        
        
        $unit= Unit::create($request->all());
         return redirect()->to('/admin/units')->with('message','Units Created Successfully!');
        
    }
    
    public function UnitsEdit($id){
        $unit= Unit::find($id);
        return view('editunit',['unit'=>$unit]);
    }
    
    public function UnitsEditSave(Request $request,$id){
        
        
        $this->validate($request, [
            'name' => 'required|max:255|unique:units',
        ]);
         $unit=Unit::find($id);
         $unit->name =$request->name;
         $unit->save();
        
         return redirect()->to('/admin/units')->with('message','Units Updated Successfully!');
        
    }
    
    public function UnitsDelete($id){
        $unit= Unit::find($id);
         $unit->delete();
        return redirect()->to('/admin/units')->with('message','Units Deleted Successfully!');
    }
    public function adjustmentSave(Request $request, $id){
        $user = \Auth::user();
        $ingredient = RawIngredient::where('id',$id)->first();
        $ingredient->quantity = $ingredient->quantity - $request->quantity;
         $t=$request->quantity * -1;
         $total =$ingredient->quantity - $request->quantity;
          $ingredient1 = array(
            'name' => $ingredient->name,
            'quantity_type' => $ingredient->quantity_type,
            'current_quantity' => $ingredient->quantity,
            'adjusted_quantity' =>$t ,
            'total_quantity' => $total,
            'user' => $user->name,
            'ingredient_id' => $ingredient->id
            
            );
            edit_ingredients::create($ingredient1);
         $ingredient->save();
         
        return redirect()->to('admin/ingredients-view')->with('message','Ingredient Adjusment Successfully!');
        
    }
    public function stockadjustmentSave(Request $request, $id){
        $data= $request->all();
        
        $plus=0;
        
        $data1= stock::where('id',$id)->first();
        
        $b=$data1->quantity_added;
        $data1->quantity_added=$data1->quantity_added-$data['quantity'];
        $data1->reduced_quantity=$data1->reduced_quantity+$data['quantity'];
        
        $ing= RawIngredient::where('id',$data1->ingredient_id)->first();
        
        
        
        
        
            
            $ing->quantity=$ing->quantity+$data['quantity'];
            
            $save2=array(
                'quantity_added'=>$data1->quantity_added,
                );
            $save3=array(
                'quantity'=>$ing->quantity,
                );

            
            
            
            $ing->update($save3);
            
            $data1->update($save2);
            
            $ingredient1 = array(
            'name' => $ing->name,
            'print_class_name' => $data1->print_class,
            'current_quantity' => $b,
            'adjusted_quantity' =>$data['quantity'] ,
            'ingredient_id'=>$ing->id
             ,
            
            );
            edit_ingredients_print_class::create($ingredient1);
         
        return redirect()->to('admin/manage-stocks')->with('message','Stock Adjusment Successfully!');
        
    }
    public function stocktransferSave(Request $request, $id){
        $data= $request->all();
        
        $plus=0;
        
        $data2=stock::where('print_class',$data['print_class'])->where('ingredient_id',$data['ingredient_id'])->first();
        
       
       $ing1= RawIngredient::where('id',$data['ingredient_id'])->first();
         
        
        if(sizeof($data2)>0){
            
            
            $data1= stock::where('id',$id)->first();
             $ing2= RawIngredient::where('id',$data1->ingredient_id)->first();
        
        $b=$data1->quantity_added;
        $data1->quantity_added=$data1->quantity_added-$data['quantity'];
        
        
        
        
        
        
            $c=$data2->quantity_added;
            $data2->quantity_added=$data2->quantity_added+$data['quantity'];
            
            $save2=array(
                'quantity_added'=>$data1->quantity_added,
                );
            $save3=array(
                'quantity_added'=>$data2->quantity_added,
                );

            
            
            
            $data2->update($save3);
            
            $data1->update($save2);
            
            $ingredient1 = array(
            'name' => $ing2->name,
            'print_class_name' => $data1->print_class,
            'current_quantity' => $b,
            'adjusted_quantity' =>(-1*$data['quantity']) ,
                        'ingredient_id'=>$ing2->id
            
            );
            edit_ingredients_print_class::create($ingredient1);
            
            
            if($c==0){
                
                $ingredient2 = array(
            'name' => $ing1->name,
            'print_class_name' => $data2->print_class,
            'current_quantity' => 0,
            'adjusted_quantity' =>$data['quantity'] ,
                        'ingredient_id'=>$ing1->id
            
            );
            edit_ingredients_print_class::create($ingredient2);
                
                
                
            }else{
                
                $ingredient2 = array(
            'name' => $ing1->name,
            'print_class_name' => $data2->print_class,
            'current_quantity' => $c,
            'adjusted_quantity' =>$data['quantity'] ,
                        'ingredient_id'=>$ing1->id
            
            );
            edit_ingredients_print_class::create($ingredient2);
                
                
            }
            
            
            
            
            
        }else{
            
            $data1= stock::where('id',$id)->first();
         $ing2= RawIngredient::where('id',$data1->ingredient_id)->first();
         
         
         $b=$data1->quantity_added;
        $data1->quantity_added=$data1->quantity_added-$data['quantity'];
        
        $save2=array(
                'quantity_added'=>$data1->quantity_added,
                );
            $save3=array(
                'quantity_added'=>$data['quantity'],
                'print_class'=>$data['print_class'],
                'ingredient_id'=>$data['ingredient_id'],
                'ingredient_name'=>$data['name'],
                );
            
            
            $data1->update($save2);
            stock::create($save3);
            
            
            $ingredient1 = array(
            'name' => $ing2->name,
            'print_class_name' => $data1->print_class,
            'current_quantity' => $data1->quantity_added,
            'adjusted_quantity' =>(-1*$data['quantity']) ,
                        'ingredient_id'=>$ing2->id
            
            );
            edit_ingredients_print_class::create($ingredient1);
            
            $ingredient2 = array(
            'name' => $ing2->name,
            'print_class_name' => $data['print_class'],
            'current_quantity' => $data2->quantity_added,
            'adjusted_quantity' =>$data['quantity'] ,
                        'ingredient_id'=>$ing2->id
            
            );
            edit_ingredients_print_class::create($ingredient2);
            
        }
        
        
        // dd($data);exit;
         
        return redirect()->to('admin/manage-stocks')->with('message','Stock Transfered Successfully!');
        
    }
    
    
    public function stocktransfercentralstoreSave(Request $request, $id){
        $data= $request->all();
        
        $plus=0;
        
        // $data2=stock::where('print_class',$data['print_class'])->where('ingredient_id',$data['ingredient_id'])->first();
        
       
       $ing1= RawIngredient::where('id',$data['ingredient_id'])->first();
         
        
       
            
            
            $data1= stock::where('id',$id)->first();
            
        
        $b=$data1->quantity_added;
           $c=$ing1->quantity;
        $data1->quantity_added=$data1->quantity_added-$data['quantity'];
        
        $ing1->quantity=$ing1->quantity+$data['quantity'];
        
        
        
        
        
           
            
            $save2=array(
                'quantity_added'=>$data1->quantity_added,
                );
            $save3=array(
                'quantity'=>$ing1->quantity,
                );

            
            
            
            $ing1->update($save3);
            
            $data1->update($save2);
            
            $ingredient1 = array(
            'name' => $ing1->name,
            'print_class_name' => $data1->print_class,
            'current_quantity' => $b,
            'adjusted_quantity' =>(-1*$data['quantity']) ,
                        'ingredient_id'=>$ing1->id,
                        'transferred_from'=>'central2'
            
            );
            edit_ingredients_print_class::create($ingredient1);
            
            
                
                 $ingredient2 = array(
            'name' => $ing1->name,
            'quantity_type' => $ing1->quantity_type,
            'current_quantity' => $c,
            'adjusted_quantity' =>$data['quantity'],
            'total_quantity' =>  $ing1->quantity,
            'ingredient_id'=>$ing1->id
            
            
            );
            edit_ingredients::create($ingredient2);
                
                
            
            
            
            
            
            
        
        
        
        
         
        return redirect()->to('admin/manage-stocks')->with('message','Stock Transfered Successfully!');
        
    }
    
    public function adjustment($id){
         $i = RawIngredient::where('id',$id)->first();
        return view('ingredient_adjustment',['i'=>$i]);
        
    }
    public function stockadjustment($id){
         $i = stock::where('id',$id)->first();
        return view('stock_adjustment',['i'=>$i]);
        
    }
    public function stocktransfer($id){
         $i = stock::where('id',$id)->first();
         $classs= PrintClasses::all();
         $stock= stock::all();
        return view('stock_transfer',['i'=>$i,'classs'=>$classs,'stock'=>$stock]);
        
    }
    
      public function stocktransfercentralstore($id){
         $i = stock::where('id',$id)->first();
         $classs= PrintClasses::all();
         $stock= stock::all();
        return view('stock_transfer_central_store',['i'=>$i,'classs'=>$classs,'stock'=>$stock]);
        
    }
    
    public function stock(){
        $unit=Unit::all();
        $print_classes = PrintClasses::all();
        $ingredients = RawIngredient::all();
        $stock = stock::all();
        return view('stock_update',['ingredients'=>$ingredients,'unit'=>$unit,'print_classes'=>$print_classes,'stock'=>$stock]);
        
    }
    
    
    public function stockAdd(Request $request){
        $data= $request->all();
        
        
        $plus=0;
        
        $ing= RawIngredient::where('id',$data['id'])->first();
        
        $data1= stock::where('ingredient_id',$data['id'])->where('print_class',$data['print_class'])->first();
        
        
        if(sizeof($data1)>0){
            
            
            $z=$data1->quantity_added;
            
            $plus=$data1->quantity_added+$data['quantity'];
            
            if($data['quantity']>$ing->quantity){
                
                return redirect()->to('admin/stock')->with('message','Quantity mismatch!');
                
            }
            
        
         $stock = array(
            'ingredient_id' => $data['id'],
            'print_class' => $data['print_class'],
            'quantity_added' => $plus,
            'ingredient_name' => $ing->name,
            
            );
            $k=$ing->quantity;
            
            $ing->quantity=$ing->quantity-$data['quantity'];
            
            
            
            $save=array(
                'name'=>$ing->name,
                'price'=>$ing->price,
                'quantity_type'=>$ing->quantity_type,
                'quantity'=>$ing->quantity,
                're_order_level'=>$ing->re_order_level,
    );
            
            
            $ing->update($save);
            
            $data1->update($stock);
            
             $ingredient1 = array(
            'name' => $ing->name,
            'quantity_type' => $ing->quantity_type,
            'current_quantity' => $k,
            'adjusted_quantity' =>(-1*$data['quantity']) ,
            'total_quantity' => $ing->quantity,
            'ingredient_id'=>$ing->id
            
            
            );
            edit_ingredients::create($ingredient1);
            
            
           
            if($z==0){
                
                 $ingredient2 = array(
            'name' => $ing->name,
            'print_class_name' => $data['print_class'],
            'current_quantity' => 0,
            'adjusted_quantity' =>$data['quantity'] ,
                        'ingredient_id'=>$ing->id,
                        'transferred_from'=>'central'
            
            );
            edit_ingredients_print_class::create($ingredient2);
                
            }else{
                
                
                 $ingredient2 = array(
            'name' => $ing->name,
            'print_class_name' => $data['print_class'],
            'current_quantity' => $z,
            'adjusted_quantity' =>$data['quantity'] ,
                        'ingredient_id'=>$ing->id,
                        'transferred_from'=>'central'
            
            );
            
            edit_ingredients_print_class::create($ingredient2);
                
            }
            
            
           
            
        }else
        {
            $ing= RawIngredient::where('id',$data['id'])->first();
            
            if($data['totalquantity']>$ing->quantity){
                
                return redirect()->to('admin/stock')->with('message','Quantity mismatch!');
            }
            $stock = array(
            'ingredient_id' => $data['id'],
            'print_class' => $data['print_class'],
            'quantity_added' => $data['totalquantity'],
            'ingredient_name' => $ing->name,
            
            
            );
            
            
            $c=$ing->quantity;
            $ing->quantity=$ing->quantity-$data['quantity'];
            
            $save=array(
                'name'=>$ing->name,
                'price'=>$ing->price,
                'quantity_type'=>$ing->quantity_type,
                'quantity'=>$ing->quantity,
                're_order_level'=>$ing->re_order_level,
    );
    
    $ing->update($save);
        
        stock::create($stock);
        
          $ingredient1 = array(
            'name' => $ing->name,
            'quantity_type' => $ing->quantity_type,
            'current_quantity' => $c,
            'adjusted_quantity' =>(-1*$data['quantity']) ,
            'total_quantity' => $ing->quantity,
            'ingredient_id'=>$ing->id
            
            
            );
            edit_ingredients::create($ingredient1);
            
            
            $ingredient2 = array(
            'name' => $ing->name,
            'print_class_name' => $data['print_class'],
            'current_quantity' => 0,
            'adjusted_quantity' =>$data['quantity'] ,
                        'ingredient_id'=>$ing->id,
                        'transferred_from'=>'central'
            
            );
            edit_ingredients_print_class::create($ingredient2);
            
        }
        
        
        
        
        
        
        
        
         return redirect()->to('admin/stock')->with('message','Stock Added Successfully!');
        
        
    }
    public function appversionview(){
        
        $versions =AppVersion::where('id',1)->first();
        $version = $versions->version ;
        
        return view('app_version',['version'=>$version]);
        
    }
    public function appversion(Request $request){
        
        $versions =AppVersion::where('id',1)->first();
        $versions->version =$request->version;
        $versions->save();
        $versions =AppVersion::where('id',1)->first();
        $version = $versions->version ;
        return view('app_version',['version'=>$version]);
        
    }

    public function xeroInstance(){


        $xero=xero::find(1);
        $xeroTenantId = $xero->tenant_id;
      
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
          'clientId'                => '7926ACDEA0224C1585A35CD1DCB79BF9',   
          'clientSecret'            => '2wLASnD9-NfmiQBIQ_YGZikLexT-SN-qcgGXKtDPNemzUXFS',
          'redirectUri'             => 'https://smith.test/callback', 
          'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
          'urlAccessToken'          => 'https://identity.xero.com/connect/token',
          'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);
      
              // echo "xero tendent id"."<br>";
              // echo $xeroTenantId."<br>";
              $refresh_token = $xero->refresh_token;
             
              
          $newAccessToken = $provider->getAccessToken('refresh_token', [
            'refresh_token' => $refresh_token,
          ]);
          
        $config = \XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( $newAccessToken );
         $apiInstance = new \XeroAPI\XeroPHP\Api\AccountingApi(
            new \GuzzleHttp\Client(),
            $config
        );

        return array('xero_tenant_id'=>$xeroTenantId,'api_instance'=>$apiInstance);

    }
    
    
    
}
