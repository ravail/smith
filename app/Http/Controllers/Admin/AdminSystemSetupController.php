<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PLUNumber;
use App\PrintClassUser;
use App\SystemSetup\Advertisements;
use App\SystemSetup\CondimentGroup;
use App\SystemSetup\Condiments;
use App\SystemSetup\PaymentMethod;
use App\SystemSetup\PrintClasses;
use App\SystemSetup\ReservationEmail;
use App\SystemSetup\Restaurants;
use App\SystemSetup\Roles;
use App\SystemSetup\Tables;
use App\Tax;
use App\User;
use App\Permissions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use DB;
use XeroAPI\XeroPHP\ApiException;
use XeroAPI\XeroPHP\AccountingObjectSerializer;
use XeroAPI\XeroPHP\PayrollAuObjectSerializer;
use App\xero;
use DateTime;

class AdminSystemSetupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function index()
    {

        $advertisements = Advertisements::all();

        return view('admin.systemsetup.admin_advertisements', compact('advertisements'));
    }
    public function addAdvertisements()
    {
        return view('admin.systemsetup.admin_add_advertisements');

    }

    public function createAdvertisements(Request $request)
    {

        $data = $request->all();

        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {

            $data['a_image'] = $imageName;

        }

        $add = Advertisements::create($data);
        Session::flash('message', 'We have sent you a verification email!');
        return redirect()->to('/admin/advertisements')->with('message', 'Advertisement Created!');

    }

    public function deleteAdvertisements($id)
    {

        $delete = Advertisements::where('id', $id)->delete();
        if ($delete) {
            Session::flash('message', 'We have sent you a verification email!');

            return back()->with('message', 'Advertisement Deleted Successfully!');

        }
    }

    public function editAdvertisements($id)
    {

        $existAdvertisement = Advertisements::where('id', $id)->first();
//return $existAdvertisement;
        return view('admin.systemsetup.admin_edit_advertisements', compact('existAdvertisement'));

    }
    public function updateAdvertisement(Request $request, $id)
    {

        $data = $request->all();

        $existAdvertisement = Advertisements::where('id', $id)->first();

        if (!empty($existAdvertisement)) {

            if ($data['a_image'] != "") {

                $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
                $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
                if ($image_uploaded) {

                    $data['a_image'] = $imageName;

                }
            }

            $profile = $existAdvertisement->update($data);

            $profile = existAdvertisement::where('id', $id)->get();
            //return $profile;
            Session::flash('message', 'We have sent you a verification email!');
            return redirect()->to('/admin/advertisements')->with('message', 'Advertisement Updated!');

        }
    }

    //////////////////////////// Table Manager ///////////////////////////////////////

    public function tables()
    {

        $tables = Tables::all();
        $restaurants = Restaurants::all();
        $printclasses = PrintClasses::all();
        return view('admin.systemsetup.admin_table_manager', compact('tables', 'restaurants','printclasses'));

    }

    public function addTable()
    {

        $restaurants = Restaurants::all();
        $printclasses = PrintClasses::all();
        return view('admin.systemsetup.admin_add_table', compact('restaurants','printclasses'));

    }
    public function EditTable($id)
    {

        $restaurants = Restaurants::all();
        $printclasses = PrintClasses::all();
        $table = Tables::where('id',$id)->first();
        return view('admin.systemsetup.admin_edit_table', compact('restaurants','printclasses','table'));

    }
    public function EditSaveTable(Request $request)
    {
        $data =  $request->all();
        $add = Tables::update($data);
        Session::flash('message', 'Table Created Successfully!');

        return redirect()->to('/admin/table-manager')->with('message', 'Table Updaed Successfully!');
    }
    public function TableChangeStatus($id,$status){
        $table = Tables::where('id', $id)->first();
        if($status == 'active'){
            $table->is_active = 'active';
        }else{
            $table->is_active = 'inactive';
        }
        $table->save();
        return 'true';
        
    }

    public function createTable(Request $request)
    {

        $data = $request->all();

        $add = Tables::create($data);

        Session::flash('message', 'Table Created Successfully!');

        return redirect()->to('/admin/table-manager')->with('message', 'Table Created Successfully!');

    }

    public function deleteTable($id)
    {

        $delete = Tables::where('id', $id)->delete();
        if ($delete) {

            Session::flash('message', 'Table Deleted Successfully!');

            return back()->with('message', 'Deleted Successfully');

        }
    }

    //////////////////////////// Reservation Email //////////////////////////////////

    public function reservationEmail()
    {
        $mails = ReservationEmail::all();

        return view('admin.systemsetup.admin_reservation_email', compact('mails'));
    }

    public function addReservationEmail()
    {

        return view('admin.systemsetup.admin_add_reserve_email');
    }

    public function createReservationEmail(Request $request)
    {

        $data = $request->all();

        $add = ReservationEmail::create($data);

        return back()->with('success_message', 'Advertisement created Successfully!');

    }

    public function deleteReservationEmail($id)
    {

        $delete = ReservationEmail::where('id', $id)->delete();
        if ($delete) {
            return back()->with('success_message', 'Deleted Successfully');

        }
    }

    /////////////////////////////////////// Payment Methods ////////////////////////////////////

    public function paymentMethods()
    {

        $paymentMethods = PaymentMethod::all();

        return view('admin.systemsetup.admin_payment_methods ', compact('paymentMethods'));
    }

    public function addPaymentMethods()
    {

        return view('admin.systemsetup.admin_add_payment_method');
    }

    public function createPaymentMethods(Request $request)
    {

        $data = $request->all();

        $add = PaymentMethod::create($data);
        Session::flash('message', 'Payment Method Created Successfully!');
        return redirect()->to('/admin/payment-methods')->with('message', 'Payment Method created Successfully!');

    }

    public function deletePaymentMethods($id)
    {

        $delete = PaymentMethod::where('id', $id)->delete();
        if ($delete) {
            Session::flash('message', 'Payment Method Deleted Successfully!');
            return back()->with('message', 'Payment Method Deleted Successfully');
        }
    }

    public function editPaymentMethods($id)
    {

        $existsPaymentMethod = PaymentMethod::where('id', $id)->first();

        return view('admin.systemsetup.admin-edit-payment-methods', compact('existsPaymentMethod'));

    }

    public function updatePaymentMethods(Request $request, $id)
    {

        $data = $request->all();

        unset($data['_token']);
        $existsPaymentMethod = PaymentMethod::where('id', $id)->first();
        //return $existsPaymentMethod;
        if (!empty($existsPaymentMethod)) {

            $profile = PaymentMethod::where('id', $id)->update($data);

            Session::flash('message', 'Payment Method Updated Successfully!');

            return redirect()->to('/admin/payment-methods')->with('message', 'Payment Method Updated Successfully!');

        }
    }

    //////////////////////////////////////////// Restaurants ///////////////////////////////////////

    public function restaurants()
    {

        $restaurants = Restaurants::all();

        return view('admin.systemsetup.admin_restaurants', compact('restaurants'));
    }

    public function addRestaurants()
    {

        return view('admin.systemsetup.admin_add_restaurant');
    }

    public function createRestaurants(Request $request)
    {

        $data = $request->all();

        $imageName = time() . '.' . $request->a_image->getClientOriginalExtension();
        $image_uploaded = $request->a_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {

            $data['a_image'] = $imageName;

        }

        $imageName = time() . '.' . $request->f_image->getClientOriginalExtension();
        $image_uploaded = $request->f_image->move(public_path('img'), $imageName);
        if ($image_uploaded) {

            $data['f_image'] = $imageName;

        }

        $add = Restaurants::create($data);

        return redirect()->to('/admin/restaurants')->with('message', 'Restaurant created Successfully!');

    }
    public function deleteRestaurants($id)
    {

        $delete = Restaurants::where('id', $id)->delete();
        if ($delete) {

            Session::flash('message', 'Restaurant Deleted Successfully!');
            return back()->with('message', 'Restaurant Deleted Successfully');
        }
    }
    /////////////////////////////////// Print Classes ////////////////////////////////////////////////

    public function printClasses()
    {

        $printClasses = PrintClasses::all();

        return view('admin.systemsetup.admin_print_class', compact('printClasses'));

    }

    public function addPrintClasses()
    {

        return view('admin.systemsetup.admin_add_print_class');
    }

    public function createPrintClasses(Request $request)
    {

        $data = $request->all();

        $add = PrintClasses::create($data);

        return redirect()->to('/admin/print-classes')->with('message', 'Print Class Created!');

    }
    public function deletePrintClasses($id)
    {

        $delete = PrintClasses::where('id', $id)->delete();
        if ($delete) {
            Session::flash('message', 'We have sent you a verification email!');

            return back()->with('message', 'Print Class Deleted Successfully!');
        }
    }

    public function editPrintClasses($id)
    {

        $existPrintClass = PrintClasses::where('id', $id)->first();
//return $existAdvertisement;
        return view('admin.systemsetup.admin_edit_print_class', compact('existPrintClass'));

    }
    public function updatePrintClasses(Request $request, $id)
    {

        $data = $request->all();

        $existAdvertisement = PrintClasses::where('id', $id)->first();

        if (!empty($existAdvertisement)) {

            $profile = $existAdvertisement->update($data);

            Session::flash('message', 'We have sent you a verification email!');

            return redirect()->to('/admin/print-classes')->with('message', 'Print Class Updated!');

        }
    }
    /////////////////////////////////// Print Class Users /////////////////////////////////

    protected function validator(array $data)
    { //return $data;
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'restaurant' => 'required|string|max:255',
            'print_class' => 'required|string|max:255',

            'email' => 'required|string|email|max:255|unique:print_class_users',
            'password' => 'required|min:6',
        ]);
    }

    protected function create(array $data)
    {
        // return $data;
        return PrintClassUser::create([
            'name' => $data['name'],
            'restaurant' => $data['restaurant'],

            'email' => $data['email'],
            'print_class' => $data['print_class'],
            'password' => bcrypt($data['password']),
            'user_type' => 'print_class_user',

        ]);
    }

    public function printClassUsers()
    {

        $printClassUsers = PrintClassUser::all();
        $printClasses = PrintClasses::all();
        $restaurants = Restaurants::all();

        return view('admin.systemsetup.admin_print_class_users', compact('printClassUsers', 'printClasses', 'restaurants'));

    }

    public function addprintClassUsers()
    {

        $printClasses = PrintClasses::all();
        $restaurants = Restaurants::all();

        return view('admin.systemsetup.admin_add_print_class_user', compact('printClasses', 'restaurants'));
    }

    public function createprintClassUsers(Request $request)
    {
        //return $request;
        $data = $request->all();

        $this->validator($request->all());
        //return $request;
        $user = $this->create($data);

        Session::flash('message', 'We have sent you a verification email!');

        return redirect()->to('/admin/print-class-user')->with('message', 'Print Class User Created!');

    }
    public function deleteprintClassUsers($id)
    {

        $delete = PrintClasses::where('id', $id)->delete();
        if ($delete) {
            Session::flash('message', 'We have sent you a verification email!');

            return back()->with('message', 'Print Class Deleted Successfully!');
        }
    }

    public function editprintClassUsers($id)
    {

        $existPrintClass = PrintClasses::where('id', $id)->first();
//return $existAdvertisement;
        return view('admin.systemsetup.admin_edit_print_class', compact('existPrintClass'));

    }
    public function updateprintClassUsers(Request $request, $id)
    {

        $data = $request->all();

        $existAdvertisement = PrintClasses::where('id', $id)->first();

        if (!empty($existAdvertisement)) {

            $profile = $existAdvertisement->update($data);

            Session::flash('message', 'We have sent you a verification email!');

            return redirect()->to('/admin/print-classes')->with('message', 'Print Class Updated!');

        }
    }

    /////////////////////////////////// Roles ///////////////////////////////

    public function roles()
    {
        $roles = Roles::all();

        return view('admin.systemsetup.admin_roles', compact('roles'));
    }
    
    public function editRoles($id)
    {
        $rol = Roles::where('id',$id)->first();
        
        $name = $rol['name'];
        $id =  $rol['id'];
        return view('admin.systemsetup.admin_update_roles', compact('name','id'));
    }
    
     public function updateRoles(Request $request)
    {
        $data = $request->all();
  
        $update = Roles::where('id',$data['id'])->update(['name' => $data['name']]);
        
        return redirect()->to('/admin/roles')->with('message', 'Role Updated!');
    
    }
    
    public function addRoles()
    {
          $name = '';
          $id = '';
        return view('admin.systemsetup.admin_add_roles', compact('name','id'));
    }

    public function createRoles(Request $request)
    {

        $data = $request->all();

        $add = Roles::create($data)->id;
        $data1['role_id'] = $add;
        $permission = Permissions::create($data1);
        return redirect()->to('/admin/roles')->with('message', 'Role Added!');

    }
    public function deleteRoles($id)
    {

        $delete = Roles::where('id', $id)->delete();
        if ($delete) {
            return back()->with('success_message', 'Deleted Successfully');
        }
    }

    public function rolePermissions($id)
    {

        $user = Permissions::where('role_id', $id)->first();

        return view('admin.systemsetup.admin_roles_permissions', compact('id', 'user'));

    }

    public function updatePermissions(Request $request, $id)
    {

        $data = $request->all();
       
      
        
        if (!$request->has('dashboard_view')) {
            $data['dashboard_view'] = 'off';
        }

        if (!$request->has('menu_setup_view')) {

            $data['menu_setup_view'] = 'off';

        }

        if (!$request->has('major_groups_manager_view')) {

            $data['major_groups_manager_view'] = 'off';

        }

        if (!$request->has('major_groups_manager_edit')) {

            $data['major_groups_manager_edit'] = 'off';

        }

        if (!$request->has('sub_major_groups_manager_add')) {

            $data['sub_major_groups_manager_add'] = 'off';

        }
        if (!$request->has('sub_major_groups_manager_delete')) {

            $data['sub_major_groups_manager_delete'] = 'off';

        }
        if (!$request->has('sub_major_groups_manager_edit')) {

            $data['sub_major_groups_manager_edit'] = 'off';

        }
        if (!$request->has('sub_major_groups_manager_view')) {

            $data['sub_major_groups_manager_view'] = 'off';

        }

        if (!$request->has('menu_item_groups_delete')) {

            $data['menu_item_groups_delete'] = 'off';
        }
        if (!$request->has('menu_item_groups_add')) {

            $data['menu_item_groups_add'] = 'off';

        }if (!$request->has('menu_item_groups_edit')) {

            $data['menu_item_groups_edit'] = 'off';

        }if (!$request->has('menu_item_groups_view')) {

            $data['menu_item_groups_view'] = 'off';

        }

        if (!$request->has('offers_view')) {

            $data['offers_view'] = 'off';

        }
        if (!$request->has('offers_add')) {

            $data['offers_add'] = 'off';

        }if (!$request->has('offers_edit')) {

            $data['offers_edit'] = 'off';

        }if (!$request->has('offers_delete')) {

            $data['offers_delete'] = 'off';

        }

        if (!$request->has('family_groups_view')) {

            $data['family_groups_view'] = 'off';

        }
        if (!$request->has('family_groups_edit')) {

            $data['family_groups_edit'] = 'off';

        }if (!$request->has('family_groups_add')) {

            $data['family_groups_add'] = 'off';

        }if (!$request->has('family_groups_delete')) {

            $data['family_groups_delete'] = 'off';

        }

        if (!$request->has('alcoholic_family_groups_add')) {

            $data['alcoholic_family_groups_add'] = 'off';

        }
        if (!$request->has('alcoholic_family_groups_edit')) {

            $data['alcoholic_family_groups_edit'] = 'off';

        }
        /////////////////////
        if (!$request->has('alcoholic_family_groups_delete')) {

            $data['alcoholic_family_groups_delete'] = 'off';

        }if (!$request->has('alcoholic_family_groups_view')) {

            $data['alcoholic_family_groups_view'] = 'off';

        }

        if (!$request->has('alcoholic_sub_family_groups_add')) {

            $data['alcoholic_sub_family_groups_add'] = 'off';

        }
        if (!$request->has('alcoholic_sub_family_groups_edit')) {

            $data['alcoholic_sub_family_groups_edit'] = 'off';

        }if (!$request->has('alcoholic_sub_family_groups_view')) {

            $data['alcoholic_sub_family_groups_view'] = 'off';

        }if (!$request->has('alcoholic_sub_family_groups_delete')) {

            $data['alcoholic_sub_family_groups_delete'] = 'off';

        }
        if (!$request->has('menu_Item_view')) {

            $data['menu_Item_view'] = 'off';
        }

        if (!$request->has('general_item_view')) {

            $data['general_item_view'] = 'off';
        }
        if (!$request->has('general_item_add')) {

            $data['general_item_add'] = 'off';
        }
        if (!$request->has('general_item_edit')) {

            $data['general_item_edit'] = 'off';
        }if (!$request->has('general_item_delete')) {

            $data['general_item_delete'] = 'off';
        }

        if (!$request->has('offer_themes_nights_view')) {

            $data['offer_themes_nights_view'] = 'off';
        }
        if (!$request->has('offer_themes_nights_edit')) {

            $data['offer_themes_nights_edit'] = 'off';
        }if (!$request->has('offer_themes_nights_add')) {

            $data['offer_themes_nights_add'] = 'off';
        }if (!$request->has('offer_themes_nights_delete')) {

            $data['offer_themes_nights_delete'] = 'off';
        }

        if (!$request->has('app_users_view')) {

            $data['app_users_view'] = 'off';
        }

        if (!$request->has('app_users_addamounttowallet')) {

            $data['app_users_addamounttowallet'] = 'off';
        }

        /////////////////////////

        if (!$request->has('manage_orders_view')) {

            $data['manage_orders_view'] = 'off';
        }

        if (!$request->has('prepaid_orders_view')) {

            $data['prepaid_orders_view'] = 'off';
        }

        if (!$request->has('postpaid_orders_view')) {

            $data['postpaid_orders_view'] = 'off';
        }

        if (!$request->has('open_orders_view')) {

            $data['open_orders_view'] = 'off';
        }
        if (!$request->has('open_orders_add')) {

            $data['open_orders_add'] = 'off';
        }if (!$request->has('open_orders_delete')) {

            $data['open_orders_delete'] = 'off';
        }if (!$request->has('open_orders_transfer')) {

            $data['open_orders_transfer'] = 'off';
        }
        if (!$request->has('void_item_view')) {

            $data['void_item_view'] = 'off';
        }
        if (!$request->has('void_item_accept')) {

            $data['void_item_accept'] = 'off';
        }
        if (!$request->has('void_item_accept')) {

            $data['void_item_accept'] = 'off';
        }
          if (!$request->has('void_item_view')) {
            $data['void_item_view'] = 'off';
        }
        if (!$request->has('void_item_accept')) {
            $data['void_item_accept'] = 'off';
        }
        if (!$request->has('void_item_reject')) {
            $data['void_item_reject'] = 'off';
        }

        if (!$request->has('bills_view')) {

            $data['bills_view'] = 'off';
        }
        if (!$request->has('generate_bills_view')) {

            $data['generate_bills_view'] = 'off';
        }

        if (!$request->has('master_bills_view')) {

            $data['master_bills_view'] = 'off';
        }
        if (!$request->has('master_bills_close')) {

            $data['master_bills_close'] = 'off';
        }
        if (!$request->has('master_bills_delete')) {

            $data['master_bills_delete'] = 'off';
        }
        if (!$request->has('master_bills_discount')) {
            $data['master_bills_discount'] = 'off';
        }

        if (!$request->has('closed_orders_view')) {

            $data['closed_orders_view'] = 'off';
        }
        if (!$request->has('closed_orders_payment_view')) {

            $data['closed_orders_payment_view'] = 'off';
        }
        if (!$request->has('closed_orders_payment_reprints')) {

            $data['closed_orders_payment_reprints'] = 'off';
        }

        if (!$request->has('complete_orders_view')) {

            $data['complete_orders_view'] = 'off';
        }
        if (!$request->has('system_setup_view')) {

            $data['system_setup_view'] = 'off';
        }
        if (!$request->has('advertisements_view')) {

            $data['advertisements_view'] = 'off';
        }
        if (!$request->has('advertisements_edit')) {

            $data['advertisements_edit'] = 'off';
        }
        if (!$request->has('advertisements_add')) {

            $data['advertisements_add'] = 'off';
        }
        if (!$request->has('advertisements_delete')) {

            $data['advertisements_delete'] = 'off';
        }

        if (!$request->has('social_links_view')) {

            $data['social_links_view'] = 'off';
        }
        if (!$request->has('social_links_edit')) {

            $data['social_links_edit'] = 'off';
        }

        if (!$request->has('reservation_email_view')) {

            $data['reservation_email_view'] = 'off';
        }
        if (!$request->has('reservation_email_add')) {

            $data['reservation_email_add'] = 'off';
        }

        if (!$request->has('reservation_email_edit')) {

            $data['reservation_email_edit'] = 'off';
        }
        if (!$request->has('reservation_email_delete')) {

            $data['reservation_email_delete'] = 'off';
        }

        if (!$request->has('payent_method_view')) {

            $data['payent_method_view'] = 'off';
        }
        if (!$request->has('payent_method_edit')) {

            $data['payent_method_edit'] = 'off';
        }

        if (!$request->has('payent_method_add')) {

            $data['payent_method_add'] = 'off';
        }
        if (!$request->has('payent_method_delete')) {

            $data['payent_method_delete'] = 'off';
        }

        if (!$request->has('app_custom_pages_view')) {

            $data['app_custom_pages_view'] = 'off';
        }
        if (!$request->has('app_custom_pages_edit')) {

            $data['app_custom_pages_edit'] = 'off';
        }

        if (!$request->has('restaurants_view')) {

            $data['restaurants_view'] = 'off';
        }
        if (!$request->has('restaurants_add')) {

            $data['restaurants_add'] = 'off';
        }if (!$request->has('restaurants_edit')) {

            $data['restaurants_edit'] = 'off';
        }
        if (!$request->has('restaurants_delete')) {

            $data['restaurants_delete'] = 'off';
        }

        if (!$request->has('print_classes_view')) {

            $data['print_classes_view'] = 'off';
        }
        if (!$request->has('print_classes_add')) {

            $data['print_classes_add'] = 'off';
        }if (!$request->has('print_classes_edit')) {

            $data['print_classes_edit'] = 'off';
        }
        if (!$request->has('print_classes_delete')) {

            $data['print_classes_delete'] = 'off';
        }

        if (!$request->has('take_away_view')) {

            $data['take_away_view'] = 'off';
        }
        if (!$request->has('take_away_edit')) {

            $data['take_away_edit'] = 'off';
        }if (!$request->has('take_away_add')) {

            $data['take_away_add'] = 'off';
        }
        if (!$request->has('take_away_delete')) {

            $data['take_away_delete'] = 'off';
        }

        if (!$request->has('employees_view')) {

            $data['employees_view'] = 'off';
        }
        if (!$request->has('employees_add')) {

            $data['employees_add'] = 'off';
        }if (!$request->has('employees_edit')) {

            $data['employees_edit'] = 'off';
        }
        if (!$request->has('employees_delete')) {

            $data['employees_delete'] = 'off';
        }
        if (!$request->has('employees_change_password')) {

            $data['employees_change_password'] = 'off';
        }
        if (!$request->has('employees_assign_table')) {

            $data['employees_assign_table'] = 'off';
        }
        

        if (!$request->has('print_class_users_view')) {

            $data['print_class_users_view'] = 'off';
        }
        if (!$request->has('print_class_users_add')) {

            $data['print_class_users_add'] = 'off';
        }if (!$request->has('print_class_users_edit')) {

            $data['print_class_users_edit'] = 'off';
        }
        if (!$request->has('print_class_users_delete')) {

            $data['print_class_users_delete'] = 'off';
        }
        if (!$request->has('print_class_users_change_password')) {

            $data['print_class_users_change_password'] = 'off';
        }
        /////////////////////////////////

        if (!$request->has('table_managers_view')) {

            $data['print_class_users_change_password'] = 'off';
        }
        if (!$request->has('table_managers_add')) {

            $data['table_managers_add'] = 'off';
        }if (!$request->has('table_managers_edit')) {

            $data['table_managers_edit'] = 'off';
        }
        if (!$request->has('table_managers_delete')) {

            $data['table_managers_delete'] = 'off';
        }

        if (!$request->has('tax_manager_view')) {

            $data['tax_manager_view'] = 'off';
        }
        if (!$request->has('tax_manager_add')) {

            $data['tax_manager_add'] = 'off';
        }if (!$request->has('tax_manager_edit')) {

            $data['tax_manager_edit'] = 'off';
        }
        if (!$request->has('tax_manager_delete')) {

            $data['tax_manager_delete'] = 'off';
        }

        if (!$request->has('settings_view')) {

            $data['settings_view'] = 'off';
        }
        if (!$request->has('settings_edit')) {

            $data['settings_edit'] = 'off';
        }

        if (!$request->has('condiments_view')) {

            $data['condiments_view'] = 'off';
        }
        if (!$request->has('member_view')) {

            $data['member_view'] = 'off';
        }

        if (!$request->has('member_add')) {

            $data['member_add'] = 'off';
        }
        if (!$request->has('member_edit')) {

            $data['member_edit'] = 'off';
        }
        if (!$request->has('member_delete')) {

            $data['member_delete'] = 'off';
        }

        if (!$request->has('groups_view')) {

            $data['groups_view'] = 'off';
        }

        if (!$request->has('groups_add')) {

            $data['groups_add'] = 'off';
        }
        if (!$request->has('groups_edit')) {

            $data['groups_edit'] = 'off';
        }
        if (!$request->has('groups_delete')) {

            $data['groups_delete'] = 'off';
        }

        if (!$request->has('roles_view')) {

            $data['roles_view'] = 'off';
        }

        if (!$request->has('roles_add')) {

            $data['roles_add'] = 'off';
        }
        if (!$request->has('roles_edit')) {

            $data['roles_edit'] = 'off';
        }
        if (!$request->has('roles_delete')) {

            $data['roles_delete'] = 'off';
        }

        if (!$request->has('recipes_view')) {

            $data['recipes_view'] = 'off';
        }if (!$request->has('WEBACCOUNTING_ERP_view')) {

            $data['WEBACCOUNTING_ERP_view'] = 'off';
        }

        if (!$request->has('reports_view')) {

            $data['reports_view'] = 'off';
        }
        if (!$request->has('reports_payment_sales_summary')) {

            $data['reports_payment_sales_summary'] = 'off';
        }
        if (!$request->has('reports_payment_sales_summary1')) {

            $data['reports_payment_sales_summary1'] = 'off';
        }
        if (!$request->has('reports_void_items')) {

            $data['reports_void_items'] = 'off';
        }
        if (!$request->has('reports_menu_item_general_sales')) {

            $data['reports_menu_item_general_sales'] = 'off';
        }
        if (!$request->has('reports_menu_tem_general_sales_with_plu')) {

            $data['reports_menu_tem_general_sales_with_plu'] = 'off';
        }
        if (!$request->has('reports_menu_item_general_sales_without_plu')) {

            $data['reports_menu_item_general_sales_without_plu'] = 'off';
        }
        if (!$request->has('reports_family_group_sales_with_gl')) {

            $data['reports_family_group_sales_with_gl'] = 'off';
        }
        if (!$request->has('reports_menu_item_group_sales')) {

            $data['reports_menu_item_group_sales'] = 'off';
        }
        if (!$request->has('reports_major_group_sales')) {

            $data['reports_major_group_sales'] = 'off';
        }
        if (!$request->has('feedback_view')) {

            $data['feedback_view'] = 'off';
        }

        if (!$request->has('reservations_view')) {

            $data['reservations_view'] = 'off';
        }
        if (!$request->has('reservations_delete')) {

            $data['reservations_delete'] = 'off';
        }
        if (!$request->has('reservations_edit')) {

            $data['reservations_edit'] = 'off';
        }

        if (!$request->has('beer_delivery_and_keg_setup_view')) {

            $data['beer_delivery_and_keg_setup_view'] = 'off';
        }
        if (!$request->has('beer_and_keg_sub_major_group_view')) {
            $data['beer_and_keg_sub_major_group_view'] = 'off';
        }

        if (!$request->has('delivery_family_groups_view')) {

            $data['delivery_family_groups_view'] = 'off';
        }
        if (!$request->has('delivery_family_groups_add')) {

            $data['delivery_family_groups_add'] = 'off';
        }

        if (!$request->has('delivery_family_groups_edit')) {

            $data['delivery_family_groups_edit'] = 'off';
        }
        if (!$request->has('delivery_family_groups_delete')) {

            $data['delivery_family_groups_delete'] = 'off';
        }

        if (!$request->has('delivery_sub_family_groups_view')) {

            $data['delivery_sub_family_groups_view'] = 'off';
        }
        if (!$request->has('delivery_sub_family_groups_add')) {

            $data['delivery_sub_family_groups_add'] = 'off';
        }

        if (!$request->has('delivery_sub_family_groups_edit')) {

            $data['delivery_sub_family_groups_edit'] = 'off';
        }
        if (!$request->has('delivery_sub_family_groups_delete')) {

            $data['delivery_sub_family_groups_delete'] = 'off';
        }

        if (!$request->has('delivery_items_view')) {

            $data['delivery_items_view'] = 'off';
        }
        if (!$request->has('delivery_items_add')) {

            $data['delivery_items_add'] = 'off';
        }

        if (!$request->has('delivery_items_edit')) {

            $data['delivery_items_edit'] = 'off';
        }
        if (!$request->has('delivery_items_delete')) {

            $data['delivery_items_delete'] = 'off';
        }
        if (!$request->has('stock')) {

            $data['stock'] = 'off';
        }
        if (!$request->has('add_ingredients')) {

            $data['add_ingredients'] = 'off';
        }
        if (!$request->has('edit_ingredients')) {

            $data['edit_ingredients'] = 'off';
        }
        if (!$request->has('delete_ingredients')) {

            $data['delete_ingredients'] = 'off';
        }
        if (!$request->has('view_ingredients')) {

            $data['view_ingredients'] = 'off';
        }
        if (!$request->has('add_recipe')) {

            $data['add_recipe'] = 'off';
        }
        if (!$request->has('edit_recipe')) {

            $data['edit_recipe'] = 'off';
        }
        if (!$request->has('delete_recipe')) {

            $data['delete_recipe'] = 'off';
        }
        if (!$request->has('view_recipe')) {

            $data['view_recipe'] = 'off';
        }


        // $users = User::where('role_id', $id)->get();
        // $users->update($data);
       $permissions = Permissions::where('role_id', $id)->first();
        $permissions->update($data);
        
       // foreach ($users as $user) {
$permission = Permissions::where('role_id',$id)->first();
        $user = User::where('role_id', $id);
          $user->update([
              'dashboard_view' => $permission->dashboard_view,
'menu_setup_view'=> $permission->menu_setup_view,
'major_groups_manager_view'=> $permission->major_groups_manager_view,
'major_groups_manager_add'=> $permission->major_groups_manager_add,
'major_groups_manager_delete'=> $permission->major_groups_manager_delete,
'major_groups_manager_edit'=> $permission->major_groups_manager_edit,
'sub_major_groups_manager_view'=> $permission->sub_major_groups_manager_view,
'sub_major_groups_manager_edit'=> $permission->sub_major_groups_manager_edit,
'sub_major_groups_manager_add'=> $permission->sub_major_groups_manager_add,
'sub_major_groups_manager_delete'=> $permission->sub_major_groups_manager_delete,
'menu_item_groups_edit'=> $permission->menu_item_groups_edit,
'menu_item_groups_view'=> $permission->menu_item_groups_view,
'menu_item_groups_add'=> $permission->menu_item_groups_add,
'menu_item_groups_delete'=> $permission->menu_item_groups_delete,
'offers_view'=> $permission->offers_view,
'offers_edit'=> $permission->offers_edit,
'offers_add'=> $permission->offers_add,
'offers_delete'=> $permission->offers_delete,
'family_groups_view'=> $permission->family_groups_view,
'family_groups_edit'=> $permission->family_groups_edit,
'family_groups_add'=> $permission->family_groups_add,
'family_groups_delete'=> $permission->family_groups_delete,
'alcoholic_family_groups_view'=> $permission->alcoholic_family_groups_view,
'alcoholic_family_groups_edit'=> $permission->alcoholic_family_groups_edit,
'alcoholic_family_groups_add'=> $permission->alcoholic_family_groups_add,
'alcoholic_family_groups_delete'=> $permission->alcoholic_family_groups_delete,
'alcoholic_sub_family_groups_view'=> $permission->alcoholic_sub_family_groups_view,
'alcoholic_sub_family_groups_edit'=> $permission->alcoholic_sub_family_groups_edit,
'alcoholic_sub_family_groups_add'=> $permission->alcoholic_sub_family_groups_add,
'alcoholic_sub_family_groups_delete'=> $permission->alcoholic_sub_family_groups_delete,
'menu_Item_view'=> $permission->menu_Item_view,
'general_item_view'=> $permission->general_item_view,
'general_item_edit'=> $permission->general_item_edit,
'general_item_add'=> $permission->general_item_add,
'general_item_delete'=> $permission->general_item_delete,
'offer_themes_nights_view'=> $permission->offer_themes_nights_view,
'offer_themes_nights_edit'=> $permission->offer_themes_nights_edit,
'offer_themes_nights_add'=> $permission->offer_themes_nights_add,
'offer_themes_nights_delete'=> $permission->offer_themes_nights_delete,
'app_users_view'=> $permission->app_users_view,
'app_users_addamounttowallet'=> $permission->app_users_addamounttowallet,
'manage_orders_view'=> $permission->manage_orders_view,
'prepaid_orders_view'=> $permission->prepaid_orders_view,
'postpaid_orders_view'=> $permission->postpaid_orders_view,
'open_orders_view'=> $permission->open_orders_view,
'open_orders_transfer'=> $permission->open_orders_transfer,
'open_orders_add'=> $permission->open_orders_add,
'open_orders_delete'=> $permission->open_orders_delete,
'void_item_view'=> $permission->void_item_view,
'void_item_accept'=> $permission->void_item_accept,
'void_item_reject'=> $permission->void_item_reject,
'bills_view'=> $permission->bills_view,
'bills_add'=> $permission->bills_add,
'bills_edit'=> $permission->bills_edit,
'bills_delete'=> $permission->bills_delete,
'generate_bills_view'=> $permission->generate_bills_view,
'master_bills_view'=> $permission->master_bills_view,
'master_bills_close'=> $permission->master_bills_close,
'master_bills_discount'=> $permission->master_bills_discount,
'master_bills_delete'=> $permission->master_bills_delete,
'closed_orders_view'=> $permission->closed_orders_view,
'closed_orders_payment_view'=> $permission->closed_orders_payment_view,
'closed_orders_payment_reprints'=> $permission->closed_orders_payment_reprints,
'complete_orders_view'=> $permission->complete_orders_view,
'system_setup_view'=> $permission->system_setup_view,
'advertisements_view'=> $permission->advertisements_view,
'advertisements_edit'=> $permission->advertisements_edit,
'advertisements_add'=> $permission->advertisements_add,
'advertisements_delete'=> $permission->advertisements_delete,
'social_links_view'=> $permission->social_links_view,
'social_links_edit'=> $permission->social_links_edit,
'reservation_email_edit'=> $permission->reservation_email_edit,
'reservation_email_view'=> $permission->reservation_email_view,
'reservation_email_add'=> $permission->reservation_email_add,
'reservation_email_delete'=> $permission->reservation_email_delete,
'payent_method_view'=> $permission->payent_method_view,
'payent_method_add'=> $permission->payent_method_add,
'payent_method_edit'=> $permission->payent_method_edit,
'payent_method_delete'=> $permission->payent_method_delete,
'app_custom_pages_view'=> $permission->app_custom_pages_view,
'app_custom_pages_edit'=> $permission->app_custom_pages_edit,
'restaurants_view'=> $permission->restaurants_view,
'restaurants_edit'=> $permission->restaurants_edit,
'restaurants_add'=> $permission->restaurants_add,
'restaurants_delete'=> $permission->restaurants_delete,
'print_classes_view'=> $permission->print_classes_view,
'print_classes_edit'=> $permission->print_classes_edit,
'print_classes_add'=> $permission->print_classes_add,
'print_classes_delete'=> $permission->print_classes_delete,
'take_away_view'=> $permission->take_away_view,
'take_away_edit'=> $permission->take_away_edit,
'take_away_add'=> $permission->take_away_add,
'take_away_delete'=> $permission->take_away_delete,
'employees_view'=> $permission->employees_view,
'employees_edit'=> $permission->employees_edit,
'employees_add'=> $permission->employees_add,
'employees_delete'=> $permission->employees_delete,
'print_class_users_view'=> $permission->print_class_users_view,
'print_class_users_add'=> $permission->print_class_users_add,
'print_class_users_edit'=> $permission->print_class_users_edit,
'print_class_users_delete'=> $permission->print_class_users_delete,
'print_class_users_change_password'=> $permission->print_class_users_change_password,
'employees_change_password'=> $permission->employees_change_password,
'employees_assign_table' => $permission->employees_assign_table,
'table_managers_view'=> $permission->table_managers_view,
'table_managers_edit'=> $permission->table_managers_edit,
'table_managers_add'=> $permission->table_managers_add,
'table_managers_delete'=> $permission->table_managers_delete,
'tax_manager_view'=> $permission->tax_manager_view,
'tax_manager_add'=> $permission->tax_manager_add,
'tax_manager_edit'=> $permission->tax_manager_edit,
'tax_manager_delete'=> $permission->tax_manager_delete,
'settings_view'=> $permission->settings_view,
'settings_edit'=> $permission->settings_edit,
'condiments_view'=> $permission->condiments_view,
'member_view'=> $permission->member_view,
'member_edit'=> $permission->member_edit,
'member_add'=> $permission->member_add,
'member_delete'=> $permission->member_delete,
'groups_view'=> $permission->groups_view,
'groups_edit'=> $permission->groups_edit,
'groups_add'=> $permission->groups_add,
'groups_delete'=> $permission->groups_delete,
'roles_view'=> $permission->roles_view,
'roles_edit'=> $permission->roles_edit,
'roles_add'=> $permission->roles_add,
'roles_delete'=> $permission->roles_delete,
'roles_permissions'=> $permission->roles_permissions,
'recipes_view'=> $permission->recipes_view,
'WEBACCOUNTING_ERP_view'=> $permission->WEBACCOUNTING_ERP_view,
'feedback_view'=> $permission->feedback_view,
'feedback_orders'=> $permission->feedback_orders,
'feedback_restaurants'=> $permission->feedback_restaurants,
'reservations_view'=> $permission->reservations_view,
'reservations_edit'=> $permission->reservations_edit,
'reservations_delete'=> $permission->reservations_delete,
'beer_delivery_and_keg_setup_view'=> $permission->beer_delivery_and_keg_setup_view,
'beer_delivery_and_keg_setup_edit'=> $permission->beer_delivery_and_keg_setup_edit,
'beer_delivery_and_keg_setup_add'=> $permission->beer_delivery_and_keg_setup_add,
'beer_delivery_and_keg_setup_delete'=> $permission->beer_delivery_and_keg_setup_delete,
'beer_and_keg_sub_major_group_view'=> $permission->beer_and_keg_sub_major_group_view,
'beer_and_keg_sub_major_group_edit'=> $permission->beer_and_keg_sub_major_group_edit,
'beer_and_keg_sub_major_group_add'=> $permission->beer_and_keg_sub_major_group_add,
'beer_and_keg_sub_major_group_delete'=> $permission->beer_and_keg_sub_major_group_delete,
'delivery_family_groups_view'=> $permission->delivery_family_groups_view,
'delivery_family_groups_edit'=> $permission->delivery_family_groups_edit,
'delivery_family_groups_add'=> $permission->delivery_family_groups_add,
'delivery_family_groups_delete'=> $permission->delivery_family_groups_delete,
'delivery_sub_family_groups_view'=> $permission->delivery_sub_family_groups_view,
'delivery_sub_family_groups_edit'=> $permission->delivery_sub_family_groups_edit,
'delivery_sub_family_groups_add'=> $permission->delivery_sub_family_groups_add,
'delivery_sub_family_groups_delete'=> $permission->delivery_sub_family_groups_delete,
'delivery_items_view'=> $permission->delivery_items_view,
'delivery_items_edit'=> $permission->delivery_items_edit,
'delivery_items_add'=> $permission->delivery_items_add,
'delivery_items_delete'=> $permission->delivery_items_delete,
'reports_view'=> $permission->reports_view,
'reports_payment_sales_summary'=> $permission->reports_payment_sales_summary,
'reports_menu_item_general_sales'=> $permission->reports_menu_item_general_sales,
'reports_menu_tem_general_sales_with_plu'=> $permission->reports_menu_tem_general_sales_with_plu,
'reports_menu_item_general_sales_without_plu'=> $permission->reports_menu_item_general_sales_without_plu,
'reports_family_group_sales_with_gl'=> $permission->reports_family_group_sales_with_gl,
'reports_family_group_sales'=> $permission->reports_family_group_sales,
'reports_menu_item_group_sales'=> $permission->reports_menu_item_group_sales,
'reports_major_group_sales'=> $permission->reports_major_group_sales,
'reports_waiter_with_family_groups'=> $permission->reports_waiter_with_family_groups,
'reports_condiments_sales_with_plu'=> $permission->reports_condiments_sales_with_plu,
'reports_get_discount_reports'=> $permission->reports_get_discount_reports,
'reports_get_cashier_reports'=> $permission->reports_get_cashier_reports,
'get_discount_reports_with_orders'=> $permission->get_discount_reports_with_orders,
'reports_get_complementary_reports_with_orders'=> $permission->reports_get_complementary_reports_with_orders,
'reports_get_cancelled_orders_reports'=> $permission->reports_get_cancelled_orders_reports,
'reports_wallet_ledger_entries'=> $permission->reports_wallet_ledger_entries,
'gl_view'=> $permission->gl_view,
'gl_add'=> $permission->gl_add,
'plu_view'=> $permission->plu_view,
'plu_add'=> $permission->plu_add,
'gl_edit'=> $permission->gl_edit,
'gl_delete'=> $permission->gl_delete,
'plu_edit'=> $permission->plu_edit,
'plu_delete'=> $permission->plu_delete,
'reports_void_items'=> $permission->reports_void_items,
'reports_payment_sales_summary1'=> $permission->reports_payment_sales_summary1,
'stock'=> $permission->stock,
'add_ingredients'=> $permission->add_ingredients,
'edit_ingredients'=> $permission->edit_ingredients,
'delete_ingredients'=> $permission->delete_ingredients,
'view_ingredients'=> $permission->view_ingredients,
'add_recipe'=> $permission->add_recipe,
'edit_recipe'=> $permission->edit_recipe,
'delete_recipe'=> $permission->delete_recipe,
'view_recipe'=> $permission->view_recipe,

]);
       // }

        Session::flash('message', 'We have sent you a verification email!');

        return redirect()->to('/admin/roles')->with('message', 'Permissions Updated!');

    }

    /////////////////////////////////////// Tax /////////////////////////////////////////////////

    public function taxes()
    {
        $taxes = Tax::all();

        return view('admin.systemsetup.admin_taxes', compact('taxes'));
    }

    public function addTax()
    {

        return view('admin.systemsetup.admin_add_tax');
    }

    public function createTax(Request $request)
    {

        $data = $request->all();

        $add = Tax::create($data);

        return back()->with('success_message', 'Role created Successfully!');

    }
    public function deleteTax($id)
    {

        $delete = Tax::where('id', $id)->delete();
        if ($delete) {
            return back()->with('success_message', 'Deleted Successfully');
        }
    }

    ///////////////////////////////////// Condiments /////////////////////////////////////

    public function condiments()
    {
        $condiments = Condiments::all();

        return view('admin.systemsetup.admin_condiments', compact('condiments'));
    }

    public function addCondiments()
    {

        $plu_numbers = PLUNumber::all();
        //return $plu_numbers;
        return view('admin.systemsetup.admin_add_condiments', compact('plu_numbers'));
    }

    public function createCondiments(Request $request)
    {

        $data = $request->all();

        $add = Condiments::create($data);

        return back()->with('success_message', 'Condiments created Successfully!');

    }
    public function deleteCondiments($id)
    {

        $delete = Condiments::where('id', $id)->delete();
        if ($delete) {
            return back()->with('success_message', 'Deleted Successfully');
        }
    }
    ///////////////////////////////////// Condiments Groups ////////////////////

    public function condimentsGroups()
    {
        $condiments = CondimentGroup::all();

        return view('admin.systemsetup.admin_condiment_groups', compact('condiments'));
    }

    public function addCondimentsGroups()
    {

        $condiments = Condiments::all();
        //return $plu_numbers;
        return view('admin.systemsetup.admin_add_condiment_groups', compact('condiments'));
    }

    public function createCondimentsGroups(Request $request)
    {

        $data = $request->all();

        $add = CondimentGroup::create($data);

        return back()->with('success_message', 'Condiments created Successfully!');

    }
    public function deleteCondimentsGroups($id)
    {

        $delete = CondimentGroup::where('id', $id)->delete();
        if ($delete) {
            return back()->with('success_message', 'Deleted Successfully');
        }
    }

    //////////////////////////// Employee ///////////////////////////////

    protected function validators(array $data)
    { //return $data;
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'restaurant_id' => 'required|string|max:255',
            'role_id' => 'required|string|max:255',

            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }

    protected function createEmplo(array $data)
    {
         return $data;
            exit;
        $roles = Roles::where('id', $data['role_id'])->first();
        $rname = $roles->name;
        //echo $rname;exit;
        return User::create([
            'name' => $data['name'],
            'restaurant_id' => $data['restaurant_id'],
            'role_id' => $data['role_id'],
            'phone_number' => $data['phone_number'],
            'badge_number' => $data['badge_number'],
            'id_number' => $data['id_number'],
            'complementary_number' => $data['complementary_number'],
            'complementary_amount' => $data['complementary_amount'],
            'discount' => $data['discount'],
            'email' => $data['email'],

            'password' => bcrypt($data['password']),
            'user_type' => $rname,

        ]);
    }

    public function employees()
    {

        $employee = User::all();
        $restaurants = Restaurants::all();
        $roles = Roles::all();

        return view('admin.systemsetup.admin_employees', compact('employee', 'roles', 'restaurants'));
    }

    public function addEmployee()
    {

        $restaurants = Restaurants::all();
        $roles = Roles::all();

        return view('admin.systemsetup.admin_add_employee', compact('roles', 'restaurants'));
    }
    public function editEmployee($id)
    {

        $restaurants = Restaurants::all();
        $roles = Roles::all();
        $user = User::where('id', $id)->first();
      
        return view('admin.systemsetup.admin_edit_employee', compact('user','roles', 'restaurants'));
    }
    public function SaveEditEmployee(Request $request )
    {

        $data = $request->all();
        $userid =  $data['id'];
        $roles = Roles::where('id', $data['role_id'])->first();
        $rname = $roles->name;
        $permission = Permissions::where('role_id',$data['role_id'])->first();
        $user = User::find($userid);
        
        if($user->Xero_id == null){
            $contacts = XeroPrivate::load('Accounting\\Contact')->where('Name', $data['name'])->execute();
             
            
            if(sizeof($contacts) > 0){
                foreach($contacts as $contact){
                
                    $id = $contact->ContactID;
                    break;
                } 
                $contact->setContactStatus('ACTIVE');
                $contact->setName($data['name']);
                $contact->setEmailAddress($data['email']);
                $contact->setContactID($id);
                $contact = XeroPrivate::save($contact);
                User::where('id',$userid)->update([
                'Xero_id' => $id, ]);
            }else{
                $contact->setName($data['name']);
                $contact->setEmailAddress($data['email']);
                $contact = XeroPrivate::save($contact);
                $sxml = simplexml_load_string($contact->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
                $contact_id = $sxml->Contacts->Contact->ContactID[0];
                User::where('id',$userid)->update([
                'Xero_id' => $contact_id, ]);
            
            }
        }else{
            $contact = new \XeroPHP\Models\Accounting\Contact();
            $contact->setContactStatus('ACTIVE');
            $contact->setName($data['name']);
            $contact->setEmailAddress($data['email']);
            $contact->setContactID($user->Xero_id);
            $contact = XeroPrivate::save($contact);
            
        }
        
        
        
        
        
        
        
        
        $user->update([
            'name' => $data['name'],
            'restaurant_id' => $data['restaurant_id'],
            'role_id' => $data['role_id'],
            'phone_number' => $data['phone_number'],
            'badge_number' => $data['badge_number'],
            'id_number' => $data['id_number'],
            'complementary_number' => $data['complementary_number'],
            'complementary_amount' => $data['complementary_amount'],
            'discount' => $data['discount'],
            'email' => $data['email'],
            'user_type' => $rname,
            'dashboard_view' => $permission->dashboard_view,
'menu_setup_view'=> $permission->menu_setup_view,
'major_groups_manager_view'=> $permission->major_groups_manager_view,
'major_groups_manager_add'=> $permission->major_groups_manager_add,
'major_groups_manager_delete'=> $permission->major_groups_manager_delete,
'major_groups_manager_edit'=> $permission->major_groups_manager_edit,
'sub_major_groups_manager_view'=> $permission->sub_major_groups_manager_view,
'sub_major_groups_manager_edit'=> $permission->sub_major_groups_manager_edit,
'sub_major_groups_manager_add'=> $permission->sub_major_groups_manager_add,
'sub_major_groups_manager_delete'=> $permission->sub_major_groups_manager_delete,
'menu_item_groups_edit'=> $permission->menu_item_groups_edit,
'menu_item_groups_view'=> $permission->menu_item_groups_view,
'menu_item_groups_add'=> $permission->menu_item_groups_add,
'menu_item_groups_delete'=> $permission->menu_item_groups_delete,
'offers_view'=> $permission->offers_view,
'offers_edit'=> $permission->offers_edit,
'offers_add'=> $permission->offers_add,
'offers_delete'=> $permission->offers_delete,
'family_groups_view'=> $permission->family_groups_view,
'family_groups_edit'=> $permission->family_groups_edit,
'family_groups_add'=> $permission->family_groups_add,
'family_groups_delete'=> $permission->family_groups_delete,
'alcoholic_family_groups_view'=> $permission->alcoholic_family_groups_view,
'alcoholic_family_groups_edit'=> $permission->alcoholic_family_groups_edit,
'alcoholic_family_groups_add'=> $permission->alcoholic_family_groups_add,
'alcoholic_family_groups_delete'=> $permission->alcoholic_family_groups_delete,
'alcoholic_sub_family_groups_view'=> $permission->alcoholic_sub_family_groups_view,
'alcoholic_sub_family_groups_edit'=> $permission->alcoholic_sub_family_groups_edit,
'alcoholic_sub_family_groups_add'=> $permission->alcoholic_sub_family_groups_add,
'alcoholic_sub_family_groups_delete'=> $permission->alcoholic_sub_family_groups_delete,
'menu_Item_view'=> $permission->menu_Item_view,
'general_item_view'=> $permission->general_item_view,
'general_item_edit'=> $permission->general_item_edit,
'general_item_add'=> $permission->general_item_add,
'general_item_delete'=> $permission->general_item_delete,
'offer_themes_nights_view'=> $permission->offer_themes_nights_view,
'offer_themes_nights_edit'=> $permission->offer_themes_nights_edit,
'offer_themes_nights_add'=> $permission->offer_themes_nights_add,
'offer_themes_nights_delete'=> $permission->offer_themes_nights_delete,
'app_users_view'=> $permission->app_users_view,
'app_users_addamounttowallet'=> $permission->app_users_addamounttowallet,
'manage_orders_view'=> $permission->manage_orders_view,
'prepaid_orders_view'=> $permission->prepaid_orders_view,
'postpaid_orders_view'=> $permission->postpaid_orders_view,
'open_orders_view'=> $permission->open_orders_view,
'open_orders_transfer'=> $permission->open_orders_transfer,
'open_orders_add'=> $permission->open_orders_add,
'open_orders_delete'=> $permission->open_orders_delete,
'void_item_view'=> $permission->void_item_view,
'void_item_accept'=> $permission->void_item_accept,
'void_item_reject'=> $permission->void_item_reject,
'bills_view'=> $permission->bills_view,
'bills_add'=> $permission->bills_add,
'bills_edit'=> $permission->bills_edit,
'bills_delete'=> $permission->bills_delete,
'generate_bills_view'=> $permission->generate_bills_view,
'master_bills_view'=> $permission->master_bills_view,
'master_bills_close'=> $permission->master_bills_close,
'master_bills_discount'=> $permission->master_bills_discount,
'master_bills_delete'=> $permission->master_bills_delete,
'closed_orders_view'=> $permission->closed_orders_view,
'closed_orders_payment_view'=> $permission->closed_orders_payment_view,
'closed_orders_payment_reprints'=> $permission->closed_orders_payment_reprints,
'complete_orders_view'=> $permission->complete_orders_view,
'system_setup_view'=> $permission->system_setup_view,
'advertisements_view'=> $permission->advertisements_view,
'advertisements_edit'=> $permission->advertisements_edit,
'advertisements_add'=> $permission->advertisements_add,
'advertisements_delete'=> $permission->advertisements_delete,
'social_links_view'=> $permission->social_links_view,
'social_links_edit'=> $permission->social_links_edit,
'reservation_email_edit'=> $permission->reservation_email_edit,
'reservation_email_view'=> $permission->reservation_email_view,
'reservation_email_add'=> $permission->reservation_email_add,
'reservation_email_delete'=> $permission->reservation_email_delete,
'payent_method_view'=> $permission->payent_method_view,
'payent_method_add'=> $permission->payent_method_add,
'payent_method_edit'=> $permission->payent_method_edit,
'payent_method_delete'=> $permission->payent_method_delete,
'app_custom_pages_view'=> $permission->app_custom_pages_view,
'app_custom_pages_edit'=> $permission->app_custom_pages_edit,
'restaurants_view'=> $permission->restaurants_view,
'restaurants_edit'=> $permission->restaurants_edit,
'restaurants_add'=> $permission->restaurants_add,
'restaurants_delete'=> $permission->restaurants_delete,
'print_classes_view'=> $permission->print_classes_view,
'print_classes_edit'=> $permission->print_classes_edit,
'print_classes_add'=> $permission->print_classes_add,
'print_classes_delete'=> $permission->print_classes_delete,
'take_away_view'=> $permission->take_away_view,
'take_away_edit'=> $permission->take_away_edit,
'take_away_add'=> $permission->take_away_add,
'take_away_delete'=> $permission->take_away_delete,
'employees_view'=> $permission->employees_view,
'employees_edit'=> $permission->employees_edit,
'employees_add'=> $permission->employees_add,
'employees_delete'=> $permission->employees_delete,
'print_class_users_view'=> $permission->print_class_users_view,
'print_class_users_add'=> $permission->print_class_users_add,
'print_class_users_edit'=> $permission->print_class_users_edit,
'print_class_users_delete'=> $permission->print_class_users_delete,
'print_class_users_change_password'=> $permission->print_class_users_change_password,
'employees_change_password'=> $permission->employees_change_password,
'employees_assign_table' => $permission->employees_assign_table,
'table_managers_view'=> $permission->table_managers_view,
'table_managers_edit'=> $permission->table_managers_edit,
'table_managers_add'=> $permission->table_managers_add,
'table_managers_delete'=> $permission->table_managers_delete,
'tax_manager_view'=> $permission->tax_manager_view,
'tax_manager_add'=> $permission->tax_manager_add,
'tax_manager_edit'=> $permission->tax_manager_edit,
'tax_manager_delete'=> $permission->tax_manager_delete,
'settings_view'=> $permission->settings_view,
'settings_edit'=> $permission->settings_edit,
'condiments_view'=> $permission->condiments_view,
'member_view'=> $permission->member_view,
'member_edit'=> $permission->member_edit,
'member_add'=> $permission->member_add,
'member_delete'=> $permission->member_delete,
'groups_view'=> $permission->groups_view,
'groups_edit'=> $permission->groups_edit,
'groups_add'=> $permission->groups_add,
'groups_delete'=> $permission->groups_delete,
'roles_view'=> $permission->roles_view,
'roles_edit'=> $permission->roles_edit,
'roles_add'=> $permission->roles_add,
'roles_delete'=> $permission->roles_delete,
'roles_permissions'=> $permission->roles_permissions,
'recipes_view'=> $permission->recipes_view,
'WEBACCOUNTING_ERP_view'=> $permission->WEBACCOUNTING_ERP_view,
'feedback_view'=> $permission->feedback_view,
'feedback_orders'=> $permission->feedback_orders,
'feedback_restaurants'=> $permission->feedback_restaurants,
'reservations_view'=> $permission->reservations_view,
'reservations_edit'=> $permission->reservations_edit,
'reservations_delete'=> $permission->reservations_delete,
'beer_delivery_and_keg_setup_view'=> $permission->beer_delivery_and_keg_setup_view,
'beer_delivery_and_keg_setup_edit'=> $permission->beer_delivery_and_keg_setup_edit,
'beer_delivery_and_keg_setup_add'=> $permission->beer_delivery_and_keg_setup_add,
'beer_delivery_and_keg_setup_delete'=> $permission->beer_delivery_and_keg_setup_delete,
'beer_and_keg_sub_major_group_view'=> $permission->beer_and_keg_sub_major_group_view,
'beer_and_keg_sub_major_group_edit'=> $permission->beer_and_keg_sub_major_group_edit,
'beer_and_keg_sub_major_group_add'=> $permission->beer_and_keg_sub_major_group_add,
'beer_and_keg_sub_major_group_delete'=> $permission->beer_and_keg_sub_major_group_delete,
'delivery_family_groups_view'=> $permission->delivery_family_groups_view,
'delivery_family_groups_edit'=> $permission->delivery_family_groups_edit,
'delivery_family_groups_add'=> $permission->delivery_family_groups_add,
'delivery_family_groups_delete'=> $permission->delivery_family_groups_delete,
'delivery_sub_family_groups_view'=> $permission->delivery_sub_family_groups_view,
'delivery_sub_family_groups_edit'=> $permission->delivery_sub_family_groups_edit,
'delivery_sub_family_groups_add'=> $permission->delivery_sub_family_groups_add,
'delivery_sub_family_groups_delete'=> $permission->delivery_sub_family_groups_delete,
'delivery_items_view'=> $permission->delivery_items_view,
'delivery_items_edit'=> $permission->delivery_items_edit,
'delivery_items_add'=> $permission->delivery_items_add,
'delivery_items_delete'=> $permission->delivery_items_delete,
'reports_view'=> $permission->reports_view,
'reports_payment_sales_summary'=> $permission->reports_payment_sales_summary,
'reports_menu_item_general_sales'=> $permission->reports_menu_item_general_sales,
'reports_menu_tem_general_sales_with_plu'=> $permission->reports_menu_tem_general_sales_with_plu,
'reports_menu_item_general_sales_without_plu'=> $permission->reports_menu_item_general_sales_without_plu,
'reports_family_group_sales_with_gl'=> $permission->reports_family_group_sales_with_gl,
'reports_family_group_sales'=> $permission->reports_family_group_sales,
'reports_menu_item_group_sales'=> $permission->reports_menu_item_group_sales,
'reports_major_group_sales'=> $permission->reports_major_group_sales,
'reports_waiter_with_family_groups'=> $permission->reports_waiter_with_family_groups,
'reports_condiments_sales_with_plu'=> $permission->reports_condiments_sales_with_plu,
'reports_get_discount_reports'=> $permission->reports_get_discount_reports,
'reports_get_cashier_reports'=> $permission->reports_get_cashier_reports,
'get_discount_reports_with_orders'=> $permission->get_discount_reports_with_orders,
'reports_get_complementary_reports_with_orders'=> $permission->reports_get_complementary_reports_with_orders,
'reports_get_cancelled_orders_reports'=> $permission->reports_get_cancelled_orders_reports,
'reports_wallet_ledger_entries'=> $permission->reports_wallet_ledger_entries,
'gl_view'=> $permission->gl_view,
'gl_add'=> $permission->gl_add,
'plu_view'=> $permission->plu_view,
'plu_add'=> $permission->plu_add,
'gl_edit'=> $permission->gl_edit,
'gl_delete'=> $permission->gl_delete,
'plu_edit'=> $permission->plu_edit,
'plu_delete'=> $permission->plu_delete,
'reports_void_items'=> $permission->reports_void_items,
'reports_payment_sales_summary1'=> $permission->reports_payment_sales_summary1
        ]);
        
        
        return redirect()->to('/admin/employees')->with('message', 'Employee Updated!');
    }
    public function ChangeUserPass(){
         $id = \Auth::user()->id;
         $user = User::where('id', $id)->first();
        return view('admin.systemsetup.admin_userchangePass', compact('user'));
    }
    public function UserPass(Request $request){
        
         $data = $request->all();
         
         $user = User::where('id', $data['id'])->first();
         
        if( \Hash::check($data['old_password'], $user->password) ){
          
         $confirm_password = $data['confirm_password'];
        $confirm_password= bcrypt($confirm_password);
        $user->password =  $confirm_password;
        $user->save();
        return redirect('/')->with(\Auth::logout());
       
        }else{
              return redirect()->to('/admin/employees')->with('error', 'Employee Password not match!');
        }
         
        
    }
    public function ChangeEmployeePass($id){

        $user = User::where('id', $id)->first();
        return view('admin.systemsetup.admin_changepass_employee', compact('user'));
    }
    public function saveEmployeePass(Request $request){
        $id = $request->id;
        $confirm_password = $request->confirm_password;
        $confirm_password= bcrypt($confirm_password);
        $user = User::where('id', $id)->first();
        $user->password =  $confirm_password;
        $user->save();
        return redirect()->to('/admin/employees')->with('message', 'Employee Password Updated!');
    }
    public function ChangeStatus($id,$status){
        $user = User::where('id', $id)->first();
        if($status == 'active'){
            $user->is_active = 'active';
        }else{
            $user->is_active = 'inactive';
        }
        $user->save();
        return 'true';
        
    }


    public function createEmployee(Request $request)
    {
        //return $request;
        $data = $request->all();
        $role = $data['role_id'];
        $roles = Roles::where('id', $data['role_id'])->first();
        $rname = $roles->name;
        $permission = Permissions::where('role_id',$role)->first();
        // DB::enableQueryLog();

        $arr_contacts = [];	

        $contact = new \XeroAPI\XeroPHP\Models\Accounting\Contact;
        $contact->setContactStatus('ACTIVE');
        $contact->setName($data['name']);
        $contact->setEmailAddress($data['email']);
        array_push($arr_contacts, $contact);

        $contacts = new \XeroAPI\XeroPHP\Models\Accounting\Contacts;
$contacts->setContacts($arr_contacts);
        try{
            $dataa= $this->xeroInstance();
            $new = $dataa['api_instance']->createContacts($dataa['xero_tenant_id'],$contacts);
        } catch (\XeroAPI\XeroPHP\ApiException $exception) {
            print_r($exception);
            exit();
        } 
        // $sxml = simplexml_load_string($res->response_body, 'SimpleXMLElement', LIBXML_NOCDATA);
        if($new->getContacts()[0]->getHasValidationErrors()){

            print_r($new->getContacts()[0]->getValidationErrors()[0]->getMessage());exit;
        }
         $contact_id = $new->getContacts()[0]->getContactId();
         
        
        //$roleSelected = Roles::where('id', $role)->first();
        
        $insert_id = User::create([
            'name' => $data['name'],
            'Xero_id' => $contact_id,
            'restaurant_id' => $data['restaurant_id'],
            'role_id' => $data['role_id'],
            'phone_number' => $data['phone_number'],
            'badge_number' => $data['badge_number'],
            'id_number' => $data['id_number'],
            'complementary_number' => $data['complementary_number'],
            'complementary_amount' => $data['complementary_amount'],
            'discount' => $data['discount'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_type' => $rname,
            'dashboard_view' => $permission->dashboard_view,
'menu_setup_view'=> $permission->menu_setup_view,
'major_groups_manager_view'=> $permission->major_groups_manager_view,
'major_groups_manager_add'=> $permission->major_groups_manager_add,
'major_groups_manager_delete'=> $permission->major_groups_manager_delete,
'major_groups_manager_edit'=> $permission->major_groups_manager_edit,
'sub_major_groups_manager_view'=> $permission->sub_major_groups_manager_view,
'sub_major_groups_manager_edit'=> $permission->sub_major_groups_manager_edit,
'sub_major_groups_manager_add'=> $permission->sub_major_groups_manager_add,
'sub_major_groups_manager_delete'=> $permission->sub_major_groups_manager_delete,
'menu_item_groups_edit'=> $permission->menu_item_groups_edit,
'menu_item_groups_view'=> $permission->menu_item_groups_view,
'menu_item_groups_add'=> $permission->menu_item_groups_add,
'menu_item_groups_delete'=> $permission->menu_item_groups_delete,
'offers_view'=> $permission->offers_view,
'offers_edit'=> $permission->offers_edit,
'offers_add'=> $permission->offers_add,
'offers_delete'=> $permission->offers_delete,
'family_groups_view'=> $permission->family_groups_view,
'family_groups_edit'=> $permission->family_groups_edit,
'family_groups_add'=> $permission->family_groups_add,
'family_groups_delete'=> $permission->family_groups_delete,
'alcoholic_family_groups_view'=> $permission->alcoholic_family_groups_view,
'alcoholic_family_groups_edit'=> $permission->alcoholic_family_groups_edit,
'alcoholic_family_groups_add'=> $permission->alcoholic_family_groups_add,
'alcoholic_family_groups_delete'=> $permission->alcoholic_family_groups_delete,
'alcoholic_sub_family_groups_view'=> $permission->alcoholic_sub_family_groups_view,
'alcoholic_sub_family_groups_edit'=> $permission->alcoholic_sub_family_groups_edit,
'alcoholic_sub_family_groups_add'=> $permission->alcoholic_sub_family_groups_add,
'alcoholic_sub_family_groups_delete'=> $permission->alcoholic_sub_family_groups_delete,
'menu_Item_view'=> $permission->menu_Item_view,
'general_item_view'=> $permission->general_item_view,
'general_item_edit'=> $permission->general_item_edit,
'general_item_add'=> $permission->general_item_add,
'general_item_delete'=> $permission->general_item_delete,
'offer_themes_nights_view'=> $permission->offer_themes_nights_view,
'offer_themes_nights_edit'=> $permission->offer_themes_nights_edit,
'offer_themes_nights_add'=> $permission->offer_themes_nights_add,
'offer_themes_nights_delete'=> $permission->offer_themes_nights_delete,
'app_users_view'=> $permission->app_users_view,
'app_users_addamounttowallet'=> $permission->app_users_addamounttowallet,
'manage_orders_view'=> $permission->manage_orders_view,
'prepaid_orders_view'=> $permission->prepaid_orders_view,
'postpaid_orders_view'=> $permission->postpaid_orders_view,
'open_orders_view'=> $permission->open_orders_view,
'open_orders_transfer'=> $permission->open_orders_transfer,
'open_orders_add'=> $permission->open_orders_add,
'open_orders_delete'=> $permission->open_orders_delete,
'void_item_view'=> $permission->void_item_view,
'void_item_accept'=> $permission->void_item_accept,
'void_item_reject'=> $permission->void_item_reject,
'bills_view'=> $permission->bills_view,
'bills_add'=> $permission->bills_add,
'bills_edit'=> $permission->bills_edit,
'bills_delete'=> $permission->bills_delete,
'generate_bills_view'=> $permission->generate_bills_view,
'master_bills_view'=> $permission->master_bills_view,
'master_bills_close'=> $permission->master_bills_close,
'master_bills_discount'=> $permission->master_bills_discount,
'master_bills_delete'=> $permission->master_bills_delete,
'closed_orders_view'=> $permission->closed_orders_view,
'closed_orders_payment_view'=> $permission->closed_orders_payment_view,
'closed_orders_payment_reprints'=> $permission->closed_orders_payment_reprints,
'complete_orders_view'=> $permission->complete_orders_view,
'system_setup_view'=> $permission->system_setup_view,
'advertisements_view'=> $permission->advertisements_view,
'advertisements_edit'=> $permission->advertisements_edit,
'advertisements_add'=> $permission->advertisements_add,
'advertisements_delete'=> $permission->advertisements_delete,
'social_links_view'=> $permission->social_links_view,
'social_links_edit'=> $permission->social_links_edit,
'reservation_email_edit'=> $permission->reservation_email_edit,
'reservation_email_view'=> $permission->reservation_email_view,
'reservation_email_add'=> $permission->reservation_email_add,
'reservation_email_delete'=> $permission->reservation_email_delete,
'payent_method_view'=> $permission->payent_method_view,
'payent_method_add'=> $permission->payent_method_add,
'payent_method_edit'=> $permission->payent_method_edit,
'payent_method_delete'=> $permission->payent_method_delete,
'app_custom_pages_view'=> $permission->app_custom_pages_view,
'app_custom_pages_edit'=> $permission->app_custom_pages_edit,
'restaurants_view'=> $permission->restaurants_view,
'restaurants_edit'=> $permission->restaurants_edit,
'restaurants_add'=> $permission->restaurants_add,
'restaurants_delete'=> $permission->restaurants_delete,
'print_classes_view'=> $permission->print_classes_view,
'print_classes_edit'=> $permission->print_classes_edit,
'print_classes_add'=> $permission->print_classes_add,
'print_classes_delete'=> $permission->print_classes_delete,
'take_away_view'=> $permission->take_away_view,
'take_away_edit'=> $permission->take_away_edit,
'take_away_add'=> $permission->take_away_add,
'take_away_delete'=> $permission->take_away_delete,
'employees_view'=> $permission->employees_view,
'employees_edit'=> $permission->employees_edit,
'employees_add'=> $permission->employees_add,
'employees_delete'=> $permission->employees_delete,
'print_class_users_view'=> $permission->print_class_users_view,
'print_class_users_add'=> $permission->print_class_users_add,
'print_class_users_edit'=> $permission->print_class_users_edit,
'print_class_users_delete'=> $permission->print_class_users_delete,
'print_class_users_change_password'=> $permission->print_class_users_change_password,
'employees_change_password'=> $permission->employees_change_password,
'employees_assign_table' => $permission->employees_assign_table,
'table_managers_view'=> $permission->table_managers_view,
'table_managers_edit'=> $permission->table_managers_edit,
'table_managers_add'=> $permission->table_managers_add,
'table_managers_delete'=> $permission->table_managers_delete,
'tax_manager_view'=> $permission->tax_manager_view,
'tax_manager_add'=> $permission->tax_manager_add,
'tax_manager_edit'=> $permission->tax_manager_edit,
'tax_manager_delete'=> $permission->tax_manager_delete,
'settings_view'=> $permission->settings_view,
'settings_edit'=> $permission->settings_edit,
'condiments_view'=> $permission->condiments_view,
'member_view'=> $permission->member_view,
'member_edit'=> $permission->member_edit,
'member_add'=> $permission->member_add,
'member_delete'=> $permission->member_delete,
'groups_view'=> $permission->groups_view,
'groups_edit'=> $permission->groups_edit,
'groups_add'=> $permission->groups_add,
'groups_delete'=> $permission->groups_delete,
'roles_view'=> $permission->roles_view,
'roles_edit'=> $permission->roles_edit,
'roles_add'=> $permission->roles_add,
'roles_delete'=> $permission->roles_delete,
'roles_permissions'=> $permission->roles_permissions,
'recipes_view'=> $permission->recipes_view,
'WEBACCOUNTING_ERP_view'=> $permission->WEBACCOUNTING_ERP_view,
'feedback_view'=> $permission->feedback_view,
'feedback_orders'=> $permission->feedback_orders,
'feedback_restaurants'=> $permission->feedback_restaurants,
'reservations_view'=> $permission->reservations_view,
'reservations_edit'=> $permission->reservations_edit,
'reservations_delete'=> $permission->reservations_delete,
'beer_delivery_and_keg_setup_view'=> $permission->beer_delivery_and_keg_setup_view,
'beer_delivery_and_keg_setup_edit'=> $permission->beer_delivery_and_keg_setup_edit,
'beer_delivery_and_keg_setup_add'=> $permission->beer_delivery_and_keg_setup_add,
'beer_delivery_and_keg_setup_delete'=> $permission->beer_delivery_and_keg_setup_delete,
'beer_and_keg_sub_major_group_view'=> $permission->beer_and_keg_sub_major_group_view,
'beer_and_keg_sub_major_group_edit'=> $permission->beer_and_keg_sub_major_group_edit,
'beer_and_keg_sub_major_group_add'=> $permission->beer_and_keg_sub_major_group_add,
'beer_and_keg_sub_major_group_delete'=> $permission->beer_and_keg_sub_major_group_delete,
'delivery_family_groups_view'=> $permission->delivery_family_groups_view,
'delivery_family_groups_edit'=> $permission->delivery_family_groups_edit,
'delivery_family_groups_add'=> $permission->delivery_family_groups_add,
'delivery_family_groups_delete'=> $permission->delivery_family_groups_delete,
'delivery_sub_family_groups_view'=> $permission->delivery_sub_family_groups_view,
'delivery_sub_family_groups_edit'=> $permission->delivery_sub_family_groups_edit,
'delivery_sub_family_groups_add'=> $permission->delivery_sub_family_groups_add,
'delivery_sub_family_groups_delete'=> $permission->delivery_sub_family_groups_delete,
'delivery_items_view'=> $permission->delivery_items_view,
'delivery_items_edit'=> $permission->delivery_items_edit,
'delivery_items_add'=> $permission->delivery_items_add,
'delivery_items_delete'=> $permission->delivery_items_delete,
'reports_view'=> $permission->reports_view,
'reports_payment_sales_summary'=> $permission->reports_payment_sales_summary,
'reports_menu_item_general_sales'=> $permission->reports_menu_item_general_sales,
'reports_menu_tem_general_sales_with_plu'=> $permission->reports_menu_tem_general_sales_with_plu,
'reports_menu_item_general_sales_without_plu'=> $permission->reports_menu_item_general_sales_without_plu,
'reports_family_group_sales_with_gl'=> $permission->reports_family_group_sales_with_gl,
'reports_family_group_sales'=> $permission->reports_family_group_sales,
'reports_menu_item_group_sales'=> $permission->reports_menu_item_group_sales,
'reports_major_group_sales'=> $permission->reports_major_group_sales,
'reports_waiter_with_family_groups'=> $permission->reports_waiter_with_family_groups,
'reports_condiments_sales_with_plu'=> $permission->reports_condiments_sales_with_plu,
'reports_get_discount_reports'=> $permission->reports_get_discount_reports,
'reports_get_cashier_reports'=> $permission->reports_get_cashier_reports,
'get_discount_reports_with_orders'=> $permission->get_discount_reports_with_orders,
'reports_get_complementary_reports_with_orders'=> $permission->reports_get_complementary_reports_with_orders,
'reports_get_cancelled_orders_reports'=> $permission->reports_get_cancelled_orders_reports,
'reports_wallet_ledger_entries'=> $permission->reports_wallet_ledger_entries,
'gl_view'=> $permission->gl_view,
'gl_add'=> $permission->gl_add,
'plu_view'=> $permission->plu_view,
'plu_add'=> $permission->plu_add,
'gl_edit'=> $permission->gl_edit,
'gl_delete'=> $permission->gl_delete,
'plu_edit'=> $permission->plu_edit,
'plu_delete'=> $permission->plu_delete,
'reports_void_items'=> $permission->reports_void_items,
'reports_payment_sales_summary1'=> $permission->reports_payment_sales_summary1

        ])->id;
        // dd(DB::getQueryLog());
        // $this->validators($request->all());
        //return $request;

        //$check = User::where('role_id', $role)->first();

        //$user = $this->createEmplo($data);

        // if ($check) {
        //     //print_r($check);exit;
        //     $selectedRole = User::where('id', $user)->first();

        //     $selectedRole->dashboard_view = $check->dashboard_view;

        //     $selectedRole->menu_setup_view = $check->menu_setup_view;

        //     $selectedRole->major_groups_manager_view = $check->major_groups_manager_view;

        //     $selectedRole->sub_major_groups_manager_view = $check->sub_major_groups_manager_view;

        //     $selectedRole->menu_item_groups_view = $check->menu_item_groups_view;

        //     $selectedRole->offers_view = $check->offers_view;

        //     $selectedRole->family_groups_view = $check->family_groups_view;

        //     $selectedRole->alcoholic_family_groups_view = $check->alcoholic_family_groups_view;

        //     $selectedRole->alcoholic_sub_family_groups_view = $check->alcoholic_sub_family_groups_view;

        //     $selectedRole->menu_Item_view = $check->menu_Item_view;

        //     $selectedRole->general_item_view = $check->general_item_view;

        //     $selectedRole->offer_themes_nights_view = $check->offer_themes_nights_view;

        //     $selectedRole->gl_view = $check->gl_view;

        //     $selectedRole->plu_view = $check->plu_view;

        //     $selectedRole->beer_delivery_and_keg_setup_view = $check->beer_delivery_and_keg_setup_view;

        //     $selectedRole->beer_and_keg_sub_major_group_view = $check->beer_and_keg_sub_major_group_view;

        //     $selectedRole->delivery_family_groups_view = $check->delivery_family_groups_view;

        //     $selectedRole->delivery_family_groups_view = $check->delivery_family_groups_view;

        //     $selectedRole->delivery_sub_family_groups_view = $check->delivery_sub_family_groups_view;

        //     $selectedRole->delivery_items_view = $check->delivery_items_view;

        //     $selectedRole->app_users_view = $check->app_users_view;

        //     $selectedRole->manage_orders_view = $check->manage_orders_view;

        //     $selectedRole->prepaid_orders_view = $check->prepaid_orders_view;

        //     $selectedRole->complete_orders_view = $check->complete_orders_view;

        //     $selectedRole->closed_orders_view = $check->closed_orders_view;

        //     $selectedRole->open_orders_view = $check->open_orders_view;

        //     $selectedRole->closed_orders_payment_view = $check->closed_orders_payment_view;

        //     $selectedRole->bills_view = $check->bills_view;

        //     $selectedRole->generate_bills_view = $check->generate_bills_view;

        //     $selectedRole->master_bills_view = $check->master_bills_view;

        //     $selectedRole->WEBACCOUNTING_ERP_view = $check->WEBACCOUNTING_ERP_view;

        //     $selectedRole->reservations_view = $check->reservations_view;

        //     $selectedRole->feedback_view = $check->feedback_view;

        //     $selectedRole->feedback_orders = $check->feedback_orders;

        //     $selectedRole->feedback_restaurants = $check->feedback_restaurants;

        //     $selectedRole->reports_view = $check->reports_view;

        //     $selectedRole->reports_payment_sales_summary = $check->reports_payment_sales_summary;

        //     $selectedRole->reports_menu_item_general_sales = $check->reports_menu_item_general_sales;
        //     ////////////////////////
        //     $selectedRole->reports_menu_tem_general_sales_with_plu = $check->reports_menu_tem_general_sales_with_plu;

        //     $selectedRole->reports_menu_item_general_sales_without_plu = $check->reports_menu_item_general_sales_without_plu;

        //     $selectedRole->reports_family_group_sales_with_gl = $check->reports_family_group_sales_with_gl;

        //     $selectedRole->reports_menu_item_group_sales = $check->reports_menu_item_group_sales;

        //     $selectedRole->reports_major_group_sales = $check->reports_major_group_sales;

        //     $selectedRole->reports_waiter_with_family_groups = $check->reports_waiter_with_family_groups;

        //     $selectedRole->reports_condiments_sales_with_plu = $check->reports_condiments_sales_with_plu;

        //     $selectedRole->reports_get_discount_reports = $check->reports_get_discount_reports;

        //     $selectedRole->reports_get_cashier_reports = $check->reports_get_cashier_reports;

        //     $selectedRole->get_discount_reports_with_orders = $check->get_discount_reports_with_orders;

        //     $selectedRole->reports_get_complementary_reports_with_orders = $check->reports_get_complementary_reports_with_orders;

        //     $selectedRole->reports_get_cancelled_orders_reports = $check->reports_get_cancelled_orders_reports;

        //     $selectedRole->reports_family_group_sales = $check->reports_family_group_sales;

        //     $selectedRole->reports_wallet_ledger_entries = $check->reports_wallet_ledger_entries;

        //     $selectedRole->postpaid_orders_view = $check->postpaid_orders_view;

        //     ////////////////////////////////////// System Setup ///////////////////////////////////////////

        //     $selectedRole->system_setup_view = $check->system_setup_view;

        //     $selectedRole->advertisements_view = $check->advertisements_view;

        //     $selectedRole->social_links_view = $check->social_links_view;

        //     $selectedRole->reservation_email_view = $check->reservation_email_view;

        //     $selectedRole->payent_method_view = $check->payent_method_view;

        //     $selectedRole->app_custom_pages_view = $check->app_custom_pages_view;

        //     $selectedRole->restaurants_view = $check->restaurants_view;

        //     $selectedRole->print_classes_view = $check->print_classes_view;

        //     $selectedRole->take_away_view = $check->take_away_view;

        //     $selectedRole->employees_view = $check->employees_view;

        //     $selectedRole->print_class_users_view = $check->print_class_users_view;

        //     $selectedRole->table_managers_view = $check->table_managers_view;

        //     $selectedRole->tax_manager_view = $check->tax_manager_view;

        //     $selectedRole->settings_view = $check->settings_view;

        //     $selectedRole->condiments_view = $check->condiments_view;

        //     $selectedRole->member_view = $check->member_view;

        //     $selectedRole->groups_view = $check->groups_view;

        //     $selectedRole->roles_view = $check->roles_view;

        //     $selectedRole->save();

        // } else {

        //     //echo "in else";exit;
        //     if ($roleSelected->name == 'Cashier') {

        //         $selectedRole = User::where('id', $user)->first();

        //         $selectedRole->manage_orders_view = "on";

        //         $selectedRole->postpaid_orders_view = "on";

        //         $selectedRole->complete_orders_view = "on";

        //         $selectedRole->closed_orders_view = "on";

        //         $selectedRole->open_orders_view = "on";

        //         $selectedRole->closed_orders_payment_view = "on";

        //         $selectedRole->bills_view = "on";

        //         $selectedRole->generate_bills_view = "on";

        //         $selectedRole->master_bills_view = "on";

        //         $selectedRole->reports_view = "on";

        //         $selectedRole->reports_get_cashier_reports = "on";

        //         $selectedRole->system_setup_view = "on";

        //         $selectedRole->employees_view = "on";

        //         $selectedRole->tax_manager_view = "on";

        //         $selectedRole->save();

        //     }

        // }

        //return $user;

        Session::flash('message', 'We have sent you a verification email!');

        return redirect()->to('/admin/employees')->with('message', 'Employee Created!');

    }
    public function deleteEmployee($id)
    {   $user =User::where('id', $id)->first();
 

        $contact = new \XeroAPI\XeroPHP\Models\Accounting\Contact;



$arr_contacts=[];


        if($user->Xero_id != null){
            $contact->setContactStatus(\XeroAPI\XeroPHP\Models\Accounting\Contact::CONTACT_STATUS_ARCHIVED);
        
        $contact->setName($user->name);
       


        

try{
    $dataa= $this->xeroInstance();
    $new = $dataa['api_instance']->updateContact($dataa['xero_tenant_id'],$user->Xero_id,$contact);
} catch (\XeroAPI\XeroPHP\ApiException $exception) {
    print_r($exception);
    exit();
} 

        
        }
        
        
        $delete = User::where('id', $id)->delete();
        if ($delete) {
            Session::flash('message', 'Employee Deleted Successfully!');

            return back()->with('message', 'Employee Deleted Successfully!');
        }
    }

//     public function editprintClassUsers($id){

//         $existPrintClass = PrintClasses::where('id', $id)->first();
    // //return $existAdvertisement;
    //         return view('admin.systemsetup.admin_edit_print_class',compact('existPrintClass'));

//     }
    //     public function updateprintClassUsers(Request $request , $id){

//         $data=$request->all();

//         $existAdvertisement = PrintClasses::where('id', $id)->first();

//         if (!empty($existAdvertisement)) {

//             $profile=$existAdvertisement->update($data);

//             Session::flash('message', 'We have sent you a verification email!');

//             return redirect()->to('/admin/print-classes')->with('message','Print Class Updated!');

//         }
    public function userProfile(){
        $user= User::where('id', \Auth::user()->id)->first();
        return view('admin.user_profile',['user'=>$user]);  
    }
    public function userProfileShow(){
          $user= User::where('id', \Auth::user()->id)->first();
          if($user->image == null || $user->image == ''){
              $user->image = 'user.jpg';
          }
        return view('admin.edit_user_profile',['user'=>$user]);  
    }
    public function userprofileSave(Request $request){
        
        
        $user= User::where('id', \Auth::user()->id)->first();
        if($request->hasFile('user_file')){
            $imageName = time() . '.' . $request->user_file->getClientOriginalExtension();
            $image_uploaded = $request->user_file->move(public_path('img'), $imageName);
            $user->image = $imageName;
        }
        $date = str_replace('/', '-', $request->birth_date);
        $user->birth_date = date('Y-m-d',strtotime($date)) ;
       
        $user->gender =  $request->gender;
        $user->address =  $request->address;
        $user->state =  $request->state;
        $user->pin_code =  $request->pincode;
        $user->country =  $request->country;
         $user->phone_number =  $request->phone_number;
        $user->save();
         return back()->with('message', 'Employee Updated Successfully!');
        
    }

    public function xeroInstance(){


        $xero=xero::find(1);
        $xeroTenantId = $xero->tenant_id;
      
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
          'clientId'                => '7926ACDEA0224C1585A35CD1DCB79BF9',   
          'clientSecret'            => '2wLASnD9-NfmiQBIQ_YGZikLexT-SN-qcgGXKtDPNemzUXFS',
          'redirectUri'             => 'https://smith.test/callback', 
          'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
          'urlAccessToken'          => 'https://identity.xero.com/connect/token',
          'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);
      
              // echo "xero tendent id"."<br>";
              // echo $xeroTenantId."<br>";
              $refresh_token = $xero->refresh_token;
             
              
          $newAccessToken = $provider->getAccessToken('refresh_token', [
            'refresh_token' => $refresh_token,
          ]);
          
        $config = \XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken( $newAccessToken );
         $apiInstance = new \XeroAPI\XeroPHP\Api\AccountingApi(
            new \GuzzleHttp\Client(),
            $config
        );

        return array('xero_tenant_id'=>$xeroTenantId,'api_instance'=>$apiInstance);

    }
}
