<?php
namespace App\Http\Controllers\Admin;

use App\SystemSetup\Restaurants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\ManageOrders\Payments;
use App\SystemSetup\PaymentMethod;
use App\SystemSetup\PrintClasses;
use App\GeneralItems;
use App\FamilyGroup;
use App\Tax;
use App\VoidItems;
use App\SalesIngredient;
use App\PostPaidOrders;
use App\PLUNumber;
use App\MajorGroup;
use App\SubMajorGroups;
use App\MenuItemsGroup;
use App\ManageOrders\BillMaster;
use App\OrderItems;
use App\Recipe;
use App\Ingredients;
use App\RawIngredient;
use App\Purchases;
use App\User;
use App\stock;
use App\PhysicalStockCS;
use App\PhysicalStockPS;
use App\PhysicalStockCSAdjustment;
use App\PhysicalStockPSAdjustment;

class AdminReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function paymentSalesSummary()
    {

        $total = 0;
        $restaurants = Restaurants::all();

        $results = DB::select('select count(id) as count,sum(total_amount) as sum ,payment_method from payments GROUP BY payment_method');

        $methods = PaymentMethod::all();

        $file = fopen("paymentSales.csv", "w");
        fputcsv($file, array(
            'Payment Sales Summary',
            '',
            ''
        ));
        // fputcsv($file, array('','','','','','',''));
        // fputcsv($file, array('','','','','','',''));
        fputcsv($file, array(
            'All Time Report',
            '',
            ''
        ));

        // fputcsv($file, array('','','','',''));
        fputcsv($file, array(
            'Title ',
            'No. Of Transactions',
            'Total Amount'
        ));
        // fputcsv($file, array('','','','',''));
        foreach ($results as $result)
        {
            foreach ($methods as $method)
            {
                if ($method->id == $result->payment_method)
                {
                    fputcsv($file, array(
                        $method->name,
                        $result->count,
                        $result->sum
                    ));
                    break;
                }
            }
            $total += $result->sum;
        }
        // fputcsv($file, array('','','','',''));
        fputcsv($file, array(
            'Total',
            '',
            $total
        ));
        fclose($file);

        //return $results;
        return view('admin.reports.admin_payments_sales_summary', compact('restaurants', 'results', 'methods'));
    }
    public function menuitemgeneralsales()
    {

        $results = DB::select('select general_item_id,count(general_item_id) as count,sum(quantity) as quantity , sum(price) as gross from order_items GROUP BY general_item_id');
        $items = GeneralItems::all();
        //return $items;
        $groups = FamilyGroup::all();
        $taxes = Tax::all();

        return view('admin.reports.admin_menu_item_general_sales', compact('results', 'items', 'groups', 'taxes'));
    }
    public function menuitemgeneralsaleswithplu()
    {

        $results = DB::select('select general_item_id,count(general_item_id) as count,sum(quantity) as quantity , sum(price) as gross from order_items GROUP BY general_item_id');
        $items = GeneralItems::all();
        //return $items;
        $groups = FamilyGroup::all();
        $taxes = Tax::all();
        $plues = PLUNumber::all();

        return view('admin.reports.admin_menu_item_general_sales_with_plu', compact('results', 'items', 'groups', 'taxes', 'plues'));
    }
    public function menuitemgeneralsaleswithoutplu()
    {

        $results = DB::select('select general_item_id,count(general_item_id) as count,sum(quantity) as quantity , sum(price) as gross from order_items GROUP BY general_item_id');
        $items = GeneralItems::all();
        //return $items;
        $groups = FamilyGroup::all();
        $taxes = Tax::all();

        return view('admin.reports.admin_menu_item_general_sales_without_plu', compact('results', 'items', 'groups', 'taxes'));
    }
    public function familygroupsales()
    {
        $results = DB::select('select general_item_id,count(general_item_id) as count,sum(quantity) as quantity , sum(price) as gross from order_items GROUP BY general_item_id');
        $items = GeneralItems::all();
        //return $items;
        $groups = FamilyGroup::all();
        $taxes = Tax::all();

        return view('admin.reports.admin_family_group_sales', compact('results', 'items', 'groups', 'taxes'));
    }
    public function familygroupsaleswithgl()
    {

        return view('admin.reports.admin_family_group_sales_with_gl');
    }
    public function menuitemgroupsales()
    {

        return view('admin.reports.admin_menu_item_group_sales');
    }
    public function majorgroupsales()
    {

        return view('admin.reports.admin_major_group_sales');
    }
    public function waiterwithfamilygroup()
    {

        return view('admin.reports.admin_waiter_with_family_group');
    }
    public function condimentsalesreportwithplu()
    {

        return view('admin.reports.admin_condiment_sales_report_with_plu');
    }
    public function discountreports()
    {

        return view('admin.reports.admin_discount_reports');
    }
    public function reports()
    {

        return view('admin.reports.admin_reports');
    }
    public function discountreportswithorder()
    {

        return view('admin.reports.admin_discount_reports_with_order');
    }
    public function complementaryreportswithorder()
    {

        return view('admin.reports.admin_complementary_reports');
    }
    public function cancelledreports()
    {

        return view('admin.reports.admin_cancelled_reports');
    }
    public function walletledgerentries()
    {

        return view('admin.reports.admin_wallet_ledger_entries');
    }

    public function openOrders()
    {

        // $orders = DB::select("SELECT post_paid_orders.* ,restaurants.name FROM `post_paid_orders` INNER join restaurants on post_paid_orders.restaurant= restaurants.id where status != 'DELIVERED' and  Date(post_paid_orders.created_at) = '".date("Y-m-d")."'");
        // $order_items = DB::select("select order_items.*,print_classes.name as print_class_name from order_items left join print_classes on order_items.print_class_id = print_classes.id where order_items.cooking_status = 'DELIVERED' and Date(order_items.created_at) = '".date("Y-m-d")."'");
        $start_date = '';
        $end_date = '';

        $data['details'] = DB::select("SELECT post_paid_orders.* ,restaurants.name FROM `post_paid_orders` INNER join restaurants on post_paid_orders.restaurant= restaurants.id where status != 'DELIVERED' and  Date(post_paid_orders.created_at) = '" . date("Y-m-d") . "'");

        foreach ($data['details'] as $key => $detail)
        {
            $data['order_it'][$key] = DB::select("select order_items.*,print_classes.name as print_class_name from order_items join print_classes on order_items.print_class_id = print_classes.id where Date(order_items.created_at) = '" . date("Y-m-d") . "' and order_items.post_paid_orders_id = $detail->id ");
        }

        $file = fopen("open_order_report.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            'Open Order Report'
        ));

        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'Today Report',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'ID',
            'Date_Time',
            'Table NO',
            'Item Description',
            'Customer Instruction',
            'Waiter',
            'Restaurant',
            'Total Amount',
            'Status',
            'Print class name'
        ));

        $total = 0;

        foreach ($data['details'] as $key => $detail)
        {
            $ord = DB::select("select * from order_items where '".$detail->id."' = post_paid_orders_id");
             
             
              if(isset($ord[0])){
                 
                 $detail->comment=$ord[0]->comments;
                 
             }
            $items_description = '';
            $print_class = '';
            foreach ($data['order_it'][$key] as $k => $order_itm)
            {
                $items_description .= "item: " . $order_itm->name . ' ' . "QTY: " . $order_itm->quantity . ", ";
                $print_class = $order_itm->print_class_name;
            }
            fputcsv($file, array(
                $detail->id,
                $detail->date_time,
                $detail->table_no,
                $items_description,
                $detail->item_description,
                $detail->waiter,
                $detail->name,
                $detail->total_amount,
                $detail->status,
                $print_class
            ));
            $total += $detail->total_amount;
        }
        // foreach($orders as $order){
        //     $items_description = "";
        //     $print_class_name = "";
        // foreach($order_items as $order_item){
        //  if($order_item->post_paid_orders_id == $order->id){
        //         //alert(result.order_items[j]['name']);
        //         $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity .", " ;
        //       $print_class_name = $order_item->print_class_name;
        //         }
        //     }
        //   fputcsv($file, array($order->id,$order->date_time,$order->table_no,$items_description,$order->waiter,$order->name,$order->total_amount,$order->status,$print_class_name));
        //         $total += $order->total_amount;
        // }
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            'Total',
            $total
        ));
        fclose($file);

        return view('admin.reports.admin_open_orders_reports', compact('start_date', 'end_date', 'data'));
    }

    public function openOrderFilter(Request $request)
    {

        $data = $request->all();
        $start_date = $data['start-date'];
        $end_date = $data['end-date'];

        //$orders = DB::select("SELECT post_paid_orders.*,restaurants.name FROM `post_paid_orders` INNER join restaurants on post_paid_orders.restaurant= restaurants.id where post_paid_orders.created_at between '".$data['start-date']."' and '".$data['end-date']."'");
        //$order_items = DB::select("select * from order_items where cooking_status = 'DELIVERED' and created_at between '".$data['start-date']."' and '".$data['end-date']."'");
        $data['details'] = DB::select("SELECT post_paid_orders.* ,restaurants.name FROM `post_paid_orders` INNER join restaurants on post_paid_orders.restaurant= restaurants.id where status != 'DELIVERED' and  post_paid_orders.created_at between '" . $data['start-date'] . "' and '" . $data['end-date'] . "'");

        foreach ($data['details'] as $key => $detail)
        {
            $data['order_it'][$key] = DB::select("select order_items.*,print_classes.name as print_class_name from order_items join print_classes on order_items.print_class_id = print_classes.id where order_items.created_at between '" . $data['start-date'] . "' and '" . $data['end-date'] . "' and order_items.post_paid_orders_id = $detail->id ");
        }
        $file = fopen("open_order_report.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            'Open Order Report'
        ));

        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'Dated',
            'From',
            $start_date,
            'To',
            $end_date,
            '',
            ''
        ));
        fputcsv($file, array(
            'ID',
            'Date_Time',
            'Table NO',
            'Item Description',
              'Customer Instruction',
            'Waiter',
            'Restaurant',
            'Total Amount',
            'Status'
        ));

        $total = 0;
        $tt = 0;

        foreach ($data['details'] as $key => $detail)
        {
              $ord = DB::select("select * from order_items where '".$detail->id."' = post_paid_orders_id");
              
             
             if(isset($ord[0])){
                 
                 $detail->comment=$ord[0]->comments;
                 
             }
                 

     
            $items_description = '';
            $print_class = '';
            foreach ($data['order_it'][$key] as $k => $order_itm)
            {
                $items_description .= "item: " . $order_itm->name . ' ' . "QTY: " . $order_itm->quantity . ", ";
                $print_class = $order_itm->print_class_name;
            }
            fputcsv($file, array(
                $detail->id,
                $detail->date_time,
                $detail->table_no,
                $items_description,
                $detail->item_description,
                $detail->waiter,
                $detail->name,
                $detail->total_amount,
                $detail->status,
                $print_class
            ));
            $total += $detail->total_amount;
        }
        // foreach($orders as $order){
        //     $tot = 0;
        //     $items_description = "";
        // foreach($order_items as $order_item){
        //  if($order_item->post_paid_orders_id == $order->id){
        //         //alert(result.order_items[j]['name']);
        //         $items_description .= "item: " . $order_item->name .  ' ' . "QTY: " . $order_item->quantity .", " ;
        //         $tot += $order_item->quantity*$order_item->price;
        //         }
        //     }
        //   fputcsv($file, array($order->id,$order->date_time,$order->table_no,$items_description,$order->waiter,$order->name,$tot,$order->status));
        //         $total += $order->total_amount;
        //         $tt += $tot;
        // }
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            'Total',
            $total
        ));
        fclose($file);

        return view('admin.reports.admin_open_orders_reports', compact('orders', 'order_items', 'start_date', 'end_date', 'data'));

    }

    public function completedOrders()
    {

        $orders = PostPaidOrders::where('status', 'DELIVERED')->get();

        return view('admin.reports.admin_completeOrders', compact('orders'));

    }

    public function productSales()
    {

        //$results = DB::select("select general_item_id,SUM(quantity*price) as qpsum, name,count(name) as count,sum(quantity) as quantity , sum(price) as gross from order_items where print_class_id <= 7 and date(order_items.created_at) = '".date("Y-m-d")."'  GROUP BY order_items.general_item_id");
        $dis = DB::select("Select SUM(discounted_amount) as dis_amount from bill_masters where date(created_at) = '" . date("Y-m-d") . "'");
        $results = DB::select("select general_item_id,name,count(name) as count,SUM(quantity*price) as qpsum,sum(quantity) as quantity , sum(price) as gross from order_items where Date(order_items.created_at) = '" . date("Y-m-d") . "' GROUP BY order_items.general_item_id");
        $items = GeneralItems::all();
        //return $results;
        $groups = FamilyGroup::all();

        $majors = MajorGroup::all();

        //return $majors;exit;
        $submajors = SubMajorGroups::all();

        $taxes = Tax::all();
        $menuGroups = MenuItemsGroup::all();
        //   return $menuGroups;
        $file = fopen("dailySales.csv", "w");
        //echo $num_rows;
        $count = 0;
        $gross = 0;
        $total = 0;
        $tt = 0;
        $flag = true;
        $sub_item_check = '';
        $family_item_check = '';
        $sales_item_check = '';
        // //exit;
        // if($num_rows>0){
        // $sum_amount=0;
        // $sum_fee=0;
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'The Smith Hotels',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '0705625356'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Product Wise Sales Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'Today Report',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            'Item',
            'Sales QTY',
            'Gross Sales'
        ));
        $file_result = DB::select("SELECT order_items.name as sale_item_name, SUM(order_items.quantity) as sale_quantity,SUM(order_items.quantity*order_items.price) as qpsum, family_groups.menu_item_groups_id,family_groups.name as family_group_item_name,menu_items_groups.name as menu_group_item_name, sub_major_groups.name as sub_major_group_item_name FROM `order_items` inner join general_items on order_items.general_item_id = general_items.id inner join family_groups on family_groups.id = general_items.family_groups_id inner join menu_items_groups on family_groups.menu_item_groups_id = menu_items_groups.id inner join sub_major_groups on sub_major_groups.id = menu_items_groups.sub_major_groups_id where order_items.`print_class_id` <= 7 and Date(order_items.created_at) = '" . date("Y-m-d") . "' group by order_items.`general_item_id` order by sub_major_groups.name, family_groups.name desc");

        foreach ($file_result as $key => $fr)
        {
            if ($fr->sub_major_group_item_name != $sub_item_check)
            {
                fputcsv($file, array(
                    $fr->sub_major_group_item_name
                ));

            }
            $sub_item_check = $fr->sub_major_group_item_name;

            if ($fr->family_group_item_name != $family_item_check)
            {
                fputcsv($file, array(
                    '',
                    $fr->family_group_item_name
                ));
            }

            $family_item_check = $fr->family_group_item_name;

            if ($fr->sale_item_name != $sales_item_check)
            {
                fputcsv($file, array(
                    '',
                    '',
                    $fr->sale_item_name,
                    $fr->sale_quantity,
                    $fr->qpsum
                ));
                $tt += $fr->qpsum;
            }
            $sales_item_check = $fr->sale_item_name;
        }
        //     foreach($majors as $major){
        //         fputcsv($file, array($major->name));
        //         foreach($submajors as $sub){
        //             $subflag = false;
        //             if($major->id == $sub->major_group_name ){
        

        //                 foreach($menuGroups as $menugroup){
        //                     if($menugroup->sub_major_groups_id == $sub->id){
        //                         //fputcsv($file, array($menugroup->name,));
        //                         foreach($groups as $familygroup){
        //                             $flag = false;
        //                             if($menugroup->id == $familygroup->menu_item_groups_id ){
        

        //                                 foreach($items as $item){
        //                                     if($familygroup->id == $item->family_groups_id){
        //                                         //fputcsv($file, array($item->name));
        //                         foreach($results as $result){
        // if($item->id == $result->general_item_id && trim($familygroup->id) == trim($item->family_groups_id)){
        //                     if($subflag == false){
        //                         fputcsv($file, array($sub->name));
        //                 }
        //         if($flag == false){
        //             fputcsv($file, array('',$familygroup->name));
        //     }
        //         $gross = $result->quantity*$item->price;
        //                 $total += $gross;
        //          $tt += $result->qpsum;
        //     fputcsv($file, array('','',$result->name,$result->quantity,$gross,$result->qpsum));
        //                     $flag = true;
        //                     $subflag = true;
        

        //                                             }
        

        //                                         }
        //                                     }
        

        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        

        //         }
        

        //     }
        fputcsv($file, array(
            '',
            '',
            '',
            'Gross Sales',
            $tt
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            'Total Discount',
            $dis[0]->dis_amount
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            'Net Sale',
            $tt - $dis[0]->dis_amount
        ));

        fclose($file);
        $restaurants = Restaurants::all();
        //return $restaurants;
        $start_date = '';
        $end_date = '';
        return view('admin.reports.admin_product_sales_report', compact('results', 'items', 'groups', 'taxes', 'restaurants', 'start_date', 'end_date'));

    }

    public function filterReports(Request $request)
    {

        $start_date = $request->start_date;
        // $start_date = date('Y-m-d', $date) ;
        // $rsDate = date('d-m-Y', $date);
        $end_date = $request->end_date;
        // $end_date = date('Y-m-d', $date);
        // $reDate = date('d-m-Y', $date);
        // $start_time = $request->startTimeSearch.":00";
        // $end_time = $request->endTimeSearch.":00";
        // $start_date = $start_date." ".$start_time ;
        // $end_date = $end_date." ".$end_time ;
        // $rsDate = $rsDate . " " . $start_time;
        // $reDate = $reDate." ".$end_time ;
        $results = DB::select("select general_item_id,name,count(name) as count,SUM(quantity*price) as qpsum,sum(quantity) as quantity , sum(price) as gross from order_items where order_items.created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "' GROUP BY order_items.general_item_id");
        $dis = DB::select("Select SUM(discounted_amount) as dis_amount from bill_masters where created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "'");

        //echo $results->toSql();
        // /return $start_date;
        //return $results;
        $items = GeneralItems::all();
        //return $items;
        $groups = FamilyGroup::all();
        $majors = MajorGroup::all();
        //return $majors;exit;
        $submajors = SubMajorGroups::all();
        $taxes = Tax::all();
        $menuGroups = MenuItemsGroup::all();

        $file = fopen("dailySales.csv", "w");
        //echo $num_rows;
        $count = 0;
        $gross = 0;
        $total = 0;
        $tt = 0;
        $flag = true;
        // //exit;
        // if($num_rows>0){
        // $sum_amount=0;
        // $sum_fee=0;
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'The Smith Hotels',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '0705625356'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Product Wise Sales Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'Report Data Search From ',
            $start_date . "  to  " . $end_date,
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));

        fputcsv($file, array(
            '',
            '',
            'Item',
            'Sales QTY',
            'Gross Sales'
        ));
        foreach ($majors as $major)
        {

            fputcsv($file, array(
                $major->name
            ));

            foreach ($submajors as $sub)
            {
                $subflag = false;
                if ($major->id == $sub->major_group_name)
                {

                    foreach ($menuGroups as $menugroup)
                    {

                        if ($menugroup->sub_major_groups_id == $sub->id)
                        {
                            //fputcsv($file, array($menugroup->name,));
                            foreach ($groups as $familygroup)
                            {

                                $flag = false;

                                if ($menugroup->id == $familygroup->menu_item_groups_id)
                                {

                                    foreach ($items as $item)
                                    {

                                        if ($familygroup->id == $item->family_groups_id)
                                        {

                                            //fputcsv($file, array($item->name));
                                            foreach ($results as $result)
                                            {

                                                if ($item->id == $result->general_item_id && $familygroup->id == $item->family_groups_id)
                                                {

                                                    if ($subflag == false)
                                                    {
                                                        fputcsv($file, array(
                                                            $sub->name
                                                        ));
                                                    }
                                                    if ($flag == false)
                                                    {

                                                        fputcsv($file, array(
                                                            '',
                                                            $familygroup->name
                                                        ));
                                                    }

                                                    $gross = $result->quantity * $item->price;
                                                    $total += $gross;
                                                    $tt += $result->qpsum;
                                                    fputcsv($file, array(
                                                        '',
                                                        '',
                                                        $result->name,
                                                        $result->quantity,
                                                        $result->qpsum
                                                    ));
                                                    $flag = true;
                                                    $subflag = true;

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

        fputcsv($file, array(
            '',
            '',
            '',
            'Gross Sales',
            $tt
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            'Total Discount',
            $dis[0]->dis_amount
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            'Net Sale',
            $tt - $dis[0]->dis_amount
        ));
        fclose($file);

        $array['results'] = $results;
        $array['items'] = $items;

        return view('admin.reports.admin_product_sales_report', compact('results', 'items', 'groups', 'taxes', 'restaurants', 'start_date', 'end_date'));

    }

    public function unpaidByWaiters()
    {

        $bills = BillMaster::whereDate('created_at', date('Y-m-d'))->where('status', 'Un-paid')
            ->get();

        $data['total'] = DB::select('select SUM(total_amount) as tt, waiter_id, waiter_name FROM `bill_masters` where date(created_at) = "' . date('Y-m-d') . '" and status="Un-paid" group by waiter_id');
        $data['waiter'] = array();
        foreach ($data['total'] as $key => $bill)
        {
            $data['waiter'][$key] = DB::select('select * FROM `bill_masters` where date(created_at) = "' . date('Y-m-d') . '" and status="Un-paid" and waiter_id = "' . $bill->waiter_id . '"');
        }

        $file = fopen("unpaid_waiters.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            'UnPaid Waiters Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'All Time Report',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            'Bill Date',
            'Bill No',
            'Bill Amount'
        ));
        $ids = array();
        $allTotal = 0;
        $total1 = 0;
        $j = 0;

        foreach ($data['total'] as $key => $waiter_name)
        {
            fputcsv($file, array(
                $waiter_name->waiter_name
            ));
            foreach ($data['waiter'][$key] as $k => $detail)
            {
                fputcsv($file, array(
                    '',
                    '',
                    $detail->date,
                    $detail->id,
                    $detail->total_amount
                ));
            }

            fputcsv($file, array(
                '',
                '',
                '',
                'Total',
                $waiter_name->tt
            ));
            $total1 += $waiter_name->tt;
        }

        fputcsv($file, array(
            'Total Amount',
            '',
            '',
            '',
            $total1
        ));
        // fputcsv($file, array('','','','Gross Sales',$total));
        fclose($file);
        $start_date = '';
        $end_date = '';
        return view('admin.reports.admin_unpaid_by_waiters', compact('bills', 'start_date', 'end_date'));

    }

    public function unpaidWaitersFilter(Request $request)
    {

        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $csvbills = DB::select("select * FROM bill_masters where status='Un-paid' AND created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "'  ORDER BY waiter_id ASC");
        //$sum = BillMaster::where('status','Un-paid')->whereBetween('created_at',[$start_date, $end_date])->sum('total_amount');
        $bills = BillMaster::where('status', 'Un-paid')->whereBetween('created_at', [$start_date, $end_date])->get();

        $data['total'] = DB::select("select SUM(total_amount) as tt, waiter_id, waiter_name FROM `bill_masters` where created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "' and status='Un-paid' group by waiter_id");
        $data['waiter'] = array();
        foreach ($data['total'] as $key => $bill)
        {
            $data['waiter'][$key] = DB::select("select * FROM `bill_masters` where created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "' and status='Un-paid' and waiter_id = $bill->waiter_id");
        }
        $file = fopen("unpaid_waiters.csv", "w");

        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            'Unpaid By Waiters',
            ''
        ));

        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'Report Data Search From ',
            $start_date . "  to  " . $end_date,
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));

        fputcsv($file, array(
            '',
            '',
            'Bill Date',
            'Bill No',
            'Bill Amount'
        ));

        $ids = array();
        $allTotal = 0;
        $total1 = 0;
        $j = 0;
        foreach ($data['total'] as $key => $waiter_name)
        {
            fputcsv($file, array(
                $waiter_name->waiter_name
            ));
            foreach ($data['waiter'][$key] as $k => $detail)
            {
                fputcsv($file, array(
                    '',
                    '',
                    $detail->date,
                    $detail->id,
                    $detail->total_amount
                ));
            }

            fputcsv($file, array(
                '',
                '',
                '',
                'Total',
                $waiter_name->tt
            ));
            $total1 += $waiter_name->tt;
        }

        fputcsv($file, array(
            'Total Amount',
            '',
            '',
            '',
            $total1
        ));
        //fputcsv($file, array('','','','Gross Sales',$total));
        fclose($file);
        return view('admin.reports.admin_unpaid_by_waiters', compact('bills', 'start_date', 'end_date'));

    }

    public function waiterDetails()
    {

        $orders = DB::select("SELECT users.id,users.name as waiter,order_items.name, sum(order_items.quantity) as quantity ,order_items.price from users INNER JOIN post_paid_orders ON users.id = post_paid_orders.waiter_id INNER JOIN order_items ON order_items.post_paid_orders_id = post_paid_orders.id where date(order_items.created_at) = '" . date("Y-m-d") . "' GROUP BY users.id,order_items.general_item_id ORDER BY waiter_id ASC");

        $ids = array();
        $total = 0;
        $grand_total = 0;
        $file = fopen("waiter_details.csv", "w");
        fputcsv($file, array(
            '',
            '',
            'Waiters Detaild Report',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            'Description',
            'Qty',
            'Total Sales'
        ));

        for ($k = 0;$k < sizeof($orders);$k++)
        {

            $flag = false;

            if (sizeof($ids) == 0)
            {

                fputcsv($file, array(
                    $orders[$k]->waiter
                ));
                array_push($ids, $orders[$k]->waiter);

            }
            for ($i = 0;$i < sizeof($ids);$i++)
            {

                // echo "in for loop";
                if ($ids[$i] == $orders[$k]->waiter)
                {

                    // echo "in if";
                    $flag = true;
                    break;

                }

            }

            if (!$flag)
            {
                fputcsv($file, array(
                    '',
                    '',
                    '',
                    'Total',
                    $total
                ));
                fputcsv($file, array(
                    $orders[$k]->waiter
                ));
                array_push($ids, $orders[$k]->waiter);
                $total = 0;
            }

            fputcsv($file, array(
                '',
                '',
                $orders[$k]->name,
                $orders[$k]->quantity,
                $orders[$k]->quantity * $orders[$k]->price
            ));

            $total += $orders[$k]->quantity * $orders[$k]->price;
            // fputcsv($file, array('','','','Total',$total));
            $grand_total += $orders[$k]->quantity * $orders[$k]->price;
        }

        fputcsv($file, array(
            '',
            '',
            '',
            'Total',
            $total
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            'Grand Total',
            $grand_total
        ));

        fclose($file);

        // return $orders;
        return view('admin.reports.admin_waiter_detailed_reports', ['previousItems' => $orders]);

    }

    public function waiterDetailsFilter(Request $request)
    {

        $start_date = $request->start_date;

        $end_date = $request->end_date;

        $orders = DB::select("SELECT
                                users.id,
                                users.name AS waiter,
                                order_items.name,
                                SUM(order_items.quantity) AS quantity,
                                order_items.price
                            FROM
                                users
                            INNER JOIN post_paid_orders ON users.id = post_paid_orders.waiter_id
                            INNER JOIN order_items ON order_items.post_paid_orders_id = post_paid_orders.id AND order_items.created_at  BETWEEN '" . $start_date . "' AND '" . $end_date . "'
                            GROUP BY
                                users.id,
                                order_items.general_item_id
                            ORDER BY
                                waiter_id ASC");

        $ids = array();
        $total = 0;
        $grand_total = 0;
        $file = fopen("waiter_details.csv", "w");
        fputcsv($file, array(
            '',
            '',
            'Waiters Detaild Report',
            '',
            ''
        ));

        fputcsv($file, array(
            'Report Data Search From ',
            $start_date . "  to  " . $end_date
        ));

        fputcsv($file, array(
            '',
            '',
            'Description',
            'Qty',
            'Total Sales'
        ));

        for ($k = 0;$k < sizeof($orders);$k++)
        {

            $flag = false;

            if (sizeof($ids) == 0)
            {

                fputcsv($file, array(
                    $orders[$k]->waiter
                ));
                array_push($ids, $orders[$k]->waiter);

            }
            for ($i = 0;$i < sizeof($ids);$i++)
            {

                // echo "in for loop";
                if ($ids[$i] == $orders[$k]->waiter)
                {

                    // echo "in if";
                    $flag = true;
                    break;

                }

            }

            if (!$flag)
            {
                fputcsv($file, array(
                    '',
                    '',
                    '',
                    'Total',
                    $total
                ));
                fputcsv($file, array(
                    $orders[$k]->waiter
                ));
                array_push($ids, $orders[$k]->waiter);
                $total = 0;
            }

            fputcsv($file, array(
                '',
                '',
                $orders[$k]->name,
                $orders[$k]->quantity,
                $orders[$k]->quantity * $orders[$k]->price
            ));
            fputcsv($file, array(
                '',
                '',
                '',
                '',
                ''
            ));

            $total += $orders[$k]->quantity * $orders[$k]->price;
            // fputcsv($file, array('','','','Total',$total));
            $grand_total += $orders[$k]->quantity * $orders[$k]->price;
        }

        fputcsv($file, array(
            '',
            '',
            '',
            'Total',
            $total
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            'Grand Total',
            $grand_total
        ));

        fclose($file);

        // return $orders;
        

        return $orders;
    }

    public function quantitySalesFilter(Request $request)
    {

        $start_date = $request->start_date;

        $end_date = $request->end_date;
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        //return $end_date;
        $results = DB::select("select general_item_id,name,count(name) as count,sum(quantity) as quantity , sum(price) as gross from order_items where order_items.created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "' GROUP BY order_items.general_item_id");
        $items = GeneralItems::all();
        //return $items;
        $groups = FamilyGroup::all();
        // $items = GeneralItems::all();
        //return $items;
        //$groups = FamilyGroup::all();
        $majors = MajorGroup::all();
        //return $majors;exit;
        $submajors = SubMajorGroups::all();
        $taxes = Tax::all();
        $menuGroups = MenuItemsGroup::all();

        $file = fopen("overallSales.csv", "w");
        //echo $num_rows;
        $count = 0;
        $gross = 0;
        $total = 0;
        $totalCount = 0;
        $flag = true;
        // //exit;
        // if($num_rows>0){
        // $sum_amount=0;
        // $sum_fee=0;
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'The Smith Hotels',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '0705625356'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Quantity control Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            'Item',
            'Sales QTY'
        ));
        foreach ($majors as $major)
        {

            // fputcsv($file, array($major->name));
            foreach ($submajors as $sub)
            {
                $subflag = false;
                if ($major->id == $sub->major_group_name)
                {

                    foreach ($menuGroups as $menugroup)
                    {

                        if ($menugroup->sub_major_groups_id == $sub->id)
                        {
                            //fputcsv($file, array($menugroup->name,));
                            foreach ($groups as $familygroup)
                            {

                                $flag = false;

                                if ($menugroup->id == $familygroup->menu_item_groups_id)
                                {

                                    foreach ($items as $item)
                                    {

                                        if ($familygroup->id == $item->family_groups_id)
                                        {

                                            foreach ($results as $result)
                                            {

                                                if ($item->id == $result->general_item_id && $familygroup->id == $item->family_groups_id)
                                                {

                                                    if ($subflag == false)
                                                    {
                                                        fputcsv($file, array(
                                                            $sub->name
                                                        ));
                                                    }
                                                    if ($flag == false)
                                                    {

                                                        fputcsv($file, array(
                                                            '',
                                                            $familygroup->name
                                                        ));
                                                    }
                                                    $totalCount += $result->quantity;
                                                    $gross = $result->quantity * $item->price;
                                                    $total += $gross;
                                                    fputcsv($file, array(
                                                        '',
                                                        '',
                                                        $result->name,
                                                        $result->quantity
                                                    ));
                                                    $flag = true;
                                                    $subflag = true;

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }

        }
        fputcsv($file, array(
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            $totalCount
        ));
        fclose($file);

        return view('admin.reports.admin_overall_sales_report', compact('results', 'items', 'groups', 'taxes', 'data'));

    }

    public function overallSales()
    {

        $data['start_date'] = '';
        $data['end_date'] = '';
        // $results = DB::select('select name,count(name) as count,sum(quantity) as quantity , sum(price) as gross from order_items  GROUP BY name');
        

        $results = DB::select("select general_item_id, name,count(name) as count,sum(quantity) as quantity , sum(price) as gross from order_items where print_class_id <= 7 and Date(created_at) = '" . date("Y-m-d") . "' GROUP BY order_items.general_item_id");
        $items = GeneralItems::all();
        //return $results;
        $groups = FamilyGroup::all();

        // $items = GeneralItems::all();
        //return $items;
        //$groups = FamilyGroup::all();
        $majors = MajorGroup::all();
        //return $majors;exit;
        $submajors = SubMajorGroups::all();
        $taxes = Tax::all();
        $menuGroups = MenuItemsGroup::all();

        $file_result = DB::select("SELECT order_items.name as sale_item_name, SUM(order_items.quantity) as sale_quantity, family_groups.menu_item_groups_id,family_groups.name as family_group_item_name,menu_items_groups.name as menu_group_item_name, sub_major_groups.name as sub_major_group_item_name FROM `order_items` inner join general_items on order_items.general_item_id = general_items.id inner join family_groups on family_groups.id = general_items.family_groups_id inner join menu_items_groups on family_groups.menu_item_groups_id = menu_items_groups.id inner join sub_major_groups on sub_major_groups.id = menu_items_groups.sub_major_groups_id where order_items.`print_class_id` <= 7 and Date(order_items.created_at) = '" . date("Y-m-d") . "' group by order_items.`general_item_id` order by sub_major_groups.name, family_groups.name desc");

        $file = fopen("overallSales.csv", "w");
        $sub_item_check = '';
        $family_item_check = '';
        $sales_item_check = '';
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'The Smith Hotels',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '0705625356'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Quantity control Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            'Item',
            'Sales QTY'
        ));
        foreach ($file_result as $key => $fr)
        {
            if ($fr->sub_major_group_item_name != $sub_item_check)
            {
                fputcsv($file, array(
                    $fr->sub_major_group_item_name
                ));

            }
            $sub_item_check = $fr->sub_major_group_item_name;

            if ($fr->family_group_item_name != $family_item_check)
            {
                fputcsv($file, array(
                    '',
                    $fr->family_group_item_name
                ));
            }

            $family_item_check = $fr->family_group_item_name;

            if ($fr->sale_item_name != $sales_item_check)
            {
                fputcsv($file, array(
                    '',
                    '',
                    $fr->sale_item_name,
                    $fr->sale_quantity
                ));
            }
            $sales_item_check = $fr->sale_item_name;
        }

        // $file = fopen("overallSales.csv","w");
        //echo $num_rows;
        $count = 0;
        $gross = 0;
        $total = 0;
        $totalCount = 0;
        $flag = true;
        // //exit;
        // if($num_rows>0){
        // $sum_amount=0;
        // $sum_fee=0;
        //     foreach($majors as $major){
        //         // fputcsv($file, array($major->name));
        //         foreach($submajors as $sub){
        //             $subflag = false;
        //             if($major->id == $sub->major_group_name ){
        

        //                 foreach($menuGroups as $menugroup){
        //                     if($menugroup->sub_major_groups_id == $sub->id){
        //                         //fputcsv($file, array($menugroup->name,));
        //             foreach($groups as $familygroup){
        //                 $flag = false;
        //         if($menugroup->id == $familygroup->menu_item_groups_id ){
        //             foreach($items as $item){
        //                 if($familygroup->my_id == $item->family_groups_id){
        

        //                     foreach($results as $result){
        // if($item->id == $result->general_item_id && $familygroup->my_id == $item->family_groups_id){
        //                     if($subflag == false){
        //                             fputcsv($file, array($sub->name));
        //                     }
        //                     if($flag == false){
        //                         fputcsv($file, array('',$familygroup->name));
        //                     }
        //                                 $totalCount += $result->quantity;
        //                         $gross = $result->quantity*$item->price;
        //                                     $total += $gross;
        //             fputcsv($file, array('','',$result->name,$result->quantity));
        //                             $flag = true;
        //                             $subflag = true;
        

        //                                             }
        //                                         }
        //                                     }
        

        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        

        //         // while ( $row = mysqli_fetch_row ( $res ) ){
        

        //                 // foreach($results as $r){
        //                 //     foreach($items as $item){
        //                 //         if($sub->major_group_id == $major->id){
        

        //                 //             fputcsv($file, array($sub->name));
        //                 //         }
        //                 //         if($item->name == $r->name){
        //                 //             $gross = $r->quantity*$item->price;
        //                 //             $total += $gross;
        //                 //             fputcsv($file, array('',$r->name,$r->quantity,$gross));
        //                 //         }
        //                 //     }
        //                 // }
        //             }
        

        //     }
        //     fputcsv($file,array('','',''));
        //     fputcsv($file, array('','','','Total Count',$totalCount));
        fclose($file);

        return view('admin.reports.admin_overall_sales_report', compact('results', 'items', 'groups', 'taxes', 'data'));

    }

    public function discountReport()
    {

        $orders = BillMaster::where('discountType', '!=', '')->get();

        $file = fopen("discount.csv", "w");

        fputcsv($file, array(
            'Discount Report'
        ));
        // fputcsv($file, array('','','','',''));
        fputcsv($file, array(
            'All Time Report'
        ));
        // fputcsv($file, array('','','','','','',''));
        fputcsv($file, array(
            'S.No.',
            'Discount Reason',
            'Order No',
            '    Amount After Discount',
            'Discount Type',
            'Discount amount',
            'Discount Percent',
            'Discounted Amount',
            'Discount added By'
        ));
        // fputcsv($file, array('','','','','','',''));
        $dd = 1;
        foreach ($orders as $order)
        {
            $orders2 = str_replace(',', '.', $order->orders);
            fputcsv($file, array(
                $dd,
                $order->discount_reason,
                $orders2,
                $order->total_amount,
                $order->discountType,
                $order->discount_amount,
                $order->discount_percent . ' %',
                $order->discounted_amount,
                $order->discountUserName
            ));
            $dd++;
        }

        fclose($file);
        $start_date = '';
        $end_date = '';
        return view('admin.reports.admin_discount_reports', compact('orders', 'start_date', 'end_date'));

    }

    public function discountFilter(Request $request)
    {

        $start_date = $request->start_date;
        // $start_date = date('Y-m-d', $date) ;
        // $rsDate = date('d-m-Y', $date);
        $end_date = $request->end_date;
        // $end_date = date('Y-m-d', $date);
        // $reDate = date('d-m-Y', $date);
        // $start_time = $request->startTimeSearch.":00";
        // $end_time = $request->endTimeSearch.":00";
        // $start_date = $start_date." ".$start_time ;
        // $end_date = $end_date." ".$end_time ;
        // $rsDate = $rsDate . " " . $start_time;
        // $reDate = $reDate." ".$end_time ;
        $orders = DB::select("select * FROM bill_masters where discount_percent != '' AND DATE(bill_masters.created_at) BETWEEN '" . $start_date . "' AND '" . $end_date . "' ORDER BY waiter_id ASC");

        $file = fopen("discount.csv", "w");
        fputcsv($file, array(
            'Discount Report'
        ));
        // fputcsv($file, array('','','','','','',''));
        fputcsv($file, array(
            'Report Data Search From ',
            $start_date . "  to  " . $end_date
        ));
        // fputcsv($file, array('','','','','','',''));
        fputcsv($file, array(
            'S.No.',
            'Discount Reason',
            'Order No',
            '    Amount After Discount',
            'Discount Type',
            'Discount amount',
            'Discount Percent',
            'Discounted Amount',
            'Discount added By'
        ));
        // fputcsv($file, array('','','','','','',''));
        $dd = 1;
        foreach ($orders as $order)
        {

            fputcsv($file, array(
                $dd,
                $order->discount_reason,
                $order->orders,
                $order->total_amount,
                $order->discountType,
                $order->discount_amount,
                $order->discount_percent . ' %',
                $order->discounted_amount,
                $order->discountUserName
            ));
            $dd++;
        }

        fclose($file);

        return view('admin.reports.admin_discount_reports', compact('orders', 'start_date', 'end_date'));

    }

    public function paymentSalesFilter(Request $request)
    {

        $total = 0;
        $start_date = $request->start_date;
        // $start_date = date('Y-m-d', $date) ;
        // $rsDate = date('d-m-Y', $date);
        $end_date = $request->end_date;
        // $end_date = date('Y-m-d', $date);
        // $reDate = date('d-m-Y', $date);
        // $start_time = $request->startTimeSearch.":00";
        // $end_time = $request->endTimeSearch.":00";
        // $start_date = $start_date." ".$start_time ;
        // $end_date = $end_date." ".$end_time ;
        // $rsDate = $rsDate . " " . $start_time;
        // $reDate = $reDate." ".$end_time ;
        $restaurants = Restaurants::all();

        $results = DB::select("select count(id) as count,sum(total_amount) as sum,payment_method from payments where payments.created_at BETWEEN '" . $start_date . "' AND '" . $end_date . "' GROUP BY payment_method");

        $methods = PaymentMethod::all();

        $file = fopen("paymentSales.csv", "w");
        fputcsv($file, array(
            'Payment Sales Summary'
        ));

        // fputcsv($file, array('','','','','','',''));
        // fputcsv($file, array('','','','','','',''));
        fputcsv($file, array(
            'Report Data Search From ',
            $start_date . "  to  " . $end_date,
        ));

        // fputcsv($file, array('','','','','',''));
        // fputcsv($file, array('','','','',''));
        fputcsv($file, array(
            'Title ',
            'No. Of Transactions',
            'Total Amount'
        ));
        // fputcsv($file, array('','','','',''));
        foreach ($results as $result)
        {
            foreach ($methods as $method)
            {
                if ($method->id == $result->payment_method)
                {
                    fputcsv($file, array(
                        $method->name,
                        $result->count,
                        $result->sum
                    ));
                    break;
                }
            }
            $total += $result->sum;
        }
        // fputcsv($file, array('','','','',''));
        fputcsv($file, array(
            'Total',
            '',
            $total
        ));
        fclose($file);

        $arr['methods'] = $methods;
        $arr['results'] = $results;

        return $arr;
    }
    public function voidReport()
    {

        $orders = VoidItems::orderBy('id', 'DESC')->get();

        $file = fopen("void_report.csv", "w");

        fputcsv($file, array(
            '',
            '',
            '',
            '',
            'Void Item Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));

        fputcsv($file, array(
            'All Time Report',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            'Order No',
            'Item Name',
            'Amount',
            'Waiter',
            'Qty',
            'Bill Id',
            'Date',
            'Void Reason',
            'Manager Comment',
            'Waiter',
            'Status',
            'Voided/Rejected By'
        ));
     
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));

        foreach ($orders as $order)
        {
            $billId = PostPaidOrders::where('id', $order->order_id)
                ->value('bill_id');
            if (!$billId)
            {
                $billId = 'N/A';
            }
            fputcsv($file, array(
                '',
                $order->order_id,
                $order->item_name,
                $order->amount,
                $order->waiter_name,
                1,
                $billId,
                $order->created_at,
                $order->comments,
                $order->manager_comment,
                $order->waiter_name,
                $order->status,
                $order->voided_name
            ));
            
                                                   
        }

        fclose($file);
        $start_date = '';
        $end_date = '';
        //return $orders;
        return view('admin.reports.admin_void_report', compact('orders', 'start_date', 'end_date'));

    }
    public function locationwiseReport()
    {

        $stocks = stock::orderBy('id', 'DESC')->get();

        $file = fopen("locationwise.csv", "w");

        
        $file = fopen("locationwise.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            'Locationwise Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));

        fputcsv($file, array(
            'All Time Report',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            'Stock No',
            'Ingrdient Name',
            'Ingrdient ID',
            'Print Class',
            'Qty'
        ));
     
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));



        $stocks=stock::all();
        
        foreach($stocks as $stock){
            fputcsv($file, array(
                '',
                $stock->id,
                $stock->ingredient_name,
                $stock->ingredient_id,
                $stock->print_class,
                $stock->quantity_added
                
            ));
            
            
            
            
            
        }
        
        
        $ing= RawIngredient::all();
        
        
        
        
        $date = date('Y-m-d');

        fclose($file);

        $print_classes = PrintClasses::all();
        $class1 = null;
        
        
        
        
        $start_date='';
        $end_date='';
        
        return view('admin.reports.admin_locationwise', compact('recipes', 'print_classes','start_date','end_date', 'class1','stocks','ing'));


    }

    public function voidReportFilter(Request $request)
    {

        $start_date = $request->start_date;
        
        $end_date = $request->end_date;
        
        $orders = DB::select("select * FROM void_items where updated_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");

        $file = fopen("void_report.csv", "w");

        fputcsv($file, array(
            '',
            '',
            '',
            '',
            'Void Item Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'Report Data Search From ',
            $start_date . "  to  " . $end_date,
            '',
            '',
            '',
            '',
            ''
        ));

        fputcsv($file, array(
            'All Time Report',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            'Order No',
            'Item Name',
            'Amount',
            'Waiter',
            'Qty',
            'Bill Id',
            'Date',
            'Void Reason',
            'Manager Comment',
            'Waiter',
            'Status',
            'Voided/Rejected By'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));

        foreach ($orders as $order)
        {

            $billId = PostPaidOrders::where('id', $order->order_id)
                ->value('bill_id');
            if (!$billId)
            {
                $billId = 'N/A';
            }
            fputcsv($file, array(
                '',
                $order->order_id,
                $order->item_name,
                $order->amount,
                $order->waiter_name,
                1,
                $billId,
                $order->created_at,
                $order->comments,
                $order->manager_comment,
                $order->waiter_name,
                $order->status,
                $order->voided_name
            ));

        }

        fclose($file);

        return view('admin.reports.admin_void_report', compact('orders', 'start_date', 'end_date'));
    }
    function Locationwise()
    {
        // dd("LocationWise");
        $file = fopen("locationwise.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            'Locationwise Report'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));

        fputcsv($file, array(
            'All Time Report',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            'Stock No',
            'Ingrdient Name',
            'Ingrdient ID',
            'Print Class',
            'Qty'
        ));
     
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));

 $timestampp = strtotime('midnight');
           
 $timestamp = strtotime('tomorrow midnight');

 $time = date("Y-m-d H:i:s",$timestampp);
  $timee = date("Y-m-d H:i:s",$timestamp);
// echo $timee;exit;

        // $stocks=stock::orderBy('ingredient_id', 'ASC')->get();
        
        //   $stocks = DB::select(" SELECT * from stock  where  updated_at > '" .$time."' ");
        
        $stocks = DB::select(" SELECT * from stock where updated_at BETWEEN '" .$time."' AND '" .$timee."'");
        // echo " SELECT * from stock where updated_at BETWEEN '" .$time."' AND '" .$timee."";exit;
         
        foreach($stocks as $key=>$value){
            
            
            
            
            
            $fourteenth_col_first = DB::select(" SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  AND  created_at BETWEEN '" .$timee."' AND '" .$time."'  ORDER BY created_at desc  LIMIT 1");

            //   $fourteenth_col_second = DB::select(" SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  AND  created_at > '" .$time."' ORDER BY created_at asc  LIMIT 1");

        
            if(isset($fourteenth_col_first[0]->current_quantity)){
                //  $value->fourteenth_col=($fourteenth_col_second[0]->current_quantity+$fourteenth_col_second[0]->adjusted_quantity)-($fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity);
                  $value->fourth_col=$fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity;
              
                
            }else{
                
                $value->fourth_col=0;
                
            }
            
        //   $test= DB::select("SELECT vo.quantity as void_quantity FROM  `void_items`  INNER join `order_items`  on order_items.id= void_items.item_id INNER join `recipe`  on recipe.genral_item_id= order_items.general_item_id");
            // echo $value->ingredient_id;
            // exit;
            // dd($test);exit;
            $print_class=PrintClasses::where('name',$value->print_class)->first();
            
            
          $value->info=RawIngredient::where('id',$value->ingredient_id)->first();
        //   dd($value->info);
        //   exit;
          $value->cs=PhysicalStockCS::where('ingredient_id',$value->ingredient_id)->first();
          
         
          
        //   $fifth_coll = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."' AND  created_at > '" .$time."' ");
            
        //   $last_coll = DB::select(" SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."' AND  created_at > '" .$time."' ORDER BY created_at asc  LIMIT 1 ");
           
           
            $fifth_coll = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  ");
            
        //   $last_coll = DB::select(" SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  ORDER BY created_at asc  LIMIT 1 ");
           
            // $seventh_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'  ");

           
        // dd($last_coll);exit;
        // echo " SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->id."' AND print_class_name='" .$value->print_class."' AND  created_at > '" .$time."' ORDER BY created_at asc  LIMIT 1 ";
        // exit;
            
           
            $value->last_col=$fifth_coll[0]->quantity;
            
         
         
          $seventh_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'  ");
        //  echo "SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'";
             if(isset($seventh_col[0]->quantity)){
            $value->seventh_col=$seventh_col[0]->quantity;
            }
            
              $twelfth_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from IS NULL  ");
        //  echo "SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'";
             if(isset($twelfth_col[0]->quantity)){
            $value->twelfth_col=$twelfth_col[0]->quantity;
            }
            
              $tenth_col = DB::select(" SELECT SUM(void) as quantity from sales_ingredient  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  AND  created_at > '" .$time."'  ");
        //  echo "SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'";
             if(isset($tenth_col[0]->quantity)){
            $value->tenth_col=$tenth_col[0]->quantity;
            }
         
          
          $eighth_col = DB::select(" SELECT * FROM physical_stock_print_store_adjustments WHERE print_class_name='" .$value->print_class."' AND  ingredient_id='" .$value->ingredient_id."' AND  created_at > '" .$time."' ORDER BY created_at desc  LIMIT 1");
          
        //   $adjust = DB::select(" SELECT * FROM physical_stock_print_store_adjustments WHERE print_class_name='" .$value->print_class."' AND  ingredient_id='" .$value->ingredient_id."'  ORDER BY created_at desc  LIMIT 1");
          
           
              $system= DB::select("Select SUM(quantity) as quantity from sales_ingredient WHERE print_class_name='" .$value->print_class."' AND  ingredient_id='" .$value->ingredient_id."' AND  created_at > '" .$time."' ");
            
            $value->system_sale=$system[0]->quantity;
            // if($value->ingredient_id==230 && $value->print_class=='KITCHEN'){
                
            //     dd($system);exit;
            // }
            
            
             $thirteenth_col=DB::select("SELECT SUM(variance) as variance from physical_stock_print_store_adjustments  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'   AND  created_at > '" .$time."' ");
         
           
           if(isset($thirteenth_col[0]->variance)){
            $value->thirteenth_col=$thirteenth_col[0]->variance;
             $value->fourteenth_col= $system[0]->quantity-$thirteenth_col[0]->variance;
            }else{
                
                $value->fourteenth_col= $system[0]->quantity;
            }
            
            // $value->fourteenth_col=$system[0]->quantity;
            // print_r($system);exit;
            
              $eleventh_col = DB::select("SELECT SUM(variance) as quantity FROM physical_stock_print_store_adjustments WHERE print_class_name='" .$value->print_class."' AND  ingredient_id='" .$value->ingredient_id."' AND  created_at > '" .$time."'  AND  type = 'edit' ");
    
     if(isset($eleventh_col[0]->quantity)){
                
              
               
            $value->eleventh_col=$eleventh_col[0]->quantity;
           
            }
            
            
            if(isset($eighth_col[0]->system_quantity)){
                
              
               
            $value->eighth_col=$eighth_col[0]->system_quantity;
            }
          
          
          
          if ($print_class) {
             
    $value->ps=PhysicalStockPS::where('ingredient_id',$value->ingredient_id)->where('print_class_id',$print_class->id)->first();
} else {
    // handle this situation
}
          
       
        }
        
        
    //   exit;
        
        // echo $stocks[0]->info->id;
        // exit;
        
        
        // $ing = DB::select(" SELECT * from raw_ingredient  where  updated_at > '" .$time."' ");
        //  $ing= RawIngredient::all();
         $timestampp = strtotime('midnight');
            $timee = date("Y-m-d H:i:s",$timestamp);

 $ing = DB::select(" SELECT * from raw_ingredient where updated_at BETWEEN '" .$timee."' AND '" .$time."'");



 foreach($ing as $key=>$value){
     
     
    
            
            
            $fourteenth_col_first = DB::select("SELECT * from edit_ingredients  where  ingredient_id='" .$value->id."'   AND  created_at BETWEEN '" .$timee."' AND '" .$time."' ORDER BY created_at desc  LIMIT 1");

            //   $fourteenth_col_second = DB::select(" SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  AND  created_at > '" .$time."' ORDER BY created_at asc  LIMIT 1");

        
            if(isset($fourteenth_col_first[0]->current_quantity)){
                //  $value->fourteenth_col=($fourteenth_col_second[0]->current_quantity+$fourteenth_col_second[0]->adjusted_quantity)-($fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity);
                  $value->fourth_col=$fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity;
              
                
            }else{
                
                $value->fourth_col=0;
                
            }
     
             
             
           
            
            
            $fourteenth_col_first2 = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->id."'  AND  created_at > '" .$time."' and transferred_from='central'  ");
            $fourteenth_col_second2 = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->id."'  AND  created_at > '" .$time."' and transferred_from='central2'  ");
        
            

                                //  $value->fourteenth_col=($fourteenth_col_second[0]->current_quantity+$fourteenth_col_second[0]->adjusted_quantity)-($fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity);            
                //  $value->fourteenth_col=$fourteenth_col_first2[0]->quantity+$fourteenth_col_second2[0]->quantity;
                
            
            
             $eighth_col = DB::select(" SELECT * FROM physical_stock_central_store_adjustments where  ingredient_id='" .$value->id."' AND  created_at > '" .$time."' ORDER BY created_at desc  LIMIT 1");
            
            //  $fifth_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients  where  ingredient_id='" .$value->id."' AND  created_at > '" .$time."' ");
             
            //  $last_col = DB::select(" SELECT * from edit_ingredients  where  ingredient_id='" .$value->id."' AND  created_at > '" .$time."' ORDER BY created_at asc  LIMIT 1 ");
            // //  dd("SELECT * from edit_ingredients  where  ingredient_id='" .$value->id."' AND  created_at > '" .$time."' ORDER BY created_at asc  LIMIT 1 ");exit;
            
             $eleventh_col = DB::select("SELECT SUM(variance) as quantity FROM physical_stock_central_store_adjustments WHERE  ingredient_id='" .$value->id."' AND  created_at > '" .$time."'  AND  type = 'edit' ");
    
     if(isset($eleventh_col[0]->quantity)){
                
              
               
            $value->eleventh_col=$eleventh_col[0]->quantity;
            
            }
            
            //   $adjust = DB::select(" SELECT * FROM physical_stock_central_store_adjustments where  ingredient_id='" .$value->id."'  ORDER BY created_at desc  LIMIT 1");
            
             $fifth_col_original = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients  where  ingredient_id='" .$value->id."' and  adjusted_quantity > 0 AND  created_at > '" .$time."'  ");
             if(isset($fifth_col_original[0]->quantity)){
            $value->fifth_col_original=$fifth_col_original[0]->quantity;
            }
            
             
              $thirteenth_col=DB::select("SELECT SUM(variance) as variance from physical_stock_central_store_adjustments  where  ingredient_id='". $value->id ."'    AND  created_at > '" .$time."'  ");
             
           if(isset($thirteenth_col[0]->variance)){
            $value->thirteenth_col=$thirteenth_col[0]->variance;
            $value->fourteenth_col=($fourteenth_col_first2[0]->quantity+$fourteenth_col_second2[0]->quantity)-$thirteenth_col[0]->variance;
            }else{
                
                $value->fourteenth_col= $fourteenth_col_first2[0]->quantity+$fourteenth_col_second2[0]->quantity;
            }
            
             $fifth_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients  where  ingredient_id='" .$value->id."'  ");
             
            //  $last_col = DB::select(" SELECT * from edit_ingredients  where  ingredient_id='" .$value->id."'  ORDER BY created_at asc  LIMIT 1 ");
         
            $value->fifth_col=$fifth_col[0]->quantity;
            
          
          
          
            $value->last_col=$fifth_col[0]->quantity;
        
           
            
            
            if(isset($eighth_col[0]->system_quantity)){
                
              
               
            $value->eighth_col=$eighth_col[0]->system_quantity;
            
           
            }
             
             
             
         }
       
        fclose($file);


        $print_classes = PrintClasses::all();
        $class1 = null;
        
        
        
       
        
        $start_date='';
        $end_date='';
        
        return view('admin.reports.admin_locationwise', compact('recipes', 'print_classes','start_date','end_date', 'class1','stocks','ing'));

    }
    function Locationwise_filter(Request $request)
    {
        
        
        // echo $request->start_date;exit;
        
         
        if($request->start_date==null){
            
            if($request->print_class=="Central Store"){
                
                
            }else{
                  $stocks= stock::where('print_class',$request->print_class)->get();
            }
            
          
            
        }else{
            
            $stocks = DB::select("select * FROM stock where print_class='" .$request->print_class."' AND  updated_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
            
        }
        
        if($request->print_class != 'BUTCHERY' && $request->print_class != 'SEPEKO'  && $request->print_class != 'SEKENANI' && $request->print_class != 'KITCHEN'){
           
           
          
           
              $stocks = DB::select("select * FROM stock where  updated_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
            //   $stocks = DB::select("select * FROM stock ");
             
        }
        
     

        $print_classes = PrintClasses::all();
        $class1 = $request->print_class;
        
        $start_date=$request->start_date;
        $end_date=$request->end_date;
        
       
        
                foreach($stocks as $key=>$value){
            
            
            // echo $value->ingredient_id;
            // exit;
            $print_class=PrintClasses::where('name',$value->print_class)->first();
            
             $fourteenth_col_first = DB::select(" SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  AND  created_at < '" .$request->start_date."'  ORDER BY created_at DESC  LIMIT 1");

              
            if(isset($fourteenth_col_first[0]->current_quantity)){
                                //  $value->fourteenth_col=($fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity)-($fourteenth_col_second[0]->current_quantity+$fourteenth_col_second[0]->adjusted_quantity);
                                //  if($value->print_class=='SEPEKO' && $value->ingredient_id==17){
                                //     //  dd($fourteenth_col_second[0]->current_quantity+$fourteenth_col_second[0]->adjusted_quantity);
                                //   echo "SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ORDER BY created_at DESC  LIMIT 1";
                                //      exit;
                                //  }
                  $value->fourth_col=$fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity;
            }else{
                
                $value->fourth_col=0;
                
            }
            
          $value->info=RawIngredient::where('id',$value->ingredient_id)->first();
        //   dd($value->info);
        //   exit;
          $value->cs=PhysicalStockCS::where('ingredient_id',$value->ingredient_id)->first();
          
          //$adjust= PhysicalStockPSAdjustment::where('print_class_name',$value->print_class)->where('ingredient_id',$value->ingredient_id)->first();
          
           $tenth_col = DB::select(" SELECT SUM(void) as quantity from sales_ingredient  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."'  ");
        //  echo "SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'";
             if(isset($tenth_col[0]->quantity)){
            $value->tenth_col=$tenth_col[0]->quantity;
            }
         
          
           
          
            $eighth_col = DB::select(" SELECT * FROM physical_stock_print_store_adjustments WHERE print_class_name='" .$value->print_class."' AND  ingredient_id='" .$value->ingredient_id."' AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ORDER BY created_at desc  LIMIT 1");
            
               $system= DB::select("Select SUM(quantity) as quantity from sales_ingredient WHERE print_class_name='" .$value->print_class."' AND  ingredient_id='" .$value->ingredient_id."' AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
            
            $value->system_sale=$system[0]->quantity;
            
            $thirteenth_col=DB::select("SELECT SUM(variance) as variance from physical_stock_print_store_adjustments  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'   AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
           if(isset($thirteenth_col[0]->variance)){
            $value->thirteenth_col=$thirteenth_col[0]->variance;
             $value->fourteenth_col= $system[0]->quantity-$thirteenth_col[0]->variance;
            }else{
                
                $value->fourteenth_col= $system[0]->quantity;
            }
            
            // $value->fourteenth_col=$system[0]->quantity;
            
            $eleventh_col = DB::select("SELECT SUM(variance) as quantity FROM physical_stock_print_store_adjustments WHERE print_class_name='" .$value->print_class."' AND  ingredient_id='" .$value->ingredient_id."' AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."'  AND  type = 'edit' ");
    
     if(isset($eleventh_col[0]->quantity)){
                
              
               
            $value->eleventh_col=$eleventh_col[0]->quantity;
            }
            
            $seventh_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' and transferred_from='central'  ");
        //  echo "SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'";
             if(isset($seventh_col[0]->quantity)){
            $value->seventh_col=$seventh_col[0]->quantity;
            }
            
             $twelfth_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."'  and transferred_from IS NULL  ");
        //  echo "SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  and  adjusted_quantity > 0 AND  created_at > '" .$time."' and transferred_from='central'";
             if(isset($twelfth_col[0]->quantity)){
            $value->twelfth_col=$twelfth_col[0]->quantity;
            }
         
            
            
            $fifth_coll = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."' AND  (created_at < '" .$request->start_date."' or created_at <= '" .$request->end_date."') ");
            
        //   $last_coll = DB::select(" SELECT * from edit_ingredients_print_class  where  ingredient_id='" .$value->ingredient_id."' AND print_class_name='" .$value->print_class."'  ORDER BY created_at desc  LIMIT 1 ");
          
          
          
          
            
                 
                 
                
               
            $value->last_col=$fifth_coll[0]->quantity;
           
                
               
                 
                    
                    
                
               
                
                
            
          
                 
                 
                 
                 
            
            
            
            
           
            
            
            
            if(isset($eighth_col[0]->system_quantity)){
                
              
               
            $value->eighth_col=$eighth_col[0]->system_quantity;
            }
          
          if ($print_class) {
             
    $value->ps=PhysicalStockPS::where('ingredient_id',$value->ingredient_id)->where('print_class_id',$print_class->id)->first();
} else {
    // handle this situation
}
          
       
        }
        
        // dd($stocks);
        // exit;
//

if($request->print_class != 'BUTCHERY' && $request->print_class != 'SEPEKO'  && $request->print_class != 'SEKENANI' && $request->print_class != 'KITCHEN')  {
    
     $ing = DB::select("select * FROM raw_ingredient where  updated_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."'");
    // dd("select * FROM raw_ingredient where  updated_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date);exit;
      //dd($ing);exit;  
         foreach($ing as $key=>$value){
             
             
             
              $fourteenth_col_first2 = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->id."'  AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' and transferred_from='central'  ");
            $fourteenth_col_second2 = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients_print_class  where  ingredient_id='" .$value->id."'  AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' and transferred_from='central2'  ");
        
            

                                //  $value->fourteenth_col=($fourteenth_col_second[0]->current_quantity+$fourteenth_col_second[0]->adjusted_quantity)-($fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity);            
               
             
             $fourteenth_col_first = DB::select(" SELECT * from edit_ingredients  where  ingredient_id='" .$value->id."'  AND  created_at < '" .$request->start_date."'  ORDER BY created_at desc  LIMIT 1");

            //  $fourteenth_col_second = DB::select(" SELECT * from edit_ingredients  where  ingredient_id='" .$value->id."'  AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ORDER BY created_at asc  LIMIT 1");

            if(isset($fourteenth_col_first[0]->current_quantity)){
                                //   $value->fourteenth_col=($fourteenth_col_second[0]->current_quantity+$fourteenth_col_second[0]->adjusted_quantity)-($fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity);              
                 $value->fourth_col=$fourteenth_col_first[0]->current_quantity+$fourteenth_col_first[0]->adjusted_quantity;
                
            }else{
                
                $value->fourth_col=0;
                
            }
            
             
            $eighth_col = DB::select(" SELECT * FROM physical_stock_central_store_adjustments where  ingredient_id='" .$value->id."' AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ORDER BY created_at desc  LIMIT 1");
             $fifth_col_original = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients  where  ingredient_id='" .$value->id."' AND  adjusted_quantity>0 AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
            
             $eleventh_col = DB::select("SELECT SUM(variance) as quantity FROM physical_stock_central_store_adjustments WHERE  ingredient_id='" .$value->id."' AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."'  AND  type = 'edit' ");
    
     if(isset($eleventh_col[0]->quantity)){
                
              
               
            $value->eleventh_col=$eleventh_col[0]->quantity;
            }
            
             $thirteenth_col=DB::select("SELECT SUM(variance) as variance from physical_stock_central_store_adjustments  where  ingredient_id='" .$value->id."'    AND  created_at BETWEEN '" .$request->start_date."' AND '" .$request->end_date."' ");
           if(isset($thirteenth_col[0]->variance)){
            $value->thirteenth_col=$thirteenth_col[0]->variance;
               $value->fourteenth_col=($fourteenth_col_first2[0]->quantity+$fourteenth_col_second2[0]->quantity)-$thirteenth_col[0]->variance;
            
            }else{
                
                $value->fourteenth_col= ($fourteenth_col_first2[0]->quantity+$fourteenth_col_second2[0]->quantity);
            }
            
            if(isset($fifth_col_original[0]->quantity)){
            $value->fifth_col_original=$fifth_col_original[0]->quantity;
            }
            
           
             $fifth_col = DB::select(" SELECT SUM(adjusted_quantity) as quantity from edit_ingredients  where  ingredient_id='" .$value->id."'  AND  (created_at < '" .$request->start_date."' or created_at <= '" .$request->end_date."') ");
            
            $value->fifth_col=$fifth_col[0]->quantity;
            
        
                 
                
               
            $value->last_col=$fifth_col[0]->quantity;
           
                
               
                 
                    
                    
                
               
                
                
         
                 
                 
                 
                 
            
            
            
            
            if(isset($eighth_col[0]->system_quantity)){
                
              
               
            $value->eighth_col=$eighth_col[0]->system_quantity;
            }
             
             
             
         }
            return view('admin.reports.admin_locationwise', compact('recipes', 'print_classes', 'class1','stocks','start_date','end_date','ing'));
    
}      
  
  $ing=array();
     return view('admin.reports.admin_locationwise', compact('recipes', 'print_classes', 'class1','stocks','start_date','end_date','ing'));    
      
        
        

     

    }
    public function percentage_profit()
    {

        // $orders = DB::select("select * FROM post_paid_orders where   date(created_at) = '" . date("Y-m-d") . "' ");
        $orders = DB::select("select * FROM post_paid_orders where   date(created_at) = '2019-10-10' ");

      
        $general_items = array();
        foreach ($orders as $key => $value)
        {
            $bill = BillMaster::where('id', $value->bill_id)
                ->first();
            $discount_percent = 0;
            if ($bill->discountType == 'percent')
            {
                $discount_percent = $bill->discount_percent;
            }
            if ($bill->discountType == 'fixed')
            {
                $total_amount = $bill->total_amount + $bill->discounted_amount;
                $discount_percent = round(($bill->discounted_amount / $total_amount) * 100);
            }
            $order = OrderItems::where('post_paid_orders_id', $value->id)
                ->get();
            foreach ($order as $key => $value)
            {
                $id=$value->general_item_id;
                $item_info = GeneralItems::where('id', $id)->first();
                if(empty($item_info) ){
                    continue;       
                }   
                if (!array_key_exists($id, $general_items))
                {  
                    if ($discount_percent != 0)
                    {
                        $price = $value->quantity * $value->price;
                        $discount = $price * $discount_percent;
                        $total_price = $price - $discount;
                        $data1 = array();
                        $data1['name'] = $item_info->name;
                        $data1['id'] = $item_info->id;
                        $data1['price'] = $total_price / $value->quantity;
                        $data1['cost'] = $item_info->purchase_price;
                        $data1['quantity_sold'] = $value->quantity;
                        $data1['total_sale'] = $total_price;
                        $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                        $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                        if ($data1['total_sale'] == 0)
                        {
                            $data1['total_sale'] = 1;
                        }
                        $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                        $general_items[$id]['discount'] = $data1;
                    }
                    else
                    {

                        $price = $value->quantity * $value->price;
                        $total_price = $price;

                        $data1 = array();
                        $data1['name'] = $item_info->name;
                        $data1['id'] = $item_info->id;
                        $data1['price'] = $total_price / $value->quantity;
                        $data1['cost'] = $item_info->purchase_price;
                        $data1['quantity_sold'] = $value->quantity;
                        $data1['total_sale'] = $total_price;
                        $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                        $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                        if ($data1['total_sale'] == 0)
                        {
                            $data1['total_sale'] = 1;
                        }
                        $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                        $general_items[$id]['simple'] = $data1;
                        // print_r($general_items);

                    }
                }
                if (!empty($general_items))
                {
                    foreach ($general_items as $key1 => $value1)
                    {
                        if ($key1 ==$id && array_key_exists('discount', $value1))
                        {
                            $price = $value->quantity * $value->price;
                            $discount = $price * $discount_percent;
                            $total_price = $price - $discount;
                            $data1 = array();
                            $data1['name'] = $item_info->name;
                            $data1['id'] = $item_info->id;
                            $data1['price'] = $total_price / $value->quantity;
                            $data1['cost'] = $item_info->purchase_price;
                            $data1['quantity_sold'] = $value->quantity;
                            $data1['total_sale'] = $total_price;
                            $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                            $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                            if ($data1['total_sale'] == 0)
                            {
                                $data1['total_sale'] = 1;
                            }
                            $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                            //set the previous + current average
                            $rr=$data1['price'] + $general_items[$id]['discount']['price'];
                            $general_items[$id]['discount']['price'] =  $rr/ 2;
                            $general_items[$id]['discount']['quantity_sold'] = $general_items[$id]['discount']['quantity_sold'] + $data1['quantity_sold'];
                            $general_items[$id]['discount']['total_sale'] = $general_items[$id]['discount']['total_sale'] + $data1['total_sale'];
                            $general_items[$id]['discount']['total_cost'] = $general_items[$id]['discount']['total_cost'] + $data1['total_cost'];
                            $tt=($general_items[$id]['discount']['profit'] + $data1['profit']);
                            $general_items[$id]['discount']['profit'] =  $tt/ 2;
                            $yy=($general_items[$id]['discount']['margin'] + $data1['margin']);
                            $general_items[$id]['discount']['margin'] = $yy / 2;
                        }
                        else if ($key1 == $id &&  array_key_exists('simple', $value1))
                        {

                            $price = $value->quantity * $value->price;
                            $total_price = $price;
                            $data1 = array();
                            $data1['name'] = $item_info->name;
                            $data1['id'] = $item_info->id;
                            $data1['price'] = $total_price / $value->quantity;
                            $data1['cost'] = $item_info->purchase_price;
                            $data1['quantity_sold'] = $value->quantity;
                            $data1['total_sale'] = $total_price;
                            $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                            $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                            if ($data1['total_sale'] == 0)
                            {
                                $data1['total_sale'] = 1;
                            }
                            $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                           
                             
                            $rr=$data1['price'] + $general_items[$id]['simple']['price'];
                            $general_items[$id]['simple']['price'] =  $rr/ 2;
                            
                            
                            $general_items[$id]['simple']['quantity_sold'] = $general_items[$id]['simple']['quantity_sold'] + $data1['quantity_sold'];
                            $general_items[$id]['simple']['total_sale'] = $general_items[$id]['simple']['total_sale'] + $data1['total_sale'];
                            $general_items[$id]['simple']['total_cost'] = $general_items[$id]['simple']['total_cost'] + $data1['total_cost'];
                            $tt=($general_items[$id]['simple']['profit'] + $data1['profit']);
                            $general_items[$id]['simple']['profit'] =  $tt/ 2;
                            $yy=($general_items[$id]['simple']['margin'] + $data1['margin']);
                            $general_items[$id]['simple']['margin'] = $yy / 2;
                        }
                        else
                        {
                            //hiiiiiiii
                        }
                    }
                }

            }

        }

        return $general_items;
    }
    
    public function grossProfit(){
        

         $orders = DB::select("select * FROM post_paid_orders where   date(created_at) = '" . date("Y-m-d") . "' ");
       // $orders = DB::select("select * FROM post_paid_orders where   date(created_at) = '2019-10-10' ");

// echo "<pre>";
        $general_items = array();
        foreach ($orders as $key => $value)
        {
            $bill = BillMaster::where('id', $value->bill_id)
                ->first();
            $discount_percent = 0;
            if ($bill->discountType == 'percent')
            {
                $discount_percent = $bill->discount_percent;
            }
            if ($bill->discountType == 'fixed')
            {
                $total_amount = $bill->total_amount + $bill->discounted_amount;
                $discount_percent = round(($bill->discounted_amount / $total_amount) * 100);
            }
            $order = OrderItems::where('post_paid_orders_id', $value->id)
                ->get();
            foreach ($order as $key => $value)
            {
                $id=$value->general_item_id;
                $item_info = GeneralItems::where('id', $id)->first();
                if(empty($item_info) ){
                    continue;       
                }   
                if (!array_key_exists($id, $general_items))
                {  
                    if ($discount_percent != 0)
                    {
                        $price = $value->quantity * $value->price;
                        $discount = $price * $discount_percent;
                        $total_price = $price - $discount;
                        $data1 = array();
                        $data1['name'] = $item_info->name;
                        $data1['id'] = $item_info->id;
                        $data1['price'] = $total_price / $value->quantity;
                        $data1['cost'] = $item_info->purchase_price;
                        $data1['quantity_sold'] = $value->quantity;
                        $data1['total_sale'] = $total_price;
                        $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                        $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                        if ($data1['total_sale'] == 0)
                        {
                            $data1['total_sale'] = 1;
                        }
                        $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                        $general_items[$id]['discount'] = $data1;
                    }
                    else
                    {

                        $price = $value->quantity * $value->price;
                        $total_price = $price;

                        $data1 = array();
                        $data1['name'] = $item_info->name;
                        $data1['id'] = $item_info->id;
                        $data1['price'] = $total_price / $value->quantity;
                        $data1['cost'] = $item_info->purchase_price;
                        $data1['quantity_sold'] = $value->quantity;
                        $data1['total_sale'] = $total_price;
                        $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                        $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                        if ($data1['total_sale'] == 0)
                        {
                            $data1['total_sale'] = 1;
                        }
                        $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                        $general_items[$id]['simple'] = $data1;
                      

                    }
                }
                if (!empty($general_items))
                {
                    foreach ($general_items as $key1 => $value1)
                    
                    {
                        if ($key1 ==$id && array_key_exists('discount', $value1))
                        {
                            $price = $value->quantity * $value->price;
                            $discount = $price * $discount_percent;
                            $total_price = $price - $discount;
                            $data1 = array();
                            $data1['name'] = $item_info->name;
                            $data1['id'] = $item_info->id;
                            $data1['price'] = $total_price / $value->quantity;
                            $data1['cost'] = $item_info->purchase_price;
                            $data1['quantity_sold'] = $value->quantity;
                            $data1['total_sale'] = $total_price;
                            $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                            $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                            if ($data1['total_sale'] == 0)
                            {
                                $data1['total_sale'] = 1;
                            }
                            $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                            //set the previous + current average
                            $rr=$data1['price'] + $general_items[$id]['discount']['price'];
                            $general_items[$id]['discount']['price'] =  $rr/ 2;
                            $general_items[$id]['discount']['quantity_sold'] = $general_items[$id]['discount']['quantity_sold'] + $data1['quantity_sold'];
                            $general_items[$id]['discount']['total_sale'] = $general_items[$id]['discount']['total_sale'] + $data1['total_sale'];
                            $general_items[$id]['discount']['total_cost'] = $general_items[$id]['discount']['total_cost'] + $data1['total_cost'];
                            $tt=($general_items[$id]['discount']['profit'] + $data1['profit']);
                            $general_items[$id]['discount']['profit'] =  $tt/ 2;
                            $yy=($general_items[$id]['discount']['margin'] + $data1['margin']);
                            $general_items[$id]['discount']['margin'] = $yy / 2;
                            //  print_r($general_items);exit;
                        }
                        else if ($key1 == $id &&  array_key_exists('simple', $value1))
                        {

                            $price = $value->quantity * $value->price;
                            $total_price = $price;
                            $data1 = array();
                            $data1['name'] = $item_info->name;
                            $data1['id'] = $item_info->id;
                            $data1['price'] = $total_price / $value->quantity;
                            $data1['cost'] = $item_info->purchase_price;
                            $data1['quantity_sold'] = $value->quantity;
                            $data1['total_sale'] = $total_price;
                            $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                            $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                            if ($data1['total_sale'] == 0)
                            {
                                $data1['total_sale'] = 1;
                            }
                            $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                           
                             
                            $rr=$data1['price'] + $general_items[$id]['simple']['price'];
                            $general_items[$id]['simple']['price'] =  $rr/ 2;
                            
                            
                            $general_items[$id]['simple']['quantity_sold'] = $general_items[$id]['simple']['quantity_sold'] + $data1['quantity_sold'];
                            $general_items[$id]['simple']['total_sale'] = $general_items[$id]['simple']['total_sale'] + $data1['total_sale'];
                            $general_items[$id]['simple']['total_cost'] = $general_items[$id]['simple']['total_cost'] + $data1['total_cost'];
                            $tt=($general_items[$id]['simple']['profit'] + $data1['profit']);
                            $general_items[$id]['simple']['profit'] =  $tt/ 2;
                            $yy=($general_items[$id]['simple']['margin'] + $data1['margin']);
                            $general_items[$id]['simple']['margin'] = $yy / 2;
                        }
                        else
                        {
                            //hiiiiiiii
                        }
                    }
                }

            }

        }
        // echo "<pre>";
        // return $general_items;
        
        
        $file = fopen("Gross_profit_analysis_report.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'The Smith Hotels',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '0705625356'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Percentage Profit Margin(Gross)'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            "Today : Posted:",
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'CODE',
            'PRICE',
            'COST',
            'QTY SOLD',
            'T: SALE',
            'T: COST',
            'PROFIT',
            'MARGIN'
        ));
        
        $cost = 0;
        $qty = 0;
        $t_sale = 0;
        $t_cost = 0;
        $profit = 0;
        $margin = 0;
        $count = 0;
        
        
        foreach($general_items as $general_item){
        
            if (array_key_exists('simple', $general_item)){
                $name = $general_item['simple']['name'];
                fputcsv($file, array(
                    "$name",
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ));
                
            }else{
                $dname = $general_item['discount']['name'];
                fputcsv($file, array(
                    "$dname",
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ));
            } 
            
            if (array_key_exists('simple', $general_item))
            {
                $did = $general_item['simple']['id'];
                $dprice = $general_item['simple']['price'];
                if ($general_item['simple']['cost'] == null)
                    $dcost = 0;
                else
                    $dcost = $general_item['simple']['cost'];
                $dqs = $general_item['simple']['quantity_sold'];
                $dts = $general_item['simple']['total_sale'];
                $dtc = $general_item['simple']['total_cost'];
                $dprofit = $general_item['simple']['profit'];
                $dmargin = $general_item['simple']['margin'];
                
                fputcsv($file, array(
                        "$did",
                        "$dprice",
                        "$dcost",
                        "$dqs",
                        "$dts",
                        "$dtc",
                        "$dprofit",
                        "$dmargin"
                    ));
                
            
                $cost += (double)$general_item['simple']['cost'];
                $qty += (double)$general_item['simple']['quantity_sold'];
                $t_sale += (double)$general_item['simple']['total_sale'];
                $t_cost += (double)$general_item['simple']['total_cost'];
                $profit += (double)$general_item['simple']['profit'];
                $margin += (double)$general_item['simple']['margin'];
                $count = $count+1 ;
            }
             
                
                
            
            
            if (array_key_exists('discount', $general_item)){
                $did = $general_item['discount']['id'];
                $dprice = $general_item['discount']['price'];
                if ($general_item['discount']['cost'] == null)
                    $dcost = 0;
                else
                    $dcost = $general_item['discount']['cost'];
                $dqs = $general_item['discount']['quantity_sold'];
                $dts = $general_item['discount']['total_sale'];
                $dtc = $general_item['discount']['total_cost'];
                $dprofit = $general_item['discount']['profit'];
                $dmargin = $general_item['discount']['margin'];
                
                fputcsv($file, array(
                        "$did",
                        "$dprice",
                        "$dcost",
                        "$dqs",
                        "$dts",
                        "$dtc",
                        "$dprofit",
                        "$dmargin"
                    ));
                
            
                $cost += (double)$general_item['discount']['cost'];
                $qty += (double)$general_item['discount']['quantity_sold'];
                $t_sale += (double)$general_item['discount']['total_sale'];
                $t_cost += (double)$general_item['discount']['total_cost'];
                $profit += (double)$general_item['discount']['profit'];
                $margin += (double)$general_item['discount']['margin'];
                $count = $count+1 ;
             
            }
            
        }   
     
        $qty = number_format((float)$qty, 2, '.', '');
        $t_sale = number_format((float)$t_sale, 2, '.', '');
        $t_cost = number_format((float)$t_cost, 2, '.', '');
        $profit = number_format((float)$profit, 2, '.', '');
        $margin /= $count;
        $margin = number_format((float)$margin, 2, '.', '');
    
        fputcsv($file, array(
            "TOTAL",
            "",
            "",
            "$qty",
            "$t_sale",
            "$t_cost",
            "$profit",
            "$margin"
        ));


    
        fclose($file);
        
        $start_date = null;
        $end_date = null;
        return view('admin.reports.admin_gross_profit_analysis_report', compact("general_items", "start_date", "end_date"));
    }
    
    public function grossProfit_filter(Request $request){
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $orders = DB::select("select * FROM post_paid_orders where  created_at BETWEEN '$start_date' AND '$end_date' ");
        // $orders = DB::select("select * FROM post_paid_orders where  DATE(post_paid_orders.created_at) BETWEEN '" . $start_date . "' AND '" . $end_date . "' ");
       // $orders = DB::select("select * FROM post_paid_orders where   date(created_at) = '2019-10-10' ");

// echo "<pre>";
        $general_items = array();
        foreach ($orders as $key => $value)
        {
            $bill = BillMaster::where('id', $value->bill_id)
                ->first();
            $discount_percent = 0;
            if ($bill->discountType == 'percent')
            {
                $discount_percent = $bill->discount_percent;
            }
            if ($bill->discountType == 'fixed')
            {
                $total_amount = $bill->total_amount + $bill->discounted_amount;
                $discount_percent = round(($bill->discounted_amount / $total_amount) * 100);
            }
            $order = OrderItems::where('post_paid_orders_id', $value->id)
                ->get();
            foreach ($order as $key => $value)
            {
                $id=$value->general_item_id;
                $item_info = GeneralItems::where('id', $id)->first();
                if(empty($item_info) ){
                    continue;       
                }   
                if (!array_key_exists($id, $general_items))
                {  
                    if ($discount_percent != 0)
                    {
                        $price = $value->quantity * $value->price;
                        $discount = $price * $discount_percent;
                        $total_price = $price - $discount;
                        $data1 = array();
                        $data1['name'] = $item_info->name;
                        $data1['id'] = $item_info->id;
                        $data1['price'] = $total_price / $value->quantity;
                        $data1['cost'] = $item_info->purchase_price;
                        $data1['quantity_sold'] = $value->quantity;
                        $data1['total_sale'] = $total_price;
                        $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                        $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                        if ($data1['total_sale'] == 0)
                        {
                            $data1['total_sale'] = 1;
                        }
                        $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                        $general_items[$id]['discount'] = $data1;
                    }
                    else
                    {

                        $price = $value->quantity * $value->price;
                        $total_price = $price;

                        $data1 = array();
                        $data1['name'] = $item_info->name;
                        $data1['id'] = $item_info->id;
                        $data1['price'] = $total_price / $value->quantity;
                        $data1['cost'] = $item_info->purchase_price;
                        $data1['quantity_sold'] = $value->quantity;
                        $data1['total_sale'] = $total_price;
                        $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                        $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                        if ($data1['total_sale'] == 0)
                        {
                            $data1['total_sale'] = 1;
                        }
                        $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                        $general_items[$id]['simple'] = $data1;
                      

                    }
                }
                if (!empty($general_items))
                {
                    foreach ($general_items as $key1 => $value1)
                    
                    {
                        if ($key1 ==$id && array_key_exists('discount', $value1))
                        {
                            $price = $value->quantity * $value->price;
                            $discount = $price * $discount_percent;
                            $total_price = $price - $discount;
                            $data1 = array();
                            $data1['name'] = $item_info->name;
                            $data1['id'] = $item_info->id;
                            $data1['price'] = $total_price / $value->quantity;
                            $data1['cost'] = $item_info->purchase_price;
                            $data1['quantity_sold'] = $value->quantity;
                            $data1['total_sale'] = $total_price;
                            $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                            $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                            if ($data1['total_sale'] == 0)
                            {
                                $data1['total_sale'] = 1;
                            }
                            $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                            //set the previous + current average
                            $rr=$data1['price'] + $general_items[$id]['discount']['price'];
                            $general_items[$id]['discount']['price'] =  $rr/ 2;
                            $general_items[$id]['discount']['quantity_sold'] = $general_items[$id]['discount']['quantity_sold'] + $data1['quantity_sold'];
                            $general_items[$id]['discount']['total_sale'] = $general_items[$id]['discount']['total_sale'] + $data1['total_sale'];
                            $general_items[$id]['discount']['total_cost'] = $general_items[$id]['discount']['total_cost'] + $data1['total_cost'];
                            $tt=($general_items[$id]['discount']['profit'] + $data1['profit']);
                            $general_items[$id]['discount']['profit'] =  $tt/ 2;
                            $yy=($general_items[$id]['discount']['margin'] + $data1['margin']);
                            $general_items[$id]['discount']['margin'] = $yy / 2;
                            //  print_r($general_items);exit;
                        }
                        else if ($key1 == $id &&  array_key_exists('simple', $value1))
                        {

                            $price = $value->quantity * $value->price;
                            $total_price = $price;
                            $data1 = array();
                            $data1['name'] = $item_info->name;
                            $data1['id'] = $item_info->id;
                            $data1['price'] = $total_price / $value->quantity;
                            $data1['cost'] = $item_info->purchase_price;
                            $data1['quantity_sold'] = $value->quantity;
                            $data1['total_sale'] = $total_price;
                            $data1['total_cost'] = $item_info->purchase_price * $value->quantity;
                            $data1['profit'] = $total_price - ($item_info->purchase_price * $value->quantity);
                            if ($data1['total_sale'] == 0)
                            {
                                $data1['total_sale'] = 1;
                            }
                            $data1['margin'] = 100 - (($data1['total_cost'] / $data1['total_sale']) * 100);
                           
                             
                            $rr=$data1['price'] + $general_items[$id]['simple']['price'];
                            $general_items[$id]['simple']['price'] =  $rr/ 2;
                            
                            
                            $general_items[$id]['simple']['quantity_sold'] = $general_items[$id]['simple']['quantity_sold'] + $data1['quantity_sold'];
                            $general_items[$id]['simple']['total_sale'] = $general_items[$id]['simple']['total_sale'] + $data1['total_sale'];
                            $general_items[$id]['simple']['total_cost'] = $general_items[$id]['simple']['total_cost'] + $data1['total_cost'];
                            $tt=($general_items[$id]['simple']['profit'] + $data1['profit']);
                            $general_items[$id]['simple']['profit'] =  $tt/ 2;
                            $yy=($general_items[$id]['simple']['margin'] + $data1['margin']);
                            $general_items[$id]['simple']['margin'] = $yy / 2;
                        }
                        else
                        {
                            //hiiiiiiii
                        }
                    }
                }

            }

        }
        
        
        $file = fopen("Gross_profit_analysis_report.csv", "w");
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'The Smith Hotels',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '0705625356'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Percentage Profit Margin(Gross)'
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            "between $start_date and $end_date : Posted:",
            '',
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            '',
            '',
            '',
            '',
            ''
        ));
        fputcsv($file, array(
            'CODE',
            'PRICE',
            'COST',
            'QTY SOLD',
            'T: SALE',
            'T: COST',
            'PROFIT',
            'MARGIN'
        ));
        
        $cost = 0;
        $qty = 0;
        $t_sale = 0;
        $t_cost = 0;
        $profit = 0;
        $margin = 0;
        $count = 0;
        
        
        foreach($general_items as $general_item){
        
            if (array_key_exists('simple', $general_item)){
                $name = $general_item['simple']['name'];
                fputcsv($file, array(
                    "$name",
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ));
                
            }else{
                $dname = $general_item['discount']['name'];
                fputcsv($file, array(
                    "$dname",
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                ));
            } 
            
            if (array_key_exists('simple', $general_item))
            {
                $did = $general_item['simple']['id'];
                $dprice = $general_item['simple']['price'];
                if ($general_item['simple']['cost'] == null)
                    $dcost = 0;
                else
                    $dcost = $general_item['simple']['cost'];
                $dqs = $general_item['simple']['quantity_sold'];
                $dts = $general_item['simple']['total_sale'];
                $dtc = $general_item['simple']['total_cost'];
                $dprofit = $general_item['simple']['profit'];
                $dmargin = $general_item['simple']['margin'];
                
                fputcsv($file, array(
                        "$did",
                        "$dprice",
                        "$dcost",
                        "$dqs",
                        "$dts",
                        "$dtc",
                        "$dprofit",
                        "$dmargin"
                    ));
                
            
                $cost += (double)$general_item['simple']['cost'];
                $qty += (double)$general_item['simple']['quantity_sold'];
                $t_sale += (double)$general_item['simple']['total_sale'];
                $t_cost += (double)$general_item['simple']['total_cost'];
                $profit += (double)$general_item['simple']['profit'];
                $margin += (double)$general_item['simple']['margin'];
                $count = $count+1 ;
            }
             
                
                
            
            
            if (array_key_exists('discount', $general_item)){
                $did = $general_item['discount']['id'];
                $dprice = $general_item['discount']['price'];
                if ($general_item['discount']['cost'] == null)
                    $dcost = 0;
                else
                    $dcost = $general_item['discount']['cost'];
                $dqs = $general_item['discount']['quantity_sold'];
                $dts = $general_item['discount']['total_sale'];
                $dtc = $general_item['discount']['total_cost'];
                $dprofit = $general_item['discount']['profit'];
                $dmargin = $general_item['discount']['margin'];
                
                fputcsv($file, array(
                        "$did",
                        "$dprice",
                        "$dcost",
                        "$dqs",
                        "$dts",
                        "$dtc",
                        "$dprofit",
                        "$dmargin"
                    ));
                
            
                $cost += (double)$general_item['discount']['cost'];
                $qty += (double)$general_item['discount']['quantity_sold'];
                $t_sale += (double)$general_item['discount']['total_sale'];
                $t_cost += (double)$general_item['discount']['total_cost'];
                $profit += (double)$general_item['discount']['profit'];
                $margin += (double)$general_item['discount']['margin'];
                $count = $count+1 ;
             
            }
            
        }   
     
        $qty = number_format((float)$qty, 2, '.', '');
        $t_sale = number_format((float)$t_sale, 2, '.', '');
        $t_cost = number_format((float)$t_cost, 2, '.', '');
        $profit = number_format((float)$profit, 2, '.', '');
        $margin /= $count;
        $margin = number_format((float)$margin, 2, '.', '');
    
        fputcsv($file, array(
            "TOTAL",
            "",
            "",
            "$qty",
            "$t_sale",
            "$t_cost",
            "$profit",
            "$margin"
        ));


    
        fclose($file);
        return view('admin.reports.admin_gross_profit_analysis_report', compact("general_items", "start_date", "end_date"));
    }

}

