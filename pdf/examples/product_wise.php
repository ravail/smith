<?php
$html = '<div style="background-color: #f6f6ff;width: 680px;
padding: 10px;
margin: auto;"><h1 style="margin-left:200px !important">Productwise Sales Report</h1><p style="    margin-left: 333px;">All Time Report</p></div>';

//Open the file.

$data = file("../../dailySales.csv");
$i = 0;

$html .= '<div style="background-color: #f6f6ff;
font-family: Calibri, Myriad;"><table style="margin: auto;
width: 600px;
border-collapse: collapse;
border: 1px solid #fff; /*for older IE*/
">
                            <thead>
								<tr>
								<th style="padding: 8px;
								background-color: #fde9d9;
								font-size: large;"></th>

                                    <th style="padding: 8px;
									background-color: #fde9d9;
									font-size: large;">FOOD & BEVERAGE</th>

									<th style="padding: 8px;
									background-color: #fde9d9;
									font-size: large;">Item</th>
									<th style="padding: 8px;
									background-color: #fde9d9;
									font-size: large;">Sales QTY</th>

									<th style="padding: 8px;
									background-color: #fde9d9;
									font-size: large;">Gross Sales</th>

                                </tr>
                            </thead><tbody style="background-color: #f6f6ff;
							font-family: Calibri, Myriad;">';
foreach ($data as $line) {
    if ($i > 7) {
        $data = explode(',', $line);
        $html .= '<tr class=odd>';
        for ($j = 0; $j < 5; $j++) {
            if ($data[$j] == '') {
                $html .= '<td style="padding: 3px;
				border-width: 1px;
				border-style: solid;
				border-color: #f79646 #ccc;">' . $data[$j] . '</td>';
            } else {
                $html .= '<td style="padding: 3px;
				border-width: 1px;
				border-style: solid;
				border-color: #f79646 #ccc;">' . $data[$j] . '</td>';
            }

        }

        $html .= '</tr>';

    }
    $i++;
}
$html .= '</tbody></table></div>';
$filename = "newpdffile";

// include autoloader
require_once '../../dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'Portrait');
 

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream($filename);
